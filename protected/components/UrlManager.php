<?
class UrlManager extends CUrlManager
{

	public function createUrl($route, $params = array(), $ampersand = '&')
	{
		if (!preg_match('#^assets/.*#', $route)) {
			$route = preg_replace_callback('/(?<![A-Z])[A-Z]/', function ($matches) {
				return '-' . lcfirst($matches[0]);
			}, $route);
			$route = implode('/', array_map(function ($item) {
				return ltrim($item, '-');
			}, explode('/', $route)));
		}
		return parent::createUrl($route, $params, $ampersand);
	}

	public function parseUrl($request)
	{
		$route = parent::parseUrl($request);
		$route = lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $route))));
		return $route;
	}
}