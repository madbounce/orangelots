<?php

Class OCHtml extends CHtml {


    public static function ajaxSubmitButton($label,$url,$ajaxOptions=array(),$htmlOptions=array())
    {
        $ajaxOptions['type']='POST';
        $htmlOptions['type']='image';
        return self::ajaxButton($label,$url,$ajaxOptions,$htmlOptions);
    }
}

?>