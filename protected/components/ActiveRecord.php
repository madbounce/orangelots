<?php
/**
 * Created by JetBrains PhpStorm.
 * User: padlyuck
 * Date: 23.04.13
 * Time: 12:15
 * To change this template use File | Settings | File Templates.
 */

class ActiveRecord extends WActiveRecord
{
	/**
	 * Достает из кеша одну модель по первичному ключу
	 * Если кеш для запрошенного ключа пустой - достает из базы и кеширует
	 * @param $pk int
	 * @return CActiveRecord|mixed
	 */
	public function getByPk($pk)
	{
		//TODO Придумать что делать со связанными данными, которые могут быть отредактированы вне родительского объекта
		if ($cacheInstance = Yii::app()->getCache()) {
			$cache = $cacheInstance->get('activerecord.' . get_called_class() . '.' . $pk);
			if ($cache) {
				$cache = unserialize($cache);
			}
			if ($cache) {
				return $cache;
			}
		}
		$class = get_called_class();
		/**@var $class CActiveRecord */
		$entity = $class::model()->findByPk($pk);
		if ($cacheInstance && $entity) {
			$cacheInstance->set('activerecord.' . $class . '.' . $pk, serialize($entity));
		}
		return $entity;
	}

	/**
	 * Сохраняет и кеширует сохраняемую модель
	 * @param bool $runValidation
	 * @param null $attributes
	 * @return bool
	 */
	public function save($runValidation = true, $attributes = NULL)
	{
		$result = parent::save($runValidation, $attributes);
		if ($cacheInstance = Yii::app()->getCache()) {
			$cacheInstance->set('activerecord.' . get_called_class() . '.' . $this->primaryKey, serialize($this));
		}
		return $result;
	}

	/**
	 * Удаляет запись и ее кеш
	 * @return bool
	 */
	public function delete()
	{
		$result = parent::delete();
		if ($cacheInstance = Yii::app()->getCache()) {
			$cacheInstance->delete('activerecord.' . get_called_class() . '.' . $this->primaryKey);
		}
		return $result;
	}
}