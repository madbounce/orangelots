<?php
Yii::import('zii.widgets.CListView');

/**
 * DSizerListView adds a block "Show by: 10 20 30" (items per page size switcher)
 *
 * <pre>
 * <?php $this->widget('DSizerListView', array(
 *     'dataProvider'=>$dataProvider,
 *     'itemView'=>'_view',
 *     'template'=>"{sizer}\n{summary}\n{items}\n{pager}",
 *     'sizerVariants'=>array(10, 20, 30),
 *     'sizerAttribute'=>'size',
 *     'sizerCssClass'=>'sorter',
 *     'sizerHeader'=>'Show per page: ',
 * )); ?>
 * </pre>
 *
 * @author ElisDN <mail@elisdn.ru>
 * @link http://www.elisdn.ru
 * @version 1.0
 */
class DSizerListView extends CListView
{
    /**
     * @var boolean whether to enable sizer
     * Defaults to true.
     */
    public $enableSizer = true;

    /**
     * @var string GET attribute
     */
    public $sizerAttribute = 'pageSize';

    /**
     * @var array items per page sizes variants
     */
    public $sizerVariants = array(10, 20, 30);

    /**
     * @var string CSS class of sorter element
     */
    public $sizerCssClass = 'sizer';

    /**
     * @var string the text shown before sizer links. Defaults to empty.
     */
    public $sizerHeader = 'Show by: ';

    /**
     * @var string the text shown after sizer links. Defaults to empty.
     */
    public $sizerFooter = '';

    public function init()
    {
        if (!isset($this->sizerVariants[0]))
            $this->sizerVariants = array(10);

        if ($this->enableSizer)
        {
            $pageSize = Yii::app()->request->getQuery($this->sizerAttribute, $this->sizerVariants[0]);
            $this->dataProvider->getPagination()->setPageSize($pageSize);
        }

        parent::init();
    }

    public function renderSizer()
    {
        if (!$this->enableSizer)
            return;

        $itemCount = $this->dataProvider->getItemCount();

        /*if ($itemCount <= 0 || $itemCount < $this->sizerVariants[0])
            return;*/

        $pageSize = $this->dataProvider->getPagination()->getPageSize();
        $pageVar = $this->dataProvider->getPagination()->pageVar;

        echo CHtml::openTag('div', array('class' => $this->sizerCssClass)) . "\n";
        echo $this->sizerHeader;
       /* echo "<ul>\n";

        foreach($this->sizerVariants as $count)
        {
            $params = array_replace($_GET, array($this->sizerAttribute => $count));

            if (isset($params[$pageVar]))
                unset($params[$pageVar]);

            echo "<li>";
            if ($count == $pageSize)
                echo $count;
            else
                echo CHtml::link($count, Yii::app()->controller->createUrl('', $params));
            echo "</li>\n";
        }

        echo "</ul>";*/

         echo "<select id = 'pagesizer'>\n";

        foreach ($this->sizerVariants as $count) {
            $params = array_replace($_GET, array($this->sizerAttribute => $count));

            if (isset($params[$pageVar]))
                unset($params[$pageVar]);


            if ($count == $pageSize):
                echo "<option value = '".$count."' data-url = ''>";
                echo $count;
                echo "</option>";
            else:
                echo "<option value = '".$count."' data-url = '".Yii::app()->controller->createAbsoluteUrl('', $params)."'>";
                echo $count;
                echo "</option>";
            endif;
        }

        echo "</select>";

        echo $this->sizerFooter;
        echo CHtml::closeTag('div');
    }
    public function renderSorter()
    {
        /*if($this->dataProvider->getItemCount()<=0 || !$this->enableSorting || empty($this->sortableAttributes))
            return;*/
        echo CHtml::openTag('div',array('class'=>$this->sorterCssClass))."\n";
        echo $this->sorterHeader===null ? Yii::t('zii','Sort by: ') : $this->sorterHeader;
        echo "<ul>\n";
        $sort=$this->dataProvider->getSort();
        foreach($this->sortableAttributes as $name=>$label)
        {
            echo "<li>";
            if(is_integer($name))
                echo $sort->link($label);
            else
                echo $sort->link($name,$label);
            echo "</li>\n";
        }
        echo "</ul>";
        echo $this->sorterFooter;
        echo CHtml::closeTag('div');
    }

    public function renderPager()
    {
        if(!$this->enablePagination)
            return;

        $pager=array();
        $class='CLinkPager';
        if(is_string($this->pager))
            $class=$this->pager;
        elseif(is_array($this->pager))
        {
            $pager=$this->pager;
            if(isset($pager['class']))
            {
                $class=$pager['class'];
                unset($pager['class']);
            }
        }
        $pager['pages']=$this->dataProvider->getPagination();

        if($pager['pages']->getPageCount()>1)
        {
//            echo '<div class="auction_list__sort_row">';
            $this->widget($class,$pager);
//            echo '</div>';
        }
        else
            $this->widget($class,$pager);
    }
}