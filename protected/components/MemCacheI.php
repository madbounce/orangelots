<?php
/**
 * Класс добавляющий возможность ставить блокировки на ключи кеша
 * (для избежания одновреенного перегенеривания кеша несколькими запросами)
 *
 * Author: Yuriy Sorokolat (yuriy.sorokolat@gmail.com) aka Sw00p (Aiken Studio)
 * Date: 17.08.2010
 * Time: 18:30:16
 */
 
class MemCacheI extends CMemCache {

    const LOCK_KEY_PREFIX = 'locked_key_';
    const OLD_KEY_POSTFIX = '_old';

    /**
     * Максимальное время, на которое по умолчанию ставится блокировка
     */
    const LOCK_MAX_TIME = 120; //seconds

    /**
     * Максимальное время, которое будет разблокировки ожидать getValue перед завершением с ошибкой
     */
    const UNLOCK_WAIT_MAX_TIME = 20; //seconds

    /**
     * Шаг времени через который проверяется снятие блокировки при ожидании
     */
    const UNLOCK_WAIT_STEP = 1; //seconds

    /**
     * Установить блокировку на ключ $key
     * @param string $key
     * @param int $time врямя актуальности блокировки
     * @return void
     */
    public function setLock($key, $time = self::LOCK_MAX_TIME)
    {
        return $this->addValue(self::LOCK_KEY_PREFIX . $key, 1, $time);
    }

    /**
     * Снять блокировку с ключа $key
     * @param string $key
     * @return void
     */
    public function unsetLock($key)
    {
        $this->deleteValue(self::LOCK_KEY_PREFIX . $key);
    }

    /**
     * Проверить установлена ли блокировка на ключе $key
     * @param string $key
     * @return void
     */
    public function isLocked($key)
    {
        return (bool)parent::getValue(self::LOCK_KEY_PREFIX . $key);
    }

    /**
     * Если пытаемся получить значение заблокированного ключа, ожидаем разблокировки
     * 
     * @throws CException
     * @param string $key
     * @return string
     */
    public function getValue($key)
    {
        $sleepSeconds = 0;
        $isLocked = $this->isLocked($key);
        if ($isLocked && !($oldValue = $this->get($key.self::OLD_KEY_POSTFIX))) {
            while ($this->isLocked($key) == true && $sleepSeconds <= self::UNLOCK_WAIT_MAX_TIME) {
                sleep(self::UNLOCK_WAIT_STEP);
                $sleepSeconds++;
            }
            if ($sleepSeconds >= self::UNLOCK_WAIT_MAX_TIME) {
                throw new CException('Время ожидания разблокировки кеша превысило ' . self::UNLOCK_WAIT_MAX_TIME . ' секунд');
            } else {
                $result = parent::getValue($key);
            }
        } elseif ($isLocked && $oldValue) {
            $result = $oldValue;
        } else {
            $result = parent::getValue($key);
        }
        return $result;
    }

    /**
     * При установки значения в кеш записываем его и как старое неумирающее значение
     * @param  $key
     * @param  $value
     * @param  $expire
     * @return bool
     */
    public function setValue($key, $value, $expire)
    {
        parent::setValue($key . self::OLD_KEY_POSTFIX, $value, 0);
        return parent::setValue($key, $value, $expire);
    }


}
