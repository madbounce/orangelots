<?php

class RenderDynamicForm extends CWidget
{
    public $r;
    public $ar = false;

    public function run()
    {
        $model = new ItemsForm();
        if (!empty($this->r)) {
            if ($this->ar) {
                foreach ($this->r as $k => $v) {
                    $errors = (isset($v->attributes['errors'])) ? $v->attributes['errors'] : array();
                    echo $this->controller->renderPartial(
                        'application.modules.auctions.views.orangelotsAuctions._dynamicForm',
                        array(
                            'uniq' => $k,
                            'value' => $v->attributes,
                            'errors' => $errors,
                            'ajax' => true,
                            'model' => $model,

                        ),
                        true,
                        true
                    );
                }
            } else {
                foreach ($this->r as $k => $v) {
                    if(!array_key_exists ('recordid',$v)){
                        $errors = (isset($v['errors'])) ? $v['errors'] : array();
                        echo $this->controller->renderPartial(
                            'application.modules.auctions.views.orangelotsAuctions._dynamicForm',
                            array(
                                'uniq' => $k,
                                'value' => $v,
                                'errors' => $errors,
                                'ajax' => false,
                                'model' => $model,

                            ),
                            true,
                            true
                        );
                    }
                }
            }
        }
    }
}

?>