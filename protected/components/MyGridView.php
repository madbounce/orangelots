<?php
Yii::import('zii.widgets.grid.CGridView');
Yii::import('zii.widgets.grid.MyGridColumn');

class MyGridView extends CGridView
{
    public function renderTableHeader()
    {
        if (!$this->hideHeader) {
            echo "<thead>\n";

            if ($this->filterPosition === self::FILTER_POS_HEADER) {
                $this->renderFilter();
            }

            echo "<tr>\n";
            foreach ($this->columns as $column) {
                $column->renderHeaderCellTd();
            }

            echo "</tr>\n";

            if ($this->filterPosition === self::FILTER_POS_BODY) {
                $this->renderFilter();
            }

            echo "</thead>\n";
        } elseif ($this->filter !== null && ($this->filterPosition === self::FILTER_POS_HEADER || $this->filterPosition === self::FILTER_POS_BODY)) {
            echo "<thead>\n";
            $this->renderFilter();
            echo "</thead>\n";
        }
    }
}

?>