<?php
/**
 * Created by JetBrains PhpStorm.
 * User: padlyuck
 * Date: 01.06.13
 * Time: 20:16
 * To change this template use File | Settings | File Templates.
 */

Yii::import('ext.yii-bootstrap.widgets.TbButtonColumn');

class CAJAXButtonColumn extends TbButtonColumn
{
	public $removeButtonIcon = 'remove';

	public $okButtonIcon = 'ok';

	protected function initDefaultButtons()
	{
		parent::initDefaultButtons();

		if ($this->removeButtonIcon !== false && !isset($this->buttons['remove']['icon'])) {
			$this->buttons['remove']['icon'] = $this->removeButtonIcon;
		}
		if ($this->okButtonIcon !== false && !isset($this->buttons['ok']['icon'])) {
			$this->buttons['ok']['icon'] = $this->okButtonIcon;
		}
	}


	protected function renderButton($id, $button, $row, $data)
	{

		if (isset($button['options']['id'])) {
			$button['options']['id'] = $this->evaluateExpression(
				$button['options']['id'],
				array('row' => $row, 'data' => $data)
			);
		}
//		if (isset($button['options']['onclick'])) {
//			$button['options']['onclick'] = $this->evaluateExpression($button['options']['onclick'], array('row' => $row, 'data' => $data));
//		}
		if (isset($button['options']['data-target'])) {
			$button['options']['data-target'] = $this->evaluateExpression(
				$button['options']['data-target'],
				array('row' => $row, 'data' => $data)
			);
		}

		if (isset($button['options']['payID'])) {
			$button['options']['payID'] = $this->evaluateExpression(
				$button['options']['payID'],
				array('row' => $row, 'data' => $data)
			);
		}

		if (isset($button['options']['payed'])) {
			$button['options']['payed'] = $this->evaluateExpression(
				$button['options']['payed'],
				array('row' => $row, 'data' => $data)
			);
		}

		if (isset($button['options']['customerID'])) {
			$button['options']['customerID'] = $this->evaluateExpression(
				$button['options']['customerID'],
				array('row' => $row, 'data' => $data)
			);
		}

		if (isset($button['options']['amount'])) {
			$button['options']['amount'] = $this->evaluateExpression(
				$button['options']['amount'],
				array('row' => $row, 'data' => $data)
			);
		}

		if (isset($button['options']['purse'])) {
			$button['options']['purse'] = $this->evaluateExpression(
				$button['options']['purse'],
				array('row' => $row, 'data' => $data)
			);
		}
		if (isset($button['options']['system_code'])) {
			$button['options']['system_code'] = $this->evaluateExpression(
				$button['options']['system_code'],
				array('row' => $row, 'data' => $data)
			);
		}

		if (isset($button['options']['remove_link'])) {
			$button['options']['remove_link'] = $this->evaluateExpression(
				$button['options']['remove_link'],
				array('row' => $row, 'data' => $data)
			);
		}

		parent::renderButton($id, $button, $row, $data);
	}


}