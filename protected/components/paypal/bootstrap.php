<?php

// Include the composer autoloader
$loader = require dirname(__DIR__) . '/paypal/vendor/autoload.php';
$loader->add('PayPal\\Test', __DIR__);
define("PP_CONFIG_PATH", __DIR__);