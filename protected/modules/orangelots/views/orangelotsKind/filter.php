<?php
/* @var $this OrangelotsKindController */
/* @var $model OrangelotsKind */

$this->breadcrumbs = array(
	'Orangelots Categories' => array('index'),
	$model->name,
);
?>

<div class="auction_list__container clearfix">
	<div class="main_container">
		<? $this->widget('ext.filter.FilterWidget'); ?>

		<div class="auction_list__main_collum clearfix">
			<div class="auction_list__head_banner">
                <?php echo $category->category_description?>
            </div>
            <?php
            $this->widget('ext.SponsoredListWidget.SponsoredListWidget', array(
                )); ?>

			<?php
            $array_url = array();
            if (isset($_REQUEST['parent'])) {
                $array_url['parent'] = $_REQUEST["parent"];
            }
            if (isset($_REQUEST['sub'])) {
                $array_url['sub'] = $_REQUEST["sub"];
            }
            if (isset($_REQUEST['alias'])) {
                $array_url['alias'] = $_REQUEST["alias"];
            }
			$puzzle = '  <ul class="auction_list__sort_row__puzzle pull-left">
            <li>
                <a href="' . ((($view == "grid") || ($view == "") || (!isset($view)) || (empty($view))) ? "javascript:void(0)" : createUrl($this->module->name . "/" . Yii::app()->controller->id . "/" . yii()->controller->action->id, array_merge($array_url, array("view" => "grid")))) . '" class="puzzle_table"></a>
            </li>
            <li>
                <a href="' . ((($view == "list") || ($view == "") || (!isset($view)) || (empty($view))) ? "javascript:void(0)" : createUrl($this->module->name . "/" . Yii::app()->controller->id . "/" . yii()->controller->action->id, array_merge($array_url, array("view" => "list")))) . '" class="puzzle_row"></a>
            </li>
        </ul>';
			?>

            <?php
            if (($view == "grid") || ($view == "") || (!isset($view)) || (empty($view)) || ($view == "false")) {
                $template = "<div class = 'auction_list__sort_row'>{sorter}" . $puzzle . "{pager}<div class = 'auction_list__sort_row__per_pages pull-right'>{sizer}</div></div>{items}</div>";
                $itemsCssClass = 'auction_list___main_list_catalog__row';
                $itemsTagName = 'div';
                $itemView = '_list';
            } else {
                if ($view == "list") {
                    $template = "<div class = 'auction_list__sort_row'>{sorter}" . $puzzle . "{pager}<div class = 'auction_list__sort_row__per_pages pull-right'>{sizer}</div></div><div class='auction_list___main_list_catalog'>{items}</div></div>";
                    $itemsCssClass = 'auction_list___main_list_catalog__row_puzzle';
                    $itemsTagName = 'table';
                    $itemView = '_list_td';
                }
            }
            ?>
            <?php
            $this->widget('DSizerListView', array(
                    'dataProvider'=>$dataProvider,
                    'itemView'=>$itemView,
                    'template'=>$template,
                    'sizerVariants'=>array(12, 24, 50),
                    'sizerAttribute'=>'size',
                    'sizerCssClass'=>'sorter another_sorter',
                    'sizerHeader'=>'per page: ',
                    'sortableAttributes' => array(
                        'units_in_lot' => 'QTY',
                        'buy_now_price' => 'PRICE',
                        'end_date_unix' => 'END TIME',
                    ),
                    'sorterHeader' => 'SORT BY:',
                    'itemsCssClass' => $itemsCssClass,
                    'itemsTagName' => $itemsTagName,
                    'pager' => array(
                        'class' => 'MyLinkPager',
                        'header' => '',
                        'htmlOptions' => array(
                            'class' => 'auction_list__sort_row__pages pull-right',
                        ),
                    ),
                ));
            ?>

		</div>

		<div class="card_detail__foot_info">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
				dolore
				magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
				commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
				pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
				id
				est
				laborum</p>

			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
				dolore
				magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
				commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
				pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim
				id
				est
				laborum</p>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$("#pagesizer option").each(function () {
				if ($(this).attr("data-url") == '') {
					$(this).attr('selected', 'selected');
				}
			}
		);
	});
</script>

