<?php
/* @var $this OrangelotsKindController */
/* @var $model OrangelotsKind */
?>

<?php
if ((isset($search_input)) && (isset($search_category))){
    Yii::app()->clientScript->registerScript('search_script','
$(document).ready(function(){
    $("input.form-control").val("'.$search_input.'");
});

');
}

?>
<div class="auction_list__container clearfix">
<div class="main_container">
	<? $this->widget('ext.filter.FilterWidget'); ?>
	<div class="auction_list__main_collum clearfix">
		<div class="auction_list__head_banner">
<!--            --><?php //if(isset($search_category)): ?>
<!--            --><?php //echo 'Search result for: "'.$search_input.'"'; ?>
<!--            --><?php //else: ?>
<!--                --><?//if(!empty($_GET['id'])):?>
<!--                    --><?php //echo OrangelotsKind::getCategoryDescription($_GET['id']);?>
<!--                --><?//endif;?>
<!--			--><?php //endif; ?>
            <?php echo $model->category_description?>
		</div>

        <?php
        if(!isset($_REQUEST['deal']))
        $this->widget('ext.SponsoredListWidget.SponsoredListWidget', array(
        )); ?>

		<?php
        $array_url = array();
        if (isset($_REQUEST['parent'])) {
            $array_url['parent'] = $_REQUEST["parent"];
        }
        if (isset($_REQUEST['sub'])) {
            $array_url['sub'] = $_REQUEST["sub"];
        }
		if (isset($_REQUEST['alias'])) {
            $array_url['alias'] = $_REQUEST["alias"];
		}
        if(isset($_REQUEST['vendor'])){
            $array_url['vendor'] = $_REQUEST["vendor"];
        }
        if(isset($_REQUEST['style'])){
            $array_url['style'] = $_REQUEST["style"];
        }
        /*else {
           $array_url = array();
       }*/
		$puzzle = '  <ul class="auction_list__sort_row__puzzle pull-left">
            <li>
                <a href="' . ((($view == "grid") || ($view == "") || (!isset($view)) || (empty($view))) ? "javascript:void(0)" : createUrl($this->module->name . "/" . Yii::app()->controller->id . "/" . yii()->controller->action->id, array_merge($array_url, array("view" => "grid")))) . '" class="puzzle_table"></a>
            </li>
            <li>
                <a href="' . ((($view == "list") || ($view == "") || (!isset($view)) || (empty($view))) ? "javascript:void(0)" : createUrl($this->module->name . "/" . Yii::app()->controller->id . "/" . yii()->controller->action->id, array_merge($array_url, array("view" => "list")))) . '" class="puzzle_row"></a>
            </li>
        </ul>';
		?>

	<?php
	if (($view == "grid") || ($view == "") || (!isset($view)) || (empty($view)) || ($view == "false")) {
		$template = "<div class = 'auction_list__sort_row'>{sorter}" . $puzzle . "{pager}<div class = 'auction_list__sort_row__per_pages pull-right'>{sizer}</div></div>{items}</div>";
		$itemsCssClass = 'auction_list___main_list_catalog__row';
		$itemsTagName = 'div';
		$itemView = '_list';
	} else {
		if ($view == "list") {
			$template = "<div class = 'auction_list__sort_row'>{sorter}" . $puzzle . "{pager}<div class = 'auction_list__sort_row__per_pages pull-right'>{sizer}</div></div><div class='auction_list___main_list_catalog'>{items}</div></div>";
			$itemsCssClass = 'auction_list___main_list_catalog__row_puzzle';
			$itemsTagName = 'table';
			$itemView = '_list_td';
		}
	}
	?>
        <?php
        $this->widget('DSizerListView', array(
                'dataProvider'=>$dataProvider,
                'itemView'=>$itemView,
                'template'=>$template,
                'sizerVariants'=>array(12, 24, 50),
                'sizerAttribute'=>'size',
                'sizerCssClass'=>'sorter another_sorter',
                'sizerHeader'=>'per page: ',
                'sortableAttributes' => array(
                    'units_in_lot' => 'QTY',
                    'buy_now_price' => 'PRICE',
                    'end_date_unix' => 'END TIME',
                ),
                'sorterHeader' => 'SORT BY:',
                'itemsCssClass' => $itemsCssClass,
                'itemsTagName' => $itemsTagName,
                'pager' => array(
                    'class' => 'MyLinkPager',
                    'header' => '',
                    'htmlOptions' => array(
                        'class' => 'auction_list__sort_row__pages pull-right',
                    ),
                ),
            ));
        ?>
	</div>

	<div class="card_detail__foot_info">
        <?php echo $model->seo_text?>
	</div>
</div>
</div>


<?php if ($scroll):?>
<script type="text/javascript">
    $(document).ready(function(){
        var scroll_search = $( ".list-view" ).offset();
        $(window, 'body').scrollTop(scroll_search.top);
    });
</script>

<?php endif;?>

<script type="text/javascript">
	$(document).ready(function () {
		$("#pagesizer option").each(function () {
				if ($(this).attr("data-url") == '') {
					$(this).attr('selected', 'selected');
				}
			}
		);
	});
</script>

