<?php
/* @var $this OrangelotsKindController */
/* @var $model OrangelotsKind */
/* @var $form CActiveForm */
CHtml::$afterRequiredLabel = '<span class="important_star"> *</span> ';

?>

<div class="form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'orangelots-category-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation' => false,
        )
    ); ?>

    <?php echo $form->errorSummary($model); ?>


    <div class="uni_form_block">
        <p class="singup__title">General</p>

        <!--<p class="uni_form_row">
            <?php /*echo $form->labelEx($model, 'url'); */?>
            <?php /*echo $form->textField($model, 'url', array('size' => 60, 'maxlength' => 255)); */?>
            <?php /*echo $form->error($model, 'url'); */?>
        </p>-->

        <p class="uni_form_row">
            <?php echo $form->labelEx($model, 'category_description'); ?>

        <div class="controls">
            <?$this->widget(
                'ext.editor.editMe.widgets.ExtEditMe',
                array(
                    'model' => $model,
                    'width' => 800,
                    'height' => 400,
                    'attribute' => 'category_description',
                    'autoLanguage' => false,
                    'filebrowserImageUploadUrl' => yii()->baseUrl . '/img-upload/',
                    'toolbar' => array(
                        array('Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'),
                        array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',),
                        array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'),
                        array(
                            'Form',
                            'Checkbox',
                            'Radio',
                            'TextField',
                            'Textarea',
                            'Select',
                            'Button',
                            'ImageButton',
                            'HiddenField'
                        ),
                        '/',
                        array(
                            'Bold',
                            'Italic',
                            'Underline',
                            'Strike',
                            'Subscript',
                            'Superscript',
                            '-',
                            'RemoveFormat',
                        ),
                        array(
                            'NumberedList',
                            'BulletedList',
                            '-',
                            'Outdent',
                            'Indent',
                            '-',
                            'Blockquote',
                            'CreateDiv',
                            '-',
                            'JustifyLeft',
                            'JustifyCenter',
                            'JustifyRight',
                            'JustifyBlock',
                            '-',
                            'BidiLtr',
                            'BidiRtl',
                        ),
                        array('Link', 'Unlink', 'Anchor',),
                        array(
                            'Image',
                            'Flash',
                            'Table',
                            'HorizontalRule',
                            'Smiley',
                            'SpecialChar',
                            'PageBreak',
                            'Iframe'
                        ),
                        '/',
                        array('Styles', 'Format', 'Font', 'FontSize',),
                        array('TextColor', 'BGColor',),
                        array('Maximize', 'ShowBlocks',),
                    )
                )
            )?>
        </div>
        <?php echo $form->error($model, 'category_description'); ?>
        </p>

        <p class="uni_form_row">
            <?php echo $form->labelEx($model, 'page_title'); ?>
            <?php echo $form->textField($model, 'page_title', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'page_title'); ?>
        </p>


    </div>
    <div class="uni_form_block">
        <p class="singup__title">SEO Text</p>
        <p class="uni_form_row">
            <?php echo $form->labelEx($model, 'meta_description'); ?>
            <?php echo $form->textField($model, 'meta_description', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'meta_description'); ?>
        </p>

        <p class="uni_form_row">
            <?php echo $form->labelEx($model, 'meta_keywords'); ?>
            <?php echo $form->textField($model, 'meta_keywords', array('size' => 60, 'maxlength' => 255)); ?>
            <?php echo $form->error($model, 'meta_keywords'); ?>
        </p>

        <p class="uni_form_row">
            <?php echo $form->labelEx($model, 'seo_text'); ?>
            <?$this->widget(
                'ext.editor.editMe.widgets.ExtEditMe',
                array(
                    'model' => $model,
                    'width' => 800,
                    'height' => 400,
                    'attribute' => 'seo_text',
                    'autoLanguage' => false,
                    'filebrowserImageUploadUrl' => yii()->baseUrl . '/img-upload/',
                    'toolbar' => array(
                        array('Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'),
                        array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',),
                        array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'),
                        array(
                            'Form',
                            'Checkbox',
                            'Radio',
                            'TextField',
                            'Textarea',
                            'Select',
                            'Button',
                            'ImageButton',
                            'HiddenField'
                        ),
                        '/',
                        array(
                            'Bold',
                            'Italic',
                            'Underline',
                            'Strike',
                            'Subscript',
                            'Superscript',
                            '-',
                            'RemoveFormat',
                        ),
                        array(
                            'NumberedList',
                            'BulletedList',
                            '-',
                            'Outdent',
                            'Indent',
                            '-',
                            'Blockquote',
                            'CreateDiv',
                            '-',
                            'JustifyLeft',
                            'JustifyCenter',
                            'JustifyRight',
                            'JustifyBlock',
                            '-',
                            'BidiLtr',
                            'BidiRtl',
                        ),
                        array('Link', 'Unlink', 'Anchor',),
                        array(
                            'Image',
                            'Flash',
                            'Table',
                            'HorizontalRule',
                            'Smiley',
                            'SpecialChar',
                            'PageBreak',
                            'Iframe'
                        ),
                        '/',
                        array('Styles', 'Format', 'Font', 'FontSize',),
                        array('TextColor', 'BGColor',),
                        array('Maximize', 'ShowBlocks',),
                    )
                )
            )?>
            <?php echo $form->error($model, 'seo_text'); ?>
        </p>
    </div>

        <button class="btn btn_orange"><?php echo $model->isNewRecord ? 'Create Category' : 'Save Category'; ?></button>

    <?php $this->endWidget(); ?>

</div><!-- form -->