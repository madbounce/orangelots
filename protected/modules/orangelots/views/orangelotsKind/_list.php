<?php
    $link = OrangelotsAuctions::SaveUnSave($data->id);
    $auct_id = $data->id;
?>
        <div class="auction_list___main_list_catalog__item">
            <p class="auction_list__carousel__date"><?php echo yii()->dateFormatter->format('MM/dd/yyyy hh:mm:ss a', $data->end_date_unix); ?> EST</p>

            <div class="auction_list__carousel__image">
                <?php if(OrangelotsAuctions::main_photo($data)):?>
                    <? foreach ($data->galleryPhotos AS $key => $value): ?>
                        <? if ($value->set_main == 1): ?>
                            <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $data->id))?>"><img src="<?php echo createUrl($value->getThumb()) ?>" alt=""></a>
                        <? endif; ?>
                    <? endforeach; ?>
                <?php else:?>
                    <?php if(isset($data->galleryPhotos[0])):?>
                <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $data->id))?>"><img src="<?php echo createUrl($data->galleryPhotos[0]->getThumb()) ?>" alt=""></a>
                    <?php else:?>
                <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $data->id))?>"><img src="<?php echo yii()->baseUrl.'/images/NIA.png' ?>" alt=""></a>
                    <?php endif; ?>
                <?php endif; ?>
                <?/* foreach ($data->galleryPhotos AS $key => $value): */?><!--
                    <?/* if ($value->set_main == 1): */?>
                        <a href = "<?php /*echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $data->id))*/?>"><img src="<?php /*echo createUrl($value->getThumb()) */?>" alt=""></a>
                    <?/* endif; */?>
                --><?/* endforeach; */?>
            </div>
            <a href="<?php echo yii()->createUrl('auctions/orangelots-auctions/view/', array('id' => $data->id))?>" class="auction_list__carousel__name"><?php echo $data->name?> - orig. price - $<?php echo $data->mvrp?></a>
            <p class="auction_list__carousel__price">Qty: <?php echo $data->units_in_lot ?> / Unit: <span>$<?php if($data->min_bid !=0):  echo $data->min_bid; else: echo $data->buy_now_price; endif;?></span> <?php if ($data->saving_buy_now != 0):?>(<?php echo round($data->saving_buy_now) ?>% off)</p><?php endif; ?>

            <p>
                <a class="btn green_bnt" href="<?php echo createUrl('auctions/orangelots-auctions/view',array('id' =>$data->id ))?>">BID NOW</a>
            </p>
            <?php if (!user()->isGuest):?>
            <p>
                <?php echo CHtml::ajaxLink(
                    "remove from wishlist",
                    yii()->createUrl('auctions/orangelots-auctions/un-save-for-later'),
                    array(
                        'type' => 'POST',
                        'data' => array(
                            'Save[auction]' => $auct_id,
                            'Save[user_id]' => yii()->user->id,
                        ),
                        'success' => "function(data){
                                if(data == 'ok'){
                                    $('#remove_from_whishlist_".$auct_id."').hide();
                                    $('#save_for_later_".$auct_id."').show();
                                    $('#remove_from_whishlist_sponsored_".$auct_id."').hide();
                                    $('#save_for_later_sponsored_".$auct_id."').show();
                                }
                        }",
                    ),
                    array(
                        'id' => 'remove_from_whishlist_'.$auct_id,
                        'style' => ($link)?'display:block':'display:none',
                        'class' => 'save_later',
                    )
                );
                ?>
                <!--                --><?php //else: ?>
                <?php echo CHtml::ajaxLink(
                    "save for later",
                    yii()->createUrl('auctions/orangelots-auctions/save-for-later'),
                    array(
                        'type' => 'POST',
                        'data' => array(
                            'Save[auction]' => $auct_id,
                            'Save[user_id]' => yii()->user->id,
                        ),
                        'success' => "function(data){
                                if(data == 'ok'){
                                    $('#save_for_later_".$auct_id."').hide();
                                    $('#remove_from_whishlist_".$auct_id."').show();
                                    $('#save_for_later_sponsored_".$auct_id."').hide();
                                    $('#remove_from_whishlist_sponsored_".$auct_id."').show();
                                }
                        }",
                    ),
                    array(
                        'id' => 'save_for_later_'.$auct_id,
                        'style' => ($link)?'display:none':'display:block',
                        'class' => 'save_later',

                    )
                );
                ?>
            </p>
            <?php endif; ?>
        </div>