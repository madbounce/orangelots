<?php
/* @var $this OrangelotsKindController */
/* @var $model OrangelotsKind */

$this->breadcrumbs=array(
	'Orangelots Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

/*$this->menu=array(
	array('label'=>'List OrangelotsKind', 'url'=>array('index')),
	array('label'=>'Create OrangelotsKind', 'url'=>array('create')),
	array('label'=>'View OrangelotsKind', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OrangelotsKind', 'url'=>array('admin')),
);
*/?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>