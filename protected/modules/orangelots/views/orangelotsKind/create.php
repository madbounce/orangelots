<?php
/* @var $this OrangelotsKindController */
/* @var $model OrangelotsKind */

$this->breadcrumbs=array(
	'Orangelots Categories'=>array('index'),
	'Create',
);

/*$this->menu=array(
	array('label'=>'List OrangelotsKind', 'url'=>array('index')),
	array('label'=>'Manage OrangelotsKind', 'url'=>array('admin')),
);
*/?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>