<li>
    <div class="flash_deals_catalog__image">
        <?php if(OrangelotsAuctions::main_photo($data)):?>
            <? foreach ($data->galleryPhotos AS $key => $value): ?>
                <? if ($value->set_main == 1): ?>
                    <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $data->id))?>"><img src="<?php echo createUrl($value->getThumb()) ?>" alt=""></a>
                <? endif; ?>
            <? endforeach; ?>
        <?php else:?>
            <?php if(isset($data->galleryPhotos[0])):?>
                <img src="<?php echo createUrl($data->galleryPhotos[0]->getThumb()) ?>" alt="">
            <?php else:?>
                <img src="<?php echo yii()->baseUrl.'/images/NIA.png' ?>" alt="">
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <a href="<?php echo yii()->createUrl('auctions/orangelots-auctions/view/', array('id' => $data->id))?>" class="flash_deals_catalog__name"><?php echo $data->name?> - orig. price - $<?php echo number_format($data->mvrp, 2)?></a>
    <p class="flash_deals_catalog__time">
        Starts in:
        <?$info = getTimeFromNowInfo($data->start_date_unix);?>
        <span>
             <?php echo $info->days.' '.plural($info->days, 'Days', 'Day', 'Days').', '?>
             <?php echo $info->h.' '.plural($info->h, 'hours', 'hour', 'hours').', '.$info->m.' '.plural($info->m,'minutes', 'minute', 'minutes')?>
        </span>
    </p>
    <p class="flash_deals_catalog__time">Closing Time: <?php echo yii()->dateFormatter->format('MM/dd/yyyy hh:mm:ss a',$data->end_date_unix); ?> EST</p>
    <p class="flash_deals_catalog__per_unit">
        Units in lot:
        <?php echo $data->lots_available ?>
        / Price per unit:
        <span>$
            <?php echo number_format($data->price_per_unit, 2) ?>
        </span>
        (<?php echo number_format($data->saving_price_per_unit, 2) ?>% off)
    </p>
</li>