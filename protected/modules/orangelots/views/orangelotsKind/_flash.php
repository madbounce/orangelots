<li>
    <div class="flash_deals_catalog__image">
        <?php if(OrangelotsAuctions::main_photo($data)):?>
            <? foreach ($data->galleryPhotos AS $key => $value): ?>
                <? if ($value->set_main == 1): ?>
                    <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $data->id))?>"><img src="<?php echo createUrl($value->getThumb()) ?>" alt=""></a>
                <? endif; ?>
            <? endforeach; ?>
        <?php else:?>
            <?php if(isset($data->galleryPhotos[0])):?>
        <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $data->id))?>"><img src="<?php echo createUrl($data->galleryPhotos[0]->getThumb()) ?>" alt=""></a>
            <?php else:?>
        <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $data->id))?>"> <img src="<?php echo yii()->baseUrl.'/images/NIA.png' ?>" alt=""></a>
            <?php endif; ?>
        <?php endif; ?>
    </div>
    <a href="<?php echo yii()->createUrl('auctions/orangelots-auctions/view/', array('id' => $data->id))?>" class="flash_deals_catalog__name"><?php echo $data->name?> - orig. price - $<?php echo number_format($data->mvrp, 2)?></a>
    <p class="flash_deals_catalog__time">End in:  <span>
                                <?php echo $days = floor(($data->end_date_unix - time()) / (24 * 60 * 60)) ?> Day,
            <?php echo $hours = floor(($data->end_date_unix - time() - ($days * (24 * 60 * 60))) / (60 * 60)) ?>
            hours
            <?php echo $minutes = floor(
                ($data->end_date_unix - time() - ($hours * (60 * 60)) - $days * (24 * 60 * 60)) / (60)
            ) ?> minutes</span></p>
    <p class="flash_deals_catalog__time">Closing Time: <?php echo yii()->dateFormatter->format(
            'MM/dd/yyyy hh:mm:ss a',
            $data->end_date_unix
        ); ?> EST</p>

    <p class="flash_deals_catalog__per_unit">Units in lot: <?php echo $data->lots_available ?>  / Price per unit: <span>$<?php echo number_format($data->price_per_unit, 2) ?></span> (<?php echo number_format($data->saving_price_per_unit, 2) ?>% off)</p>

    <div class="flash_deals_catalog__button_row">
        <label>Lots:</label>
        <div class="flash_deals_catalog__button_row__select_container <?php echo 'flash_'.$data->id?>" >
            <?php echo $select = OrangelotsAuctions::select_step_flashdeal($data); ?>
        </div>
        <?php echo CHtml::ajaxSubmitButton(
            'buy now',
            Yii::app()->createUrl('auctions/orangelots-auctions/buy-now'),
            array(
                'type' => 'post',
                'data' => array(
                    'model_id' => $data->id,
                    'count' => "js:$('div.flash_".$data->id." span.filter-option.pull-left').html()",
                ),
                'success' => 'function(data) {
                                            var obj = JSON.parse(data);
                                            console.log(obj.status);
                                            if (obj.status == "error") {
                                             $("body").append(obj.html);
                                            }
                                            else if (obj.status == "some_error") {
                                                alert(obj.html);
                                            }
                                            else if (obj.status == "bidding_save") {
                                                $("body").append(obj.html);
                                            }

                                        }',
            ),
            array(
                'class' => 'btn green_bnt',
                'type'=>'image',
                'src'=>yii()->baseUrl . '/images/btn/btn_buy_now.png',
                'style' => 'height: 28px !important;
                            padding: 2px 12px !important;
                            font-size: 14px;
                            width: 94px;'
            )
        );
        ?>
    </div>
</li>