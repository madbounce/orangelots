<?php
/* @var $this OrangelotsKindController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orangelots Categories',
);

$this->menu=array(
	array('label'=>'Create OrangelotsKind', 'url'=>array('create')),
	array('label'=>'Manage OrangelotsKind', 'url'=>array('admin')),
);
?>

<h1>Orangelots Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
