<?php
/* @var $this OrangelotsKindController */
/* @var $model OrangelotsKind */

$this->breadcrumbs=array(
	'Orangelots Categories'=>array('index'),
	'Manage',
);

/*$this->menu=array(
	array('label'=>'List OrangelotsKind', 'url'=>array('index')),
	array('label'=>'Create OrangelotsKind', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#orangelots-category-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orangelots-category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id_auction',
		'name',
//		'parent_id',
        array(
            'name' => 'parent_id',
            'type' => 'raw',
            'value' => '$data->getParent($data->parent_id)',
            'filter' => CHtml::listData(OrangelotsKind::model()->findAll(), 'id', 'name'),
        ),
        array(
            'name' => 'on_main',
            'type' => 'raw',
            'value' => '$data->active==1?Show:Hide',
            'filter' => array(0=>'Hide',1=>'Show'),
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{delete}{update}',
		),
	),
)); ?>
