<?php $category = OrangelotsCategory::model()->findByPk($category_id); ?>
<?php if (!empty($category)): ?>
	<?php if (!empty($category->categoryGroups)): ?>
		<?php foreach ($category->categoryGroups as $groupAttribute): ?>
			<?php if (!empty($groupAttribute)): ?>
				<div class="fields-group">
					<div class="title">
						<a href="#" class="js-group-title"><?= $groupAttribute->title ?></a>
					</div>
					<br/>
					<?php foreach ($groupAttribute->relAttributes as $attribute): ?>
						<div class="control-group">
							<?php if (in_array($attribute->type, array(OrangelotsAttribute::TYPE_INPUT_STRING, OrangelotsAttribute::TYPE_INPUT_INTEGER, OrangelotsAttribute::TYPE_INPUT_FLOAT))): ?>
								<label class="control-label <?= isset($errors[$attribute->id]) ? 'error' : '' ?>"
								       for="Orangelots_attributes_<?= $attribute->id ?>"><?= $attribute->title ?>
									<?= ($attribute->is_required) ? '<span class="required">*</span>' : '' ?>
								</label>
								<div class="controls">
									<div class="input-append">
										<input style="display:inline" type="text"
										       id="Orangelots_attributes_<?= $attribute->id ?>"
										       name="Orangelots[post_attributes][<?= $attribute->id ?>]"
										       value="<?= (isset($dataAttributes[$attribute->id])) ? $dataAttributes[$attribute->id] : '' ?>"
										       class="span3 <?= isset($errors[$attribute->id]) ? 'error' : '' ?>">
										<?php if (trim($attribute->unit) != ''): ?>
											<span class="add-on"><?= $attribute->unit ?></span>
										<?php endif; ?>
										<?php if (isset($errors[$attribute->id])): ?>
											<span class="help-inline error"><?= $errors[$attribute->id][0] ?></span>
										<?php endif; ?>
									</div>
								</div>
							<?php endif; ?>
							<?php if ($attribute->type == OrangelotsAttribute::TYPE_CHECKBOX): ?>
								<div class="controls">
									<label class="checkbox">
										<input type="checkbox" id="Orangelots_attributes_<?= $attribute->id ?>"
										       name="Orangelots[post_attributes][<?= $attribute->id ?>]"
										       value="1" <?= (isset($dataAttributes[$attribute->id])) ? 'checked="checked"' : '' ?>>
										<?= $attribute->title ?>
									</label>
								</div>
							<?php endif; ?>
							<?php if ($attribute->type == OrangelotsAttribute::TYPE_SELECT): ?>
								<label class="control-label"
								       for="Orangelots_attributes_<?= $attribute->id ?>"><?= $attribute->title ?>
									<?= ($attribute->is_required) ? '<span class="required">*</span>' : '' ?>
								</label>
								<div class="controls">
									<?php $options = $attribute->getCacheOptions(); ?>
									<select id="Orangelots_attributes_<?= $attribute->id ?>"
									        name="Orangelots[post_attributes][<?= $attribute->id ?>]"
									        class="span3 <?= isset($errors[$attribute->id]) ? 'error' : '' ?>">
										<option value="">-</option>
										<?php foreach ($options as $option): ?>
											<option <?= (isset($dataAttributes[$attribute->id]) && $dataAttributes[$attribute->id] == $option->id) ? 'selected="selected"' : '' ?>
												value="<?= $option->id ?>"><?= $option->value ?></option>
										<?php endforeach; ?>
									</select>
									<?php if (isset($errors[$attribute->id])): ?>
										<span class="help-inline error"><?= $errors[$attribute->id][0] ?></span>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
<?php endif; ?>