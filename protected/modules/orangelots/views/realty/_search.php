<?php
/**
 * Шаблон формы поиска
 * @author maxshurko@gmail.com
 */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<table class="items filters search">
	<thead>
		<tr>
			<td colspan="2">
				<span class="left">Фильтр</span>
				<a href="#" class="toggle"><span>Развернуть</span></a>
			</td>
		</tr>
	</thead>
	<tbody>

		<tr>
			<td><?php echo $form->label($model,'id');?></td>
			<td><?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>4));?></td>
		</tr>

		<tr>
			<td><?php echo $form->label($model,'is_active'); ?></td>
			<td><?php echo $form->dropDownList($model,'is_active', SearchFilterHelper::getDefaultValue()+SearchFilterHelper::getYesNoFilter()); ?></td>
		</tr>
	
		<?php $this->widget('CalendarWidget', array('model' => $model, 'field' => 'create_date_from', 'form' => $form, 'time' => true,));?>
		<?php $this->widget('CalendarWidget', array('model' => $model, 'field' => 'create_date_to', 'form' => $form, 'time' => true,));?>
	
		<?php $this->widget('CalendarWidget', array('model' => $model, 'field' => 'start_publish_from', 'form' => $form, 'time' => true,));?>
		<?php $this->widget('CalendarWidget', array('model' => $model, 'field' => 'start_publish_to', 'form' => $form, 'time' => true,));?>
		
		<?php $this->widget('CalendarWidget', array('model' => $model, 'field' => 'finish_publish_from', 'form' => $form, 'time' => true,));?>
		<?php $this->widget('CalendarWidget', array('model' => $model, 'field' => 'finish_publish_to', 'form' => $form, 'time' => true,));?>
		
		<tr>
			<td><?php echo $form->label($model,'alias'); ?></td>
			<td><?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?></td>
		</tr>
	
		<tr>
			<td><?php echo $form->label($model,'category_id'); ?></td>
			<td><?php echo $form->dropDownList($model,'category_id', SearchFilterHelper::getDefaultValue()+NewsCategory::getAll()); ?></td>
		</tr>
	
		<tr>
			<td><?php echo $form->label($model,'title'); ?></td>
			<td><?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?></td>
		</tr>
	
		<tr>
			<td><?php echo $form->label($model,'description'); ?></td>
			<td><?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>255)); ?></td>
		</tr>
	
		<tr>
			<td><?php echo $form->label($model,'content'); ?></td>
			<td><?php echo $form->textField($model,'content',array('size'=>60,'maxlength'=>255)); ?></td>
		</tr>
		
		<tr>
			<td><?php echo $form->label($model,'seo_keywords'); ?></td>
			<td><?php echo $form->textField($model,'seo_keywords',array('size'=>60,'maxlength'=>255)); ?></td>
		</tr>
		
		<tr>
			<td><?php echo $form->label($model,'seo_description'); ?></td>
			<td><?php echo $form->textField($model,'seo_description',array('size'=>60,'maxlength'=>255)); ?></td>
		</tr>
	
		<tr>
			<td>&nbsp</td>
	        <td><button class="button"><span class="save">Искать</span></button></td>
		<tr>
		
	</tbody>
</table>

<?php $this->endWidget();?>
