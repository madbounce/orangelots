<?php
/**
 * Шаблон формы редактирования новостей
 * @author maxshurko@gmail.com
 * @var TbActiveForm $form
 */
?>

<?php $this->widget('bootstrap.widgets.TbTabs', array(
	'tabs' => $this->getTabularTabs($model),
)); ?>
