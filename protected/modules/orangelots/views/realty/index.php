<?php
/**
 * Шаблон списка в панеле управления
 * @author fen0man
 */
?>

<?php

$this->widget('bootstrap.widgets.TbGridView', array(
	'type' => 'striped bordered condensed',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'template' => "{summary}{items}{pager}",
	'columns' => array(
		array(
			'name' => 'id',
			'header' => '#'
		),
		'title',
		array(
			'name' => 'category_id',
//			'value' => '$data->getCategoryTitle()',
//			'filter' => OrangelotsCategory::getAllCategory(),
		),
		array(
			'name' => 'is_active',
			'value' => '$data->is_active ? "Да" : "Нет"',
//			'filter' => SearchFilterHelper::getYesNoFilter(),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'htmlOptions' => array('style' => 'width: 55px'),
			'template' => '{update} {delete}',
		),
	),
)); ?>
