<?php
/**
 */
?>

<?= CHtml::link('Создать', createUrl('//realty/group/create')) ?>
<?php $this->widget(
	'bootstrap.widgets.TbGridView',
	array(
		'type' => 'striped bordered condensed',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'template' => "{items}{pager}",
		'columns' => array(
			array(
				'name' => 'id',
				'header' => '#',
			),
			'title',
			array(
				'name' => 'category_id',
				'value' => '$data->category ? $data->category->title : "-"',
				'filter' => OrangelotsCategory::model()->getAll(),
			),
			'rank',
			array(
				'template' => '{update} {delete}',
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'htmlOptions' => array('style' => 'width: 55px'),
			),
		),
	)
); ?>
