<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'news-category-form',
    'type'=>'horizontal',
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),	
)); ?>

    <?php echo $form->textFieldRow($model,'rank',array('size'=>60,'maxlength'=>255)); ?>

    <?php echo $form->textFieldRow($model,'title',array('size'=>60,'maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'category_id', OrangelotsCategory::model()->getAll(), array('empty'=>'-')); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'htmlOptions'=>array('name'=>'yt0'),
		'type'=>'success',
		'label'=>'Сохранить')); ?>
		
    <?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit', 
		'htmlOptions'=>array('name'=>'apply', 'class'=>'btn-apply'),
		'label'=>'Применить')); ?>
		
    <?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'htmlOptions'=>array('name'=>'yt1', 'class'=>'btn-cancel'),
		'label'=>'Отмена')); ?>
</div>
<?php $this->endWidget(); ?>