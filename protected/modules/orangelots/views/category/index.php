<?php
/**
 * Шаблон списка в панеле управления
 * @author dorosh_2009@meta.ua
 */
?>

<?= CHtml::link('Создать', createUrl("//realty/category/create")) ?>



<?php $this->widget(
	'bootstrap.widgets.TbGridView',
	array(
		'type' => 'striped bordered condensed',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'template' => "{items}{pager}",
		'columns' => array(
			array(
				'name' => 'id',
				'header' => '#',
			),
			'title',
			array(
				'name' => 'is_active',
				'value' => '$data->is_active ? "Да" : "Нет"',
				'filter' => /*snitka*/ /*SearchFilterHelper::getYesNoFilter()*/
					'',
			),
			'rank',
//		'activity_count',
//		'activity_count_active',
			'alias',
			array(
				'template' => '{update} {delete}',
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'htmlOptions' => array('style' => 'width: 55px'),
			),
		),
	)
); ?>
