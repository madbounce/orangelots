<?php
/**
 * Шаблон формы редактирования категории статьи
 * @author dorosh_2009@meta.ua
 */
?>

<h1>Редактирование категории мероприятия</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>