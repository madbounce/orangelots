<?php
/**
 * Шаблон формы поиска
 * @author dorosh_2009@meta.ua
 */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<table class="items filters search">
	<thead>
		<tr>
			<td colspan="2">
				<span class="left">Фильтр</span>
				<a href="#" class="toggle"><span>Развернуть</span></a>
			</td>
		</tr>
	</thead>
	<tbody>

		<tr>
			<td><?php echo $form->label($model,'id');?></td>
			<td><?php echo $form->textField($model,'id',array('size'=>60,'maxlength'=>4));?></td>
		</tr>

		<tr>
			<td><?php echo $form->label($model,'is_active'); ?></td>
			<td><?php echo $form->dropDownList($model,'is_active', SearchFilterHelper::getDefaultValue()+SearchFilterHelper::getYesNoFilter()); ?></td>
		</tr>

		<tr>
			<td><?php echo $form->label($model,'rank'); ?></td>
			<td><?php echo $form->textField($model,'rank',array('size'=>60,'maxlength'=>6)); ?></td>
		</tr>
	
		<tr>
			<td><?php echo $form->label($model,'alias'); ?></td>
			<td><?php echo $form->textField($model,'alias',array('size'=>60,'maxlength'=>255)); ?></td>
		</tr>
	
		<tr>
			<td><?php echo $form->label($model,'title'); ?></td>
			<td><?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?></td>
		</tr>
	
		<tr>
			<td>&nbsp </td>
	        <td><button class="button"><span class="save">Искать</span></button></td>
		<tr>
		
	</tbody>
</table>

<?php $this->endWidget();?>
