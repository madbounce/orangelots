<?php
/**
 * Шаблон формы редактирования данных пользователя
 * @author dorosh_2009@meta.ua
 */
?>

<?php $form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm',
	array(
		'id' => 'news-category-form',
		'type' => 'horizontal',
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)
); ?>

<?php echo $form->checkBoxRow($model, 'is_active', array('class' => 'check')); ?>

<?php echo $form->textFieldRow($model, 'rank', array('size' => 60, 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'alias', array('size' => 60, 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>

<?php echo $form->textAreaRow($model, 'description', array()); ?>

<?php $checkedGroups = $model->getCheckedGroups(); ?>
<div id="container_checkbox_groups" class="control-group" style="margin-left: 120px;">
	<label>Groups Attribute</label>

	<div style="margin-left: 100px;">
		<?php $catGroups = isset($model->categoryGroups) ? $model->categoryGroups : array() ?>

		<?php foreach ($catGroups as $categoryGroup): ?>
			<label class="checkbox">
				<input name="ActivityCategory[post_groups][]" value="<?= $categoryGroup->id ?>"
					<?= in_array($categoryGroup->id, $checkedGroups) ? 'checked="checked"' : '' ?>
					   type="checkbox"><?= $categoryGroup->title ?></label>
		<?php endforeach; ?>
	</div>
</div>

<?php echo $form->fileFieldRow($model, 'main_photo'); ?>

<?php echo $form->textFieldRow($model, 'seo_keywords', array('size' => 60, 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'seo_description', array('size' => 60, 'maxlength' => 255)); ?>

<?php if ($model->isNewRecord == false): ?>
	<img src="<?= $model->getThumb(50, 50, 'crop') ?>"/>
<?php endif; ?>

<div class="form-actions">
	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'submit',
			'htmlOptions' => array('name' => 'yt0'),
			'type' => 'success',
			'label' => 'Сохранить'
		)
	); ?>

	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'submit',
			'htmlOptions' => array('name' => 'apply', 'class' => 'btn-apply'),
			'label' => 'Применить'
		)
	); ?>

	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'submit',
			'htmlOptions' => array('name' => 'yt1', 'class' => 'btn-cansel'),
			'label' => 'Отмена'
		)
	); ?>
</div>
<?php $this->endWidget(); ?>

<script>
	$('#ActivityCategory_parent_id').change(function () {
		$.post('/?r=activity/category/groups', {'category_id': $(this).val()}, function (response) {
			$('#container_checkbox_groups div').html('');
			$.each(response.items, function (i, v) {
				$('#container_checkbox_groups div').append('<label class="checkbox"><input name="ActivityCategory[post_groups][]" value="' + i + '" type="checkbox">' + v + '</label>');
			})
		}, 'json')
	})
</script>