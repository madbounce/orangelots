<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'news-category-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<?php echo $form->textFieldRow($model, 'rank', array('size' => 60, 'maxlength' => 255, 'class' => 'span1')); ?>

<?php echo $form->checkBoxRow($model, 'is_required', array('class' => 'check')); ?>

<?php echo $form->checkBoxRow($model, 'is_filter', array('class' => 'check')); ?>

<?php echo $form->textFieldRow($model, 'title', array('size' => 60, 'maxlength' => 255)); ?>

<?php echo $form->dropDownListRow($model, 'group_id', CHtml::listData(OrangelotsGroupAttribute::model()->findAll(), "id", "title"), array('empty' => '-')); ?>

<?php echo $form->dropDownListRow($model, 'type', OrangelotsAttribute::getTypes()); ?>

<div id="options_container" class="row-fluid" style="margin-left: 50px;">

	<?php if (!empty($model->sizesErrors)): ?>
		<div class="alert alert-block alert-error">
			<p>Необходимо исправить следующие ошибки:</p>
			<ul>
				<?php foreach ($model->sizesErrors as $err): ?>
					<li><?= $err ?></li>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>

	<div class="control-group ">
		<label class="control-label" for="CatalogProduct_sizes">Значения списка</label>

		<div class="controls">
			<div class="row" style="width: 800px;">

				<div id="container-list-options">
					<?php $dataOptions = $model->getDataOptions(); ?>

					<?php if (!empty($dataOptions)): ?>
						<?php foreach ($dataOptions as $i => $option): ?>

							<div class="size_item">

								<?php if (isset($option['id'])): ?>
									<?= CHtml::hiddenField("OrangelotsAttribute[post_options][{$i}][id]", $option['id']) ?>
								<?php endif; ?>

								<div class="clear" style="height: 10px;"></div>

								<div class="span6">
									<?= CHtml::textField("OrangelotsAttribute[post_options][{$i}][value]", $option['value'], array("class" => (isset($model->optionsErrorsIndex['value'][$i]) ? 'error' : ''))) ?>
								</div>

								<?php if ($i > 0): ?>
									<div class="span1">
										<a class="remove btn btn-danger" href="#"
										   onclick="$(this).parent().parent().remove(); return false">
											x
										</a>
									</div>
								<?php endif; ?>

							</div>

						<?php endforeach; ?>

					<?php else : ?>
						<div class="size_item">

							<div class="clear" style="height: 10px;"></div>

							<div class="span6  js-no-standart">
								<?= CHtml::textField("OrangelotsAttribute[post_options][0][value]", '') ?>
							</div>


						</div>
					<?php endif; ?>
				</div>

				<div class="clear"></div>
				<a href="#" class="btn btn-success js-btn-add-line">+</a>

			</div>

		</div>

	</div>
</div>

<?php echo $form->textFieldRow($model, 'unit', array('size' => 60, 'maxlength' => 255, 'class' => 'span2')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'htmlOptions' => array('name' => 'yt0'),
		'type' => 'success',
		'label' => 'Сохранить')); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'htmlOptions' => array('name' => 'apply', 'class' => 'btn-apply'),
		'label' => 'Применить')); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'htmlOptions' => array('name' => 'yt1', 'class' => 'btn-cancel'),
		'label' => 'Отмена')); ?>
</div>
<?php $this->endWidget(); ?>

<script>
	var rowIndex = <?=sizeof($dataOptions)?>;
	$('.js-btn-add-line').click(function (e) {
		e.preventDefault();
		rowIndex++;

		html = '<div class="size_item">' +
			'<div class="clear" style="height: 2px;"></div>' +
			'<div class="span6">' +
			'<input class="js-sizes-size" type="text" value="" name="OrangelotsAttribute[post_options][' + rowIndex + '][value]">' +
			'</div>' +
			'<div class="span1">' +
			'<a class="remove btn btn-danger" href="#" onclick="$(this).parent().parent().remove(); return false">x</a>' +
			'</div>' +
			'</div>';

		$('#container-list-options').append(html);
	});


	$('#OrangelotsAttribute_type').change(function () {
		val = $(this).val();
		showFields(val);
	});

	function showFields(val) {
		$('#OrangelotsAttribute_unit').parent().parent().hide();
		$('#options_container').hide();

		if (val == <?=OrangelotsAttribute::TYPE_SELECT?>) {
			$('#options_container').show();
		}

		if (val == <?=OrangelotsAttribute::TYPE_INPUT_STRING?> ||
			val == <?=OrangelotsAttribute::TYPE_INPUT_INTEGER?> ||
			val == <?=OrangelotsAttribute::TYPE_INPUT_FLOAT?>
			) {
			$('#OrangelotsAttribute_unit').parent().parent().show();
		}
	}
	showFields(<?=$model->type?>);
</script>