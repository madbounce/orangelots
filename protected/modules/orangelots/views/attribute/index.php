<?php
/**
 * Шаблон списка в панеле управления
 * @author dorosh_2009@meta.ua
 */
?>

<?php $this->widget(
	'bootstrap.widgets.TbGridView',
	array(
		'type' => 'striped bordered condensed',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'template' => "{items}{pager}",
		'columns' => array(
			array(
				'name' => 'id',
				'header' => '#',
			),
			'title',
			'unit',
			array(
				'name' => 'group_id',
				'value' => '$data->group ? $data->group->title : "-"',
				'filter' => CHtml::listData(OrangelotsGroupAttribute::model()->findAll(), "id", "title"),
			),
			array(
				'name' => 'type',
				'value' => '$data->typeTitle',
				'filter' => OrangelotsAttribute::getTypes(),
			),
			array(
				'header' => 'Category',
                'name' => 'category_pointer',
				'value' => '$data->group->category->title',
                'filter'=>OrangelotsCategory::getAllCategory(),
			),
			array(
				'name' => 'is_required',
				'value' => '$data->is_required ? "Да" : "Нет"',
				'filter' => '' //SearchFilterHelper::getYesNoFilter(),
			),
			array(
				'name' => 'is_filter',
				'value' => '$data->is_filter ? "Да" : "Нет"',
				'filter' => '' //SearchFilterHelper::getYesNoFilter(),
			),
			'rank',
			array(
				'template' => '{update} {delete}',
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'htmlOptions' => array('style' => 'width: 55px'),
			),
		),
	)
); ?>
