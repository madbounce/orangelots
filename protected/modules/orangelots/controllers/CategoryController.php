<?php

/**
 * Класс CategoryController
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 *
 * Работа с категориями статей в панеле управления
 */
class CategoryController extends Controller
{

	public $layout = '//layouts/admin';

	public function filters()
	{
		return array(
			'rights',
		);
	}


	public function actionCreate()
	{
		$model = new OrangelotsCategory;
		$data = Yii::app()->getRequest()->getParam('OrangelotsCategory', null);
		if (!empty($data)) {
			$model->attributes = $data;
			$model->main_photo = CUploadedFile::getInstance($model, 'main_photo');
			if ($model->save()) {
				$this->redirect("index");
			}
		}

		$this->setPageTitle(Yii::app()->name . ' - Add Type');
		$this->render(
			'create',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Редактирование категории
	 * @param integer $id
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$data = Yii::app()->getRequest()->getParam('OrangelotsCategory', null);
		if (!empty($data)) {
			$model->attributes = $data;
			$model->main_photo = CUploadedFile::getInstance($model, 'main_photo');
			if ($model->save()) {
				$this->redirect("../index");
			}
		}

		$this->setPageTitle(Yii::app()->name . ' - Редактировать категорию обьявления');
		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$model = $this->loadModel($id);
			$model->delete();
			$ajax = Yii::app()->request->getParam('ajax', null);
			if (empty($ajax)) {
				$returnUrl = Yii::app()->request->getParam('returnUrl', null);
				$this->redirect(!empty($returnUrl) ? $returnUrl : array('index'));
			}
		} else {
			throw new CHttpException(400, "error request");
		}
	}

	/**
	 * Показать все категории
	 */
	public function actionIndex()
	{
		$model = new OrangelotsCategory('search');
		$model->unsetAttributes();

		$data = Yii::app()->getRequest()->getParam('OrangelotsCategory', null);
		if (!empty($data)) {
			$model->attributes = $data;
		}

		$this->setPageTitle(Yii::app()->name . ' - Список категорий обьявлений');
		$this->render(
			'index',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Загружаем затребованную модель
	 * @param integer
	 */
	public function loadModel($id)
	{
		$model = OrangelotsCategory::model()->findByPk($id);
		if (empty($model)) {
			throw new CHttpException(404, 'error request');
		}
		return $model;
	}

	/**
	 * Получить все группы атрибутов категории
	 */
	public function actionGroups()
	{
		$model = $this->loadModel(Yii::app()->request->getParam('category_id'));
		$response = array();
		$groups = $model->categoryGroups;
		foreach ($groups as $group) {
			$response['items'][$group->id] = $group->title;
		}
		echo json_encode($response);
	}

}
