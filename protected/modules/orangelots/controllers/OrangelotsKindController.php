<?php

class OrangelotsKindController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = 'application.views.layouts.admin';


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		/*return array(
			'accessControl',
			'postOnly + delete',
		);*/
		return array('rights');
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	/*public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow',
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',
				'users'=>array('*'),
			),
		);
	}*/

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($parent = null, $sub = null,/*$id*/$alias=null , $vendor = null, $view=null, $style=null)
	{
        /*for alias*/
        if ((isset($alias)) && (!empty($alias))){
            $kind = OrangelotsKind::model()->findByAttributes(array('alias'=>$alias));
            if ((isset($kind)) && (!empty($kind)))
                $categoryId = $kind->id;
            else
                throw new CHttpException(404, 'This page not found!');
        }
        /*for alias*/
        unset($_SESSION['checked']);
		unset($_SESSION['Filter']);
        unset($_SESSION['minPrice']);
        unset($_SESSION['maxPrice']);
        unset($_SESSION['minSavings']);
        unset($_SESSION['maxSavings']);

		$this->layout = 'application.views.layouts.main';
		$view = $this->ToView();

        if(!empty($_SESSION['condition'])){
            $condition = $_SESSION['condition'];
        }

		$criteria = new CDbCriteria();
        $staticPage = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'all'));
        if(!empty($categoryId)){
            $criteria->condition = 'category_id = ' . $categoryId;
        }elseif(!empty($vendor)){
            $manufctr = OrangelotsManufacture::model()->findByAttributes(array('alias'=>$vendor));
            if(isset($manufctr)&&!empty($manufctr))
            $criteria->condition = 'manufacture = '.$manufctr->id;
            else
                throw new CHttpException(404, 'This page not found');

        }elseif(!empty($style)){
            $styleParam = OrangelotsStyle::model()->findByAttributes(array('alias'=>$style));
            if(isset($styleParam)&&!empty($styleParam)){
                $criteria->condition = 'style_id = '.$styleParam->id;
            }else{
                throw new CHttpException(404, 'This page not found');
            }
        }
        if (!empty($condition) and $condition != $staticPage->id) {
            $criteria->addCondition('t.condition = ' . $condition);
        }
        $criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');
        if($_SESSION['flashDeal']){
            $flash = true;
            $criteria->addCondition('is_flashdeal = 1');
        }else{
            $criteria->addCondition('is_flashdeal = 0');
        }

        if($_SESSION['upcomingDeal']){
            $upcoming = true;
            $criteria->addCondition('start_date_unix>='.time());
        }else{
            $criteria->addCondition('start_date_unix<='.time());
        }
		$dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
				'criteria' => $criteria,
				'sort' => array(
					'attributes' => array(
						'units_in_lot' => array(
							'asc' => 'units_in_lot',
							'desc' => 'units_in_lot DESC',
						),
						'buy_now_price' => array(
							'asc' => 'buy_now_price',
							'desc' => 'buy_now_price DESC',
						),
						'end_date_unix' => array(
							'asc' => 'end_date_unix',
							'desc' => 'end_date_unix DESC',
						),
					),
				),
			)
		);

        if($flash){
            $this->render(
                'flash',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                    'itemView' => '_flash',
                )
            );
        }elseif($upcoming){
            $this->render(
                'upcoming',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                    'itemView' => '_upcoming',
                )
            );
        } else{
            $this->render(
                'view',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                )
            );
        }
	}

    public function actionVendor($parent = null, $sub = null, $alias=null , $vendor = null)
    {
        /*for alias*/
        if ((isset($alias)) && (!empty($alias))){
            $auction = OrangelotsKind::model()->findByAttributes(array('alias'=>$alias));
            if ((isset($auction)) && (!empty($auction)))
                $id = $auction->id;
            else
                throw new CHttpException(404, 'This page not found');
        }
        $staticPage = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'all'));
        /*for alias*/
        unset($_SESSION['checked']);
        unset($_SESSION['Filter']);
        unset($_SESSION['minPrice']);
        unset($_SESSION['maxPrice']);
        unset($_SESSION['minSavings']);
        unset($_SESSION['maxSavings']);

        $this->layout = 'application.views.layouts.main';
        $view = $this->ToView();

        if(!empty($_SESSION['condition'])){
            $condition = $_SESSION['condition'];
        }

        $criteria = new CDbCriteria();

        if(!empty($id)){
            $criteria->condition = 'category_id = ' . $id;
        }

        if(!empty($vendor)){
            $manufctr = OrangelotsManufacture::model()->findByAttributes(array('alias'=>$vendor));
            if(isset($manufctr)&&!empty($manufctr))
                $criteria->condition = 'manufacture = '.$manufctr->id;
            else
                throw new CHttpException(404, 'This page not found');

        }
        if (!empty($condition) and $condition != $staticPage->id) {
            $criteria->addCondition('t.condition = ' . $condition);
        }
        $criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');
        if($_SESSION['flashDeal']){
            $flash = true;
            $criteria->addCondition('is_flashdeal = 1');
        }else{
            $criteria->addCondition('is_flashdeal = 0');
        }

        if($_SESSION['upcomingDeal']){
            $upcoming = true;
            $criteria->addCondition('start_date_unix>='.time());
        }else{
            $criteria->addCondition('start_date_unix<='.time());
        }

        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'units_in_lot' => array(
                            'asc' => 'units_in_lot',
                            'desc' => 'units_in_lot DESC',
                        ),
                        'buy_now_price' => array(
                            'asc' => 'buy_now_price',
                            'desc' => 'buy_now_price DESC',
                        ),
                        'end_date_unix' => array(
                            'asc' => 'end_date_unix',
                            'desc' => 'end_date_unix DESC',
                        ),
                    ),
                ),
            )
        );

        if($flash){
            $this->render(
                'flash',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                    'itemView' => '_flash',
                )
            );
        }elseif($upcoming){
            $this->render(
                'upcoming',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                    'itemView' => '_upcoming',
                )
            );
        } else{
            $this->render(
                'view',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                )
            );
        }
    }

    public function actionStyle($parent = null, $sub = null, $alias=null , $style = null)
    {
        /*for alias*/
        if ((isset($alias)) && (!empty($alias))){
            $auction = OrangelotsKind::model()->findByAttributes(array('alias'=>$alias));
            if ((isset($auction)) && (!empty($auction)))
                $id = $auction->id;
            else
                throw new CHttpException(404, 'This page not found');
        }
        /*for alias*/
        unset($_SESSION['checked']);
        unset($_SESSION['Filter']);
        unset($_SESSION['minPrice']);
        unset($_SESSION['maxPrice']);
        unset($_SESSION['minSavings']);
        unset($_SESSION['maxSavings']);

        $this->layout = 'application.views.layouts.main';
        $view = $this->ToView();

        if(!empty($_SESSION['condition'])){
            $condition = $_SESSION['condition'];
        }

        $criteria = new CDbCriteria();

        if(!empty($id)){
            $criteria->condition = 'category_id = ' . $id;
        }
        $staticPage = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'all'));

        /*if(!empty($vendor)){
            $manufctr = OrangelotsManufacture::model()->findByAttributes(array('alias'=>$vendor));
            if(isset($manufctr)&&!empty($manufctr))
                $criteria->condition = 'manufacture = '.$manufctr->id;
            else
                throw new CHttpException(404, 'This page not found');

        }*/
        if(!empty($style)){
            $styleParam = OrangelotsStyle::model()->findByAttributes(array('alias'=>$style));
            if(isset($styleParam)&&!empty($styleParam)){
                $criteria->condition = 'style_id = '.$styleParam->id;
            }else{
                throw new CHttpException(404, 'This page not found');
            }
        }

        if (!empty($condition) and $condition != $staticPage->id) {
            $criteria->addCondition('t.condition = ' . $condition);
        }
        $criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');
        if($_SESSION['flashDeal']){
            $flash = true;
            $criteria->addCondition('is_flashdeal = 1');
        }else{
            $criteria->addCondition('is_flashdeal = 0');
        }

        if($_SESSION['upcomingDeal']){
            $upcoming = true;
            $criteria->addCondition('start_date_unix>='.time());
        }else{
            $criteria->addCondition('start_date_unix<='.time());
        }

        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'units_in_lot' => array(
                            'asc' => 'units_in_lot',
                            'desc' => 'units_in_lot DESC',
                        ),
                        'buy_now_price' => array(
                            'asc' => 'buy_now_price',
                            'desc' => 'buy_now_price DESC',
                        ),
                        'end_date_unix' => array(
                            'asc' => 'end_date_unix',
                            'desc' => 'end_date_unix DESC',
                        ),
                    ),
                ),
            )
        );

        if($flash){
            $this->render(
                'flash',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                    'itemView' => '_flash',
                )
            );
        }elseif($upcoming){
            $this->render(
                'upcoming',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                    'itemView' => '_upcoming',
                )
            );
        } else{
            $this->render(
                'view',
                array(
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                )
            );
        }
    }

	public function actionNew()
	{
        unsetSessionElems();
        $_SESSION['condition'] = Condition::getIdByName('new');

		$this->layout = 'application.views.layouts.main';
		$view = $this->ToView();

		$criteria = new CDbCriteria();
		$criteria->condition = 't.condition = 1';
		$criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');
        $criteria->addCondition('is_flashdeal = 0');
        $criteria->addCondition('start_date_unix<='.time());


		$dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
				'criteria' => $criteria,
				'sort' => array(
					'attributes' => array(
						'units_in_lot' => array(
							'asc' => 'units_in_lot',
							'desc' => 'units_in_lot DESC',
						),
						'buy_now_price' => array(
							'asc' => 'buy_now_price',
							'desc' => 'buy_now_price DESC',
						),
						'end_date_unix' => array(
							'asc' => 'end_date_unix',
							'desc' => 'end_date_unix DESC',
						),
					),
				)
			)
		);
		$model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'new'));

		$this->render(
			'view',
			array(
				'model' => $model,
				'view' => $view,
				'dataProvider' => $dataProvider,
			)
		);
	}

	public function actionUsed()
	{
        unsetSessionElems();
        $_SESSION['condition'] = Condition::getIdByName('used');

        $this->layout = 'application.views.layouts.main';
		$view = $this->ToView();
		$criteria = new CDbCriteria();
		$criteria->condition = 't.condition = 2';
		$criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');
        $criteria->addCondition('start_date_unix<='.time());
        $criteria->addCondition('is_flashdeal = 0');

		$dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
				'criteria' => $criteria,
				'sort' => array(
					'attributes' => array(
						'units_in_lot' => array(
							'asc' => 'units_in_lot',
							'desc' => 'units_in_lot DESC',
						),
						'buy_now_price' => array(
							'asc' => 'buy_now_price',
							'desc' => 'buy_now_price DESC',
						),
						'end_date_unix' => array(
							'asc' => 'end_date_unix',
							'desc' => 'end_date_unix DESC',
						),
					),
				),
			)
		);
		$model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'used'));

		$this->render(
			'view',
			array(
				'model' => $model,
				'view' => $view,
				'dataProvider' => $dataProvider,
			)
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new OrangelotsKind;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['OrangelotsKind'])) {
			$model->attributes = $_POST['OrangelotsKind'];
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render(
			'create',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['OrangelotsKind'])) {
			$model->attributes = $_POST['OrangelotsKind'];
			if ($model->update()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	public function actionEdit()
	{
        if(isset($_GET['alias'])){
            $alias = $_GET['alias'];
        } else {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
		$model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => $alias));
        if (!isset($model)){
            throw new CHttpException(404, 'The requested page does not exist.');
        }
		if (isset($_POST['OrangelotsStaticCategoryPages'])) {
			$model->attributes = $_POST['OrangelotsStaticCategoryPages'];
			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}
		$this->render(
			'_form_alias',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

		/*if ((isset($_REQUEST['OrangelotsAuctions_page'])) || (isset($_REQUEST['ajax'])) || (isset($_REQUEST['id']))) {
			$this->forward(
				'view',
				array(
					'id' => $_REQUEST['id'],
				)
			);
		}*/
        if ((isset($_REQUEST['OrangelotsAuctions_page'])) || (isset($_REQUEST['ajax'])) || (isset($_REQUEST['alias']))) {
            $this->forward(
                'view',
                array(
                    'alias' => $_REQUEST['alias'],
                )
            );
        }
		$dataProvider = new CActiveDataProvider('OrangelotsKind');
		$this->render(
			'index',
			array(
				'dataProvider' => $dataProvider,
			)
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new OrangelotsKind('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['OrangelotsKind'])) {
			$model->attributes = $_GET['OrangelotsKind'];
			$model->on_main = $_GET['OrangelotsKind']['on_main'];
		}

		$this->render(
			'admin',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OrangelotsKind the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = OrangelotsKind::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OrangelotsKind $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'orangelots-category-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function ToView()
	{
		if ((isset($_REQUEST['view'])) && (($_REQUEST['view'] == 'grid') || ($_REQUEST['view'] == 'list'))) {
			return $_REQUEST['view'];
		} else {
			return 'false';
		}
	}

	public function actionFilter($alias = null)
	{
		$this->layout = 'application.views.layouts.main';
		$view = $this->ToView();
        if(!empty($_SESSION['kind_id'])){
            $kind = $_SESSION['kind_id'];
            $model = $this->loadModel($kind);
        }
        if(!empty($alias)){
            $category = OrangelotsStaticCategoryPages::getCategoryByUrl($alias);
        }
        $staticPage = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'all'));

        if (!empty($_SESSION['condition']) and $_SESSION['condition'] != $staticPage->id) {
            $condition = $_SESSION['condition'];
        }
        $a = array();
        $elements = array();
        if(isset($_POST['Filter'])){
            yii()->session['checked'] = array();
            foreach ($_POST['Filter'] as $key => $value) {
                $a[$key] = $value;
                $arr = explode('_', $key);
                if (empty($elements[$arr[0]])) {
                    $elements[$arr[0]] = array();
                }
                $elements[$arr[0]][] = $value;
            }
            yii()->session['checked'] = $a;
        }

		foreach ($elements as $subKey => $subArr) {
			if ($subKey == 'vendor') {
				$vendorIds = implode(",", $subArr);
			} elseif ($subKey == 'style') {
				$styleIds = implode(",", $subArr);
			} elseif ($subKey == 'saving') {
				$min = min($subArr);
				$max = max($subArr);
				$maxTo = OrangelotsAuctions::$savingCriterions[$max]['to'];
				$savings = true;
                yii()->session['minSavings'] = $min;
                yii()->session['maxSavings'] = $maxTo;
			} elseif ($subKey == 'price') {
				$prices = OrangelotsAuctions::getPrices($subArr);
				$minPrice = $prices['min'];
				$maxPrice = $prices['max'];
            } elseif ($subKey == 'custom') {
				if (!empty($subArr[0]) || !empty($subArr[1])) {
					$minPrice = $subArr[0];
					$maxPrice = $subArr[1];
				    if (empty($minPrice)) {
		        	    $minPrice = 1;
				    }
				}
			}
            if(!empty($minPrice)){
                yii()->session['minPrice'] = $minPrice;
            }
            if(!empty($maxPrice)){
                yii()->session['maxPrice'] = $maxPrice;
            }
		}

		$criteria = new CDbCriteria();
		$criteria->condition = 'active = 1';
        $criteria->addCondition('status = 1');

        if($_SESSION['flashDeal']){
            $flash = true;
            $criteria->addCondition('is_flashdeal = 1');
        }elseif(!$_SESSION['upcomingDeal']){
            $criteria->addCondition('is_flashdeal = 0');
        }

        if($_SESSION['upcomingDeal']){
            $upcoming = true;
            $criteria->addCondition('is_flashdeal = 1');
            $criteria->addCondition('start_date_unix>='.time());
        }else{
            $criteria->addCondition('start_date_unix<='.time());
        }

		if (!empty($kind)) {
			$criteria->addCondition('category_id = ' . $kind);
		}
        if (!empty($condition)) {
			$criteria->addCondition('t.condition = ' . $condition);
		}
		if (!empty($styleIds)) {
			$criteria->addCondition('style_id in (' . $styleIds . ')');
		}
		if (!empty($vendorIds)) {
			$criteria->addCondition('manufacture in (' . $vendorIds . ')');
		}
		if ($savings) {
			$criteria->addCondition('saving_buy_now <> 0 and saving_min_bid <> 0');
			$criteria->addCondition('(saving_buy_now >=' . yii()->session['minSavings'] . ' and saving_buy_now <=' . yii()->session['maxSavings'] . ') or (saving_min_bid >=' . yii()->session['minSavings'] . ' and saving_min_bid <=' . yii()->session['maxSavings'] . ')');
		}
		if (!empty(yii()->session['minPrice'])) {
			if (!empty(yii()->session['maxPrice'])) {
				$criteria->addCondition('(min_bid >=' . yii()->session['minPrice'] . ' and min_bid<=' . yii()->session['maxPrice'] . ') or (buy_now_price >= ' . yii()->session['minPrice'] . ' and buy_now_price<=' . yii()->session['maxPrice'] . ')');
			} else {
				$criteria->addCondition('(min_bid >=' . yii()->session['minPrice'] . ') or (buy_now_price >= ' . yii()->session['minPrice'] . ')');
			}
		}
		$dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
				'criteria' => $criteria,
				'sort' => array(
					'attributes' => array(
						'units_in_lot' => array(
							'asc' => 'units_in_lot',
							'desc' => 'units_in_lot DESC',
						),
						'buy_now_price' => array(
							'asc' => 'buy_now_price',
							'desc' => 'buy_now_price DESC',
						),
						'end_date_unix' => array(
							'asc' => 'end_date_unix',
							'desc' => 'end_date_unix DESC',
						),
					),
				),
			)
		);

        if($flash){
            $this->render(
                'flash',
                array(
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                    'itemView' => '_flash',
                    'category' => $category
                )
            );
        }elseif($upcoming){
        $this->render(
            'upcoming',
            array(
                'model' => $model,
                'dataProvider' => $dataProvider,
                'view' => $view,
                'itemView' => '_upcoming',
                'category' => $category
            )
        );
        } else{
            $this->render(
                'filter',
                array(
                    'model' => $model,
                    'dataProvider' => $dataProvider,
                    'view' => $view,
                    'category' => $category
                )
            );
        }
	}

    public function actionSearch(){

        if((isset($_POST['Query']['category'])) && (isset($_POST['Query']['query']))){
            Yii::app()->session['query'] = $_POST['Query']['query'];
            Yii::app()->session['category']= $_POST['Query']['category'];
            $scroll = true;
        } else {
            $scroll = false;
        }
        if((isset(Yii::app()->session['query'])) && (isset(Yii::app()->session['category']))){
            $ids = array();
            if (ctype_digit(Yii::app()->session['query'])){
                $ids[] = Yii::app()->session['query'];
            } else {
            $query = Yii::app()->session['query'];
            $search = Yii::App()->sphinx;
            $search->setSelect('*');
            $search->setArrayResult(true);
            $search->setMatchMode(SPH_MATCH_ANY);
            $resArray = $search->query( $query, 'orangelots_auctions_index');
            $resArrayItems = $search->query( $query, 'orangelots_items_index');
            }

        unset($_SESSION['checked']);
        unset($_SESSION['kind_id']);
        unset($_SESSION['condition_id']);
        unset($_SESSION['Filter']);
        $this->layout = 'application.views.layouts.main';
        $view = $this->ToView();
        $criteria = new CDbCriteria();
        $criteria->condition = 't.condition = '.Yii::app()->session['category'];
        if (isset($resArray)){
            foreach($resArray['matches'] AS $key => $value){
                $ids[]=$value['id'];
            }
        }
        if (isset($resArrayItems)){
            foreach($resArrayItems['matches'] AS $key => $value){
                $ids[]=$value['attrs']['auction_deal_id'];
            }
        }
        $criteria->addCondition('active = 1');
        if (!empty($ids)){
            $criteria->addInCondition('id', $ids);
        }


        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'units_in_lot' => array(
                            'asc' => 'units_in_lot',
                            'desc' => 'units_in_lot DESC',
                        ),
                        'buy_now_price' => array(
                            'asc' => 'buy_now_price',
                            'desc' => 'buy_now_price DESC',
                        ),
                        'end_date_unix' => array(
                            'asc' => 'end_date_unix',
                            'desc' => 'end_date_unix DESC',
                        ),
                    ),
                ),
            )
        );
        $model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'search'));

            if (empty($ids)){
                $model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'search'));
                $dataProvider = new CActiveDataProvider($model, array('data' => array()));
            }
    }  else  {
            $model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'search'));
            $dataProvider = new CActiveDataProvider($model, array('data' => array()));
        }

        $this->render(
            'view',
            array(
                'model' => $model,
                'view' => $view,
                'dataProvider' => $dataProvider,
                'search_input' => Yii::app()->session['query'],
                'search_category' => Yii::app()->session['category'],
                'scroll' => $scroll,
            )
        );
    }

    public function actionAll()
    {
        unsetSessionElems();
        $_SESSION['condition'] = Condition::getIdByName('all');

        $this->layout = 'application.views.layouts.main';
        $view = $this->ToView();
        $criteria = new CDbCriteria();
        $criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');

        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'units_in_lot' => array(
                            'asc' => 'units_in_lot',
                            'desc' => 'units_in_lot DESC',
                        ),
                        'buy_now_price' => array(
                            'asc' => 'buy_now_price',
                            'desc' => 'buy_now_price DESC',
                        ),
                        'end_date_unix' => array(
                            'asc' => 'end_date_unix',
                            'desc' => 'end_date_unix DESC',
                        ),
                    ),
                ),
            )
        );
        $model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'all'));

        $this->render(
            'view',
            array(
                'model' => $model,
                'view' => $view,
                'dataProvider' => $dataProvider,
            )
        );
    }

    public function actionFlash()
    {
        unsetSessionElems();
        $_SESSION['flashDeal'] = true;
        $this->layout = 'application.views.layouts.main';
        $view = $this->ToView();
        $criteria = new CDbCriteria();
        $criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');
        $criteria->addCondition('is_flashdeal = 1');
        $criteria->addCondition('start_date_unix <= '.time());

        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'units_in_lot' => array(
                            'asc' => 'units_in_lot',
                            'desc' => 'units_in_lot DESC',
                        ),
                        'buy_now_price' => array(
                            'asc' => 'buy_now_price',
                            'desc' => 'buy_now_price DESC',
                        ),
                        'end_date_unix' => array(
                            'asc' => 'end_date_unix',
                            'desc' => 'end_date_unix DESC',
                        ),
                    ),
                ),
            )
        );
        $model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'flash'));

        $this->render(
            'flash',
            array(
                'model' => $model,
                'view' => $view,
                'dataProvider' => $dataProvider,
                'itemView' => '_flash',
            )
        );
    }

    public function actionUpcomingDeals(){
        unsetSessionElems();
        $_SESSION['upcomingDeal'] = true;
        $this->layout = 'application.views.layouts.main';
        $view = $this->ToView();

        $criteria = new CDbCriteria();
        $criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');
        $criteria->addCondition('is_flashdeal = 1');
        $criteria->addCondition('start_date_unix >'.time());

        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'units_in_lot' => array(
                            'asc' => 'units_in_lot',
                            'desc' => 'units_in_lot DESC',
                        ),
                        'buy_now_price' => array(
                            'asc' => 'buy_now_price',
                            'desc' => 'buy_now_price DESC',
                        ),
                        'end_date_unix' => array(
                            'asc' => 'end_date_unix',
                            'desc' => 'end_date_unix DESC',
                        ),
                    ),
                ),
            )
        );

        $model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'upcoming_deals'));

        $this->render(
            'upcoming',
            array(
                'model' => $model,
                'view' => $view,
                'dataProvider' => $dataProvider,
            )
        );
    }

    public function actionOrangeBlossom()
    {
        unsetSessionElems();
        $_SESSION['condition'] = Condition::getIdByName('refurbished');
        $_SESSION['orangeBlossom'] = true;

        $this->layout = 'application.views.layouts.main';
        $view = $this->ToView();
        $criteria = new CDbCriteria();
        $criteria->condition = 't.condition = 3';
        $criteria->addCondition('active = 1');
        $criteria->addCondition('status = 1');
        $criteria->addCondition('is_flashdeal = 0');
        $criteria->addCondition('start_date_unix <= '.time());

        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
                'criteria' => $criteria,
                'sort' => array(
                    'attributes' => array(
                        'units_in_lot' => array(
                            'asc' => 'units_in_lot',
                            'desc' => 'units_in_lot DESC',
                        ),
                        'buy_now_price' => array(
                            'asc' => 'buy_now_price',
                            'desc' => 'buy_now_price DESC',
                        ),
                        'end_date_unix' => array(
                            'asc' => 'end_date_unix',
                            'desc' => 'end_date_unix DESC',
                        ),
                    ),
                ),
            )
        );
        $model = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => 'refurbished'));

        $this->render(
            'view',
            array(
                'model' => $model,
                'view' => $view,
                'dataProvider' => $dataProvider,
            )
        );
    }
}
