<?php

/**
 * Класс AttributeController
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 *
 */
class AttributeController extends Controller
{

	public $layout = '//layouts/admin';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	/**
	 * Добавить группу атрибутов
	 */
	public function actionCreate()
	{
		$model = new OrangelotsAttribute;

		$data = Yii::app()->getRequest()->getParam('OrangelotsAttribute', null);
		if (!empty($data)) {
			$model->attributes = $data;
			if ($model->save()) {
//				Yii::app()->logging->save(Log::ACTION_CREATE, sprintf('Добавлен атрибут: %s', $model->title));
//				$this->afterSaveRedirect($model);
				$this->redirect("index");
			}
		}

		$this->setPageTitle(Yii::app()->name . ' - Добавить атрибут');
		$this->render(
			'create',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Редактирование группы атрибутов
	 * @param integer $id
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$data = Yii::app()->getRequest()->getParam('OrangelotsAttribute', null);
		if (!empty($data)) {
			$model->attributes = $data;
			if ($model->save()) {
//				Yii::app()->logging->save(Log::ACTION_UPDATE, sprintf('Изменен атрибут: %s', $model->title));
//				$this->afterSaveRedirect($model);
				$this->redirect("../index");
			}
		}

		$this->setPageTitle(Yii::app()->name . ' - Редактировать атрибут');
		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Удалить категорию
	 * @param integer $id
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$model = $this->loadModel($id);
//			Yii::app()->logging->save(Log::ACTION_DELETE, sprintf('Удален атрибут: %s', $model->title));
			$model->delete();

			$ajax = Yii::app()->request->getParam('ajax', null);
			if (empty($ajax)) {
				$returnUrl = Yii::app()->request->getParam('returnUrl', null);
				$this->redirect(!empty($returnUrl) ? $returnUrl : array('index'));
			}
		} else {
			throw new CHttpException(400, INVALID_REQUEST);
		}
	}

	/**
	 * Показать все группы атрибутов
	 */
	public function actionIndex()
	{
		$model = new OrangelotsAttribute('search');
		$model->unsetAttributes();

		$data = Yii::app()->getRequest()->getParam('OrangelotsAttribute', null);
		if (!empty($data)) {
			$model->attributes = $data;
		}

		$this->setPageTitle(Yii::app()->name . ' - List Attributes');
		$this->render(
			'index',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Загружаем затребованную модель
	 * @param integer
	 */
	public function loadModel($id)
	{
		$model = OrangelotsAttribute::model()->findByPk($id);
		if (empty($model)) {
			throw new CHttpException(404, REQUESTED_PAGE_DOES_NOT_EXIST);
		}
		return $model;
	}

}
