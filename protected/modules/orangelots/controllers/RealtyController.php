<?php

/**
 * Класс OrangelotsController
 * @author fen0man
 * @copyright Aiken Interactive 2014
 *
 * Работа с учебными программами в панеле управления
 */
class RealtyController extends Controller
{
	/**
	 * Список фильтров
	 * @return array
	 */

	public $layout = '//layouts/main';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionCreate()
	{
		$model = new Orangelots;

		$data = Yii::app()->getRequest()->getParam('Orangelots', null);
		if (!empty($data)) {
			$model->attributes = $data;
			$model->file = CUploadedFile::getInstance($model, 'file');

			if ($model->save()) {
				$this->redirect('index');
			}
		}
		$this->registerScriptFiles();
		$this->setPageTitle(yii()->name . ' - Create Auction');
		$this->render(
			'create',
			array(
				'model' => $model,
			)
		);
	}


    public function actionCreateDeal()
    {
        $model = new Orangelots;

        $data = Yii::app()->getRequest()->getParam('Orangelots', null);
        if (!empty($data)) {
            $model->attributes = $data;
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($model->save()) {
                $this->redirect('index');
            }
        }
        $this->registerScriptFiles();
        $this->setPageTitle(yii()->name . ' - Добавить программу');
        $this->render(
            'create',
            array(
                'model' => $model,
            )
        );
    }

	/**
	 * Редактирование отзыв
	 * @param integer $id
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$model->scenario = 'updateAttributes';


		$data = Yii::app()->getRequest()->getParam('Orangelots', null);
		if (!empty($data)) {
			$model->attributes = $data;
			$model->file = CUploadedFile::getInstance($model, 'file');

			if ($model->save()) {
				$this->redirect('../index');
			}
		}

		$this->registerScriptFiles();
		$this->setPageTitle(yii()->name . ' - Редактировать программу');

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Удалить отзыв
	 * @param integer $id
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$model = $this->loadModel($id);
			$model->delete();

			$ajax = Yii::app()->request->getParam('ajax', null);
			if (empty($ajax)) {
				$returnUrl = Yii::app()->request->getParam('returnUrl', null);
				$this->redirect(!empty($returnUrl) ? $returnUrl : array('index'));
			}
		} else {
			throw new CHttpException(400, 'error');
		}
	}

	/**
	 * Показать все отзывы
	 */
	public function actionIndex()
	{
		$model = new Orangelots('search');
		$model->unsetAttributes();

		$data = Yii::app()->getRequest()->getParam('Orangelots', null);
		if (!empty($data)) {
			$model->attributes = $data;
		}
		$this->setPageTitle(Yii::app()->name . ' - Список учебных заведений');
		$this->render(
			'index',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Загружаем затребованную модель
	 * @param integer
	 */
	public function loadModel($id)
	{
		$model = Orangelots::model()->findByPk($id);
		if (empty($model)) {
			throw new CHttpException(404, 'error');
		}
		return $model;
	}

	/**
	 * Подключаем необходимые скрипты
	 */
	private function registerScriptFiles()
	{
//		registerScriptFile(baseUrl() . '/admin/js/browser.video.js');
//		registerScriptFile(baseUrl() . '/admin/js/multipic.js');
	}

	public function getTabularTabs($model)
	{
		$tabs = array();

		$tabs[] = array(
			'active' => true,
			'label' => 'Auctions',
			'content' => $this->renderPartial('parts/_form', array('model' => $model,), true),
		);

		$tabs[] = array(
			'active' => false,
			'label' => 'Photo Gallery',
			'content' => $this->renderPartial('parts/_photo', array('model' => $model,), true),
		);

		$tabs[] = array(
			'active' => false,
			'label' => 'Attributes',
			'content' => $this->renderPartial('parts/_attributes_form', array('model' => $model,), true),
		);

		return $tabs;
	}

	/**
	 * Список атрибутов категории для формы обьявления
	 */
	public function actionAttributes()
	{
		$category_id = Yii::app()->getRequest()->getParam('category_id');
		$this->renderPartial(
			'parts/_attributes',
			array(
				'dataAttributes' => array(),
				'category_id' => $category_id,
			)
		);
	}
}
