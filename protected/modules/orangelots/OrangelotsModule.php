<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 24.05.14
 * Time: 14:05
 */

class OrangelotsModule extends CWebModule
{
	public $defaultController = "Default";

	public function init()
	{
		$this->setImport(array(
			'orangelots.models.*',
			'orangelots.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}