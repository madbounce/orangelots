<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href=""></a></li>
            <li><a href="">Payments Received</a></li>
        </ul>
    </div>
</div>

<div class="received__container">
    <div class="main_container">
        <p class="posted_by__title">Search by</p>
        <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'model-form',
                'enableAjaxValidation' => false,
            )); ?>
        <div class="received__row__form_container">
            <div class="received__row">
                <div class="received__row__block received__row__block__id">
                    <label>Transaction ID</label>
                    <?php echo $form->textField($searchModel, 'transaction_id', array('type' => 'text','maxlength' => 50)); ?>
                    <?php echo $form->error($searchModel, 'transaction_id'); ?>
                </div>
                <div class="received__row__block received__row__block__id">
                    <label>PayPal ID</label>
                    <?php echo $form->textField($searchModel, 'paypal_id', array('type' => 'text','maxlength' => 50)); ?>
                    <?php echo $form->error($searchModel, 'paypal_id'); ?>
                </div>
                <div class="received__row__block received__row__block__email">
                    <label>From, email, phone</label>
                    <?php echo $form->textField($searchModel, 'to_who', array('type' => 'text','maxlength' => 50)); ?>
                    <?php echo $form->error($searchModel, 'to_who'); ?>
                </div>
            </div>
            <div class="received__row">
                <div class="received__row__block received__row__block__deal_id">
                    <label>Auction ID /  Deal ID</label>
                    <?php echo $form->textField($searchModel, 'auction_deal_id', array('type' => 'text','maxlength' => 50)); ?>
                    <?php echo $form->error($searchModel, 'auction_deal_id'); ?>
                </div>

                <div class="received__row__block__calendar_block">
                    <p>Payment Date</p>
                    <label>from</label>
                    <?php
                    $this->widget('application.components.WasDatepicker.WasDatepicker',array(
                            'name' => 'dateFrom',
                            'options'=>array(
                                'language'=>'en',
                                'format'=>'m/d/Y',
                                'autoclose'=>'true',
                                'endDate'=>date('m/d/Y', time()),
                                'weekStart'=>1,
                                'startView'=>2,
                                'keyboardNavigation'=>true
                            ),
                        ));
                    ?>
                    <label>to</label>
                    <?php
                    $this->widget('application.components.WasDatepicker.WasDatepicker',array(
                            'name' => 'dateTo',
                            'options'=>array(
                                'language'=>'en',
                                'format'=>'dd.mm.yyyy',
                                'autoclose'=>'true',
                                'endDate'=>date('m/d/Y', time()),
                                'weekStart'=>1,
                                'startView'=>2,
                                'keyboardNavigation'=>true
                            ),
                        ));
                    ?>
                    <?php echo $form->hiddenField($searchModel, 'payed', array('value' => OrangelotsInvoices::WITHDRAW)); ?>
                </div>

                <div class="received__row__block">
                    <button class="btn_image">
                        <img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt="">
                    </button>
                </div>

            </div>
        </div>
        <?php $this->endWidget(); ?>

        <div class="posted_by__table_auction_list__container text-center">
            <table>
                <thead>
                <tr>
                    <td width="70">Trans.ID</td>
                    <td width="100">From</td>
                    <td>Email</td>
                    <td width="115">Phone</td>
                    <td width="85">Amount</td>
                    <td width="55">Fee</td>
                    <td width="55">Net payment</td>
                    <td width="90">AuctionID/<br/>DealID</td>
                    <td width="55">Status</td>
                    <td width="70">Invoice</td>
                </tr>
                </thead>
                <tbody>

                <?php
                $this->widget('bootstrap.widgets.TbListView', array(
                        'id' => 'withdrawal-grid',
                        'dataProvider' => $payments,
                        'ajaxUpdate' => true,
                        'itemView' => 'common/_payments',
                        'emptyText' => '<tr><td colspan="14" class="empty"><span class="empty">No results found.</span></td></tr>',
                        'summaryText' => false,
                        'template' => '{items}{pager}',
                    ));?>
                </tbody>
            </table>
        </div>
    </div>
</div>