<div class="popup__container" id="paypal-popup-form">
    <div class="popup__bg"></div>
    <div class="popup_inner">
        <div class="popup__content" id="paypal_popup_content">
            <a href="" class="popup_close">X</a>

            <div class="popup_body clearfix">
                <?$info = OrangelotsUsers::getUserInfo(user()->id)?>
                <div class="popup__title_complite">
                    <p class="popup__title_complite__name"><?php echo ucfirst($info->firstname)?>, please check out your card information</p>

<!--                <div class="popup__right_info">-->
                    <?if(!empty($cardInfo)):?>
                        <div class="card_info_wrapper">
                            <p class="card_info_fl"><span>Owner name: </span><?php echo ucfirst($cardInfo->firstname).' '.ucfirst($cardInfo->lastname)?></p>
                            <p class="card_info_fl"><span>Number: </span> <?php echo $cardInfo->viewNumber()?></p>
                            <p class="card_info_fl"><span>Expiration date: </span><?php echo $cardInfo->expire_month.'/'.$cardInfo->expire_year?></p>
                        </div>
                    <?endif;?>
                        <?php echo CHtml::ajaxLink(
                        "Pay with PayPal",
                        yii()->createUrl('pay/index/pay'),
                        array(
                            'type' => 'POST',
                            'data' => array(
                                'invoiceId' => $invoiceId,
                            ),
                            'beforeSend' => 'function(){
                                var currentSum = '.$currentSum.'
                            if(currentSum>MaxSum){
                                alertAppendTo("paypal_popup_content","error", "Payment amount exceeds allowed to pay via paypal. Please, print out the invoice and pay through the bank.");
                                return false;
                            }else{
                                $(".main_content").append(preload);
                            }
                        }',
                        'success' => 'function(data){
                            $(".preload_mask").remove();
                            $(".preloader").remove();
                            var resp = $.parseJSON(data);
                            if ("error" in resp) {
                                alertAppendTo("paypal_popup_content","error", resp.error);
                            }else{
                                $("#paypal_pay_now").hide();
                                alertAppendTo("paypal_popup_content","success", "Payment is successful");
                            }
                        }',
                        ),
                        array('class' => 'btn green_bnt', 'id' => 'paypal_pay_now')
                    )
                    ?>
<!--                </div>-->
                </div>
            </div>
        </div>
    </div>
</div>