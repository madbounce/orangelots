<?

class WithdrawalController extends Controller
{
    public $layout = '//layouts/main';

    public function filters()
    {
        return array('rights');
    }

    public function actionIndex(){
        $withdrawals = OrangelotsInvoices::model()->getWithdrawals(user()->id);
        $searchModel = new OrangelotsInvoices();
        if(isset($_POST['OrangelotsInvoices']) || (!empty($_POST['dateFrom']) || !empty($_POST['dateTo']))){
            $withdrawals = OrangelotsInvoices::model()->userFilter($_POST['OrangelotsInvoices'], $_POST['dateFrom'], $_POST['dateTo'], true);
        }
        $this->render(
            'withdrawal',
            array(
                'payments' => $withdrawals,
                'searchModel' => $searchModel
            )
        );
    }
}