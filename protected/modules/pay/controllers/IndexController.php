<?

class IndexController extends Controller
{
    public $layout = '//layouts/main';

    public function filters()
    {
        return array('rights');
    }

    public function actionPay(){
        if(yii()->request->isAjaxRequest){
            if(isset($_POST['invoiceId'])){
                $pay = array();
                $invoice = $this->loadModel($_POST['invoiceId']);
                if(!empty($invoice)){
                    $pay = CardInfo::pay($invoice);
                    if($pay['payed']){
                        $invoice->payed();
                        $invoice->payed = OrangelotsInvoices::PAYED;
                        $invoice->paypal_id = $pay['payPalId'];
                        $invoice->pay_type = OrangelotsInvoices::PAY_WITH_PAYPAL;
                        $invoice->transaction_date = time();
                        $invoice->fee = OrangelotsInvoices::getFee($invoice->amount, OrangelotsInvoices::PAY_WITH_PAYPAL);
                    }
                    $invoice->save(false);
                }

                echo CJSON::encode($pay);
            }
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

    public function actionPopUp(){
        if(yii()->request->isAjaxRequest){
            if(isset($_POST['invoiceId'])){
                $invoice = $this->loadModel($_POST['invoiceId']);
                $this->renderPartial('popup', array('cardInfo' => CardInfo::getUserCardInfo(user()->id), 'invoiceId' => $_POST['invoiceId'], 'currentSum' => $invoice->amount), false, true);
            }
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

    public function actionPayWithBank($id){
        $invoice = $this->loadModel($id);
        $invoice->pay_type = OrangelotsInvoices::PAY_WITH_BANK;
        $invoice->fee = OrangelotsInvoices::getFee($invoice->amount,OrangelotsInvoices::PAY_WITH_BANK);
        $invoice->save();
        $this->redirect(createUrl('user/buy/confirmation', array('transaction' => $invoice->transaction_id)));
    }

    public  function loadModel($id){
        $model = OrangelotsInvoices::model()->findByPk($id);
        if(empty($model)){
            throw new CHttpException('400', 'Bad request');
        }else{
            return $model;
        }
    }

}