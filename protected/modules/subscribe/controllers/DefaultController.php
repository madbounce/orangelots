<?php
yii::import('subscribe.models.*');

class DefaultController extends Controller
{

    public $layout = '//layouts/main';

    public function filters()
    {
        return array('rights');
    }



    public function actionIndex()
    {
        $this->layout = 'application.views.layouts.admin';

//        $model = new OrangelotsUsers('search');
        $model = new OrangelotsSubscribe('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['OrangelotsUsers'])) {
            $model->attributes = $_GET['OrangelotsUsers'];
            $model->role = $_GET['OrangelotsUsers']['role'];
        }
        $this->render('index', array(
                'model' => $model,
            ));
    }


    public function loadModel($id)
    {
        $model = Realty::model()->findByPk($id);
        if (empty($model)) {
            throw new CHttpException(404, REQUESTED_PAGE_DOES_NOT_EXIST);
        }
        return $model;
    }

    public function actionExport(){
        $criteria = new CDbCriteria;
//        $criteria->addCondition('subscribe = 1 AND id <> 1');
        $criteria->addCondition('');
        CsvExport::export(
//            OrangelotsUsers::model()->findAll($criteria),
            OrangelotsSubscribe::model()->findAll($criteria),
            array('email'=>array('text')),
            true, // boolPrintRows
            'registers-upto--'.date('m/d/Y H-i').".csv"
        );
    }


}
