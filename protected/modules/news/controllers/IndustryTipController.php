<?

class IndustryTipController extends Controller
{
	public $layout = '//layouts/main';

	public function behaviors()
	{
		return array(
			'seo' => array('class' => 'ext.seo.components.SeoControllerBehavior'),
		);
	}


	public function actionView($type, $alias = null)
	{
        $categories = IndustryTipCategory::getCategoriesByType($type);
        if(empty($alias)){
            if(!empty($categories[0])){
                $alias = $categories[0]->alias;
            }
            $catId = $categories[0]->id;
        }else{
            $category = IndustryTipCategory::getCategoryByAlias($alias);
            $catId = $category->id;
        }
		$news = News::getNewsByCategory($catId);
		$this->render('index', array('categories' => $categories, 'alias' => $alias,'news' => $news,'type'=>$type));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModelByAliasAndType($alias, $type)
	{
		$model = IndustryTipCategory::model()->findByAttributes(array('alias' => $alias));
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

    public function actionTips($type, $id, $see_type, $see_alias = null){
        $news = News::model()->findByPk($id);
        $categories = IndustryTipCategory::getCategoriesByType($type);
        $alias = $news->category->alias;
        if (isset($news)){
            $this->render('tips', array('info' => $news, 'id' => $news->tip_category_id, 'type' => $type, 'alias' => $alias, 'categories' => $categories, 'see_type' => $see_type, 'see_alias' => $see_alias));
        }
    }

}