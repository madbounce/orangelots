<?php

class DefaultController extends Controller
{
	public $layout = '//layouts/admin';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	public function actionView($id)
	{
		$this->render(
			'modules.news.views.default.view',
			array(
				'model' => $this->loadModel($id),
			)
		);
	}

	public function actionCreate()
	{
		$model = new News;

		$this->performAjaxValidation($model);

		if (isset($_POST['News'])) {
			$model->attributes = $_POST['News'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render(
			'modules.news.views.default.create',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['News'])) {
			$model->attributes = $_POST['News'];
			if ($model->save()) {
				$this->redirect(array('index'));
			}
		}

		$this->render(
			'modules.news.views.default.update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new News('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['News'])) {
			$model->attributes = $_GET['News'];
		}

		$this->render(
			'modules.news.views.default.admin',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = News::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'news-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionCreateCategory()
	{
		$model = new IndustryTipCategory();
		if (isset($_POST['IndustryTipCategory'])) {
			$model->attributes = $_POST['IndustryTipCategory'];
            $model->types = serialize($_POST['IndustryTipCategory']['types']);
			if ($model->validate()) {
				$model->save();
				$this->redirect(createUrl('news/default/viewCategory', array('id' => $model->id)));
			}
		}
		$this->render('createCategory', array('model' => $model));
	}

	public function actionViewCategory($id)
	{
		$model = IndustryTipCategory::model()->findByPk($id);
		$this->render(
			'modules.news.views.default.viewCategory',
			array(
				'model' => $model,
			)
		);
	}

	public function actionCategoriesList()
	{
		$model = new IndustryTipCategory('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['IndustryTipCategory'])) {
			$model->attributes = $_GET['IndustryTipCategory'];
		}

		$this->render(
			'modules.news.views.default.adminCategory',
			array(
				'model' => $model,
			)
		);
	}

	public function actionDeleteCategory($id)
	{
		$model = IndustryTipCategory::model()->findByPk($id);
		if (!empty($model)) {
			$model->delete();
		}
	}

	public function actionUpdateCategory($id)
	{
		$model = IndustryTipCategory::model()->findByPk($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (isset($_POST['IndustryTipCategory'])) {
			$model->attributes = $_POST['IndustryTipCategory'];
            $model->types = serialize($_POST['types']);
            if ($model->save()) {
				$this->redirect(createUrl('news/default/categories-list'));
			}
		}
		$this->render(
			'modules.news.views.default.updateCategory',
			array(
				'model' => $model,
			)
		);
	}
}
