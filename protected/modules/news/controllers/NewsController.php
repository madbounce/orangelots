<?

class NewsController extends Controller
{
	public $layout = '//layouts/main';

	public function behaviors()
	{
		return array(
			'seo' => array('class' => 'ext.seo.components.SeoControllerBehavior'),
		);
	}

	public function actionIndex()
	{
		$model = News::model()->findAll();
		$this->render(
			'modules.news.views.news.index',
			array(
				'model' => $model
			)
		);
	}

	public function actionView($alias)
	{
		$model = $this->loadModel($alias);

		$this->render(
			'modules.news.views.news.view',
			array(
				'model' => $model,
			)
		);
	}

	public function loadModel($alias)
	{
		return News::model()->findByAttributes(array('alias' => $alias));
	}

}