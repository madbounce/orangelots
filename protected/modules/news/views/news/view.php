<?php
/**
 * Created by PhpStorm.
 * User: vintage-fen0man
 * Date: 18.06.14
 * Time: 22:13
 * @var $model News
 */
$this->metaKeywords = $model->seo_keywords;
$this->metaDescription = $model->seo_description;
?>
<h1><?= $model->title ?></h1>
<h3><?= $model->description ?></h3>
<h3><?= $model->content ?></h3>