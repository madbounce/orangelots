<?php
/**
 * Created by PhpStorm.
 * User: vintage-fen0man
 * Date: 18.06.14
 * Time: 22:13
 *
 * @var $model News
 */
$this->metaKeywords = yii()->params['news_description'];
$this->metaDescription = yii()->params['news_keywords'];
?>
	<h1>News</h1>
<? if (!empty($model)) : ?>
	<? foreach ($model as $key => $val): ?>
		<h3><?= $val->title ?></h3>
		<?= $val->description ?>
		<br>
		<a href="<?= $val->url ?>">See more</a>
		<hr>
	<? endforeach ?>
<? endif ?>