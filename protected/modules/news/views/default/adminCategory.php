<h1>Manage Categories</h1>

<?php $this->widget(
	'zii.widgets.grid.CGridView',
	array(
		'id' => 'news-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => array(
			'id',
			'name',
			'alias',
            array(
                'name' => 'types',
                'filter' => false,
                'value' => 'IndustryTipCategory::getTypesLabel($data->types)'
            ),
			array(
				'class' => 'bootstrap.widgets.TbButtonColumn',
				'template' => '{update}{delete}',
				'buttons' => array(
					/*'view' => array(
						'url' => 'createUrl("/news/default/viewCategory",array("id"=>$data->id))',
					),*/
					'update' => array(
						'url' => 'createUrl("/news/default/updateCategory",array("id"=>$data->id))',
					),
					'delete' => array(
						'url' => 'createUrl("/news/default/deleteCategory",array("id"=>$data->id))',
					),
				),
			),
		),
	)
); ?>
