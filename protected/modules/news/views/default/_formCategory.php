<?/***
 *
 * @var $form TbActiveForm
 * @var $model News
 */
?>
<?php $form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    array(
        'id' => 'news-form',
//		'type' => 'horizontal',
        'enableAjaxValidation' => false,
    )
); ?>

<!--<p class="help-block">Fields with <span class="required">*</span> are required.</p>-->

<?php echo $form->errorSummary($model); ?>

<div class="row">

    <?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>
</div>

<div class="row">
    <?php echo $form->textFieldRow($model, 'alias', array('class' => 'span5', 'maxlength' => 50)); ?>
</div>

<div class="row type_inline">
    <?php echo CHtml::checkBoxList('types', $model->checked(), IndustryTipCategory::$types, array('class' => 'hor'))?>
    <?php echo $form->error($model, 'types')?>
</div>

<div class="form-actions">
    <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'type' => 'primary',
            'label' => $model->isNewRecord ? 'Create' : 'Save',
            'htmlOptions' => array(
                'class' => 'btn_orange'
            )
        )
    ); ?>
</div>

<?php $this->endWidget(); ?>
