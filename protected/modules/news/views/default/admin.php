<h1>Manage tips</h1>

<?php $this->widget(
	'zii.widgets.grid.CGridView',
	array(
		'id' => 'news-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => array(
			'id',
			'title',
//			array(
//				'name' => 'is_active',
//				'value' => 'empty($data->is_active) ? "" : "Yes"',
//			),
            array(
				'name' => 'tip_category_id',
				'value' => '$data->category->name',
			),
			array(
				'class' => 'bootstrap.widgets.TbButtonColumn',
                'template' => '{update}{delete}'
			),
		),
	)
); ?>
