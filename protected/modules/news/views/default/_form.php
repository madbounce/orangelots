<?/***
 *
 * @var $form TbActiveForm
 * @var $model News
 */
?>
<?php $form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm',
	array(
		'id' => 'news-form',
		'type' => 'horizontal',
		'enableAjaxValidation' => false,
	)
); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->checkBoxRow($model, 'is_active', array('class' => 'check')); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->dropDownListRow($model, 'tip_category_id', CHtml::listData(IndustryTipCategory::model()->findAll(), 'id', 'name'), array('class' => 'span5', 'prompt' => 'choose category')); ?>

<?php echo $form->textFieldRow($model, 'alias', array('class' => 'span5', 'maxlength' => 255)); ?>


<?php //echo $form->textFieldRow($model, 'description', array('class' => 'span5', 'maxlength' => 1000)); ?>
<div class="control-group">
    <?php echo $form->label($model, 'description', array('class' => 'control-label')); ?>
    <div class="controls">
        <?$this->widget(
            'ext.editor.editMe.widgets.ExtEditMe',
            array(
                'model' => $model,
                'width' => 800,
                'height' => 400,
                'attribute' => 'description',
                'autoLanguage' => false,
                'filebrowserImageUploadUrl' => yii()->baseUrl . '/img-upload/',
                'toolbar' => array(
                    array('Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'),
                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',),
                    array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'),
                    array(
                        'Form',
                        'Checkbox',
                        'Radio',
                        'TextField',
                        'Textarea',
                        'Select',
                        'Button',
                        'ImageButton',
                        'HiddenField'
                    ),
                    '/',
                    array(
                        'Bold',
                        'Italic',
                        'Underline',
                        'Strike',
                        'Subscript',
                        'Superscript',
                        '-',
                        'RemoveFormat',
                    ),
                    array(
                        'NumberedList',
                        'BulletedList',
                        '-',
                        'Outdent',
                        'Indent',
                        '-',
                        'Blockquote',
                        'CreateDiv',
                        '-',
                        'JustifyLeft',
                        'JustifyCenter',
                        'JustifyRight',
                        'JustifyBlock',
                        '-',
                        'BidiLtr',
                        'BidiRtl',
                    ),
                    array('Link', 'Unlink', 'Anchor',),
                    array(
                        'Image',
                        'Flash',
                        'Table',
                        'HorizontalRule',
                        'Smiley',
                        'SpecialChar',
                        'PageBreak',
                        'Iframe'
                    ),
                    '/',
                    array('Styles', 'Format', 'Font', 'FontSize',),
                    array('TextColor', 'BGColor',),
                    array('Maximize', 'ShowBlocks',),
                )
            )
        )?>
    </div>
</div>

<div class="control-group">
    <?php echo $form->label($model, 'content', array('class' => 'control-label')); ?>
    <div class="controls">
        <?$this->widget(
            'ext.editor.editMe.widgets.ExtEditMe',
            array(
                'model' => $model,
                'width' => 800,
                'height' => 400,
                'attribute' => 'content',
                'autoLanguage' => false,
                'filebrowserImageUploadUrl' => baseUrl() . '/img-upload/',
                'toolbar' => array(
                    array('Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'),
                    array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',),
                    array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'),
                    array(
                        'Form',
                        'Checkbox',
                        'Radio',
                        'TextField',
                        'Textarea',
                        'Select',
                        'Button',
                        'ImageButton',
                        'HiddenField'
                    ),
                    '/',
                    array(
                        'Bold',
                        'Italic',
                        'Underline',
                        'Strike',
                        'Subscript',
                        'Superscript',
                        '-',
                        'RemoveFormat',
                    ),
                    array(
                        'NumberedList',
                        'BulletedList',
                        '-',
                        'Outdent',
                        'Indent',
                        '-',
                        'Blockquote',
                        'CreateDiv',
                        '-',
                        'JustifyLeft',
                        'JustifyCenter',
                        'JustifyRight',
                        'JustifyBlock',
                        '-',
                        'BidiLtr',
                        'BidiRtl',
                    ),
                    array('Link', 'Unlink', 'Anchor',),
                    array(
                        'Image',
                        'Flash',
                        'Table',
                        'HorizontalRule',
                        'Smiley',
                        'SpecialChar',
                        'PageBreak',
                        'Iframe'
                    ),
                    '/',
                    array('Styles', 'Format', 'Font', 'FontSize',),
                    array('TextColor', 'BGColor',),
                    array('Maximize', 'ShowBlocks',),
                )
            )
        )?>
    </div>
</div>
<?php echo $form->textFieldRow($model, 'seo_keywords', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'seo_description', array('class' => 'span5', 'maxlength' => 1000)); ?>

<?php //echo $form->textFieldRow($model, 'timeCreate', array('class' => 'span5')); ?>

<div class="form-actions">
	<?php $this->widget(
		'bootstrap.widgets.TbButton',
		array(
			'buttonType' => 'submit',
			'type' => 'primary',
			'label' => $model->isNewRecord ? 'Create' : 'Save',
            'htmlOptions' => array(
                'class' => 'btn_orange'
            )
		)
	); ?>
</div>

<?php $this->endWidget(); ?>
