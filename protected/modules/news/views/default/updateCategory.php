<?php
$this->breadcrumbs = array(
	'Category' => array('index'),
	$model->name => array('view', 'id' => $model->id),
	'Update',
);
?>

	<h1>Update Category <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_formCategory', array('model' => $model)); ?>