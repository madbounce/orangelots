<div class="breadcrumb_row clearfix">
	<div class="main_container">
		<ul>
			<li><a class="breadcrumb_row__main_link" href=""></a></li>
			<li><a href="">New furniture</a></li>
		</ul>
	</div>
</div>

<div class="industry_tops__container clearfix">
	<div class="main_container">
		<div class="industry_tops__left_side pull-left">
			<div class="industry_tops__deliver_container">
				<? $this->widget('ext.TipsCatMenu.TipsCatMenuWidget',  array('categories'=>$categories,'type'=> $type,'tips' => $info->tip_category_id)) ?>
			</div>
		</div>
		<div class="industry_tops__main_collum pull-left">
			<ul>
						<li>
							<p class="industry_tops__main_collum__title"><?php echo $info->title ?></p>
                            <p class="industry_tops__main_collum__text">
                                <?php echo $info->description ?>
                            </p>
							<p class="industry_tops__main_collum__text">
								<?php echo $info->content ?>
							</p>
						</li>
			</ul>

		</div>
        <p class="text-right"> <a href="<?php echo createUrl('news/industry-tip/view', array('type'=>$see_type,'alias' => $see_alias))?>" class="orange_link">see more</a></p>

	</div>
</div>