<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href=""></a></li>
            <li><a href="<?php echo createUrl('news/industry-tip/view', array('type' => $type))?>"><?php echo IndustryTipCategory::$labelsFor[$type]?></a></li>
            <?if(!empty($alias)):?>
                <li><a href=""><?php echo ucfirst(IndustryTipCategory::getCategoryNameByAlias($alias))?></a></li>
            <?endif;?>
        </ul>
    </div>
</div>

<div class="industry_tops__container clearfix">
    <div class="main_container">
        <div class="industry_tops__left_side pull-left">
            <div class="industry_tops__deliver_container">
                <? $this->widget('ext.TipsCatMenu.TipsCatMenuWidget', array('categories'=>$categories,'type'=> $type)) ?>
            </div>
        </div>
        <div class="industry_tops__main_collum pull-left">
            <ul>
                <? if (!empty($news)): ?>
                    <? foreach ($news as $info): ?>
                        <li>
                            <p class="industry_tops__main_collum__title"><?php echo $info->title ?></p>

                            <p class="industry_tops__main_collum__text">
                                <?if($type != IndustryTipCategory::buyer_label and $type !=IndustryTipCategory::vendor_label):?>
                                    <?php echo $info->description ?>
                                <?php else: ?>
                                    <?php echo $info->content ?>
                                <?php endif;?>
                            </p>
                            <?if($type != IndustryTipCategory::buyer_label and $type !=IndustryTipCategory::vendor_label):?>
							<p class="text-right">
								<a href="<?php echo createUrl('news/industry-tip/tips',array('id' =>$info->id, 'type' => $type, 'see_type' =>$_GET['type'], 'see_alias' =>$_GET['alias']))?>" class="orange_link">
                                    Read full story
                                </a>
							</p>
                            <?endif;?>
                        </li>
                    <? endforeach; ?>
                <? endif; ?>
            </ul>

        </div>
    </div>
</div>