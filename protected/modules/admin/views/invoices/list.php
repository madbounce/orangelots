<h1>Manage invoices</h1>
<?php $this->widget(
    'zii.widgets.grid.CGridView',
    array(
        'id' => 'invoices-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            'transaction_id',
            'auction_deal_id',
            array(
                'header' => 'purchase date',
                'name' => 'purchase_date',
                'value' => 'date("m/d/Y H:m", $data->purchase_date)',
                'filter' => false
            ),
            'from_who',
            'to_who',
            array(
                'header' => 'vendor email',
                'name' => 'email'
            ),
            array(
                'header' => 'vendor phone',
                'name' => 'phone'
            ),
            array(
                'header' => 'buyer email',
                'name' => 'email_to'
            ),
            array(
                'header' => 'buyer phone',
                'name' => 'phone_to'
            ),
            array(
                'name' => 'amount',
                'value' => '"$".number_format($data->amount, 2)'
            ),
            array(
                'name' => 'fee',
                'value' => '"$".number_format($data->fee, 2)'
            ),
            array(
                'header'=>'Net payment',
                'value' => '"$".number_format($data->amount-$data->fee, 2)'
            ),
            array(
                'header'=>'Payment method',
                'name' => 'pay_type',
                'value' => 'OrangelotsInvoices::$payTypeText[$data->pay_type]',
                'filter' => OrangelotsInvoices::$payTypeText
            ),
            array(
                'header' => 'Status',
                'name' => 'payed',
                'value' => 'OrangelotsInvoices::$statusesText[$data->payed]',
                'filter' => OrangelotsInvoices::$statusesText,
            ),
            'paypal_id',
            array(
                'header' => 'Transaction date',
                'name' => 'transaction_date',
                'value' => '!empty($data->transaction_date) ? date("m/d/Y H:m", $data->transaction_date) : ""',
                'filter' => false
            ),
            array(
                'header' => 'Withdraw date',
                'name' => 'withdraw_date',
                'value' => '!empty($data->withdraw_date) ? date("m/d/Y H:m", $data->withdraw_date) : ""',
                'filter' => false
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template'=>'{update}',
                'buttons' => array(
                    'update' => array(
                        'options' => array(
                            'title' => 'Update',
                        ),
                    ),
                ),
            ),
        )
)
); ?>