<?php
/* @var $this OrangelotsInvoicesController */
/* @var $model OrangelotsInvoices */
/* @var $form CActiveForm */
CHtml::$afterRequiredLabel = '<span class="important_star"> *</span> ';
?>

<div class="form main_container">

    <p>
        Current status: <?php echo OrangelotsInvoices::$statusesText[$model->payed]?>
    </p>

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'orangelots-invoices-update-status-form',
            'enableAjaxValidation' => false,
        )
    ); ?>
    <div class="uni_form_block">
        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'transaction_id'); ?>
            <?php echo $model->transaction_id?>
        </p>

        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'amount'); ?>
            <?php echo number_format($model->amount,2)?>
        </p>

        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'fee'); ?>
            <?php echo "$". number_format($model->fee,2)?>
        </p>

        <p class="uni_form_row ">
            <label>Net payment</label>
            <?php echo number_format($model->amount-$model->fee,2)?>
        </p>

        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'transaction_date'); ?>
            <?
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'transaction_date',
                    'options'=>array(
                        'showAnim'=>'fade',
                        'minDate'=>'new Date(parseInt('.$model->purchase_date.')*1000)'
                    ),
                    'htmlOptions' =>
                        array('value' => !empty($model->transaction_date) ? date('m/d/Y h:ma',$model->transaction_date) : '')
                )
            );
            ?>
        </p>

        <?if($model->payed >0):?>
            <p class="uni_form_row ">
                <?php echo $form->labelEx($model, 'withdraw_date'); ?>
                <?
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,
                        'attribute'=>'withdraw_date',
                        'options'=>array(
                            'showAnim'=>'fade',
                            'minDate'=>'new Date(parseInt('.$model->purchase_date.')*1000)'
                        ),
                        'htmlOptions' =>
                            array('value' => !empty($model->withdraw_date) ? date('m/d/Y h:ma',$model->withdraw_date) : '')
                    )
                );
                ?>
            </p>
        <?endif;?>

        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'payed'); ?>
            <?php echo $form->dropDownList($model, 'payed', OrangelotsInvoices::$adminStatuses, array('empty' => 'Choose status'))?>
        </p>

        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'admin_comment'); ?>
            <?php echo $form->textArea($model, 'admin_comment', array('cols' => 60, 'rows' => 10, 'value' => '', 'style' => 'min-width:296px;min-height:150px;max-height:300px; max-width:296px;')); ?>
        </p>

        <?if(!empty($model->admin_comment)):?>
        <p class="uni_form_row ">
            <?echo $model->admin_comment;?>
        </p>
        <?endif;?>

    </div>
    <button class="btn btn_orange">Set status</button>
    <?php $this->endWidget(); ?>
</div><!-- form -->
