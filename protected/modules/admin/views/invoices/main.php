<?

/*path*/
Yii::setPathOfAlias('helpers', realpath(dirname(__FILE__) . '/../components/helpers'));
Yii::setPathOfAlias('bootstrap', realpath(dirname(__FILE__) . '/../extensions/bootstrap'));
Yii::setPathOfAlias('modules', realpath(dirname(__FILE__) . '/../modules'));
Yii::setPathOfAlias('models', realpath(dirname(__FILE__) . '/../models'));
//Yii::setPathOfAlias('PayPal',Yii::getPathOfAlias('application.components.paypal.vendor.paypal.rest-api-sdk-php.lib.PayPal'));
Yii::setPathOfAlias('PayPal',Yii::getPathOfAlias('application.components.paypal.rest-api-sdk-php.lib.PayPal'));
Yii::setPathOfAlias('bootstr',Yii::getPathOfAlias('application.components.paypal'));


/*widgets*/
Yii::setPathOfAlias('CategoryWidget', dirname(__FILE__) . '/../extensions/category.CategoryWidget');
Yii::setPathOfAlias('RealtyWidget', dirname(__FILE__) . '/../extensions/realty.RealtyWidget');
Yii::setPathOfAlias('HeadMenuWidget', dirname(__FILE__) . '/../extensions/headMenu.HeadMenuWidget');

$arConfig = array(
	'theme' => 'bootstrap',
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'Orangelots',
	'preload' => array('log', 'bootstrap'),
	'language' => 'en',
	'sourceLanguage' => 'ru',
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.modules.user.*',
		'application.modules.pay.models.*',
		'application.modules.user.models.*',
		'application.modules.user.components.*',
		'application.modules.admin.modules.rights.*',
		'application.modules.admin.modules.rights.models.*',
		'application.modules.admin.modules.rights.components.*',
		'application.modules.cms.CmsModule',
		'application.modules.cms.models.*',
        'ext.adminMenu.*',
		'ext.managerMenu.*',
		'ext.realtorMenu.*',
		'ext.SocialShareWidget.*',
		'ext.login.*',
		'ext.headMenu.*',
		'ext.footerMenu.*',
		'ext.TextHelper',
		'ext.realty.RealtyWidget',
		'ext.category.CategoryWidget',
		'ext.CJuiDateTimePicker.CJuiDateTimePicker',
	),
	'modules' => array(
		'admin' => array(
			'defaultController' => 'admin',
			'components' => array(
				'econfig' => array(
					'class' => 'EConfig',
					'configTableName' => '{{config}}',
					'strictMode' => false,
				),
			),
			'modules' => array(
				'rights' => array(
					'debug' => YII_DEBUG,
				),
			),
		),
		'user' => array(
			'hash' => 'md5',
			'sendActivationMail' => true,
			'loginNotActiv' => false,
			'activeAfterRegister' => true,
			'autoLogin' => false,
			'registrationUrl' => array('//user/orangelots-users/create'),
			'recoveryUrl' => array('//user/recovery/set-new-password'),
			'loginUrl' => array('//user/login'),
			'logoutUrl' => array('//user/logout'),
			'returnUrl' => array('//user'),
			'returnLogoutUrl' => array('//site/index'),
			'loginRequiredAjaxResponse' => array('/site/login'),
			'profileUrl' => array('/'),
			'managerReturnUrl' => array('//admin'),
		),
		'orangelots' => array(),
		'account' => array(),
		'auctions' => array(),
		'attributes' => array(),
		'cms' => array(),
		'flashdeal' => array(),
		'news' => array(),
        'pay' => array(),
        'subscribe' => array(),
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'enabled' => YII_DEBUG,
			'password' => '1234',
			'ipFilters' => false,
			'generatorPaths' => array(
				'bootstrap.gii',
			),
		),
	),
	'components' => array(
		'image' => array(
			'class' => 'application.extensions.image.CImageComponent',
			// GD or ImageMagick
			'driver' => 'GD',
		),
		'file' => array(
			'class' => 'application.extensions.file.CFile',
		),
		'request' => array(
			'enableCookieValidation' => true,
			'enableCsrfValidation' => false,
			'class' => 'HttpRequest',
			'noCsrfValidationRoutes' => array(),
		),
		'clientScript' => array(
			'scriptMap' => array(
				'jquery.js' => false,
			),
			'enableJavaScript' => true,
		),
		'cache' => array(
			'class' => 'CDummyCache',
			'enabled' => !YII_DEBUG,
		),
		/*'cache' => array(
			'class' => 'application.components.MemCacheI',
			'servers' => array(
				array(
					'host' => '127.0.0.1',
					'port' => 11211,
					'weight' => 60,
				),
			),
		),*/
		'user' => array(
			'class' => 'CRWebUser',
			'allowAutoLogin' => true,
			'autoUpdateFlash' => false,
		),
		'authManager' => array(
			'class' => 'RDbAuthManager',
			'defaultRoles' => array('Guest')
		),
		'urlManager' => array(
			'class' => 'UrlManager',
			'urlFormat' => 'path',
			'showScriptName' => false,
			'rules' => array(

				'' => 'site/index',

				'page/<name>-<id:\d+>.html' => 'cms/node/page',
                'industry-tips/<type:[\w\-]+>/<alias:[\w\- ]+>'=>'news/industry-tip/view',
                'industry-tips/<type:[\w\-]+>'=>'news/industry-tip/view',
                'industry-tips/<id:\d+>/<type:[\w\- ]+>/<see_type:[\w\- ]+>/<see_alias:[\w\- ]+>'=>'news/industry-tip/tips',

                '/new-furniture/<view:grid|list>' => 'orangelots/orangelots-kind/new',
                '/new-furniture' => 'orangelots/orangelots-kind/new',
                '/used-furniture/<view:grid|list>' => 'orangelots/orangelots-kind/used',
                '/used-furniture' => 'orangelots/orangelots-kind/used',
                '/orange-blossom-furniture/<view:grid|list>' => 'orangelots/orangelots-kind/orange-blossom',
                '/orange-blossom-furniture' => 'orangelots/orangelots-kind/orange-blossom',
                '/upcoming-deals-furniture/<view:grid|list>' => 'orangelots/orangelots-kind/upcoming-deals',
                '/upcoming-deals-furniture' => 'orangelots/orangelots-kind/upcoming-deals',
                '/flash-deals-furniture/<view:grid|list>' => 'orangelots/orangelots-kind/flash',
                '/flash-deals-furniture' => 'orangelots/orangelots-kind/flash',
                /*for alias in category*/
                '/account'=>'account/default/index',
                '/admin'=>'admin/admin/index',
                'user/logout'=>'user/logout/logout',
                'user/login'=>'user/login/login',


//                'orangelots/orangelots-kind/view/<vendor:\d+>'=>'orangelots/orangelots-kind/view',

				/*realty*/
				'real-estate/<category:[-a-z-A-Z-0-9]+>/<alias:[-a-z-A-Z-0-9]+>' => 'RealEstate/view',
				'real-estate/<category:[-a-z-A-Z-0-9]+>' => 'RealEstate/category',
				/*news*/
				'news' => 'news/news/index',
				'news/<alias:[-a-z-A-Z-0-9]+>' => 'news/news/view',
				/*all*/
				'user/register_as_vendor' => 'user/orangelots-users/create',
				'user/messages' => 'account/default/messages',
				'user/sell/auction/posted-by-me' => 'auctions/orangelots-auctions/posted-by-me',
				'user/sell/deals/sales/<id:\d+>' => 'auctions/orangelots-auctions/sales/<id>',
				'pay/withdrawal' => 'pay/withdrawal/index',
				'admin/invoices' => 'admin/invoices/list',
				'user/sell/deals/sales' => 'auctions/orangelots-auctions/sales',
				'industry_tips' => 'news/industry-tip/list',
				'user/questions' => 'user/questions/index',
				'site/upload' => 'site/upload',
                		'realty-gallery/ajax-upload/<gallery_id:\d+>' => 'realty-gallery/ajax-upload/<gallery_id>',

				'admin/rights' => 'admin/rights',
				'delivery/admin' => 'delivery/admin',
				'shipping-terms/admin' => 'shipping-terms/admin',


				'orangelots-manufacture/admin' => 'orangelots-manufacture/admin',
				'orangelots-style/admin' => 'orangelots-style/admin',

                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<sub:[\w\-]+>/<alias:[\w\-]+>/<view:grid|list>'=>'orangelots/orangelots-kind/view',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<sub:\w+>/<alias:\w+>/by-vendor/<vendor:[\w-]+>'=>'orangelots/orangelots-kind/vendor',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<sub:\w+>/<alias:\w+>/by-style/<style:[\w-]+>'=>'orangelots/orangelots-kind/style',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<alias:[\w\-]+>/<view:grid|list>'=>'orangelots/orangelots-kind/view',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<alias:[\w-]+>/by-vendor/<vendor:[\w-]+>'=>'orangelots/orangelots-kind/vendor',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<alias:[\w-]+>/by-style/<style:[\w-]+>'=>'orangelots/orangelots-kind/style',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/by-vendor/<vendor:[\w-]+>'=>'orangelots/orangelots-kind/vendor',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/by-style/<style:[\w-]+>'=>'orangelots/orangelots-kind/style',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<sub:[\w\-]+>/<alias:[\w\-]+>'=>'orangelots/orangelots-kind/view',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<alias:\w+>'=>'orangelots/orangelots-kind/view',
                '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<alias:\w+>/<view:grid|list>'=>'orangelots/orangelots-kind/view',
                '<sub:[\w\-]+>/<alias:[\w\-]+>/<view:grid|list>'=>'orangelots/orangelots-kind/view',
                '<sub:[\w\-]+>/<alias:[\w\-]+>/by-vendor/<vendor:[\w\-]+>'=>'orangelots/orangelots-kind/vendor',
                '<sub:[\w\-]+>/<alias:[\w\-]+>/by-style/<style:[\w\-]+>'=>'orangelots/orangelots-kind/style',
                '<alias:\w+>/by-vendor/<vendor:[\w\-]+>'=>'orangelots/orangelots-kind/vendor',
                '<alias:\w+>/by-style/<style:[\w\-]+>'=>'orangelots/orangelots-kind/style',
                '<alias:\w+>/<view:grid|list>'=>'orangelots/orangelots-kind/view',
                'by-vendor/<vendor:[\w\-]+>'=>'orangelots/orangelots-kind/vendor',
                'by-style/<style:[\w\-]+>'=>'orangelots/orangelots-kind/style',
                '<alias:\w+>/<view:grid|list>'=>'orangelots/orangelots-kind/view',
                '<sub:[\w\-]+>/<alias:[\w\-]+>'=>'orangelots/orangelots-kind/view',
                '<alias:\w+>'=>'orangelots/orangelots-kind/view',



				'<controller:[\w\-]+>/<id:\d+>' => '<controller>/view',
				'<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
				'<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',

				'<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<module>/<controller>/<action>',
				'<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>' => '<module>/<controller>/<action>',
//                '<module:[\w\-]+>/<controller:[\w\-]+>/<action:[\w\-]+>/<alias:[-a-z-A-Z-0-9]+>' => '<module>/<controller>/<action>',

			),
		),
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=orangelots',
			'emulatePrepare' => true,
			'username' => 'vsnit',
			'password' => 'TIpass2145',
			'charset' => 'utf8',
			'tablePrefix' => '',
			'enableProfiling' => YII_DEBUG,
			'schemaCachingDuration' => (YII_DEBUG ? 0 : 3600),
		),
		'errorHandler' => array(
			'errorAction' => '//site/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.Bootstrap',
		),
		'storage' => array(
			'class' => 'CRStorage',
			'directory' => 'webroot.photos.avatars.',
			'subDirectory' => true,
			'subDirectoryLevel' => 2,
			'subDirectoryChunk' => 2,
		),
		'cms' => array(
			'class' => 'cms.components.Cms',
			'languages' => array('en' => 'English'),
			'defaultLanguage' => 'en',
			'allowedFileTypes' => 'jpg, gif, png',
			'allowedFileSize' => 1024,
			'attachmentPath' => '/files/cms/attachments/',
			'headingTemplate' => '<h1 class="heading">{heading}</h1>',
			'widgetHeadingTemplate' => '<h3 class="heading">{heading}</h3>',
			'pageTitleTemplate' => '{title} | {appName}',
			'appLayout' => 'application.views.layouts.main',
		),
        'sphinx' => array(
            'class' => 'application.extensions.DGSphinxSearch.DGSphinxSearch',
            'server' => 'localhost',
            'port' => 3312,
            'maxQueryTime' => 3000,
            'enableProfiling'=>0,
            'enableResultTrace'=>0,
            'fieldWeights' => array(
//                'name' => 1000,
//                'keywords' => 100,
            ),
        ),
	),
	'params' => array(
        'payPal'=>array(
            'sandbox' => array(
                'client_id' => 'AZ_SEhBwrVFR65knxNhxvn_CY5yPdkQuTslJfZymuJkqlI0eziaVcmrzQmia',
                'client_secret' => 'EDK_pRBSgs4cMMfpWP2xyQAwR-dumXCHcp5QHc11FfQrxPM_TJmRfEa54iT9'
            ),
            'live' => array(
                'client_id' => 'AWzY4hBrUKlDyScYdrHr4wkHBcsfvDkqh9JYEOWVJeIf0F9SPPNb0jGJpeSS',
                'client_secret' => 'EM8c9BCjSIKeH7rTlOh_uEHYILykuLFGcNnfUfT9EHwKTU9P_BGVd9_lsyOp'
            )
        ),
		'adminEmail' => 'orangelots@gmail.com',
		'files' => array(
			'allowImageTypes' => 'jpg,jpeg,png,bmp,gif,tif,tiff',
			'maxUploadSize' => 10 * 1024 * 1024,
			'imageWidth' => 120,
			'thumbnailWidth' => 120,
			'imageHeight' => 120,
			'thumbnailHeight' => 120,
			'relatedLimit' => 4,
		),
		'languages' => array('en' => 'English'),
		'LoginSmsCode' => 'Код подтверждения #CODE#',
	),
);
$sLocalConfig = getLocalConfigName(__FILE__);
$arConfig = (file_exists($sLocalConfig)) ? CMap::mergeArray($arConfig, require_once($sLocalConfig)) : $arConfig;
return $arConfig;