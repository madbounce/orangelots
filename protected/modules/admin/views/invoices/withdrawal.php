<?php
/* @var $this OrangelotsAuctionsController */
/* @var $model OrangelotsAuctions */
?>
<div class="breadcrumb_row clearfix">
    <div class="main_container">

        <?php
        $this->widget(
            'zii.widgets.CBreadcrumbs',
            array(
                'links' => array(
                    'Update invoice status',
                ),
                'separator' => '',
                'tagName' => 'ul',
                'htmlOptions' => array('class' => ''),
                'inactiveLinkTemplate' => '<li><a href="">{label}</a></li>',
                'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                        "site/index"
                    ) . '"></a></li>',
            )
        );
        ?>
    </div>
</div>

<div class="form main_container">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'id' => 'orangelots-invoices-update-status-form',
            'enableAjaxValidation' => false,
        )
    ); ?>
    <div class="uni_form_block">
        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'id'); ?>
            <?php echo $model->id?>
        </p>

        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'amount'); ?>
            <?php echo $model->amount?>
        </p>

        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'fee'); ?>
            <?php echo $model->fee?>
        </p>
        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'purchase_date'); ?>
            <?php echo date('m/d/Y', $model->purchase_date)?>
        </p>
        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'transaction_date'); ?>
            <?php echo date('m/d/Y', $model->transaction_date)?>
        </p>

        <?php echo $form->hiddenField($model, 'payed', array('value' => OrangelotsInvoices::WITHDRAW))?>
    </div>
    <?if($model->payed != OrangelotsInvoices::WITHDRAW):?>
        <button class="btn btn_orange">Set as paid</button>
    <?else:?>
        You have already processed this invoice.
    <?endif;?>
    <?php $this->endWidget(); ?>
</div><!-- form -->
