<div class="received__container">
    <div class="main_container">
        <div class="received__row__form_container">
            <div class="received__row">
                <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'model-search-admin-pay-form',
                        'enableAjaxValidation' => false,
                    )); ?>

                <div class="received__row__block received__row__block__id">
                    <?php echo $form->labelEx($model, 'id'); ?>
                    <?php echo $form->textField($model, 'id', array('type' => 'text','maxlength' => 50)); ?>
                </div>

                <div class="received__row__block received__row__block__id">
                    <label>Vendor id</label>
                    <?php echo $form->textField($model, 'vendor_id', array('type' => 'text','maxlength' => 50)); ?>
                </div>

                <div class="received__row__block received__row__block__id">
                    <label>Amount</label>
                    <?php echo $form->textField($model, 'amount', array('type' => 'text','maxlength' => 50)); ?>
                </div>

                <div class="received__row__block received__row__block__id">
                    <label>Email</label>
                    <?php echo CHtml::telField('vendor_email','', array('type' => 'text','maxlength' => 50))?>
                </div>

                <div class="received__row__block__calendar_block">
                    <label>Create date</label>
                    <br>
                    <label>from</label>
                    <?php
                    $this->widget('application.components.WasDatepicker.WasDatepicker',array(
                            'name' => 'dateFrom',
                            'options'=>array(
                                'language'=>'en',
                                'format'=>'m/d/Y',
                                'autoclose'=>'true',
                                'endDate'=>date('m/d/Y', time()),
                                'weekStart'=>1,
                                'startView'=>2,
                                'keyboardNavigation'=>true
                            ),
                        ));
                    ?>
                    <label>to</label>
                    <?php
                    $this->widget('application.components.WasDatepicker.WasDatepicker',array(
                            'name' => 'dateTo',
                            'options'=>array(
                                'language'=>'en',
                                'format'=>'m/d/Y',
                                'autoclose'=>'true',
                                'endDate'=>date('m/d/Y', time()),
                                'weekStart'=>1,
                                'startView'=>2,
                                'keyboardNavigation'=>true
                            ),
                        ));
                    ?>

                    <?if(!$active):?>
                        <br>
                        <br>
                        <label>Pay date</label>
                        <br>
                        <label>from</label>
                        <?php
                        $this->widget('application.components.WasDatepicker.WasDatepicker',array(
                                'name' => 'payDateFrom',
                                'options'=>array(
                                    'language'=>'en',
                                    'format'=>'m/d/Y',
                                    'autoclose'=>'true',
                                    'endDate'=>date('m/d/Y', time()),
                                    'weekStart'=>1,
                                    'startView'=>2,
                                    'keyboardNavigation'=>true
                                ),
                            ));
                        ?>
                        <label>to</label>
                        <?php
                        $this->widget('application.components.WasDatepicker.WasDatepicker',array(
                                'name' => 'payDateTo',
                                'options'=>array(
                                    'language'=>'en',
                                    'format'=>'m/d/Y',
                                    'autoclose'=>'true',
                                    'endDate'=>date('m/d/Y', time()),
                                    'weekStart'=>1,
                                    'startView'=>2,
                                    'keyboardNavigation'=>true
                                ),
                            ));
                        ?>
                    <?endif;?>
                </div>

                <div class="received__row__block">
                    <button class="btn_image">
                        <img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt="">
                    </button>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        </div>

    </div>
</div>

<?if($active):?>
    <?$this->renderPartial('active_pay_list', array('model' => $model, 'dataProvider' => $dataProvider))?>
<?else:?>
    <?$this->renderPartial('archive_pay_list', array('model' => $model, 'dataProvider' => $dataProvider))?>
<?endif;?>