<?php
/* @var $this OrangelotsAuctionsController */
?>
    <div class="breadcrumb_row clearfix">
        <div class="main_container">

            <?php
            $this->widget(
                'zii.widgets.CBreadcrumbs',
                array(
                    'links' => array(
                        'Update pay status',
                    ),
                    'separator' => '',
                    'tagName' => 'ul',
                    'htmlOptions' => array('class' => ''),
                    'inactiveLinkTemplate' => '<li><a href="">{label}</a></li>',
                    'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                            "site/index"
                        ) . '"></a></li>',
                )
            );
            ?>
        </div>
    </div>

<?$this->renderPartial('_form', array('model' => $model))?>