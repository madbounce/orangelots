<?php
/**
 * @var $this AdminController
 * @var $forms CForm[]
 * @var $models CFormModel[]
 * @var TbActiveForm $form
 */
?>
<style>
	.settings-form div{
		margin-left:0;
	}
</style>
<div id="content">
	<?$form = $this->beginWidget(
		'bootstrap.widgets.TbActiveForm',
		array(
			'id' => 'horizontalForm',
			'type' => 'horizontal',
		)
	);?>
	<div>
		<?$this->widget(
			'bootstrap.widgets.TbTabs',
			array(
				'tabs' => $this->action->getTabularFormTabs($forms, $models),
			)
		);?>
	</div>
<!--	<div class="form-actions">-->
        <? $this->widget(
            'bootstrap.widgets.TbButton',
            array('buttonType' => 'submit', 'type' => 'success', 'label' => 'Submit')
        ); ?>
<!--    </div>-->
	<? $this->endWidget(); ?>
</div>
