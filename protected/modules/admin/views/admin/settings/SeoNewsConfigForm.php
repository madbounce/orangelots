<?php

return array(
    'elements' => array(
        'main' => array(
            'type' => 'form',
            'elements' => array(
                'news_keywords' => array('type' => 'text',),
                'news_description' => array('type' => 'text',),
            ),
        ),
    ),
);

?>
