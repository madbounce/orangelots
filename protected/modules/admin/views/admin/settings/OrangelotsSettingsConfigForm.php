<?php
return array(
	'elements' => array(
		'main' => array(
			'type' => 'form',
			'elements' => array(
				'maxPaySum' => array('type' => 'text'),
				'payPalServiceFee' => array('type' => 'text'),
				'payPalProcessingFee' => array('type' => 'text'),
				'bankWireServiceFee' => array('type' => 'text'),
				'bankWireSumFee' => array('type' => 'text'),
                'payDetails' => array('type' => 'ext.editor.editMe.widgets.ExtEditMe', 'htmlOptions' => ''),
            ),
		),
	),
);?>
