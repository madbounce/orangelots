<div class="form">
	<?php $form = $this->beginWidget('CActiveForm'); ?>
	<div class="row">
		<?php echo $form->dropDownList($model, 'itemname', $itemnameSelectOptions); ?>
		<?php echo $form->error($model, 'itemname'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::htmlButton(Rights::t('core', 'Добавить в группу'), array('class' => 'btn btn-success', 'type' => 'submit')); ?>
		<?=CHtml::link(
			Rights::t('core', 'К списку пользователей'),
			createUrl('//admin/user/admin/'),
			array(
				'class' => 'btn btn-warning',
				'style' => 'color:white;text-decoration:none;font-weight:normal;',
			)
		)?>
	</div>
	<?php $this->endWidget(); ?>
</div>