<?php
class AdminController extends Controller
{
	public $layout = '//layouts/admin';

	public function filters()
	{
		return array(
			'rights',
		);
	}

	protected function beforeAction($action)
	{
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'settings' => array(
				'class' => 'admin.controllers.admin.CSettingsAction',
			),
		);
	}

	public function actionIndex()
	{
		if (user()->isGuest) {
			$this->redirect('//admin/admin/login');
		}
		$this->render('index');
	}
}
