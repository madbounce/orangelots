<?php
class InvoicesController extends Controller
{
    public $layout = '//layouts/admin';
    public $defaultAction = 'list';

    public function filters()
    {
        return array(
            'rights',
        );
    }

    protected function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    public function actionList()
    {
        $model = new OrangelotsInvoices('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['OrangelotsInvoices'])) {
            $model->attributes = $_GET['OrangelotsInvoices'];
        }

        $this->render(
            'list',
            array(
                'model' => $model,
            )
        );
    }

    public function actionUpdate($id){
        $invoice = OrangelotsInvoices::model()->findByPk($id);
        if(!empty($invoice)){
            if(isset($_POST['OrangelotsInvoices'])){
                if(isset($_POST['OrangelotsInvoices']['payed'])){
                    $adminComment = $invoice->admin_comment;
                    $invoice->attributes = $_POST['OrangelotsInvoices'];
                    if(!empty($_POST['OrangelotsInvoices']['admin_comment'])){
                        $invoice->admin_comment = !empty($adminComment) ? $adminComment.'<br><hr>'.$_POST['OrangelotsInvoices']['admin_comment'] : $_POST['OrangelotsInvoices']['admin_comment'];
                    }
                    $invoice->adminActions();
                    $invoice->save();
                }
                user()->setFlash('success', 'Status of invoice №'.$id.' is updated');
                $this->redirect(createUrl('/admin/invoices/list'));
            }
            $this->render('update', array('model' => $invoice));
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

    public function actionView($id){
        $invoice = OrangelotsInvoices::model()->findByPk($id);
        if(!empty($invoice)){
            $this->render('view', array('model' => $invoice));
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }
}