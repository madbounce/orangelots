<?php

Yii::import('bootstrap.widgets.TbActiveForm');

class CSettingsAction extends CAction
{
	public static $models = array(
        'OrangelotsSettings',
		'SeoMain',
		'SeoNews',
	);

	public function run()
	{
		$forms = $models = array();
		$bFormHasErrors = false;
		foreach (self::$models as $modelName) {
			$modelName = ucfirst($modelName);
			$className = "{$modelName}ConfigForm";
			$model = new $className;

			if (!yii()->request->isPostRequest) {
				foreach ($model->attributes as $attr => $val) {
					$model->$attr = Yii::app()->getModule('admin')->econfig->get($attr);
				}
			} else {
				$model->attributes = $_POST[$className];
			}

			$form = new CForm("admin.views.admin.settings.{$modelName}ConfigForm", $model);

			if (yii()->request->isPostRequest && $form->validate()) {
				foreach ($model->attributes as $attr => $val) {
					Yii::app()->getModule('admin')->econfig->set($attr, $val);
				}
			} elseif (yii()->request->isPostRequest) {
				$bFormHasErrors = true;
				$arErrors[$modelName] = $model->getErrors();
			}

			$models[$modelName] = $model;
			$forms[$modelName] = $form;
		}
		if ($bFormHasErrors) {
			user()->setFlash('error', 'Check the correctness of filling data' . var_export($arErrors, 1));
		}

		$this->controller->render('settings', array('forms' => $forms, 'models' => $models));
	}

	public function getTabularFormTabs($forms, $models, $activeTabName = '')
	{
		$tabs = array();
		$activeTabName = $activeTabName ? $activeTabName : key($models);
		foreach ($models as $modelName => $model) {
			$tabs[] = array(
				'active' => $modelName === $activeTabName,
				'label' => $model::$modelLabel,
				'content' => $this->controller->renderPartial(
						'admin.views.admin.settings._tabular',
						array('form' => $forms[$modelName], 'model' => $model),
						true
					),
			);
		}
		return $tabs;
	}
}