<?php
class OrangelotsSettingsConfigForm extends CFormModel
{
	public static $modelLabel = 'Site Settings';
	public $maxPaySum;
    public $payDetails;
    public $payPalServiceFee;
    public $payPalProcessingFee;
    public $bankWireServiceFee;
    public $bankWireSumFee;

    public function rules()
	{
		return array(
			array('maxPaySum, payPalServiceFee, payPalProcessingFee, bankWireServiceFee, bankWireSumFee, payDetails', 'required'),
			array('maxPaySum, payPalServiceFee, payPalProcessingFee, bankWireServiceFee, bankWireSumFee', 'numerical', 'min' => 0),
		);
	}

	public function attributeLabels()
	{
		return array(
			'maxPaySum' => Yii::t('app', 'Maximum amount to pay via PayPal'),
			'payDetails' => Yii::t('app', 'Bank details'),
			'payPalServiceFee' => Yii::t('app', '% PP service fee'),
			'payPalProcessingFee' => Yii::t('app', '% PP processing fee'),
			'bankWireServiceFee' => Yii::t('app', '% Bank service fee'),
			'bankWireSumFee' => Yii::t('app', 'Bank fee sum'),
		);
	}
}