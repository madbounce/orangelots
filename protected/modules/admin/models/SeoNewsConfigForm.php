<?php

class SeoNewsConfigForm extends CFormModel
{
	public static $modelLabel = 'Settings SEO Tips';
	public $news_keywords;
	public $news_description;


	public function rules()
	{
		return array(
			array('', 'required'),
			array('news_keywords, news_description', 'length', 'max' => 500),
		);
	}

	public function attributeLabels()
	{
		return array(
			'news_keywords' => 'Keywords',
			'news_description' => 'Description',
		);
	}
}