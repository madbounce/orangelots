<?php

class SeoMainConfigForm extends CFormModel
{
	public static $modelLabel = 'Settings SEO Main';
	public $main_keywords;
	public $main_description;


	public function rules()
	{
		return array(
			array('', 'required'),
			array('main_keywords, main_description', 'length', 'max' => 500),
		);
	}

	public function attributeLabels()
	{
		return array(
			'main_keywords' => 'Keywords',
			'main_description' => 'Description',
		);
	}
}