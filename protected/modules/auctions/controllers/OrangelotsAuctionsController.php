<?php

class OrangelotsAuctionsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/main';

    public $auctions = null;

    /**
     * @return array action filters
     */
    public function filters()
    {
        /*return array(
            'accessControl',
            'postOnly + delete',
        );*/
        return array('rights');
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        /*return array(
            array(
                'allow',
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array(
                'allow',
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array(
                'allow',
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array(
                'deny',
                'users' => array('*'),
            ),
        );*/
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id, $questionId = null, $show = false)
    {
        $this->layout = 'application.views.layouts.main';
        $model = $this->loadModel($id);

        $bid = OrangelotsBiddingOn::model()->findByAttributes(
            array('id_user' => yii()->user->id, 'id_auction' => $id),
            array('order' => 'id DESC')

        );
        $vendor = OrangelotsUsers::model()->findByPK($model->user_id);
        $saveforlater = SaveForLater::model()->findByAttributes(array('user_id'=>yii()->user->id, 'auction_id' =>$model->id));
        if (isset($saveforlater)){
            $link = true;
        } else {
            $link = false;
        }

        $rating = OrangelotsFeedback::model()->findByAttributes(array('user_id_rate'=>$model->user_id), array('order' => 'id DESC'));
        if ((isset($rating)) && (!empty($rating))){
            $rating = $rating->average;
        } else {
            $rating = 0;
        }

        $winner = OrangelotsWinners::model()->findByAttributes(array('user_id' => user()->id, 'auction_id' => $model->id));

        if((int)$model->start_date_unix > time()){
            $this->render(
                'viewUpcoming',
                array(
                    'model' => $model,
                    'bid' => $bid,
                    'vendor' => $vendor,
                    'link' => $link,
                    'rating' => $rating,
                    'questionId' => $questionId,
                    'show' => $show
                )
            );
        }else{
            $this->render(
                'view',
                array(
                    'model' => $model,
                    'bid' => $bid,
                    'vendor' => $vendor,
                    'link' => $link,
                    'rating' => $rating,
                    'questionId' => $questionId,
                    'show' => $show,
                    'canBid' => OrangelotsAuctions::canBid($model->id),
                    'winner' => $winner
                )
            );
        }
    }

    public function actionCopy($id)
    {
        if (isset($id)){
            $copy = OrangelotsAuctions::model()->findByPk($id);
            yii()->session['copy_auction'] = null;
            yii()->session['copy_auction'] = $copy->attributes;
            $this->redirect(yii()->createUrl('auctions/orangelots-auctions/create/'));
        }
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        if ((isset($_POST['OrangelotsAuctions'])) && (isset($_POST['OrangelotsAuctions']['id']))) {
            $id = (int)$_POST['OrangelotsAuctions']['id'];
            yii()->session['auction_id'] = $id;
        } else {
            $this->clearNotActiveAuctions(yii()->user->id);
            $my_model = new OrangelotsAuctions('my_create');
            $my_model->user_id = yii()->user->id;
            $my_model->units_in_lot = '1';
            if (!user()->isAdmin()) {
                $info = '';
                $usr = OrangelotsUsers::model()->findByPk(yii()->user->id);
                if (!empty($usr)) {
                    $info = $usr->company . PHP_EOL . $usr->address_1 . ' '  . $usr->address_2 . ' ' . OrangelotsStates::getStateLabel($usr->state).  ' '  . $usr->zipcode.', ' . 'USA';
                    $my_model->vendors_information = $info;
                }
            }
            if ($my_model->save()) {
                $id = $my_model->id;
                yii()->session['auction_id'] = $id;
            }
        }

        $this->forward_yii(yii()->createUrl('auctions/orangelots-auctions/update/', array('id' => $id)));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate()
    {
        $id = $_REQUEST['id'];
        if (isset($this->auctions)) {
            $id = $this->auctions;
            $this->auctions = null;
        }
        if (user()->isAdmin()) {
            $this->layout = 'application.views.layouts.admin';
        }
        $model = $this->loadModel($id);
        $itm = Items::model()->findAllByAttributes(array('auction_deal_id'=>$id));
        if(!OrangelotsAuctions::canEdit($id)){
            throw new CHttpException('400', 'Bad request');
        }else{

        }
        $model->scenario = 'create';

        /*
         * Проверка на доступ к закрытому аукциону
         *
         * */
        if (!user()->isAdmin()) {
            if ($model->user_id != yii()->user->id) {
                $this->redirect(createUrl('site/index'));
            } else {
                if ($model->status == 2) {
                    $this->redirect(createUrl('site/index'));
                }
            }
        }

        if($model->is_flashdeal == 1){
            $this->redirect(createUrl('flashdeal/default/update', array('id' => $model->id)));
        }

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset(yii()->session['copy_auction'])){
            $model->attributes = yii()->session['copy_auction'];
            yii()->session['copy_auction'] = null;
        }
        if (isset($_POST['OrangelotsAuctions'])) {
            $model->attributes = $_POST['OrangelotsAuctions'];
            $start_date = $model->au_date . ' ' . $model->au_time . ' ' . $model->au_am_pm;
            $end_date = $model->au_date_end . ' ' . $model->au_time_end . ' ' . $model->au_am_pm_end;
            $model->start_date_unix = CDateTimeParser::parse($start_date, 'M/dd/yyyy HH:mm a');
            $model->end_date_unix = CDateTimeParser::parse($end_date, 'M/dd/yyyy HH:mm a');
            $model->start_date = yii()->dateFormatter->format('M/dd/yyyy', $model->start_date_unix);
            $model->end_date = yii()->dateFormatter->format('M/dd/yyyy', $model->end_date_unix);
            if (($_POST['OrangelotsAuctions']['usr'] != 'empty') && (isset($_POST['OrangelotsAuctions']['usr'])) && (!empty($_POST['OrangelotsAuctions']['usr'])) && (is_numeric ($_POST['OrangelotsAuctions']['usr']))){
                $user_aviable = OrangelotsUsers::model()->findByPk($_POST['OrangelotsAuctions']['usr']);
                if ((isset($user_aviable))&&(!empty($user_aviable)))
                    $model->user_id = $_POST['OrangelotsAuctions']['usr'];
            }


            if ($model->shipping_terms == "3") {
                $model->scenario = 'shipping_terms_other';
            } else {
                if ($model->delivery == "5") {
                    $model->scenario = 'delivery_other';
                } else {
                    if ($model->delivery == "5" && $model->shipping_terms == "3") {
                        $model->scenario = 'delivery_shipping_terms_other';
                    }
                }
            }
            $model->status = 1;
            $items_errors = false;
            if(isset($_POST['items'])){
                if(!empty($_POST['items'])){
                    foreach ($_POST['items'] as $k=>$v) {
                        $frm = new ItemsForm();
                        $frm->attributes = $v;
                        if(!$frm->validate()){
                            $_POST['items'][$k]['errors'] = $frm->getErrors();
                            $items_errors = true;
                        }
                    }
                }
            }
            if($items_errors){
                $model->item_validate = false;
            }

            if ($model->validate()) {
                $model->active = '1';
            }

            if ($model->save()) {
                if(isset($_POST['items']))
                    $this->saveItems($model->id, $_POST['items']);
                if (user()->isAdmin()){
                    $this->redirect(array('admin'));
                } else {
                    $this->redirect(createUrl('auctions/orangelots-auctions/posted-by-me'));
                }


            }
        }
        $this->render(
            'update',
            array(
                'model' => $model,
                'itm' => $itm,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('OrangelotsAuctions');
        $this->render(
            'index',
            array(
                'dataProvider' => $dataProvider,
            )
        );
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->layout = 'application.views.layouts.admin';
        $model = new OrangelotsAuctions('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['OrangelotsAuctions'])) {
            $model->attributes = $_GET['OrangelotsAuctions'];
            $model->owner_email = $_GET['OrangelotsAuctions']['owner_email'];
            $model->on_main = $_GET['OrangelotsAuctions']['on_main'];
        }

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    public function actionBuyNow()
    {
        if (yii()->user->isGuest) {
            echo json_encode(
                array(
                    'status' => 'error',
                    'url' => '',
                    'html' => $this->renderPartial(
                            'application.modules.user.views.user.login_popup',
                            array('model' => new OrangelotsUsers),
                            true,
                            true
                        )
                )
            );
        } else {
            if ((isset($_POST['model_id'])) && (isset($_POST['count']))) {
                $auction = OrangelotsAuctions::model()->findByPk($_POST['model_id']);
            }
            elseif (isset($_POST['model'])) {
                $auction = OrangelotsAuctions::model()->findByPk($_POST['model']['id']);
            }
            if ($auction->user_id == yii()->user->id) {
                echo json_encode(
                    array(
                        'status' => 'some_error',
                        'html' =>"Sorry. This is YOUR auction, you can't buy now!"
                    )
                );
                return;
            }
            $user = OrangelotsUsers::model()->findByPk(yii()->user->id);
            $card = CardInfo::getUserCardInfo(user()->id);
            $_SESSION['buy_now'] = true;
            if(empty($card)){
                echo json_encode(
                    array(
                        'status' => 'error',
                        'url' => '',
                        'html' => $this->renderPartial(
                                'application.modules.user.views.user.paypal_form',
                                array('model' => new CardInfo(), 'name' => $user->firstname,'skipDis' => true),
                                true,
                                true
                            )
                    )
                );
            }else{
                if ((isset($_POST['model_id'])) && (isset($_POST['count']))) {
                    // BUY NOW FOR FLASH DEAL
                    if (!isset($user)) {
                        echo json_encode(
                            array(
                                'status' => 'error',
                                'html' => 'Some Error With This User',
                            )
                        );
                    }
                    $auction = OrangelotsAuctions::model()->findByPk($_POST['model_id']);
                    if (!isset($auction)){
                        echo json_encode(
                            array(
                                'status' => 'error',
                                'html' => 'There is no auction',
                            )
                        );
                    }
                    if ((!isset($_POST['count'])) && (empty($_POST['count']))) {
                        echo json_encode(
                            array(
                                'status' => 'some_error',
                                'html' =>"Sorry. This is not a valid quantity!"
                            )
                        );
                        return;
                    }
                    if ($auction->lots_available<$_POST['count']){
                        echo json_encode(
                            array(
                                'status' => 'some_error',
                                'html' =>"Sorry. Quantity not enough!"
                            )
                        );
                        return;
                    }

                    if ($auction->status == 2){
                        echo json_encode(
                            array(
                                'status' => 'some_error',
                                'html' =>"Sorry. Auction is closed"
                            )
                        );
                        return;
                    }
                    if (isset($auction)) {
                        $winner = new OrangelotsWinners;
                        $winner->user_id = yii()->user->id;
                        $winner->auction_id = $_POST['model_id'];
                        $winner->bid_id = '';//TODO:WHAT IN BID ID?
                        $winner->qty = (int)$_POST['count'];
                        $winner->price_per_unit = $auction->price_per_unit;
                        $winner->summary_price = ($auction->units_in_lot*$auction->price_per_unit)*(int)$_POST['count'];
                        $winner->owner_auction_id = $auction->user_id;
                        $winner->is_buy_now = 1;
                        if ($winner->save()) {
                            $auction->lots_available = $auction->lots_available - (int)$_POST['count'];
                            if ($auction->lots_available == 0) {
                                $auction->status = 2;
                            } elseif($auction->lots_available < 0){
                                echo json_encode(
                                    array(
                                        'status' => 'some_error',
                                        'html' =>"Sorry. Some Error With Quantity"
                                    )
                                );
                                return;
                            }
                            $auction->save();
                            $seller = OrangelotsUsers::model()->findByPk($auction->user_id);
                            $invoice = OrangelotsInvoices::model()->createInvoice($user,$seller , $winner->summary_price, $_POST['model_id'], $winner->id);
                            if ($invoice) {
                                if ($auction->lots_available == 0) {
                                    echo json_encode(
                                        array(
                                            'status' => 'bidding_save',
                                            'html' => $this->renderPartial(
                                                    'application.modules.user.views.user.bid_save_invoice',
                                                    array(
                                                        'model' => OrangelotsUsers::model()->findByPk(yii()->user->id),
                                                        'buy' => true,
                                                        'invoice'=>$invoice,
                                                    ),
                                                    true,
                                                    true
                                                )
                                        )
                                    );
                                } else {
                                    echo json_encode(
                                        array(
                                            'status' => 'bidding_save',
                                            'html' => $this->renderPartial(
                                                    'application.modules.user.views.user.bid_save',
                                                    array(
                                                        'model' => OrangelotsUsers::model()->findByPk(yii()->user->id),
                                                        'buy' => true
                                                    ),
                                                    true,
                                                    true
                                                )
                                        )
                                    );
                                }

                            };
                        }
                    }
                }
                elseif (isset($_POST['model'])) {
                    if (!isset($user)) {
                        echo json_encode(
                            array(
                                'status' => 'error',
                                'html' => 'Some Error With This User',
                            )
                        );
                    }
                    if (isset($auction)) {
                        if ((!isset($_POST['qty'])) && (empty($_POST['qty']))) {
                            echo json_encode(
                                array(
                                    'status' => 'some_error',
                                    'html' =>"Sorry. This is no valid quantity!"
                                )
                            );
                            return;
                        }
                        if ($_POST['model']['units_in_lot']<$_POST['qty']){
                            echo json_encode(
                                array(
                                    'status' => 'some_error',
                                    'html' =>"Sorry. Quantity not enough!"
                                )
                            );
                            return;
                        }


                        if (OrangelotsAuctions::checkQuantity($auction->units_in_lot, $auction->min_unit, $_POST['qty'])){
                            echo json_encode(
                                array(
                                    'status' => 'some_error',
                                    'html' =>"Sorry. Quantity not correct!"
                                )
                            );
                            return;
                        }
                        if ($auction->status == 2){
                            echo json_encode(
                                array(
                                    'status' => 'some_error',
                                    'html' =>"Sorry. Auction is closed"
                                )
                            );
                            return;
                        }
                        $bid = OrangelotsBiddingOn::model()->findByAttributes(
                            array('id_auction' => $_POST['model']['id'], 'id_user' => yii()->user->id)
                        );
                        if ($bid == null) {
                            $bid = new OrangelotsBiddingOn;
                        }
                        $bid->id_user = yii()->user->id;
                        $bid->id_owner = $auction->user_id;
                        $bid->id_auction = $_POST['model']['id'];
                        $bid->qty = (int)$_POST['qty'];
                        $bid->summary_price = $_POST['model']['buy_now_price']*$bid->qty;
                        $bid->price_per_unit = $auction->buy_now_price;
                        $bid->buy_now = 1;
                        if ($bid->save()) {
                            $winner = new OrangelotsWinners;
                            $winner->user_id = yii()->user->id;
                            $winner->auction_id = $_POST['model']['id'];
                            $winner->bid_id = $bid->id;
                            $winner->qty = (int)$_POST['qty'];
                            $winner->price_per_unit = $auction->buy_now_price;
                            $winner->summary_price = $_POST['model']['buy_now_price']*$winner->qty;
                            $winner->owner_auction_id = $_POST['model']['user_id'];
                            $winner->is_buy_now = 1;
                            if ($winner->save()) {
                                $auction->units_in_lot = $auction->units_in_lot - (int)$_POST['qty'];
                                if ($auction->units_in_lot == 0) {
                                    $auction->status = 2;
                                } elseif($auction->units_in_lot < 0){
                                    echo json_encode(
                                        array(
                                            'status' => 'some_error',
                                            'html' =>"Sorry. Some Error With Quantity"
                                        )
                                    );
                                    return;
                                }
                                $auction->save();

                                $invoice = OrangelotsInvoices::model()->createInvoice($user, OrangelotsUsers::model()->findByPk($auction->user_id), $winner->summary_price, $auction->id, $winner->id);
                                if ($invoice) {
                                    echo json_encode(
                                        array(
                                            'status' => 'bidding_save',
                                            'html' => $this->renderPartial(
                                                    'application.modules.user.views.user.bid_save',
                                                    array(
                                                        'model' => OrangelotsUsers::model()->findByPk(
                                                                yii()->user->id
                                                            ),
                                                        'buy' => true
                                                    ),
                                                    true,
                                                    true
                                                )
                                        )
                                    );
                                };
                            }


                        }
                    }
                }
            }
        }
    }

    public function actionPlaceBid()
    {
        if (yii()->user->isGuest) {
            echo json_encode(
                array(
                    'status' => 'error',
                    'url' => '',
                    'html' => $this->renderPartial(
                            'application.modules.user.views.user.login_popup',
                            array('model' => new OrangelotsUsers),
                            true,
                            true
                        )
                )
            );
        } else {
            $card = CardInfo::getUserCardInfo(user()->id);
            $user = OrangelotsUsers::getUserInfo(user()->id);
            if(empty($card)){
                echo json_encode(
                    array(
                        'status' => 'error',
                        'url' => '',
                        'html' => $this->renderPartial(
                                'application.modules.user.views.user.paypal_form',
                                array('model' => new CardInfo(), 'name' => $user->firstname,'skipDis' => true),
                                true,
                                true
                            )
                    )
                );
            }
            else{
                if ((isset($_POST['Bid'])) && (isset($_POST['Bid']['qty'])) && (isset($_POST['Bid']['price_per_unit'])) && ($_POST['Bid']['id_auction'] != '')) {
                    $auctions = OrangelotsAuctions::model()->findByPk((int)$_POST['Bid']['id_auction']);
                    dump($auctions->min_bid);
                    dump($_POST['Bid']);die;
                    if (isset($auctions)) {
                        if ($auctions->user_id == yii()->user->id) {
                            echo json_encode(
                                array(
                                    'status' => 'some_error',
                                    'html' =>"Sorry. This is YOUR auction, you can't place a bid!"
                                )
                            );
                            return;
                        }
                    } else {
                        echo json_encode(
                            array(
                                'status' => 'some_error',
                                'html' =>"Auction Problem"
                            )
                        );
                        return;
                    }
                    if (OrangelotsAuctions::checkQuantity($auctions->units_in_lot, $auctions->min_unit, $_POST['Bid']['qty'])){
                        echo json_encode(
                            array(
                                'status' => 'some_error',
                                'html' =>"Sorry. Quantity not correct!"
                            )
                        );
                        return;
                    }
                    if ($auctions->buy_now_price<=$_POST['Bid']['price_per_unit']){
                        echo json_encode(
                            array(
                                'status' => 'some_error',
                                'html' =>"Your bid is more than buy now  price. To get this lot better use buy now."
                            )
                        );
                        return;
                    }
                    $bid = OrangelotsBiddingOn::model()->findByAttributes(
                        array('id_auction' => $_POST['Bid']['id_auction'], 'id_user' => yii()->user->id)
                    );
                    if ($bid == null) {
                        $bid = new OrangelotsBiddingOn;
                    }

                    if (isset($auctions)) {
                        if (($_POST['Bid']['price_per_unit'] < $auctions->min_bid) || ($_POST['Bid']['qty'] < $auctions->min_unit)) {
                            echo json_encode(
                                array(
                                    'status' => 'some_error',
                                    'html' => 'Your bid is less than the minimum for this auction! '
                                )
                            );
                            return;
                        }
                    }

                    $bid->attributes = $_POST['Bid'];
                    $bid->id_owner = OrangelotsAuctions::model()->findByPk($_POST['Bid']['id_auction'])->user_id;
                    $bid->id_user = yii()->user->id;
                    $bid->id_auction = $_POST['Bid']['id_auction'];
                    $bid->summary_price = $_POST['Bid']['qty'] * $_POST['Bid']['price_per_unit'];
                    $bid->buy_now = 0;
                    if ($bid->save()) {
                        $notification = new OrangelotsNotifications();
                        $notification->user_id = OrangelotsAuctions::model()->findByPk($_POST['Bid']['id_auction'])->user_id;
                        $notification->message_id = OrangelotsNotifications::NEW_BID;
                        $notification->auction_id =$_POST['Bid']['id_auction'];
                        $notification->content = "New Bid of $".$_POST['Bid']['price_per_unit']." arrived for <a href = '".createUrl('auctions/orangelots-auctions/posted-by-me/'.OrangelotsAuctions::model()->findByPk($_POST['Bid']['id_auction'])->id)."'>".OrangelotsAuctions::model()->findByPk($_POST['Bid']['id_auction'])->name."</a> from ".OrangelotsUsers::model()->findByPk(yii()->user->id)->firstname.", ".OrangelotsStates::model()->findBypk(OrangelotsUsers::model()->findByPk(yii()->user->id)->state)->state_abbreviation."";
                        $notification->save();


                        $bidders_criteria =  new CDbCriteria;
                        $bidders_criteria->addCondition('id_auction = '. $_POST['Bid']['id_auction']. ' AND price_per_unit <'.$_POST['Bid']['price_per_unit']);
                        $bidders_criteria->order = 'price_per_unit DESC';
                        $bidders_for_auction = OrangelotsBiddingOn::model()->findAll($bidders_criteria);
                        if (isset($bidders_for_auction)) {
                            foreach($bidders_for_auction AS $key_bid => $value_bid){
                                $notification_overbid = new OrangelotsNotifications();
                                $notification_overbid->user_id = $value_bid->id_user;
                                $notification_overbid->message_id = OrangelotsNotifications::OVERBID;
                                $notification_overbid->auction_id =$_POST['Bid']['id_auction'];
                                $notification_overbid->content = "".OrangelotsUsers::model()->findByPk(yii()->user->id)->firstname.", ".OrangelotsStates::model()->findBypk(OrangelotsUsers::model()->findByPk(yii()->user->id)->state)->state_abbreviation." overbid you on  <a href = '".createUrl('auctions/orangelots-auctions/bidding/'.OrangelotsAuctions::model()->findByPk($_POST['Bid']['id_auction'])->id)."'>".OrangelotsAuctions::model()->findByPk($_POST['Bid']['id_auction'])->name."</a> - new bid is $".$_POST['Bid']['price_per_unit']."";
                                $notification_overbid->save();
                            }
                        }

                        echo json_encode(
                            array(
                                'status' => 'bidding_save',
                                'html' => $this->renderPartial(
                                        'application.modules.user.views.user.bid_save',
                                        array('model' => OrangelotsUsers::model()->findByPk(yii()->user->id)),
                                        true,
                                        true
                                    )
                            )
                        );

                    }
                } else {
                    echo json_encode(
                        array(
                            'status' => 'some_error',
                            'html' => 'Incorrect Value'
                        )
                    );

                }
            }
        }
    }

    public function actionBidding($id = null)
    {
        $visible_search = 'hide';
        $criteria = new CDbCriteria;
        $criteria->compare('id_user', yii()->user->id);
        if(!empty($id)){
            $criteria->compare('id_auction', $id);
        }
        if (isset($_POST['Filter']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-category-grid')){
            if(isset($_POST['Filter'])){
                $_SESSION['Filter'] = $_POST['Filter'];
            }
            if(($_SESSION['Filter']['posted_by__form_row_checkers_radio']) != 1) {
                if((isset($_SESSION['Filter']['name'])) && ($_SESSION['Filter']['name'] != '')) {

                    if(ctype_digit($_SESSION['Filter']['name'])){
                        $criteria->addCondition('t.id_auction =' .$_SESSION['Filter']['name']);
                    } else {
                        $array_with[] = 'auction';
                        $criteria->with = array_unique($array_with);
                        $criteria->addCondition('auction.name LIKE "%' .$_SESSION['Filter']['name'].'%"');
                    }

                }

                if((isset($_SESSION['Filter']['status'])) && ($_SESSION['Filter']['status'] != 'empty')) {
                    $array_with[] = 'auction';
                    $criteria->with = array_unique($array_with);
                    $criteria->compare('auction.status' , $_SESSION['Filter']['status']);
                }
                if((isset($_SESSION['Filter']['end'])) && ($_SESSION['Filter']['end'] != 'empty')) {
                    $array_with[] = 'auction';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('(('.(time()-24*60*60*$_SESSION['Filter']['end']).' - auction.end_date_unix)'.'<='.(24*60*60*$_SESSION['Filter']['end']).') AND ('.(time()-24*60*60*$_SESSION['Filter']['end']).' - auction.end_date_unix >= 0 )');
                }

                // Advanced Search
                if((isset($_SESSION['Filter']['start_date_from'])) && ($_SESSION['Filter']['start_date_from'] != '')) {
                    $from = CDateTimeParser::parse($_SESSION['Filter']['start_date_from'], 'yyyy/MM/dd');
                    $array_with[] = 'auction';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction.start_date_unix >= '.$from.'');
                    $visible_search = 'block';
                }

                if((isset($_SESSION['Filter']['start_date_to'])) && ($_SESSION['Filter']['start_date_to'] != '')) {
                    $to = CDateTimeParser::parse($_SESSION['Filter']['start_date_to'], 'yyyy/MM/dd');
                    $array_with[] = 'auction';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction.start_date_unix <='.$to);
                    $visible_search = 'block';
                }

                if((isset($_SESSION['Filter']['end_date_from'])) && ($_SESSION['Filter']['end_date_from'] != '')) {
                    $from = CDateTimeParser::parse($_SESSION['Filter']['end_date_from'], 'yyyy/MM/dd');
                    $array_with[] = 'auction';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction.end_date_unix >= '.$from.'');
                    $visible_search = 'block';
                }

                if((isset($_SESSION['Filter']['end_date_to'])) && ($_SESSION['Filter']['end_date_to'] != '')) {
                    $to = CDateTimeParser::parse($_SESSION['Filter']['end_date_to'], 'yyyy/MM/dd');
                    $array_with[] = 'auction';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction.end_date_unix <='.$to);
                    $visible_search = 'block';
                }

                if((isset($_SESSION['Filter']['count_bids_from'])) && ($_SESSION['Filter']['count_bids_from'] != '')) {
                    $from = $_SESSION['Filter']['count_bids_from'];
                    $criteria->addCondition('(SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction=t.id_auction GROUP BY id_auction)>='.$from.'');
                    $visible_search = 'block';
                }

                if((isset($_SESSION['Filter']['count_bids_to'])) && ($_SESSION['Filter']['count_bids_to'] != '')) {
                    $to = $_SESSION['Filter']['count_bids_to'];
                    $criteria->addCondition('(SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction=t.id_auction GROUP BY id_auction) <='.$to.'');
                    $visible_search = 'block';
                }

                if((isset($_SESSION['Filter']['current_bid_from'])) && ($_SESSION['Filter']['current_bid_from'] != '')) {
                    $from = $_SESSION['Filter']['current_bid_from'];
//                $criteria->addCondition('(t.price_per_unit >='.$from.')');
                    $criteria->addCondition('(t.price_per_unit = (SELECT max(price_per_unit) FROM orangelots_bidding_on WHERE id_auction = t.id_auction)) AND (t.price_per_unit>='.$from.')');
                    $visible_search = 'block';
                }

                if((isset($_SESSION['Filter']['current_bid_to'])) && ($_SESSION['Filter']['current_bid_to'] != '')) {
                    $to = $_SESSION['Filter']['current_bid_to'];
//                $criteria->addCondition('(t.price_per_unit <='.$to.')');
                    $criteria->addCondition('(t.price_per_unit = (SELECT max(price_per_unit) FROM orangelots_bidding_on WHERE id_auction = t.id_auction)) AND (t.price_per_unit<='.$to.')');
                    $visible_search = 'block';
                }

                if((isset($_SESSION['Filter']['posted_by__form_row_checkers_radio'])) && ($_SESSION['Filter']['posted_by__form_row_checkers_radio']) == 3) {
                    $array_with[] = 'winners';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('winners.user_id ='.yii()->user->id);
                }
                if((isset($_SESSION['Filter']['posted_by__form_row_checkers_radio'])) && ($_SESSION['Filter']['posted_by__form_row_checkers_radio']) == 1) {
//                $criteria->addCondition('LEFT JOIN save_for_later sfl ON t.id_auction = sfl.auction_id WHERE sfl.user_id = t.id_user AND t.id_auction = sfl.auction_id');
                    $criteria->join = 'LEFT JOIN save_for_later sfl ON t.id_auction = sfl.auction_id';
                    $criteria->addCondition('sfl.user_id = t.id_user AND t.id_auction = sfl.auction_id');
                }

            }
        }else{
            unset($_SESSION['Filter']);
        }
        if((isset($_SESSION['Filter']['posted_by__form_row_checkers_radio'])) && ($_SESSION['Filter']['posted_by__form_row_checkers_radio']) == 1) {

            $criteria = $this->saved_for_later($_POST);
            $dataProvider = new CActiveDataProvider('SaveForLater', array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 10,
                ),
            ));

        } else {
            $dataProvider = new CActiveDataProvider('OrangelotsBiddingOn', array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 10,

                ),
            ));
        }

        if((isset($_SESSION['Filter']['posted_by__form_row_checkers_radio'])) && ($_SESSION['Filter']['posted_by__form_row_checkers_radio']) == 1) {
            $this->render(
                'saved_for_later',
                array(
                    'dataProvider' => $dataProvider,
                    'data' => $_SESSION['Filter'],
                    'visible' => $visible_search,
                )
            );
        } else {
            $this->render(
                'bidding',
                array(
                    'dataProvider' => $dataProvider,
                    'data' => $_SESSION['Filter'],
                    'visible' => $visible_search,
                    'auId' => $id
                )
            );
        }
    }

    public function actionPostedByMe($id=null)
    {
        $visible_search = 'hide';
        $criteria = new CDbCriteria;
        $array_with = array();
        if (isset($_POST['Filter']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-category-grid')){
            if(isset($_POST['Filter'])){
                $_SESSION['Filter'] = $_POST['Filter'];
            }
            if((isset($_SESSION['Filter']['name'])) && ($_SESSION['Filter']['name'] != '')) {
                if(ctype_digit($_SESSION['Filter']['name'])){
                    $criteria->addCondition('t.id =' .$_SESSION['Filter']['name']);
                } else {
                    $criteria->addCondition('t.name LIKE "%' .$_SESSION['Filter']['name'].'%"');
                }

            }
            if((isset($_SESSION['Filter']['bid_id_email_name'])) && ($_SESSION['Filter']['bid_id_email_name'] != '')) {
                $array_with[] = 'users';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('users.id = "' .$_SESSION['Filter']['bid_id_email_name'].'" OR users.email LIKE "%'.$_SESSION['Filter']['bid_id_email_name'].'" OR users.firstname LIKE "%'.$_SESSION['Filter']['bid_id_email_name'].'%"');
            }

            if((isset($_SESSION['Filter']['status'])) && ($_SESSION['Filter']['status'] != 'empty')) {
                $criteria->compare('t.status' , $_SESSION['Filter']['status']);
            }

            if((isset($_SESSION['Filter']['end'])) && ($_SESSION['Filter']['end'] != 'empty')) {
                $criteria->addCondition('(('.(time()-24*60*60*$_SESSION['Filter']['end']).' - t.end_date_unix)'.'<='.(24*60*60*$_SESSION['Filter']['end']).') AND ('.(time()-24*60*60*$_SESSION['Filter']['end']).' - t.end_date_unix >= 0 )');
            }
            if((isset($_SESSION['Filter']['start'])) && ($_SESSION['Filter']['start'] != 'empty')) {
                $criteria->addCondition('t.start_date_unix >= '.time().' AND '.'t.start_date_unix <='.(time()+24*60*60*$_POST['Filter']['start']));
            }

            // Advanced Search
            if((isset($_SESSION['Filter']['start_date_from'])) && ($_SESSION['Filter']['start_date_from'] != '') ) {
                $from = CDateTimeParser::parse($_SESSION['Filter']['start_date_from'], 'yyyy/MM/dd');
                $criteria->addCondition('t.start_date_unix >= '.$from.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['start_date_to'])) && ($_SESSION['Filter']['start_date_to'] != '')) {
                $to = CDateTimeParser::parse($_SESSION['Filter']['start_date_to'], 'yyyy/MM/dd');
                $criteria->addCondition('t.start_date_unix <='.$to);
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['end_date_from'])) && ($_SESSION['Filter']['end_date_from'] != '')) {
                $from = CDateTimeParser::parse($_SESSION['Filter']['end_date_from'], 'yyyy/MM/dd');
                $criteria->addCondition('t.end_date_unix >= '.$from.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['end_date_to'])) && ($_SESSION['Filter']['end_date_to'] != '')) {
                $to = CDateTimeParser::parse($_SESSION['Filter']['end_date_to'], 'yyyy/MM/dd');
                $criteria->addCondition('t.end_date_unix <='.$to);
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['count_bids_from'])) && ($_SESSION['Filter']['count_bids_from'] != '')) {
                $array_with[] = 'bidding';
                $criteria->with = array_unique($array_with);
                $from = $_SESSION['Filter']['count_bids_from'];
                $criteria->addCondition('(((SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction = t.id))>='.$from.')');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['count_bids_to'])) && ($_SESSION['Filter']['count_bids_to'] != '')) {
                $array_with[] = 'bidding';
                $criteria->with = array_unique($array_with);
                $to = $_SESSION['Filter']['count_bids_to'];
                $criteria->addCondition('(((SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction = t.id)) <='.$to.')');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['current_bid_from'])) && ($_SESSION['Filter']['current_bid_from'] != '')) {
                $array_with[] = 'bidding';
                $criteria->with = array_unique($array_with);
                $from = $_SESSION['Filter']['current_bid_from'];
                $criteria->addCondition('(SELECT MAX(price_per_unit) FROM orangelots_bidding_on WHERE id_auction = t.id) >='.$from.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['current_bid_to'])) && ($_SESSION['Filter']['current_bid_to'] != '')) {
                $array_with[] = 'bidding';
                $criteria->with = array_unique($array_with);
                $to = $_SESSION['Filter']['current_bid_to'];
                $criteria->addCondition('(SELECT MAX(price_per_unit) FROM orangelots_bidding_on WHERE id_auction = t.id) <='.$to.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['posted_by__form_row_checkers_radio'])) && ($_SESSION['Filter']['posted_by__form_row_checkers_radio'] == '2')) {
                $criteria->addCondition('(SELECT COUNT(id) FROM orangelots_winners ow WHERE ow.auction_id = t.id) > 0');
            }
        }else{
            unset($_SESSION['Filter']);
        }
        if(isset($id) and !empty($id)){
            $criteria->compare('t.id', $id);
        }
        $criteria->compare('t.is_flashdeal', '0');
        $criteria->compare('t.user_id', yii()->user->id);
        $criteria->compare('t.active', '1');
        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));


        $this->render(
            'posted_by_me',
            array(
                'dataProvider' => $dataProvider,
                'data' => $_SESSION['Filter'],
                'visible' => $visible_search,
                'auId' => $id
            )
        );
    }

    public function actionBidders($id = null)
    {
        $visible_search = 'hide';

        $criteria = new CDbCriteria;
        $array_with = array();
        if (isset($_POST['Filter']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-category-grid')){
            if(isset($_POST['Filter'])){
                $_SESSION['Filter'] = $_POST['Filter'];
            }
            if((isset($_SESSION['Filter']['name'])) && ($_SESSION['Filter']['name'] != '')) {

                if(ctype_digit($_SESSION['Filter']['name'])){
                    $criteria->addCondition('t.id_auction =' .$_SESSION['Filter']['name']);
                } else {
                    $array_with[] = 'auction';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction.name LIKE "%' .$_SESSION['Filter']['name'].'%"');
                }

            }
            if((isset($_SESSION['Filter']['status'])) && ($_SESSION['Filter']['status'] != 'empty')) {
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->compare('auction.status' , $_SESSION['Filter']['status']);
            }
            if((isset($_SESSION['Filter']['end'])) && ($_SESSION['Filter']['end'] != 'empty')) {
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('(('.(time()-24*60*60*$_SESSION['Filter']['end']).' - auction.end_date_unix)'.'<='.(24*60*60*$_SESSION['Filter']['end']).') AND ('.(time()-24*60*60*$_SESSION['Filter']['end']).' - auction.end_date_unix >= 0 )');
            }
            // Advanced Search
            if((isset($_SESSION['Filter']['start_date_from'])) && ($_SESSION['Filter']['start_date_from'] != '')) {
                $from = CDateTimeParser::parse($_SESSION['Filter']['start_date_from'], 'yyyy/MM/dd');
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction.start_date_unix >= '.$from);
                $visible_search = 'block';
            }
            if((isset($_SESSION['Filter']['start_date_to'])) && ($_SESSION['Filter']['start_date_to'] != '')) {
                $to = CDateTimeParser::parse($_SESSION['Filter']['start_date_to'], 'yyyy/MM/dd');
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction.start_date_unix <='.$to);
                $visible_search = 'block';
            }


            if((isset($_SESSION['Filter']['end_date_from'])) && ($_SESSION['Filter']['end_date_from'] != '')) {
                $from = CDateTimeParser::parse($_SESSION['Filter']['end_date_from'], 'yyyy/MM/dd');
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction.end_date_unix >= '.$from);
                $visible_search = 'block';
            }
            if((isset($_SESSION['Filter']['end_date_to'])) && ($_SESSION['Filter']['end_date_to'] != '')) {
                $to = CDateTimeParser::parse($_SESSION['Filter']['end_date_to'], 'yyyy/MM/dd');
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction.end_date_unix <='.$to);
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['count_bids_from'])) && ($_SESSION['Filter']['count_bids_from'] != '')) {
                $from = $_SESSION['Filter']['count_bids_from'];
                $criteria->addCondition('(((SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction = t.id))>='.$from.')');
                $visible_search = 'block';
            }
            if((isset($_SESSION['Filter']['count_bids_to'])) && ($_SESSION['Filter']['count_bids_to'] != '')) {
                $to = $_SESSION['Filter']['count_bids_to'];
                $criteria->addCondition('(((SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction = t.id)) <='.$to.')');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['current_bid_from'])) && ($_SESSION['Filter']['current_bid_from'] != '')) {
                $from = $_SESSION['Filter']['current_bid_from'];
                $criteria->addCondition('(SELECT price_per_unit FROM orangelots_bidding_on WHERE id_auction = t.id AND price_per_unit >='.$from);
                $visible_search = 'block';
            }
            if((isset($_SESSION['Filter']['current_bid_to'])) && ($_SESSION['Filter']['current_bid_to'] != '')) {
                $to = $_SESSION['Filter']['current_bid_to'];
                $criteria->addCondition('(SELECT price_per_unit FROM orangelots_bidding_on WHERE id_auction = t.id AND price_per_unit <='.$to.')');
                $visible_search = 'block';
            }
            if((isset($_SESSION['Filter']['posted_by__form_row_checkers_radio'])) && ($_SESSION['Filter']['posted_by__form_row_checkers_radio'] == '2')) {
                $criteria->addCondition('(SELECT COUNT(id) FROM orangelots_winners ow WHERE ow.auction_id = t.id_auction) > 0');
            }

        }else{
            unset($_SESSION['Filter']);
        }
        if(isset($_REQUEST['sort'])){
            $criteria->order = $_REQUEST["sort"].' DESC' ;
        }
        if(isset($id)){
            $auction = OrangelotsAuctions::model()->findByPk($id);
            if(!empty($auction) and $auction->user_id != user()->id){
                throw new CHttpException('400', 'Bad request');
            }
            $criteria->compare('id_auction', $id);
        }else{
            $criteria->compare('id_owner', user()->id);
            $criteria->order = 'datetime ASC';
        }
        $dataProvider = new CActiveDataProvider('OrangelotsBiddingOn', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $this->render(
            'bidders',
            array(
                'dataProvider' => $dataProvider,
                'data' => $_SESSION['Filter'],
                'visible' => $visible_search,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return OrangelotsAuctions the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = OrangelotsAuctions::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param OrangelotsAuctions $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'orangelots-auctions-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function clearNotActiveAuctions($id)
    {
        OrangelotsAuctions::model()->deleteAllByAttributes(array('user_id' => $id, 'active' => '0', 'import'=>0));
    }

    public function forward_yii($route, $exit = false)
    {
        $routeArr = explode("/", $route);
        if (isset($routeArr[(count($routeArr) - 1)])) {
            $this->auctions = $routeArr[(count($routeArr) - 1)];
        }

        parent::forward($routeArr[(count($routeArr) - 2)], $exit);

    }
    public function actionSaveForLater()
    {
        if(isset($_POST['Save'])){
            $saveforlater = SaveForLater::model()->findByAttributes(array('user_id'=>$_POST['Save']['user_id'], 'auction_id' =>$_POST['Save']['auction']));
            if (!isset($saveforlater)){
                $saveforlater = new SaveForLater;
            }
            $saveforlater->user_id = $_POST['Save']['user_id'];
            $saveforlater->auction_id = $_POST['Save']['auction'];
            if ($saveforlater->save()) {
                echo 'ok';
            } else {
                echo 'error';
            };
        }
    }
    public function actionUnSaveForLater()
    {
        if(isset($_POST['Save'])){
            $saveforlater = SaveForLater::model()->findByAttributes(array('user_id'=>$_POST['Save']['user_id'], 'auction_id' =>$_POST['Save']['auction']));
            if (isset($saveforlater)){
                if ($saveforlater->delete()) {
                    echo 'ok';
                } else {
                    echo 'error';
                };
            }

        }
    }
    public function actionDismiss()
    {
        if(isset($_POST['Dismiss'])){
            $sql = "UPDATE orangelots_notifications SET reading = '1' WHERE id = :id";
            $command = yii()->db->createCommand($sql);
            if($command->execute(array(':id' => $_POST['Dismiss']['id']))){
                echo 'ok';
            } else {
                echo 'error';
            }
        }
    }

    public function actionAskRender(){
        if(yii()->request->isAjaxRequest){
            $auction = $_POST['auction'];
            $model = new AuctionQuestion();
            $this->renderPartial('question_popup', array('model' => $model,'auctionId' => $auction), false, true);
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

    public function actionAskQuestion(){
        if(yii()->request->isAjaxRequest){
            $model = new AuctionQuestion();
            if(user()->isGuest){
                $model->setScenario('guestQuestion');
            }
            $this->performQuestionFormValidation($model);
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

    public function actionSaveQuestion(){
        if(yii()->request->isAjaxRequest){
            if(isset($_POST['AuctionQuestion'])){
                $auctionId = $_POST['AuctionQuestion']['auction_id'];
                $respArr = array();
                $respArr['err'] = 1;
                if(OrangelotsAuctions::checkExist($auctionId)){
                    $question = new AuctionQuestion();
                    $question->date_create = time();
                    $question->attributes = $_POST['AuctionQuestion'];
                    if(user()->isGuest){
                        $question->setScenario('guestQuestion');
                    }else{
                        $question->user_id = user()->id;
                    }
                    $question->owner_id = OrangelotsAuctions::getOwner($auctionId);
                    if($question->validate()){
                        $respArr['err'] = 0;
                        $question->save();
                    }else{
                        $errArray = $question->getErrors();
                        foreach ($errArray as $attr => $error){
                            $respArr[$attr] = $error[0];
                        }
                    }
                }
                echo CJSON::encode($respArr);
            }
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

    protected function performQuestionFormValidation($model){
        if (isset($_POST['ajax'])) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionSaveAnswer($id){
        if(yii()->request->isAjaxRequest){
            if(isset($_POST['AuctionQuestion']) and isset($_POST['auction_id'])){
                $respArr = array();
                $respArr['err'] = 1;
                if(OrangelotsAuctions::isOwner($_POST['auction_id'])){
                    $model = AuctionQuestion::getModel($id);
                    if(!empty($model)){
                        $model->setScenario('answer');
                        $model->answer = $_POST['AuctionQuestion']['answer'];
                        $model->date_answer = time();
                        $model->archive = AuctionQuestion::ARCHIVE;
                        $model->setScenario('answerQuestion');
                        if($model->validate()){
                            $respArr['err'] = 0;
                            $model->save();
                            $respArr['text'] = $model->answer;
                        }else{
                            $respArr['errText'] = $model->getError('answer');
                        }
                    }
                }else{
                    $respArr['errText'] = 'You can not answer on this question, because this auction is not your auction.';
                }
                echo CJSON::encode($respArr);
            }
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

    function saved_for_later($data){
        $criteria = new CDbCriteria;
        $criteria->compare('t.user_id', yii()->user->id);
        if (isset($data['Filter'])){

            if((isset($data['Filter']['name'])) && ($data['Filter']['name'] != '')) {

                if(ctype_digit($data['Filter']['name'])){
                    $criteria->addCondition('t.auction_id =' .$data['Filter']['name']);
                } else {
                    $array_with[] = 'auction';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction.name LIKE "%' .$data['Filter']['name'].'%"');
                }

            }

            if((isset($data['Filter']['status'])) && ($data['Filter']['status'] != 'empty')) {
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->compare('auction.status' , $data['Filter']['status']);
            }
            if((isset($data['Filter']['end'])) && ($data['Filter']['end'] != 'empty')) {
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('(('.(time()-24*60*60*$data['Filter']['end']).' - auction.end_date_unix)'.'<='.(24*60*60*$data['Filter']['end']).') AND ('.(time()-24*60*60*$data['Filter']['end']).' - auction.end_date_unix >= 0 )');
            }

            // Advanced Search
            if((isset($data['Filter']['start_date_from'])) && ($data['Filter']['start_date_from'] != '')) {
                $from = CDateTimeParser::parse($data['Filter']['start_date_from'], 'yyyy/MM/dd');
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction.start_date_unix >= '.$from.'');
                $visible_search = 'block';
            }

            if((isset($data['Filter']['start_date_to'])) && ($data['Filter']['start_date_to'] != '')) {
                $to = CDateTimeParser::parse($data['Filter']['start_date_to'], 'yyyy/MM/dd');
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction.start_date_unix <='.$to);
                $visible_search = 'block';
            }

            if((isset($data['Filter']['end_date_from'])) && ($data['Filter']['end_date_from'] != '')) {
                $from = CDateTimeParser::parse($data['Filter']['end_date_from'], 'yyyy/MM/dd');
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction.end_date_unix >= '.$from.'');
                $visible_search = 'block';
            }

            if((isset($data['Filter']['end_date_to'])) && ($data['Filter']['end_date_to'] != '')) {
                $to = CDateTimeParser::parse($data['Filter']['end_date_to'], 'yyyy/MM/dd');
                $array_with[] = 'auction';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction.end_date_unix <='.$to);
                $visible_search = 'block';
            }

            if((isset($data['Filter']['count_bids_from'])) && ($data['Filter']['count_bids_from'] != '')) {
                $from = $data['Filter']['count_bids_from'];
                $criteria->addCondition('(SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction=t.auction_id GROUP BY id_auction)>='.$from.'');
                $visible_search = 'block';
            }

            if((isset($data['Filter']['count_bids_to'])) && ($data['Filter']['count_bids_to'] != '')) {
                $to = $data['Filter']['count_bids_to'];
                $criteria->addCondition('(SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction=t.auction_id GROUP BY id_auction) <='.$to.'');
                $visible_search = 'block';
            }

            if((isset($data['Filter']['current_bid_from'])) && ($data['Filter']['current_bid_from'] != '')) {
                $from = $data['Filter']['current_bid_from'];
                $criteria->addCondition('t.auction_id = (SELECT id_auction FROM orangelots_bidding_on ob WHERE ob.price_per_unit = (SELECT max(obo.price_per_unit) FROM orangelots_bidding_on obo WHERE obo.id_auction = t.auction_id) AND ob.price_per_unit>='.$from.')');
                $visible_search = 'block';
            }

            if((isset($data['Filter']['current_bid_to'])) && ($data['Filter']['current_bid_to'] != '')) {
                $to = $data['Filter']['current_bid_to'];
                $criteria->addCondition('t.auction_id = (SELECT id_auction FROM orangelots_bidding_on ob WHERE ob.price_per_unit = (SELECT max(obo.price_per_unit) FROM orangelots_bidding_on obo WHERE obo.id_auction = t.auction_id) AND ob.price_per_unit<='.$to.')');
                $visible_search = 'block';
            }

        }
        return $criteria;
    }

    public function actionSales($id=null)
    {
        $visible_search = 'hide';
        $criteria = new CDbCriteria;
        $array_with = array();
        if (isset($_POST['Filter']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-category-grid')){
            if(isset($_POST['Filter'])){
                $_SESSION['Filter'] = $_POST['Filter'];
            }
            if((isset($_SESSION['Filter']['name'])) && ($_SESSION['Filter']['name'] != '')) {
                if(ctype_digit($_SESSION['Filter']['name'])){
                    $array_with[] = 'auction_deal';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction_deal.id ='.$_SESSION['Filter']['name']);
                } else {
                    $array_with[] = 'auction_deal';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction_deal.name LIKE "%' .$_SESSION['Filter']['name'].'%"');
                }

            }
            if((isset($_SESSION['Filter']['status'])) && ($_SESSION['Filter']['status'] != 'empty')) {
                $array_with[] = 'auction_deal';
                $criteria->with = array_unique($array_with);
                $criteria->compare('auction_deal.status' , $_SESSION['Filter']['status']);
            }

            if((isset($_SESSION['Filter']['end'])) && ($_SESSION['Filter']['end'] != 'empty')) {
                $array_with[] = 'auction_deal';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('(('.(time()-24*60*60*$_SESSION['Filter']['end']).' - auction_deal.end_date_unix)'.'<='.(24*60*60*$_SESSION['Filter']['end']).') AND ('.(time()-24*60*60*$_SESSION['Filter']['end']).' - auction_deal.end_date_unix >= 0 )');
            }
            if((isset($_SESSION['Filter']['start'])) && ($_SESSION['Filter']['start'] != 'empty')) {
                $array_with[] = 'auction_deal';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('auction_deal.start_date_unix >= '.time().' AND '.'auction_deal.start_date_unix <='.(time()+24*60*60*$_SESSION['Filter']['start']));
            }
            // Advanced Search
            if((isset($_SESSION['Filter']['start_date_from'])) && ($_SESSION['Filter']['start_date_from'] != '') ) {
                $array_with[] = 'auction_deal';
                $criteria->with = array_unique($array_with);
                $from = CDateTimeParser::parse($_SESSION['Filter']['start_date_from'], 'yyyy/MM/dd');
                $criteria->addCondition('auction_deal.start_date_unix >= '.$from.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['start_date_to'])) && ($_SESSION['Filter']['start_date_to'] != '')) {
                $array_with[] = 'auction_deal';
                $criteria->with = array_unique($array_with);
                $to = CDateTimeParser::parse($_SESSION['Filter']['start_date_to'], 'yyyy/MM/dd');
                $criteria->addCondition('auction_deal.start_date_unix <='.$to);
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['end_date_from'])) && ($_SESSION['Filter']['end_date_from'] != '')) {
                $array_with[] = 'auction_deal';
                $criteria->with = array_unique($array_with);
                $from = CDateTimeParser::parse($_SESSION['Filter']['end_date_from'], 'yyyy/MM/dd');
                $criteria->addCondition('auction_deal.end_date_unix >= '.$from.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['end_date_to'])) && ($_SESSION['Filter']['end_date_to'] != '')) {
                $array_with[] = 'auction_deal';
                $criteria->with = array_unique($array_with);
                $to = CDateTimeParser::parse($_SESSION['Filter']['end_date_to'], 'yyyy/MM/dd');
                $criteria->addCondition('auction_deal.end_date_unix <='.$to);
                $visible_search = 'block';
            }
        }else{
            unset($_SESSION['Filter']);
        }

        $array_with[] = 'auction_deal';
        $criteria->with = array_unique($array_with);
        $criteria->compare('auction_deal.is_flashdeal', '1');
        $criteria->compare('auction_deal.active', '1');
        if((isset($id)) && ($id!=null))
            $criteria->compare('auction_deal_id', $id);

        $criteria->compare('t.owner_id', yii()->user->id);
        $criteria->addCondition('t.payed != -1');

        $dataProvider = new CActiveDataProvider('OrangelotsInvoices', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $this->render(
            'sales',
            array(
                'dataProvider' => $dataProvider,
                'data' => $_SESSION['Filter'],
                'visible' => $visible_search,
            )
        );

    }

    public function actionDeal()
    {
        $visible_search = 'hide';
        $criteria = new CDbCriteria;
        $array_with = array();
        if (isset($_POST['Filter']) || (isset($_GET['ajax']) and $_GET['ajax'] =='orangelots-category-grid')){
            if(isset($_POST['Filter'])){
                $_SESSION['Filter'] = $_POST['Filter'];
            }
            if((isset($_SESSION['Filter']['name'])) && ($_SESSION['Filter']['name'] != '')) {
                if(ctype_digit($_SESSION['Filter']['name'])){
                    $array_with[] = 'auction_deal';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction_deal.id ='.$_SESSION['Filter']['name']);
                } else {
                    $array_with[] = 'auction_deal';
                    $criteria->with = array_unique($array_with);
                    $criteria->addCondition('auction_deal.name LIKE "%' .$_SESSION['Filter']['name'].'%"');
                }

            }
            if((isset($_SESSION['Filter']['bid_id_email_name'])) && ($_SESSION['Filter']['bid_id_email_name'] != '')) {
                $array_with[] = 'owner';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('owner.lastname LIKE "%'.$_SESSION['Filter']['bid_id_email_name'].'" OR owner.firstname LIKE "%'.$_SESSION['Filter']['bid_id_email_name'].'%" OR owner.company LIKE "%'.$_SESSION['Filter']['bid_id_email_name'].'%"');
            }

            if((isset($_SESSION['Filter']['status'])) && ($_SESSION['Filter']['status'] != 'empty')) {
                $array_with[] = 'auction_deal';
                $criteria->with = array_unique($array_with);
                $criteria->compare('auction_deal.status' , $_SESSION['Filter']['status']);
            }
        }else{
            unset($_SESSION['Filter']);
        }

        $array_with[] = 'auction_deal';
        $criteria->with = array_unique($array_with);
        $criteria->compare('auction_deal.is_flashdeal', '1');
        $criteria->compare('auction_deal.active', '1');

        $criteria->compare('t.winner_id', yii()->user->id);
        $criteria->addCondition('t.payed != -1');

        $dataProvider = new CActiveDataProvider('OrangelotsInvoices', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));


        $this->render(
            'deal',
            array(
                'dataProvider' => $dataProvider,
                'data' => $_POST['Filter'],
                'visible' => $visible_search,
            )
        );

    }
    public function actionWatchlist(){
        if(yii()->request->isAjaxRequest){
            if (yii()->user->isGuest) {
                echo json_encode(
                    array(
                        'status' => 'error',
                        'url' => '',
                        'html' => $this->renderPartial(
                                'application.modules.user.views.user.login_popup',
                                array('model' => new OrangelotsUsers),
                                true,
                                true
                            )
                    )
                );
            }
            else {
                echo json_encode(
                    array(
                        'status' => 'ok',
                        'url' => '',
                        'href' => createUrl('auctions/orangelots-auctions/bidding'),
                    )
                );

            }
        } else {
            throw new CHttpException('400', 'Bad request');
        }


    }

    public function actionSellDeal($id=null)
    {
        $visible_search = 'hide';
        $criteria = new CDbCriteria;
        $array_with = array();
        if (isset($_POST['Filter']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-category-grid')){
            if(isset($_POST['Filter'])){
                $_SESSION['Filter'] = $_POST['Filter'];
            }
            if((isset($_SESSION['Filter']['name'])) && ($_SESSION['Filter']['name'] != '')) {
                if(ctype_digit($_SESSION['Filter']['name'])){
                    $criteria->addCondition('t.id =' .$_SESSION['Filter']['name']);
                } else {
                    $criteria->addCondition('t.name LIKE "%' .$_SESSION['Filter']['name'].'%"');
                }

            }
            if((isset($_SESSION['Filter']['bid_id_email_name'])) && ($_SESSION['Filter']['bid_id_email_name'] != '')) {
                $array_with[] = 'users';
                $criteria->with = array_unique($array_with);
                $criteria->addCondition('users.id = "' .$_SESSION['Filter']['bid_id_email_name'].'" OR users.email LIKE "%'.$_SESSION['Filter']['bid_id_email_name'].'" OR users.firstname LIKE "%'.$_SESSION['Filter']['bid_id_email_name'].'%"');
            }

            if((isset($_SESSION['Filter']['status'])) && ($_SESSION['Filter']['status'] != 'empty')) {
                $criteria->compare('t.status' , $_SESSION['Filter']['status']);
            }

            if((isset($_SESSION['Filter']['end'])) && ($_SESSION['Filter']['end'] != 'empty')) {
//                $criteria->addCondition('t.end_date_unix >='.(time()-24*60*60*$_POST['Filter']['end']));
                $criteria->addCondition('(('.(time()-24*60*60*$_SESSION['Filter']['end']).' - t.end_date_unix)'.'<='.(24*60*60*$_SESSION['Filter']['end']).') AND ('.(time()-24*60*60*$_SESSION['Filter']['end']).' - t.end_date_unix >= 0 )');
            }
            if((isset($_SESSION['Filter']['start'])) && ($_SESSION['Filter']['start'] != 'empty')) {
                $criteria->addCondition('t.start_date_unix >= '.time().' AND '.'t.start_date_unix <='.(time()+24*60*60*$_SESSION['Filter']['start']));
            }

            // Advanced Search
            if((isset($_SESSION['Filter']['start_date_from'])) && ($_SESSION['Filter']['start_date_from'] != '') ) {
                $from = CDateTimeParser::parse($_SESSION['Filter']['start_date_from'], 'yyyy/MM/dd');
                $criteria->addCondition('t.start_date_unix >= '.$from.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['start_date_to'])) && ($_SESSION['Filter']['start_date_to'] != '')) {
                $to = CDateTimeParser::parse($_SESSION['Filter']['start_date_to'], 'yyyy/MM/dd');
                $criteria->addCondition('t.start_date_unix <='.$to);
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['end_date_from'])) && ($_SESSION['Filter']['end_date_from'] != '')) {
                $from = CDateTimeParser::parse($_SESSION['Filter']['end_date_from'], 'yyyy/MM/dd');
                $criteria->addCondition('t.end_date_unix >= '.$from.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['end_date_to'])) && ($_SESSION['Filter']['end_date_to'] != '')) {
                $to = CDateTimeParser::parse($_SESSION['Filter']['end_date_to'], 'yyyy/MM/dd');
                $criteria->addCondition('t.end_date_unix <='.$to);
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['count_bids_from'])) && ($_SESSION['Filter']['count_bids_from'] != '')) {
                $array_with[] = 'bidding';
                $criteria->with = array_unique($array_with);
                $from = $_SESSION['Filter']['count_bids_from'];
                $criteria->addCondition('(((SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction = t.id))>='.$from.')');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['count_bids_to'])) && ($_SESSION['Filter']['count_bids_to'] != '')) {
                $array_with[] = 'bidding';
                $criteria->with = array_unique($array_with);
                $to = $_SESSION['Filter']['count_bids_to'];
                $criteria->addCondition('(((SELECT COUNT(id) FROM orangelots_bidding_on WHERE id_auction = t.id)) <='.$to.')');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['current_bid_from'])) && ($_SESSION['Filter']['current_bid_from'] != '')) {
                $array_with[] = 'bidding';
                $criteria->with = array_unique($array_with);
                $from = $_SESSION['Filter']['current_bid_from'];
                $criteria->addCondition('(SELECT MAX(price_per_unit) FROM orangelots_bidding_on WHERE id_auction = t.id) >='.$from.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['current_bid_to'])) && ($_SESSION['Filter']['current_bid_to'] != '')) {
                $array_with[] = 'bidding';
                $criteria->with = array_unique($array_with);
                $to = $_SESSION['Filter']['current_bid_to'];
                $criteria->addCondition('(SELECT MAX(price_per_unit) FROM orangelots_bidding_on WHERE id_auction = t.id) <='.$to.'');
                $visible_search = 'block';
            }

            if((isset($_SESSION['Filter']['posted_by__form_row_checkers_radio'])) && ($_SESSION['Filter']['posted_by__form_row_checkers_radio'] == '2')) {
                $criteria->addCondition('(SELECT COUNT(id) FROM orangelots_winners ow WHERE ow.auction_id = t.id) > 0');
            }
        }else{
            unset($_SESSION['Filter']);
        }
        $criteria->compare('t.is_flashdeal', '1');

        $criteria->compare('t.user_id', yii()->user->id);
        if(!empty($id)){
            $criteria->compare('t.id', $id);
        }
        $criteria->compare('t.active', '1');
        $dataProvider = new CActiveDataProvider('OrangelotsAuctions', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));

        $this->render(
            'posted_by_me_deals',
            array(
                'dataProvider' => $dataProvider,
                'data' => $_SESSION['Filter'],
                'visible' => $visible_search,
                'auId' => $id
            )
        );

    }

    public function actionGetForm()
    {
        if (yii()->request->isAjaxRequest) {
            if (isset($_POST['uniq'])) {
                $model = new ItemsForm();
                echo json_encode(
                    array(
                        'status' => 'OK',
                        'url' => '',
                        'html' => $this->renderPartial(
                            'application.modules.auctions.views.orangelotsAuctions._dynamicForm',
                            array(
                                'uniq'=>$_POST['uniq'],
                                'ajax' => false,
                                'model' => $model,
                            ),
                            true,
                            true
                        )
                    )
                );
            }
        }
    }
    public function actionDeleteItems()
    {
        if(yii()->request->isAjaxRequest){
            if(isset($_POST['record'])){
                $id = (int)$_POST['record'];
                if(isset($id)){
                    $itm = Items::model()->deleteByPk($id);
                    if($itm == 1){
                        echo "200";
                    } else {
                        echo "ERROR";
                    }
                }
            }
        }
    }

    public function saveItems($id, $r){
        if(isset($id) && (!empty($r))){
            foreach($r as $k=>$v){
                if(array_key_exists ('recordid',$v))
                    $frm = Items::model()->findByPk($v['recordid']);
                else
                    $frm = new Items();

                $frm->attributes = $v;
                $frm->auction_deal_id = $id;
                $frm->save();
            }
        }
    }


}
