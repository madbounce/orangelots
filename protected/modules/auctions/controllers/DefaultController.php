<?php

class DefaultController extends Controller
{
    /**
     * Список фильтров
     * @return array
     */

    public $layout = '//layouts/main';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Правила доступа к действиям
     * @return array
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('index', 'create', 'update', 'delete', 'Attributes'),
                'roles' => array('admin', 'moderator','Realter'),
            ),
            array(
                /*'deny',
                'users' => array('*',),*/
            ),
        );
    }

    /**
     * Добавить отзыв
     */
    public function actionCreate()
    {
        $model = new Orangelots();

        $data = Yii::app()->getRequest()->getParam('Orangelots', null);
        if (!empty($data)) {
            $model->attributes = $data;
            $model->user_id = user()->id;
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($model->save()) {
                $this->redirect('index');
            }
        }
        $this->registerScriptFiles();
        $this->setPageTitle(yii()->name . ' - Добавить программу');
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Редактирование отзыв
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $model->scenario = 'updateAttributes';


        $data = Yii::app()->getRequest()->getParam('Realty', null);
        if (!empty($data)) {
            $model->attributes = $data;
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($model->save()) {
                $this->redirect('../index');
            }
        }

        $this->registerScriptFiles();
        $this->setPageTitle(yii()->name . ' - Редактировать программу');

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Удалить отзыв
     * @param integer $id
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id);
            $model->delete();

            $ajax = Yii::app()->request->getParam('ajax', null);
            if (empty($ajax)) {
                $returnUrl = Yii::app()->request->getParam('returnUrl', null);
                $this->redirect(!empty($returnUrl) ? $returnUrl : array('index'));
            }
        } else {
            throw new CHttpException(400, INVALID_REQUEST);
        }
    }

    /**
     * Показать все отзывы
     */
    public function actionIndex()
    {
        $model = new Orangelots('search');
        $user = OrangelotsUsers::model()->findByPk(yii()->user->id);
        $model->unsetAttributes();

        $data = Yii::app()->getRequest()->getParam('Orangelots', null);
        if (!empty($data)) {
            $model->attributes = $data;
        }
        $model->user_id = user()->id;
        $this->setPageTitle(Yii::app()->name . ' - Список учебных заведений');
        $this->render('index', array(
            'model' => $model,
            'user' => $user,
        ));
    }

    /**
     * Загружаем затребованную модель
     * @param integer
     */
    public function loadModel($id)
    {
        $model = Realty::model()->findByPk($id);
        if (empty($model)) {
            throw new CHttpException(404, REQUESTED_PAGE_DOES_NOT_EXIST);
        }
        return $model;
    }

    /**
     * Подключаем необходимые скрипты
     */
    private function registerScriptFiles()
    {
//		registerScriptFile(baseUrl() . '/admin/js/browser.video.js');
//		registerScriptFile(baseUrl() . '/admin/js/multipic.js');
    }

    public function getTabularTabs($model)
    {
        $tabs = array();

        $tabs[] = array(
            'active' => true,
            'label' => 'Программа',
            'content' => $this->renderPartial('parts/_form', array('model' => $model,), true),
        );

        $tabs[] = array(
            'active' => false,
            'label' => 'Фотогалерея',
            'content' => $this->renderPartial('parts/_photo', array('model' => $model,), true),
        );

        $tabs[] = array(
            'active' => false,
            'label' => 'Атрибуты',
            'content' => $this->renderPartial('parts/_attributes_form', array('model' => $model,), true),
        );

        return $tabs;
    }

    /**
     * Список атрибутов категории для формы обьявления
     */
    public function actionAttributes()
    {
        $category_id = Yii::app()->getRequest()->getParam('category_id');
        $this->renderPartial('parts/_attributes', array(
            'dataAttributes' => array(),
            'category_id' => $category_id,
        ));
    }
}
