<?php

class AuctionsModule extends CWebModule
{
	public $defaultController = "OrangelotsAuctions";


	public function init()
	{
		$this->setImport(array(
			'auctions.models.*',
			'auctions.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}