<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href="<?php echo yii()->createUrl("site/index")?>"></a></li>
            <li><a>Buy</a></li>
            <li><a>Deals</a></li>
        </ul>
    </div>
</div>

<div class="posted_by__container">
<div class="main_container">
<p class="posted_by__title">Search by</p>
<div class="posted_by__form_container clearfix">
    <form method="post" id="search_form">
        <div class="posted_by__form_left_side pull-left">
            <div class="posted_by__form_row">
                <div class="posted_by__form_element posted_by__form_element__name">
                    <label>Deal #, name</label>
                    <input type="text" name="Filter[name]" value="<?php if(isset($data['name'])):if(!empty($data['name'])): echo $data['name']; endif;endif; ?>">
                </div>

                <div class="posted_by__form_element posted_by__form_element__email">
                    <label>Vendor name</label>
                    <input type="text" name="Filter[bid_id_email_name]" value="<?php if(isset($data['bid_id_email_name'])):if(!empty($data['bid_id_email_name'])): echo $data['bid_id_email_name'];  endif;endif; ?>">
                </div>

                <div class="posted_by__form_element posted_by__form_element__status">
                    <label>Status</label>
                    <select class="selectpicker" name="Filter[status]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['status'])):if($data['status'] == 1): echo 'selected="selected"';  endif;endif; ?>>Open</option>
                        <option value="2"<?php if(isset($data['status'])):if($data['status'] == 2): echo 'selected="selected"';  endif;endif; ?>>Close</option>
                    </select>
                </div>
            </div>

            <button class="btn_image">
                <img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt="">
            </button>

            <div class="posted_by__form_row_checkers">
            </div>

        </div>

        <?php if ((user()->isVendor()) || (user()->isAdmin())):?>
        <div class="posted_by__form__right_side text-center pull-left">
            <a href="<?php echo createUrl('flashdeal/default/create')?>" class="btn btn_image">
                <img src="<?php echo baseUrl().'/images/btn/new_deal_btn.png'?>" alt="">
            </a>
        </div>
        <?php endif; ?>
    </form>
</div>
    <div class="posted_by__table_auction_list__container">
        <?php $this->widget(
            'MyGridView',
            array(
                'id' => 'orangelots-category-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => '',
                'columns' => array(
                    array(
                        'name' => 'auction_deal_id',
                        'header' => 'Deal#',
                        'type' => 'raw',
                        'sortable' => true,
                        'value' => '$data->auction_deal_id',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Name',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->name',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Vendor',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => '(OrangelotsUsers::model()->findByPk($data->owner_id)->company == "")?OrangelotsUsers::model()->findByPk($data->owner_id)->firstname." ".OrangelotsUsers::model()->findByPk($data->owner_id)->lastname:OrangelotsUsers::model()->findByPk($data->owner_id)->company',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Start Time',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->start_date_unix)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Close Time',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->end_date_unix)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Status',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsAuctionsStatus::model()->findByPk(OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->status)->name',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Price',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => '"$".OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->price_per_unit*OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->units_in_lot',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'transaction_id',
                        'header' => 'TransactionID',
                        'type' => 'raw',
                        'sortable' => false,
                        'htmlOptions'=>array('width'=>'70'),
                        'value' => 'CHtml::link($data->transaction_id, createUrl("user/buy/confirmation/", array("transaction" => $data->transaction_id)), array("class" => "orange_link"))'
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Sold Qty',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsWinners::model()->findByPk($data->winner_table_id)->qty',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Sold',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => '"$".OrangelotsWinners::model()->findByPk($data->winner_table_id)->qty*(OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->price_per_unit*OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->units_in_lot)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'purchase_date',
                        'header' => 'Purchase date',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", $data->purchase_date)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => '',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsAuctions::invoice_href($data->transaction_id)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                ),
                'pager' => array(
                    'htmlOptions' => array(
                        'class' => 'pager_lk'
                    )
                ),
                'itemsCssClass' => 'text-center',
            )
        ); ?>
    </div>
</div>
</div>


