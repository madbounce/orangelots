<?php
/* @var $this OrangelotsAuctionsController */
/* @var $model OrangelotsAuctions */
//echo '<image src = '. createUrl($model->galleryPhotos[0]->getOrigin()) .'>';
?>
<div class="breadcrumb_row clearfix">
    <div class="main_container">

        <?php
        $this->widget(
            'zii.widgets.CBreadcrumbs',
            array(
                'links' => array(
                    OrangelotsKind::model()->findByPK($model->category_id)->name,
                    $model->name,
                ),
                'separator' => '',
                'tagName' => 'ul',
                'htmlOptions' => array('class' => ''),
                'inactiveLinkTemplate' => '<li><a href="'. createUrl("orangelots/orangelots-kind/view/", array("alias" => OrangelotsKind::model()->findByPK($model->category_id)->alias)).'">{label}</a></li>',
                'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                        "site/index"
                    ) . '"></a></li>',
            )
        );
        ?>
    </div>
</div>


<div class="catalog_detail_container clearfix">
<div class="main_container">
    <p class="catalog_detail__title">
        <?php if ($model->is_flashdeal == 1): ?>
            Deal ID
        <?php else: ?>
            Auction ID
        <? endif; ?>
        <?php echo $model->id ?>
        - <?php echo $model->name ?>
        <span class="catalog_detail">(Original MSRP $<?php echo number_format($model->mvrp, 2) ?>)</span>
    <span class="catalog_detail">SKU:<?php echo $model->sku ?></span>

    <div class="row">
        <div class="main_collum">
            <div class="catalog_detail__image pull-left">
                <div class="catalog_detail__main_image">
                    <?php if(OrangelotsAuctions::main_photo($model)):?>
                        <? foreach ($model->galleryPhotos AS $key => $value): ?>
                            <? if ($value->set_main == 1): ?>
                                <img src="<?php echo createUrl($value->getThumb()) ?>" alt="">
                            <? endif; ?>
                        <? endforeach; ?>
                    <?php else:?>
                        <?php if(isset($model->galleryPhotos[0])):?>
                            <img src="<?php echo createUrl($model->galleryPhotos[0]->getThumb()) ?>" alt="">
                        <?php else:?>
                            <img src="<?php echo yii()->baseUrl.'/images/NIA.png' ?>" alt="">
                        <?php endif; ?>
                    <?php endif; ?>

                </div>

                <div class="catalog_detail__galery_image">
                    <div class="catalog_detail__galery_image__carousel" id="card_detail_carousel">
                        <ul>
                            <?php foreach ($model->galleryPhotos AS $key => $value): ?>
                                <li>
                                    <a href="">
                                        <img src="<?php echo createUrl($value->getThumb()) ?>" alt=""/>
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                    <div class="catalog_detail__galery_image__control">
                        <a href="" class="catalog_detail__galery_image__control__btn prev"></a>
                        <a href="" class="catalog_detail__galery_image__control__btn next"></a>
                    </div>
                </div>
            </div>
            <?php if ($model->is_flashdeal == 0):?>
            <div class="catalog_detail__price_container auction pull-left">
                <?php if ($model->status == 1):?>
                    <?if($canBid):?>
                        <p class="catalog_detail__price__units">Units in Lot:
                        <span><?php echo $model->units_in_lot ?></span></p>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($model->buy_now_price != 0):?>
                <?php if ($model->status == 1): ?>
                    <?if($canBid):?>
                    <p class="catalog_detail__price__numbers pull-left">Buy Now Price:
                        <span>$<?php echo number_format($model->buy_now_price, 2) ?></span>
                        <span class="catalog_detail__price__numbers__info">(Saving <?php echo number_format($model->saving_buy_now, 2) ?>
                            %)</span>
                    </p>
                        <?else:?>
                        <p class="pull-left">You have already bought <?php echo $winner->qty.' '.plural($winner->qty, 'units', 'unit', 'units')?> of this lot for $<?php echo $winner->bid->price_per_unit?> on total amount $<?php echo $winner->summary_price?>
                        </p>
                    <?php endif; ?>
                <?php endif; ?>

                <div class="catalog_detail__price__form">
                    <form>
                        <?php if ($model->status == 1): ?>
                            <?if($canBid):?>
                                <?php echo $select = OrangelotsAuctions::select_step($bid, $model,'qty_buy_now');?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <p class="text-center">
                            <?php if ($model->status == 1){ ?>
                            <?if($canBid):?>
                            <?php echo CHtml::ajaxSubmitButton(
                                'BUY NOW',
                                CHtml::normalizeUrl(array('orangelots-auctions/buy-now')),
                                array(
                                    'type' => 'post',
                                    'data' => array(
                                        'model' => $model->attributes,
                                        'qty' => 'js:$("div.catalog_detail__price__form button.btn.dropdown-toggle.selectpicker.btn-default").attr("title")'
                                    ),
                                    'success' => 'function(data) {
                                            var obj = JSON.parse(data);
                                            if (obj.status == "error") {
                                             $("body").append(obj.html);
                                            }
                                            else if (obj.status == "some_error") {
                                                alert(obj.html);
                                            }
                                            else if (obj.status == "bidding_save") {
                                                $("body").append(obj.html);
                                            }

                                        }',
                                ),
                                array(
                                    'class' => 'btn_buy_now',
                                    'type'=>'image',
                                    'src'=>yii()->baseUrl . '/images/btn/btn_buy_now.png',
                                )
                            );
                            ?>
                        <?endif?>
                        <div class="total_amount_buy_now"></div>
                    <?php } ?>
                    </p>
                </form>
                </div>
<?php endif; ?>
<?if($canBid):?>
<div class="catalog_detail__bind_container">
    <?php if (($model->buy_now_price != 0) && ($model->min_bid !=0)):?>
        <?php if($model->status == 1):?>
            <p class="catalog_detail__bind_of">-------- or --------</p>
        <?php endif;?>
    <?php endif;?>
    <?php if (($model->min_bid !=0)):?>
        <?php if($model->status == 1):?>
            <p class="catalog_detail__price__numbers pull-left">Place your Bid -
                <span>min. $<?php echo $model->min_bid ?></span>
                            <span
                                class="catalog_detail__price__numbers__info">Min Qty of units - <?php echo $model->min_unit ?>
                                (Saving <?php echo number_format($model->saving_min_bid, 2) ?>%)</span>
            </p>
        <?php endif; ?>


        <div class="clearfix"></div>
        <?php if($model->status == 1):?>

            <div class="catalog_detail__price__numbers__select_row">
                <div class="catalog_detail__price__numbers__select_row__select_container">
                    <?php if($model->status == 1):?>
                        <p class="catalog_detail__price__numbers__select_row__title">Units</p>
                        <?php echo $select = OrangelotsAuctions::select_step($bid, $model,'qty_unit');?>
                    <?php endif; ?>
                </div>
                <div class="catalog_detail__price__numbers__select_row__input_container">
                    <?php if($model->status == 1):?>
                        <p class="catalog_detail__price__numbers__select_row__title">Bid per Unit</p>
                        <input type="text" id="price_per_unit" value="<?php if(isset($bid->price_per_unit)): echo number_format($bid->price_per_unit, 2); endif; ?>"/>
                    <?php endif; ?>
                </div>
                <div class="catalog_detail__price__numbers__select_row__btn_container">
                    <?php if ($model->status == 1) { ?>
                        <?php echo CHtml::ajaxSubmitButton(
                            'place a bid',
                            CHtml::normalizeUrl(array('orangelots-auctions/place-bid')),
                            array(
                                'type' => 'post',
                                'data' => array(
                                    'Bid[qty]' => 'js:$("div.catalog_detail__price__numbers__select_row button.btn.dropdown-toggle.selectpicker.btn-default").attr("title")',
                                    'Bid[price_per_unit]' => 'js:$("#price_per_unit").val()',
                                    'Bid[id_auction]' => $model->id,

                                ),
                                'success' => 'function(data) {
                                            var obj = JSON.parse(data);
                                            if (obj.status == "error") {
                                             $("body").append(obj.html);
                                            }
                                            else if (obj.status == "some_error") {
                                                alert(obj.html);
                                            }
                                            else if (obj.status == "bidding_save") {
                                                $("body").append(obj.html);
                                            }

                                        }',
                            ),
                            array('class' => 'btn green_bnt')
                        );
                        ?>
                    <?php } ?>
                    <?php if($model->status == 1):?>
                        <?php if(!yii()->user->isGuest):?>
                            <?php echo CHtml::ajaxLink(
                                "remove from wishlist",
                                yii()->createUrl('auctions/orangelots-auctions/un-save-for-later'),
                                array(
                                    'type' => 'POST',
                                    'data' => array(
                                        'Save[auction]' => $model->id,
                                        'Save[user_id]' => yii()->user->id,
                                    ),
                                    'success' => "function(data){
                                if(data == 'ok'){
                                    $('#remove_from_whishlist_".$model->id."').hide();
                                    $('#save_for_later_".$model->id."').show();
                                    $('#remove_from_whishlist_sponsored_".$model->id."').hide();
                                    $('#save_for_later_sponsored_".$model->id."').show();
                                }
                        }",
                                ),
                                array(
                                    'id' => 'remove_from_whishlist_'.$model->id,
                                    'style' => ($link)?'display:block':'display:none',
                                )
                            );
                            ?>
                            <?php echo CHtml::ajaxLink(
                                "save for later",
                                yii()->createUrl('auctions/orangelots-auctions/save-for-later'),
                                array(
                                    'type' => 'POST',
                                    'data' => array(
                                        'Save[auction]' => $model->id,
                                        'Save[user_id]' => yii()->user->id,
                                    ),
                                    'success' => "function(data){
                                if(data == 'ok'){
                                    $('#save_for_later_".$model->id."').hide();
                                    $('#remove_from_whishlist_".$model->id."').show();
                                    $('#save_for_later_sponsored_".$model->id."').hide();
                                    $('#remove_from_whishlist_sponsored_".$model->id."').show();
                                }
                        }",
                                ),
                                array(
                                    'id' => 'save_for_later_'.$model->id,
                                    'style' => ($link)?'display:none':'display:block',

                                )
                            );
                            ?>
                        <?php endif ;?>
                    <?php endif ;?>
                </div>
                <?php if($model->status == 1):?>
                    <div class="total_amount_per_unit">Total Amount   <p class="price_amount">$<?php echo isset($bid->price_per_unit) ? number_format($bid->price_per_unit*$bid->qty, 2) :  "";?></p></div>
                    <?php $au_bids = OrangelotsBiddingOn::model()->findAllByAttributes(array('id_auction' => $model->id, 'buy_now' => 0), array('limit' => 3, 'order' => 'summary_price DESC'));?>
                    <ul class="catalog_detail__price__numbers__highest_binds_list">
                        <li class="catalog_detail__price__numbers__highest_binds_list__title"><?php echo count($au_bids)?> Highest Bids
                            (per unit)
                        </li>
                        <?php
                        if(!empty($au_bids)):
                            foreach($au_bids AS $key => $value):?>
                                <li>$<?php echo $value->price_per_unit?> - from <?php echo OrangelotsStates::getStateLabel(OrangelotsStates::getUserState($value->id_user))?></li>
                            <?php endforeach; endif;?>
                    </ul>
                <?php endif ;?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
</div>
<?endif;?>
<?php if($model->status == 1): ?>
    <div class="catalog_detail__price__time_ended">
        <p>End in:<span>
                                <?php echo $days = floor(($model->end_date_unix - time()) / (24 * 60 * 60)) ?> Day,
                <?php echo $hours = floor(($model->end_date_unix - time() - ($days * (24 * 60 * 60))) / (60 * 60)) ?>
                hours
                <?php echo $minutes = floor(
                    ($model->end_date_unix - time() - ($hours * (60 * 60)) - $days * (24 * 60 * 60)) / (60)
                ) ?> minutes</span></p>

        <p class="catalog_detail__price__time_ended__info">Closing Time: <?php echo yii()->dateFormatter->format(
                'MM/dd/yyyy hh:mm:ss a',
                $model->end_date_unix
            ); ?> EST</p>
    </div>

<?php else:?>
    <p class="catalog_detail__price__units" id="minus_margin">Units in lot:
        <span><?php echo $model->units_in_lot ?></span>
    </p>
    <p class="catalog_detail__price__numbers">Buy Now Price:
        <span>$<?php echo number_format($model->buy_now_price, 2) ?></span>
    </p>
    <p class="catalog_detail__bind_of">-------- or --------</p>

    <p class="catalog_detail__price__numbers">Min bid:
        <span>$<?php echo $model->min_bid ?></span>
    </p>
    <p class="catalog_detail__price__units ">Min Qty of units:
        <?php echo $model->min_unit ?>
    </p>

    <div class="clearfix"></div>
    <div class="catalog_detail__price__time_ended">
        <p style = 'color:red'>Closed</p>
        <p class="catalog_detail__price__time_ended__info ">Time start: <?php echo yii()->dateFormatter->format(
                'MM/dd/yyyy hh:mm a',
                $model->start_date_unix
            ); ?> EST</p>
        <p class="catalog_detail__price__time_ended__info ">Closing Time: <?php echo yii()->dateFormatter->format(
                'MM/dd/yyyy hh:mm a',
                $model->end_date_unix
            ); ?> EST</p>
    </div>

<?endif;?>

</div>

<?php elseif($model->is_flashdeal == 1): ?>
    <div class="catalog_detail__price_container pull-left auction">
        <p class="catalog_detail__price__numbers" style="margin-left:15px">Price per unit: <span>$<?php echo number_format($model->price_per_unit, 2)?></span></p>
        <p class="catalog_detail__price__units" style="margin-left:15px">Lots available: <span><?php echo $model->lots_available?></span></p>
        <p class="catalog_detail__price__units" style="margin-left:15px">Units in Lot: <span><?php echo $model->units_in_lot?></span></p>

        <p class="catalog_detail__price__main_price">Price: <span>$<?php echo number_format($model->price_per_unit*$model->units_in_lot, 2, '.', '') ?></span></p>

        <div class="catalog_detail__price__form dropdwn">
            <?php if($model->status == 1): ?>
                <form>
                    <div class="select_container">
                        <?php echo CHtml::dropDownList('select_flashdeal', OrangelotsAuctions::select_step_flashdeal($model,false,true,true),
                            OrangelotsAuctions::select_step_flashdeal($model,true),
                            array(
                                'onChange' => '
                            var selected = $(this).context.value;
                            var ppu = '.$model->price_per_unit*$model->units_in_lot.';
                            var result = ppu*selected;
                            $("p.catalog_detail__price__main_price span").html("$"+numeric_format(result,"","."))',
                                'class'=>'selectpicker',
                                'id'=>'slct_flsh',

                            )); ?>
                    </div>
                    <?php echo CHtml::ajaxSubmitButton(
                        'BUY NOW',
                        CHtml::normalizeUrl(array('orangelots-auctions/buy-now')),
                        array(
                            'type' => 'post',
                            'data' => array(
                                'model_id' => $model->id,
                                'count' => "js:$('div.dropdwn span.filter-option.pull-left').html()",
                            ),
                            'success' => 'function(data) {
                                            var obj = JSON.parse(data);
                                            console.log(obj.status);
                                            if (obj.status == "error") {
                                             $("body").append(obj.html);
                                            }
                                            else if (obj.status == "some_error") {
                                                alert(obj.html);
                                            }
                                            else if (obj.status == "bidding_save") {
                                                $("body").append(obj.html);
                                            }

                                        }',
                        ),
                        array(
                            'class' => 'btn_buy_now',
                            'type'=>'image',
                            'src'=>yii()->baseUrl . '/images/btn/btn_buy_now.png',
                        )
                    );
                    ?>
                </form>
            <?php endif; ?>
        </div>
        <?php if($model->status == 1): ?>
            <div class="catalog_detail__price__time_ended">
                <p>End in:  <span>
                                <?php echo $days = floor(($model->end_date_unix - time()) / (24 * 60 * 60)) ?> Day,
                        <?php echo $hours = floor(($model->end_date_unix - time() - ($days * (24 * 60 * 60))) / (60 * 60)) ?>
                        hours
                        <?php echo $minutes = floor(
                            ($model->end_date_unix - time() - ($hours * (60 * 60)) - $days * (24 * 60 * 60)) / (60)
                        ) ?> minutes</span></p>
                <p class="catalog_detail__price__time_ended__info">Closing Time: <?php echo yii()->dateFormatter->format(
                        'MM/dd/yyyy hh:mm:ss a',
                        $model->end_date_unix
                    ); ?> EST</p>
            </div>
        <?php elseif($model->status == 2): ?>
            <div class="catalog_detail__price__time_ended">
                <?php echo "<p style = 'color:red'>Closed</p>"; ?>
            </div>
        <?php endif; ?>
    </div>

<?php endif; ?>
<div class="clearfix"></div>
<div class="card_detail__tabs_container clearfix">
    <div class="card_detail_tabs_menu">
        <?$questions = AuctionQuestion::getAuctionQuestions($model->id);?>
        <ul>
            <li>
                <a href="javascript:void(0)" class="question <?php echo !empty($questions) ? 'active':''?>">Questions</a>
            </li>
            <li>
                <a href="javascript:void(0)" class="description <?php echo empty($questions) ? 'active':''?>">Descriptions</a>
            </li>
            <li>
                <a href="javascript:void(0)" class="terms">Terms</a>
            </li>
            <li>
                <a href="javascript:void(0)" class="returns_and_warranty">Returns and warranty</a>
            </li>
            <li>
                <a href="javascript:void(0)" class="comments">Feedback</a>
            </li>
        </ul>
    </div>

    <div class="card_detail__tabs_content card_detail__tab_question <?php echo empty($questions) ? 'hide':''?>">
        <?if(!OrangelotsAuctions::isOwner($model->id)):?>
            <div class="clearfix text-right">
                <?php echo CHtml::ajaxLink(
                    "Ask a question",
                    yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                    array(
                        'data' => array(
                            'auction' => $model->id
                        ),
                        'type' => 'POST',
                        'success' => "function(data){ $('body').append(data);}",
                    ),
                    array(
                        'class' => 'btn question_btn'
                    )
                );
                ?>
            </div>
        <?endif;?>
        <div class="card_detail__tab_question__list ">
            <?$this->widget('ext.AuctionQuestionsWidget.AuctionQuestionsWidget', array('auctionId' => $model->id))?>
        </div>
    </div>
    <div class="card_detail__tabs_content card_detail__tab_description <?php echo !empty($questions) ? 'hide':''?>">
        <?if(!OrangelotsAuctions::isOwner($model->id)):?>
            <div class="clearfix text-right">
                <?php echo CHtml::ajaxLink(
                    "Ask a question",
                    yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                    array(
                        'data' => array(
                            'auction' => $model->id
                        ),
                        'type' => 'POST',
                        'success' => "function(data){ $('body').append(data);}",
                    ),
                    array(
                        'class' => 'btn question_btn'
                    )
                );
                ?>
            </div>
        <?endif;?>
        <p><label>NMFC Class:</label><?php echo $model->nmfc ?></p>
        <p><label>Description:</label><?php echo $model->description ?></p>
        <p><label>Condition:</label><?php echo Condition::model()->findByPk($model->condition)->name ?></p>
        <p><label>Manufacture:</label><?php echo OrangelotsManufacture::model()->findByPk($model->manufacture)->name ?></p>
        <p><label>Color:</label><?php echo $model->color ?></p>
        <?php $model->itemsView($model->id);?>
        <p><label>Product dimensions:</label><?php echo $model->dimensions ?></p>
        <p><label>V:</label><?php echo $model->itemsV($id) ?></p>
        <?if(!empty($model->upholstery_material)):?>
            <p><label>Upholstery material:</label><?php echo $model->upholstery_material ?></p>
        <?endif;?>
        <?if(!empty($model->number_of_pieces)):?>
            <p><label>Number of pieces:</label><?php echo $model->number_of_pieces ?></p>
        <?endif;?>
        <?if(!empty($model->furniture_function)):?>
            <p><label>Furniture function:</label><?php echo $model->furniture_function ?></p>
        <?endif;?>
        <?if(!empty($model->style_id)):?>
            <p><label>Style:</label><?php echo OrangelotsStyle::getNameById($model->style_id); ?></p>
        <?endif;?>
    </div>
    <div class="card_detail__tabs_content card_detail__tab_terms hide">
        <?if(!OrangelotsAuctions::isOwner($model->id)):?>
            <div class="clearfix text-right">
                <?php echo CHtml::ajaxLink(
                    "Ask a question",
                    yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                    array(
                        'data' => array(
                            'auction' => $model->id
                        ),
                        'type' => 'POST',
                        'success' => "function(data){ $('body').append(data);}",
                    ),
                    array(
                        'class' => 'btn question_btn'
                    )
                );
                ?>
            </div>
        <?endif;?>
        <?php echo $model->terms ?>
    </div>
    <div class="card_detail__tabs_content card_detail__tab_returns_and_warranty hide">
        <?if(!OrangelotsAuctions::isOwner($model->id)):?>
            <div class="clearfix text-right">
                <?php echo CHtml::ajaxLink(
                    "Ask a question",
                    yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                    array(
                        'data' => array(
                            'auction' => $model->id
                        ),
                        'type' => 'POST',
                        'success' => "function(data){ $('body').append(data);}",
                    ),
                    array(
                        'class' => 'btn question_btn'
                    )
                );
                ?>
            </div>
        <?endif;?>
        <?php echo $model->returns_warranty ?>
    </div>
    <div class="card_detail__tabs_content card_detail__tab_comments hide">
        <div class="clearfix text-right">
            <?if(!OrangelotsAuctions::isOwner($model->id)):?>
                <?php echo CHtml::ajaxLink(
                    "Ask a question",
                    yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                    array(
                        'data' => array(
                            'auction' => $model->id
                        ),
                        'type' => 'POST',
                        'success' => "function(data){ $('body').append(data);}",
                    ),
                    array(
                        'class' => 'btn question_btn'
                    )
                );
                ?>
            <?endif;?>
        </div>
        <?php echo OrangelotsAuctions::commentAuction($model->user_id);?>
    </div>
</div>
</div>

<div class="second_collum">
    <div class="card_detail__delivery_info">
        <div class="card_detail__delivery_info__shipping">
            <p class="card_detail__delivery_info__title">Shipping information</p>

            <p class="card_detail__delivery_info__shiping_text weight">
                <span>Shipping Weight:</span> <span class="clear"><?php echo $model->weight+($model->weightItems($model->id)) ?></span> pounds</p>

            <p class="card_detail__delivery_info__shiping_text">
                <span>Shipping Terms:</span> <?php echo ShippingTerms::model()->findByPK(
                    $model->shipping_terms
                )->name ?></p>

            <p class="card_detail__delivery_info__shiping_text">
                <span>Delivery:</span> <?php echo Delivery::model()->findByPK($model->delivery)->name ?></p>
        </div>
        <div class="card_detail__delivery_info__vendor">
            <p class="card_detail__delivery_info__vendor_title">Vendor information</p>

            <p class="card_detail__delivery_info__vendor_adress"><?php echo $model->vendors_information?><?php /*echo $vendor->company*/?><!-- <br/> --><?php /*echo $vendor->address_1 . ' '  . $vendor->address_2 . ' ' . OrangelotsStates::getStateLabel($vendor->state).  ' '  . $vendor->zipcode.', ' . 'USA' */?></p>
            <p class="card_detail__delivery_info__vendor_rate">Rating:
                <a href="javascript:void(0)" onclick="js:$('div.card_detail_tabs_menu ul li').eq(4).find('a').click()" ><?php echo $rating; ?> out of 10</a>
            </p>
        </div>
    </div>
    <?php $this->widget('ext.RightColumnListWidget.RightColumnListWidget', array('seller'=> $model->user_id, 'category' => $model->category_id)); ?>
</div>
</div>

</p>
</div>
</div>

<script type="text/javascript">
    var id = '<?php echo $questionId?>';
    var show = '<?php echo $show?>';
    $(document).ready(function(){
        if (id) {
            if(show){
                if(show == 0){
                    $("#reply_"+id).click();
                    $(window).scrollTop($("#reply_"+id).closest("li").offset().top)
                }else if(show == 2){
                    $(window).scrollTop($("#question_"+id).offset().top);
                }else if(show == 1){
                    $(window).scrollTop($("#reply_"+id).closest("li").offset().top)
                }
            }
        }

        $(document).on('change','#price_per_unit', function(){
            var a = parseFloat($('#price_per_unit').val());
            var b = $('div.catalog_detail__price__numbers__select_row span.filter-option.pull-left').html()
            var res = a*b;
            var msg = 'Total Amount   <p class="price_amount"> $' + numeric_format(res,"",".")+'</p>';
            $('div.total_amount_per_unit').html(msg);
        });

        $('div.catalog_detail__price__numbers__select_row span.filter-option.pull-left').bind("DOMSubtreeModified",function(){
            var a = parseFloat($('#price_per_unit').val());
            var b = $('div.catalog_detail__price__numbers__select_row span.filter-option.pull-left').html()
            var res = a*b;
            var msg = 'Total Amount   <p class="price_amount"> $' + numeric_format(res,"",".")+'</p>';
            $('div.total_amount_per_unit').html(msg);
            // ship weight
            var weight = '<?php echo $model->weight+($model->weightItems($model->id)) ?>';
            var w = parseFloat(weight);
            var res_w = b*w;
            $('p.card_detail__delivery_info__shiping_text.weight span.clear').html(res_w)

        });

        $('div.catalog_detail__price__form span.filter-option.pull-left').bind("DOMSubtreeModified",function(){
            var c = '<?php echo $model->buy_now_price ?>';
            var d = $('div.catalog_detail__price__form span.filter-option.pull-left').html()
            var result = c*d;
            var message = 'Total Amount   <p class="price_amount"> $' + numeric_format(result,"",".")+'</p>';
            $('div.total_amount_per_unit').html(message);
            // ship weight
            var weight = '<?php echo $model->weight+($model->weightItems($model->id)) ?>';
            var w = parseFloat(weight);
            var res_w = d*w;
            $('p.card_detail__delivery_info__shiping_text.weight span.clear').html(res_w)
        });
    });
</script>