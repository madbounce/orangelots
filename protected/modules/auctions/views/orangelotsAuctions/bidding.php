<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href="<?php echo yii()->createUrl("site/index")?>"></a></li>
            <li><a>Buy</a></li>
            <li><a>Auctions</a></li>
        </ul>
    </div>
</div>


<div class="posted_by__container">
<div class="main_container">
    <p class="posted_by__title">Search by</p>
    <div class="posted_by__form_container clearfix">
        <form method="POST">
            <div class="posted_by__form_left_side pull-left">
                <div class="posted_by__form_row fl_l ">
                    <div class="posted_by__form_element posted_by__form_element__name">
                        <label>Auction #, name</label>
                        <?if(isset($data['name']) and !empty($data['name'])) {
                            $value = $data['name'];
                        }
                        if(isset($auId) and !empty($auId)){
                            $value = $auId;
                        }
                        ?>
                        <input type="text" name="Filter[name]" value="<?php echo $value?>">
                    </div>

                    <div class="posted_by__form_element posted_by__form_element__status">
                        <label>Status</label>
                        <select class="selectpicker" name="Filter[status]">
                            <option value="empty">Choose if need</option>
                            <option value="1" <?php if(isset($data['status'])):if($data['status'] == 1): echo 'selected="selected"';  endif;endif; ?>>Open</option>
                            <option value="2"<?php if(isset($data['status'])):if($data['status'] == 2): echo 'selected="selected"';  endif;endif; ?>>Close</option>
                        </select>
                    </div>
                </div>

                <div class="posted_by__form_row ">
                    <div class="posted_by__form_element posted_by__form_element__days">
                        <label>Close in</label>
                        <select class="selectpicker" name="Filter[end]">
                            <option value="empty">Choose if need</option>
                            <option value="1" <?php if(isset($data['end'])):if($data['end'] == 1): echo 'selected="selected"';  endif;endif; ?>>1 day</option>
                            <option value="3" <?php if(isset($data['end'])):if($data['end'] == 3): echo 'selected="selected"';  endif;endif; ?>>3 days</option>
                            <option value="7" <?php if(isset($data['end'])):if($data['end'] == 7): echo 'selected="selected"';  endif;endif; ?>>1 week</option>
                            <option value="14" <?php if(isset($data['end'])):if($data['end'] == 14): echo 'selected="selected"';  endif;endif; ?>>2 weeks</option>
                            <option value="30" <?php if(isset($data['end'])):if($data['end'] == 30): echo 'selected="selected"';  endif;endif; ?>>month</option>
                        </select>
                    </div>

                    <div class="posted_by__form_element posted_by__form_element__days">

                        <button class="btn_image"><img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt=""></button>
                    </div>
                </div>

                <div class="posted_by__form_row_checkers">
                    <label><input name="Filter[posted_by__form_row_checkers_radio]" type="radio" <?php if(isset($data['posted_by__form_row_checkers_radio'])):if($data['posted_by__form_row_checkers_radio'] == 1): echo 'checked="true"';  endif;endif; ?> value="1" >Auctions, saved for later</label>
                    <label><input name="Filter[posted_by__form_row_checkers_radio]" type="radio" <?php if(isset($data['posted_by__form_row_checkers_radio'])):if($data['posted_by__form_row_checkers_radio'] == 2): echo 'checked="true"';  endif;endif; ?> value="2">Auctions, I placed bid on</label>
                    <label><input name="Filter[posted_by__form_row_checkers_radio]" type="radio" <?php if(isset($data['posted_by__form_row_checkers_radio'])):if($data['posted_by__form_row_checkers_radio'] == 3): echo 'checked="true"';  endif;endif; ?> value="3"> Auctions, I won</label>
                </div>

                <div>
                    <p><a href="" class="orange_link advanced_search">Advanced search</a></p>

                    <div class="advanced_search_block" style="display:<?php echo $visible?>">
                        <div class="posted_by__form_block">
                            <label>Current Bid</label>
                            <input type="text" placeholder="from" name="Filter[current_bid_from]" value="<?php if(isset($data['current_bid_from'])):if(!empty($data['current_bid_from'])): echo $data['current_bid_from']; endif;endif; ?>"/>

                            <input type="text" placeholder="to" name="Filter[current_bid_to]" value="<?php if(isset($data['current_bid_to'])):if(!empty($data['current_bid_to'])): echo $data['current_bid_to']; endif;endif; ?>"/>
                        </div>
                        <div class="posted_by__form_block">
                            <label># Bids</label>
                            <input type="text" placeholder="from" name="Filter[count_bids_from]" value="<?php if(isset($data['count_bids_from'])):if(!empty($data['count_bids_from'])): echo $data['count_bids_from']; endif;endif; ?>"/>
                            <input type="text" placeholder="to" name="Filter[count_bids_to]" value="<?php if(isset($data['count_bids_to'])):if(!empty($data['count_bids_to'])): echo $data['count_bids_to']; endif;endif; ?>"/>
                        </div>

                        <div class="posted_by__form_block">
                            <label>Start Date</label>
                            <?php
                            $this->widget(
                                'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                                array(
                                    'model' => new OrangelotsAuctions,
                                    'attribute' => 'au_date',
                                    'language' => 'en-GB', //default Yii::app()->language
                                    'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                                    'options' => array(
                                        'dateFormat' => 'yy/mm/dd',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'uni_form__calendar',
                                        'placeholder'=> 'from',
                                        'name'=> "Filter[start_date_from]",
                                        'value'=> (!empty($data['start_date_from']))?$data['start_date_from']:'',
                                    )
                                )
                            );
                            ?>
                            <?php
                            $this->widget(
                                'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                                array(
                                    'model' => new OrangelotsAuctions,
                                    'attribute' => 'au_date',
                                    'language' => 'en-GB', //default Yii::app()->language
                                    'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                                    'options' => array(
                                        'dateFormat' => 'yy/mm/dd',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'uni_form__calendar',
                                        'placeholder'=> 'to',
                                        'name'=> "Filter[start_date_to]",
                                        'value'=> (!empty($data['start_date_to']))?$data['start_date_to']:'',
                                    )

                                )
                            );
                            ?>
                        </div>

                        <div class="posted_by__form_block">
                            <label>Close Date</label>
                            <?php
                            $this->widget(
                                'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                                array(
                                    'model' => new OrangelotsAuctions,
                                    'attribute' => 'au_date',
                                    'language' => 'en-GB', //default Yii::app()->language
                                    'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                                    'options' => array(
                                        'dateFormat' => 'yy/mm/dd',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'uni_form__calendar',
                                        'placeholder'=> "from",
                                        'name'=> "Filter[end_date_from]",
                                        'value'=> (!empty($data['end_date_from']))?$data['end_date_from']:'',
                                    )

                                )
                            );
                            ?>
                            <?php
                            $this->widget(
                                'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                                array(
                                    'model' => new OrangelotsAuctions,
                                    'attribute' => 'au_date',
                                    'language' => 'en-GB', //default Yii::app()->language
                                    'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                                    'options' => array(
                                        'dateFormat' => 'yy/mm/dd',
                                    ),
                                    'htmlOptions' => array(
                                        'class' => 'uni_form__calendar',
                                        'placeholder'=> "to",
                                        'name'=> "Filter[end_date_to]",
                                        'value'=> (!empty($data['end_date_to']))?$data['end_date_to']:'',
                                    )

                                )
                            );
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <?php if ((user()->isVendor()) || (user()->isAdmin())):?>
            <div class="posted_by__form__right_side text-center pull-left">
                <a href="<?php echo createUrl('auctions/orangelots-auctions/create')?>" class="btn btn_image"><img src="<?php echo baseUrl().'/images/btn/new_auction_btn.png'?>" alt=""></a>
                <p><a href="<?php echo Yii::app()->createUrl('site/upload')?>" class="orange_link">or Import data file</a></p>
            </div>
            <?php endif; ?>
        </form>
    </div>

    <div class="posted_by__table_auction_list__container">
        <?php $this->widget(
            'MyGridView',
            array(
                'id' => 'orangelots-category-grid',
                'dataProvider' => $dataProvider,
                'rowCssClassExpression'=>'(OrangelotsAuctions::colorRowAuction($data->id_auction))?"auction_winner":""',
                'summaryText' => '',
                'columns' => array(
                    array(
                        'name' => 'id_auction',
                        'header' => 'Auction #',
                        'type' => 'raw',
                        'htmlOptions'=>array('width'=>'70'),
                        'value'=>'CHtml::link($data->id_auction,array("orangelots-auctions/view/".$data->id_auction),array("class" => "orange_link"))',
                    ),
                    array(
                        'name' => 'auction_name',
                        'header' => 'Name',
                        'type' => 'raw',
                        'value' => 'CHtml::link(OrangelotsAuctions::model()->findByPk($data->id_auction)->name,array("orangelots-auctions/view/".$data->id_auction),array("class" => "orange_link"))',
                    ),
                    array(
                        'name' => 'vendor',
                        'header' => 'Vendor',
                        'type' => 'raw',
                        'value' => 'OrangelotsUsers::model()->findByPk(OrangelotsAuctions::model()->findByPk($data->id_auction)->user_id)->firstname',
                    ),
                    array(
                        'name' => 'time',
                        'type' => 'raw',
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", OrangelotsAuctions::model()->findByPk($data->id_auction)->start_date_unix)." <br>- ".yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", OrangelotsAuctions::model()->findByPk($data->id_auction)->end_date_unix)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'status',
                        'type' => 'raw',
                        'header' => 'Status',
                        'value' => 'OrangelotsAuctionsStatus::model()->findByPk(OrangelotsAuctions::model()->findByPk($data->id_auction)->status)->name',
                        'htmlOptions'=>array('width'=>'50'),
                    ),
                    array(
                        'name' => 'qty_in_lot',
                        'header' => 'Qty in lot',
                        'type' => 'raw',
                        'value' => 'OrangelotsAuctions::model()->findByPk($data->id_auction)->units_in_lot',
                    ),
                    array(
                        'name' => 'count_bids',
                        'header' => '#Bids',
                        'type' => 'raw',
                        'value' => 'OrangelotsBiddingOn::model()->count("id_auction=:id_auction", array("id_auction" => $data->id_auction));',
                    ),
                    array(
                        'name' => 'bidder_name',
                        'header' => 'Bidder',
                        'type' => 'raw',
                        'value' => 'OrangelotsUsers::model()->findByPk(yii()->db->createCommand("SELECT id_user FROM orangelots_bidding_on WHERE id_auction = ".$data->id_auction." AND summary_price = (SELECT max(summary_price) FROM orangelots_bidding_on WHERE id_auction = ".$data->id_auction.")")->queryRow())->firstname',
                    ),
                    array(
                        'name' => 'current_bid',
                        'header' => 'Current Bid',
                        'type' => 'raw',
                        'value' => '"$".OrangelotsBiddingOn::model()->findByPk(yii()->db->createCommand("SELECT id FROM orangelots_bidding_on WHERE id_auction = ".$data->id_auction." AND price_per_unit = (SELECT max(price_per_unit) FROM orangelots_bidding_on WHERE id_auction = ".$data->id_auction.")")->queryRow())->price_per_unit',
                    ),
                    array(
                        'name' => 'my_bid',
                        'type' => 'raw',
                        'header' => '#My bid',
                        'value' => 'OrangelotsBiddingOn::model()->findByAttributes(array("id_user"=>yii()->user->id, "id_auction"=>$data->id_auction),array("order"=>"id DESC"))->id .", ".OrangelotsAuctions::AmericanDate(OrangelotsBiddingOn::model()->findByAttributes(array("id_user"=>yii()->user->id, "id_auction"=>$data->id_auction),array("order"=>"id DESC"))->datetime)',
                        'sortable' => false,
                        'htmlOptions'=>array('width'=>'60'),
                    ),
                    array(
                        'name' => 'my_bid',
                        'type' => 'raw',
                        'header' => 'My bid',
                        'value' => 'OrangelotsAuctions::getMyBid($data->id_auction)',
                        'sortable' => false,
                        'htmlOptions'=>array('width'=>'60'),
                    ),
                    array(
                        'name' => 'qty_in_lot',
                        'type' => 'raw',
                        'header' => 'Units',
                        'value' => 'OrangelotsBiddingOn::model()->findByAttributes(array("id_user"=>yii()->user->id),array("order"=>"id DESC"))->qty',
                        'sortable' => false,
                        'htmlOptions'=>array('width'=>'60'),
                    ),
                    array(
                        'name' => 'actions',
                        'type' => 'raw',
                        'header' => 'Actions',
                        'value' => 'OrangelotsAuctions::getAction($data->id_auction)',
                        'sortable' => false,
                        'htmlOptions'=>array('width'=>'65'),

                    ),
                ),
                'pager' => array(
                    'htmlOptions' => array(
                        'class' => 'pager_lk'
                    )
                ),
                'itemsCssClass' => 'text-center',
            )
        ); ?>
    </div>
</div>
</div>



