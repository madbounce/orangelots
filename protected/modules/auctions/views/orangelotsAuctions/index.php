<?php
/* @var $this OrangelotsAuctionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orangelots Auctions',
);

$this->menu=array(
	array('label'=>'Create OrangelotsAuctions', 'url'=>array('create')),
	array('label'=>'Manage OrangelotsAuctions', 'url'=>array('admin')),
);
?>

<h1>Orangelots Auctions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
