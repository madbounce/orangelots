<div class="breadcrumb_row clearfix">
    <div class="main_container">

        <?php
        $this->widget(
            'zii.widgets.CBreadcrumbs',
            array(
                'links' => array(
                    'Sell',
                    'Auctions',
                ),
                'separator' => '',
                'tagName' => 'ul',
                'htmlOptions' => array('class' => ''),
                'inactiveLinkTemplate' => '<li><a>{label}</a></li>',
                'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                        "site/index"
                    ) . '"></a></li>',
            )
        );
        ?>
    </div>
</div>

<div class="posted_by__container">
<div class="main_container">
<p class="posted_by__title">Search by</p>
<div class="posted_by__form_container clearfix">
    <form method="post" id="search_form">
        <div class="posted_by__form_left_side pull-left">
            <div class="posted_by__form_row fl_l">
                <div class="posted_by__form_element posted_by__form_element__name">
                    <label>Auction #, name</label>
                    <?if(isset($data['name']) and !empty($data['name'])){
                        $value = $data['name'];
                    }
                    if(isset($auId) and !empty($auId)){
                        $value = $auId;
                    }
                    ?>
                    <input type="text" name="Filter[name]" value="<?php echo $value?>">
                </div>

                <div class="posted_by__form_element posted_by__form_element__email">
                    <label>Bid ID, email, name</label>
                    <input type="text" name="Filter[bid_id_email_name]" value="<?php if(isset($data['bid_id_email_name'])):if(!empty($data['bid_id_email_name'])): echo $data['bid_id_email_name'];  endif;endif; ?>">
                </div>

                <div class="posted_by__form_element posted_by__form_element__status">
                    <label>Status</label>
                    <select class="selectpicker" name="Filter[status]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['status'])):if($data['status'] == 1): echo 'selected="selected"';  endif;endif; ?>>Open</option>
                        <option value="2"<?php if(isset($data['status'])):if($data['status'] == 2): echo 'selected="selected"';  endif;endif; ?>>Close</option>
                    </select>
                </div>
            </div>

            <div class="posted_by__form_row ">
                <div class="posted_by__form_element posted_by__form_element__days">
                    <label>Start in</label>
                    <select class="selectpicker" name="Filter[start]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['start'])):if($data['start'] == 1): echo 'selected="selected"';  endif;endif; ?>>1 day</option>
                        <option value="3" <?php if(isset($data['start'])):if($data['start'] == 3): echo 'selected="selected"';  endif;endif; ?>>3 days</option>
                        <option value="7" <?php if(isset($data['start'])):if($data['start'] == 7): echo 'selected="selected"';  endif;endif; ?>>1 week</option>
                        <option value="14" <?php if(isset($data['start'])):if($data['start'] == 14): echo 'selected="selected"';  endif;endif; ?>>2 weeks</option>
                        <option value="30" <?php if(isset($data['start'])):if($data['start'] == 30): echo 'selected="selected"';  endif;endif; ?>>month</option>
                    </select>
                </div>

                <div class="posted_by__form_element posted_by__form_element__days">
                    <label>Close in</label>
                    <select class="selectpicker" name="Filter[end]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['end'])):if($data['end'] == 1): echo 'selected="selected"';  endif;endif; ?>>1 day</option>
                        <option value="3" <?php if(isset($data['end'])):if($data['end'] == 3): echo 'selected="selected"';  endif;endif; ?>>3 days</option>
                        <option value="7" <?php if(isset($data['end'])):if($data['end'] == 7): echo 'selected="selected"';  endif;endif; ?>>1 week</option>
                        <option value="14" <?php if(isset($data['end'])):if($data['end'] == 14): echo 'selected="selected"';  endif;endif; ?>>2 weeks</option>
                        <option value="30" <?php if(isset($data['end'])):if($data['end'] == 30): echo 'selected="selected"';  endif;endif; ?>>month</option>
                    </select>
                </div>

                <div class="posted_by__form_element posted_by__form_element__days">

                    <button class="btn_image">
                        <img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt="">
                    </button>
                </div>
            </div>

            <div class="posted_by__form_row_checkers">
                <label><input name="Filter[posted_by__form_row_checkers_radio]" type="radio" value = "2" <?php if(isset($data['posted_by__form_row_checkers_radio'])):if($data['posted_by__form_row_checkers_radio'] == 2): echo 'checked="true"';  endif;endif; ?>>Auctions with winner selected</label>
            </div>

            <div>
                <p><a href="" class="orange_link advanced_search">Advanced search</a></p>

                <div class="advanced_search_block" style="display:<?php echo $visible?>">
                <div class="posted_by__form_block">
                    <label>Current Bid</label>
                    <input type="text" placeholder="from" name="Filter[current_bid_from]" value="<?php if(isset($data['current_bid_from'])):if(!empty($data['current_bid_from'])): echo $data['current_bid_from']; endif;endif; ?>"/>

                    <input type="text" placeholder="to" name="Filter[current_bid_to]" value="<?php if(isset($data['current_bid_to'])):if(!empty($data['current_bid_to'])): echo $data['current_bid_to']; endif;endif; ?>"/>
                </div>
                <div class="posted_by__form_block">
                    <label># Bids</label>
                    <input type="text" placeholder="from" name="Filter[count_bids_from]" value="<?php if(isset($data['count_bids_from'])):if(!empty($data['count_bids_from'])): echo $data['count_bids_from']; endif;endif; ?>"/>
                    <input type="text" placeholder="to" name="Filter[count_bids_to]" value="<?php if(isset($data['count_bids_to'])):if(!empty($data['count_bids_to'])): echo $data['count_bids_to']; endif;endif; ?>"/>
                </div>

                <div class="posted_by__form_block">
                    <label>Start Date</label>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> 'from',
                                'name'=> "Filter[start_date_from]",
                                'value'=> (!empty($data['start_date_from']))?$data['start_date_from']:'',
                            )

                        )
                    );
                    ?>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> 'to',
                                'name'=> "Filter[start_date_to]",
                                'value'=> (!empty($data['start_date_to']))?$data['start_date_to']:'',
                            )

                        )
                    );
                    ?>
                </div>

                <div class="posted_by__form_block">
                    <label>Close Date</label>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> "from",
                                'name'=> "Filter[end_date_from]",
                                'value'=> (!empty($data['end_date_from']))?$data['end_date_from']:'',
                            )

                        )
                    );
                    ?>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> "to",
                                'name'=> "Filter[end_date_to]",
                                'value'=> (!empty($data['end_date_to']))?$data['end_date_to']:'',
                            )

                        )
                    );
                    ?>
                </div>
            </div>
            </div>
        </div>

        <?php if ((user()->isVendor()) || (user()->isAdmin())):?>
        <div class="posted_by__form__right_side text-center pull-left">
            <a href="<?php echo createUrl('auctions/orangelots-auctions/create')?>" class="btn btn_image">
                <img src="<?php echo baseUrl().'/images/btn/new_auction_btn.png'?>" alt="">
            </a>
            <p><a href="<?php echo Yii::app()->createUrl('site/upload')?>" class="orange_link">or Import data file</a></p>
        </div>
        <?php endif; ?>
    </form>
</div>
    <div class="posted_by__table_auction_list__container">
        <?php $this->widget(
            'MyGridView',
            array(
                'id' => 'orangelots-category-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => '',
                'columns' => array(
                    array(
                        'name' => 'id',
                        'header' => 'Auction #',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'CHtml::link("$data->id",array("orangelots-auctions/view/".$data->id));',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'name',
                        'header' => 'Name',
                        'type' => 'raw',

                    ),
                    array(
                        'name' => 'time',
                        'type' => 'raw',
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", $data->start_date_unix)." <br>- ".yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", $data->end_date_unix)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'status',
                        'type' => 'raw',
                        'header' => 'Status',
                        'value' => 'OrangelotsAuctionsStatus::model()->findByPk($data->status)->name',
                        'htmlOptions'=>array('width'=>'50'),
                    ),
                    array(
                        'name' => 'units_in_lot',
                        'header' => 'Qty in lot',
                        'type' => 'raw',
                    ),
                    array(
                        'name' => 'buy_now_price',
                        'header' => 'Buy now price',
                        'type' => 'raw',
                        'value' => '"$".$data->buy_now_price',
                    ),
                    array(
                        'name' => 'min_unit',
                        'header' => 'Min unit',
                        'type' => 'raw',
                    ),
                    array(
                        'name' => 'buy_now_price',
                        'header' => 'Min bid',
                        'type' => 'raw',
                        'value' => '"$".$data->min_bid',
                    ),
                    array(
                        'name' => 'count_bids',
                        'header' => '#Bids',
                        'type' => 'raw',
                        'value' => 'CHtml::link(OrangelotsBiddingOn::model()->count("id_auction=:id_auction", array("id_auction" => $data->id)),array("orangelots-auctions/bidders/".$data->id."?sort=datetime"), array("class" => "orange_link"));',
                    ),
                    array(
                        'name' => 'bidder_name',
                        'header' => 'Top Bidder',
                        'type' => 'raw',
                        'value' => 'OrangelotsUsers::model()->findByPk(yii()->db->createCommand("SELECT id_user FROM orangelots_bidding_on WHERE id_auction = ".$data->id." AND summary_price = (SELECT max(summary_price) FROM orangelots_bidding_on WHERE id_auction = ".$data->id.")")->queryRow())->firstname',
                    ),
                    array(
                        'name' => 'bid_per_unit',
                        'header' => 'Bid per unit',
                        'type' => 'raw',
                        'value' => '"$".OrangelotsBiddingOn::model()->findByPk(yii()->db->createCommand("SELECT id FROM orangelots_bidding_on WHERE id_auction = ".$data->id." AND buy_now = 0 AND summary_price = (SELECT max(summary_price) FROM orangelots_bidding_on WHERE id_auction = ".$data->id.")")->queryRow())->price_per_unit',
                    ),
                    array(
                        'name' => 'qty_in_lot',
                        'header' => 'Units',
                        'type' => 'raw',
                        'value' => 'OrangelotsBiddingOn::model()->findByPk(yii()->db->createCommand("SELECT id FROM orangelots_bidding_on WHERE id_auction = ".$data->id." AND summary_price = (SELECT max(summary_price) FROM orangelots_bidding_on WHERE id_auction = ".$data->id.")")->queryRow())->qty',
                    ),
                    array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'header'=>'Actions',
                        'template'=>'{ed}</br>{cp}<!--</br>{sw}-->',
                        'buttons'=>array
                        (
                            'ed' => array
                            (
                                'label'=>'Edit',
                                'url'=>'($_REQUEST["deal"]=="true")?createUrl("flashdeal/default/update",array("id" => $data->id)):createUrl("auctions/orangelots-auctions/update",array("id" => $data->id))',
                                'options' => array(
                                    'class' => 'orange_link'
                                ),
                                'visible'=>'$data->visibleEdit($data->id)',
                            ),
                            'cp' => array
                            (
                                'label'=>'Copy',
                                'url'=>'createUrl("auctions/orangelots-auctions/copy/",array("id" => $data->id))',
                                'options' => array(
                                    'class' => 'orange_link'
                                )
                            ),
                        ),
                    ),
                ),
                'pager' => array(
                    'htmlOptions' => array(
                        'class' => 'pager_lk'
                    )
                ),
                'itemsCssClass' => 'text-center',
            )
        ); ?>
    </div>
</div>
</div>


