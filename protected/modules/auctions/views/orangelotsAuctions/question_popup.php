<div class="popup__container" id="ask-question-popup">
    <div class="popup__bg"></div>
    <div class="popup_inner" style="width:400px;">
        <div class="popup__content">
            <a href="" class="popup_close">X</a>
            <div class="popup_body clearfix">
                <p class="popup__right_info__title ">Ask your question</p>
                <div class="popup__right_info__form__container ">
                    <?php $form = $this->beginWidget('CActiveForm',
                        array(
                            'id' => 'ask-question-form',
                            'enableAjaxValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                                'validateOnChange' => true,
                                'validateOnType' => true,
                            ),
                            'action' => createUrl('/auctions/orangelots-auctions/ask-question'),
                        )
                    );
                    ?>
                    <?if(user()->isGuest):?>
                    <div class="popup_form_row">
                        <?php echo $form->labelEx($model, 'user_name'); ?>
                        <?php echo $form->textField($model, 'user_name', array('size' => 60, 'maxlength' => 30)); ?>
                        <?php echo $form->error($model, 'user_name'); ?>
                    </div>
                    <?endif;?>
                    <div class="popup_form_row">
                        <?php echo $form->labelEx($model, 'subject'); ?>
                        <?php echo $form->textField($model, 'subject', array('size' => 60, 'maxlength' => 30)); ?>
                        <?php echo $form->error($model, 'subject'); ?>
                    </div>
                    <div class="popup_form_row">
                        <?php echo $form->labelEx($model, 'question'); ?>
                        <?php echo $form->textArea($model, 'question', array('cols' => 60, 'rows' => 10)); ?>
                        <?php echo $form->error($model, 'question'); ?>
                        <?php echo $form->hiddenField($model, 'auction_id', array('value' => $auctionId))?>
                    </div>
                    <div class="text-center">
                    <?php echo CHtml::button('Ask your question',
                        array(
                            'id' => 'askQuestionButton',
                            'class' => 'btn green_bnt',

                            'onClick' => 'js:askQuestion($("#ask-question-form").serializeArray())'
                        )
                    ); ?>
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

