<div class="breadcrumb_row clearfix">
    <div class="main_container">

        <?php
        $this->widget(
            'zii.widgets.CBreadcrumbs',
            array(
                'links' => array(
                    'Sell',
                    'Deals',
                ),
                'separator' => '',
                'tagName' => 'ul',
                'htmlOptions' => array('class' => ''),
                'inactiveLinkTemplate' => '<li><a>{label}</a></li>',
                'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                        "site/index"
                    ) . '"></a></li>',
            )
        );
        ?>
    </div>
</div>


<div class="posted_by__container">
<div class="main_container">
<p class="posted_by__title">Search by</p>
<div class="posted_by__form_container clearfix">
    <form method="post" id="search_form">
        <div class="posted_by__form_left_side pull-left">
            <div class="posted_by__form_row fl_l">
                <div class="posted_by__form_element posted_by__form_element__name">
                    <label>Auction #, name</label>
                    <?if(isset($data['name']) and !empty($data['name'])){
                        $value = $data['name'];
                    }
                    if(isset($auId) and !empty($auId)){
                        $value = $auId;
                    }
                    ?>
                    <input type="text" name="Filter[name]" value="<?php echo $value?>">
                </div>

                <div class="posted_by__form_element posted_by__form_element__email">
                    <label>Bid ID, email, name</label>
                    <input type="text" name="Filter[bid_id_email_name]" value="<?php if(isset($data['bid_id_email_name'])):if(!empty($data['bid_id_email_name'])): echo $data['bid_id_email_name'];  endif;endif; ?>">
                </div>

                <div class="posted_by__form_element posted_by__form_element__status">
                    <label>Status</label>
                    <select class="selectpicker" name="Filter[status]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['status'])):if($data['status'] == 1): echo 'selected="selected"';  endif;endif; ?>>Open</option>
                        <option value="2"<?php if(isset($data['status'])):if($data['status'] == 2): echo 'selected="selected"';  endif;endif; ?>>Close</option>
                    </select>
                </div>
            </div>

            <div class="posted_by__form_row ">
                <div class="posted_by__form_element posted_by__form_element__days">
                    <label>Start in</label>
                    <select class="selectpicker" name="Filter[start]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['start'])):if($data['start'] == 1): echo 'selected="selected"';  endif;endif; ?>>1 day</option>
                        <option value="3" <?php if(isset($data['start'])):if($data['start'] == 3): echo 'selected="selected"';  endif;endif; ?>>3 days</option>
                        <option value="7" <?php if(isset($data['start'])):if($data['start'] == 7): echo 'selected="selected"';  endif;endif; ?>>1 week</option>
                        <option value="14" <?php if(isset($data['start'])):if($data['start'] == 14): echo 'selected="selected"';  endif;endif; ?>>2 weeks</option>
                        <option value="30" <?php if(isset($data['start'])):if($data['start'] == 30): echo 'selected="selected"';  endif;endif; ?>>month</option>
                    </select>
                </div>

                <div class="posted_by__form_element posted_by__form_element__days">
                    <label>Close in</label>
                    <select class="selectpicker" name="Filter[end]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['end'])):if($data['end'] == 1): echo 'selected="selected"';  endif;endif; ?>>1 day</option>
                        <option value="3" <?php if(isset($data['end'])):if($data['end'] == 3): echo 'selected="selected"';  endif;endif; ?>>3 days</option>
                        <option value="7" <?php if(isset($data['end'])):if($data['end'] == 7): echo 'selected="selected"';  endif;endif; ?>>1 week</option>
                        <option value="14" <?php if(isset($data['end'])):if($data['end'] == 14): echo 'selected="selected"';  endif;endif; ?>>2 weeks</option>
                        <option value="30" <?php if(isset($data['end'])):if($data['end'] == 30): echo 'selected="selected"';  endif;endif; ?>>month</option>
                    </select>
                </div>

                <div class="posted_by__form_element posted_by__form_element__days">

                    <button class="btn_image">
                        <img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt="">
                    </button>
                </div>
            </div>

            <div class="posted_by__form_row_checkers">
            </div>

            <div>
                <p><a href="" class="orange_link advanced_search">Advanced search</a></p>

                <div class="advanced_search_block" style="display:<?php echo $visible?>">

                <div class="posted_by__form_block">
                    <label>Start Date</label>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> 'from',
                                'name'=> "Filter[start_date_from]",
                                'value'=> (!empty($data['start_date_from']))?$data['start_date_from']:'',
                            )

                        )
                    );
                    ?>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> 'to',
                                'name'=> "Filter[start_date_to]",
                                'value'=> (!empty($data['start_date_to']))?$data['start_date_to']:'',
                            )

                        )
                    );
                    ?>
                </div>

                <div class="posted_by__form_block">
                    <label>Close Date</label>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> "from",
                                'name'=> "Filter[end_date_from]",
                                'value'=> (!empty($data['end_date_from']))?$data['end_date_from']:'',
                            )

                        )
                    );
                    ?>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> "to",
                                'name'=> "Filter[end_date_to]",
                                'value'=> (!empty($data['end_date_to']))?$data['end_date_to']:'',
                            )

                        )
                    );
                    ?>
                </div>
            </div>
            </div>
        </div>

        <?php if ((user()->isVendor()) || (user()->isAdmin())):?>
        <div class="posted_by__form__right_side text-center pull-left">
            <a href="<?php echo createUrl('flashdeal/default/create')?>" class="btn btn_image" >
                <img src="<?php echo baseUrl().'/images/btn/new_deal_btn.png'?>" alt="">
            </a>
            <p><a href="<?php echo Yii::app()->createUrl('site/upload-deal')?>" class="orange_link">or Import data file</a></p>
        </div>
        <?php endif; ?>
    </form>
</div>
    <div class="posted_by__table_auction_list__container">
        <?php $this->widget(
            'MyGridView',
            array(
                'id' => 'orangelots-category-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => '',
                'columns' => array(
                    array(
                        'name' => 'id',
                        'header' => 'Deal #',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'CHtml::link("$data->id",array("orangelots-auctions/view/".$data->id));',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'name',
                        'header' => 'Name',
                        'type' => 'raw',

                    ),
                    array(
                        'name' => 'time',
                        'header' => 'Start Time',
                        'type' => 'raw',
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", $data->start_date_unix)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'time',
                        'header' => 'Close Time',
                        'type' => 'raw',
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", $data->end_date_unix)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'status',
                        'type' => 'raw',
                        'header' => 'Status',
                        'value' => 'OrangelotsAuctionsStatus::model()->findByPk($data->status)->name',
                        'htmlOptions'=>array('width'=>'50'),
                    ),
                    array(
                        'name' => 'lots_available',
                        'header' => 'Lots available',
                        'type' => 'raw',
                    ),
                    array(
                        'name' => 'units_in_lot',
                        'header' => 'Units in lot',
                        'type' => 'raw',
                    ),
                    array(
                        'name' => 'price_per_unit',
                        'header' => 'Price per unit',
                        'type' => 'raw',
                        'value' => '"$".$data->price_per_unit',
                    ),
                    array(
                        'name' => 'colum_table',
                        'header' => 'Sold Lots',
                        'type' => 'raw',
                        'value' => 'OrangelotsAuctions::sold_lots($data->id)',
                    ),
                    array(
                        'name' => 'colum_table',
                        'header' => 'Sold Units',
                        'type' => 'raw',
                        'value' => 'OrangelotsAuctions::sold_units($data->id)',
                    ),
                    array(
                        'name' => 'colum_table',
                        'header' => 'Sold',
                        'type' => 'raw',
                        'value' => '"$".OrangelotsAuctions::sold($data->id)',
                    ),
                    array(
                        'name' => 'colum_table',
                        'header' => 'Details',
                        'type' => 'raw',
                        'value' => '($data->status == 1)?CHtml::link("view details",array("/flashdeal/default/update/".$data->id), array("class" => "orange_link")):"-";',
                    ),
                    array(
                        'name' => 'colum_table',
                        'header' => 'Sales',
                        'type' => 'raw',
                        'value' => 'CHtml::link("view sales",array("/auctions/orangelots-auctions/sales/".$data->id), array("class" => "orange_link"));',
                    ),
                ),
                'itemsCssClass' => 'text-center',
                'pager' => array(
                    'htmlOptions' => array(
                        'class' => 'pager_lk'
                    )
                ),
            )
        ); ?>
    </div>
</div>
</div>


