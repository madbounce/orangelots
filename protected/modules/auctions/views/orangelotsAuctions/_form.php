<?php
/* @var $this OrangelotsAuctionsController */
/* @var $model OrangelotsAuctions */
/* @var $form CActiveForm */
CHtml::$afterRequiredLabel = '<span class="important_star"> *</span> ';
$time = array(
    '00:00' => '00:00',
    '00:30' => '00:30',
    '01:00' => '01:00',
    '01:30' => '01:30',
    '02:00' => '02:00',
    '02:30' => '02:30',
    '03:00' => '03:00',
    '03:30' => '03:30',
    '04:00' => '04:00',
    '04:30' => '04:30',
    '05:00' => '05:00',
    '05:30' => '05:30',
    '06:00' => '06:00',
    '06:30' => '06:30',
    '07:00' => '07:00',
    '07:30' => '07:30',
    '08:00' => '08:00',
    '08:30' => '08:30',
    '09:00' => '09:00',
    '09:30' => '09:30',
    '10:00' => '10:00',
    '10:30' => '10:30',
    '11:00' => '11:00',
    '11:30' => '11:30',
);
?>

<div class="form main_container">

<?php $form = $this->beginWidget(
    'CActiveForm',
    array(
        'id' => 'orangelots-auctions-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )
); ?>

<?php //echo $form->errorSummary($model); ?>
<div class="uni_form_block">
    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'category_id'); ?>
        <?php //echo $form->textField($model, 'category_id'); ?>
        <?php echo $form->dropDownList(
            $model,
            'category_id',
            CHtml::listData(OrangelotsKind::model()->findAll(), 'id', 'name')
        ); ?>
        <?php echo $form->error($model, 'category_id'); ?>
    </p>

    <p class="uni_form_row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'manufacture'); ?>
        <?php //echo $form->textField($model, 'manufacture', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->dropDownList(
            $model,
            'manufacture',
            CHtml::listData(OrangelotsManufacture::model()->findAll(), 'id', 'name')
        ); ?>
        <?php echo $form->error($model, 'manufacture'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'mvrp'); ?>
        <?php echo $form->textField($model, 'mvrp'); ?>
        <?php echo $form->error($model, 'mvrp'); ?>
    </p>
    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'sku'); ?>
        <?php echo $form->textField($model, 'sku'); ?>
        <?php echo $form->error($model, 'sku'); ?>
    </p>
    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'nmfc'); ?>
        <?php echo $form->textField($model, 'nmfc'); ?>
        <?php echo $form->error($model, 'nmfc'); ?>
    </p>
    <!--    <p class="uni_form_row ai">-->
    <?php
    if(isset($itm)){
        $this->widget('RenderDynamicForm', array('r'=>$itm, 'ar'=>true));
    }
    if(isset($_POST['items'])){
        $this->widget('RenderDynamicForm', array('r'=>$_POST['items']));
    }
    ?>
    <a href="" id="addItems" class="btn btn-success">Add Items</a>
    <!--    </p>-->

</div>
<div class="uni_form_block">
    <p class="singup__title">Auction detail</p>

    <p class="uni_form_row price_input">
        <?php echo $form->labelEx($model, 'units_in_lot'); ?>
        <?php echo $form->textField($model, 'units_in_lot'); ?>
        <?php echo $form->error($model, 'units_in_lot'); ?>
    </p>

    <p class="uni_form_row price_input">
        <?php echo $form->labelEx($model, 'buy_now_price'); ?>
        <?php echo $form->textField($model, 'buy_now_price'); ?>
        <span class="uni_form_row__afterinput_text">saving <span
                class="uni_form_row__afterinput_text_price"><?php echo $model->saving_buy_now ?>%</span></span>
        <?php echo $form->error($model, 'buy_now_price'); ?>
        <?php echo $form->hiddenField($model, 'saving_buy_now'); ?>
    </p>

    <p class="uni_form_row price_input">
        <?php echo $form->labelEx($model, 'min_bid'); ?>
        <?php echo $form->textField($model, 'min_bid'); ?>
        <span class="uni_form_row__afterinput_text">saving <span
                class="uni_form_row__afterinput_text_price"><?php echo $model->saving_min_bid ?>%</span></span>
        <?php echo $form->error($model, 'min_bid'); ?>
        <?php echo $form->hiddenField($model, 'saving_min_bid'); ?>
    </p>

    <p class="uni_form_row price_input">
        <?php echo $form->labelEx($model, 'min_unit'); ?>
        <?php echo $form->textField($model, 'min_unit'); ?>
        <?php echo $form->error($model, 'min_unit'); ?>
    </p>

    <p class="uni_form_row price_input ">
        <?php echo $form->labelEx($model, 'au_date'); ?>
        <?php //echo $form->textField($model, 'start_date', array('class' => 'uni_form__calendar')); ?>
        <?php
        if ($model->au_date == '0000-00-00') {
            $model->au_date = yii()->dateFormatter->format('M/d/yyyy', time());
        } else {
            $model->au_date = yii()->dateFormatter->format('M/d/yyyy', $model->au_date);
        }

        $this->widget(
            'ext.CJuiDateTimePicker.CJuiDateTimePicker',
            array(
                'model' => $model,
                'attribute' => 'au_date',
                'language' => 'en-GB', //default Yii::app()->language
                'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                'options' => array(
                    'dateFormat' => 'm/d/yy',
                    //'timeFormat' => '',//'hh:mm tt' default
                ),
                'htmlOptions' => array(
                    'class' => 'uni_form__calendar'
                )

            )
        );
        ?>
        <span class="uni_form__select_container">
        <?php
        $model->au_time = yii()->dateFormatter->format('hh:mm', $model->au_time);
        echo $form->dropDownList($model, 'au_time', $time, array('class' => 'uni_form__time '));
        ?>
        <?php echo $form->error($model, 'au_time'); ?>
            </span>
        <span class="uni_form__select_container">
        <?php echo $form->dropDownList(
            $model,
            'au_am_pm',
            array('AM' => 'AM', 'PM' => 'PM'),
            array('class' => 'bootstrap-select lil_width')
        ); ?>
        </span>
    </p>

    <p class="uni_form_row price_input ">
        <?php echo $form->labelEx($model, 'au_date_end'); ?>
        <?php
        if ($model->au_date_end == '0000-00-00') {
            $model->au_date_end = yii()->dateFormatter->format('M/dd/yyyy', time());
        } else {
            $model->au_date_end = yii()->dateFormatter->format('M/dd/yyyy', $model->au_date_end);
        }
        $this->widget(
            'ext.CJuiDateTimePicker.CJuiDateTimePicker',
            array(
                'model' => $model,
                'attribute' => 'au_date_end',
                'language' => 'en-GB', //default Yii::app()->language
                'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                'options' => array(
                    'dateFormat' => 'm/dd/yy',
                ),
                'htmlOptions' => array(
                    'class' => 'uni_form__calendar'
                )

            )
        );
        ?>
        <?php echo $form->error($model, 'au_date_end'); ?>
        <span class="uni_form__select_container">
        <?php
        $model->au_time_end = yii()->dateFormatter->format('hh:mm', $model->au_time_end);

        echo $form->dropDownList($model, 'au_time_end', $time, array('class' => 'uni_form__time'));
        ?>
        <?php echo $form->error($model, 'au_time_end'); ?>
            </span>
        <span class="uni_form__select_container">
        <?php echo $form->dropDownList(
            $model,
            'au_am_pm_end',
            array('AM' => 'AM', 'PM' => 'PM'),
            array('class' => 'bootstrap-select lil_width')
        ); ?>
        </span>
        <?php echo $form->error($model, 'au_am_pm_end'); ?>
        <?php echo $form->error($model, 'end_date_unix'); ?>
    </p>
</div>
<div class="uni_form_block">
    <p class="singup__title">Lot details</p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'condition'); ?>
        <?php //echo $form->textField($model, 'condition', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->dropDownList(
            $model,
            'condition',
            CHtml::listData(Condition::model()->findAll(), 'id', 'name')
        ); ?>
        <?php echo $form->error($model, 'condition'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'color'); ?>
        <?php echo $form->textField($model, 'color', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'color'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'style_id'); ?>
        <?php echo $form->dropDownList(
            $model,
            'style_id',
            CHtml::listData(OrangelotsStyle::model()->findAll(), 'id', 'name')
        ); ?>
        <?php echo $form->error($model, 'style_id'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'dimensions'); ?>
        <?php echo $form->textField($model, 'dimensions', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'dimensions'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'weight'); ?>
        <?php echo $form->textField($model, 'weight'); ?>
        <?php echo $form->error($model, 'weight'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'upholstery_material'); ?>
        <?php echo $form->textField($model, 'upholstery_material'); ?>
        <?php echo $form->error($model, 'upholstery_material'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'number_of_pieces'); ?>
        <?php echo $form->textField($model, 'number_of_pieces'); ?>
        <?php echo $form->error($model, 'number_of_pieces'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'furniture_function'); ?>
        <?php echo $form->textField($model, 'furniture_function'); ?>
        <?php echo $form->error($model, 'furniture_function'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'description'); ?>

    <div class="uni_form__textarea_large_container">
        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
    </div>
    <?php echo $form->error($model, 'description'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'terms'); ?>

    <div class="uni_form__textarea_large_container">
        <?php echo $form->textArea($model, 'terms', array('rows' => 6, 'cols' => 50)); ?>
    </div>
    <?php echo $form->error($model, 'terms'); ?>
    </p>


    <?php

    if ($model->isNewRecord) {
        echo '<p>Before add photos to news gallery, you need to save new</p>';
    } else {
        $this->widget(
            'ext.galleryManager.GalleryManager',
            array(
                'gallery' => $model,
                'controllerRoute' => 'realtyGallery',
            )
        );
    }
    ?>
    </br>
    <div class="clearfix"></div>
    <p class="singup__title">Shopping and vendor details</p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'shipping_terms'); ?>
        <?php //echo $form->textField($model, 'shipping_terms'); ?>
        <?php echo $form->dropDownList(
            $model,
            'shipping_terms',
            CHtml::listData(ShippingTerms::model()->findAll(), 'id', 'name')
        ); ?>
        <?php echo $form->error($model, 'shipping_terms'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'shipping_terms_other'); ?>

    <div class="uni_form__textarea_large_container">
        <?php echo $form->textArea($model, 'shipping_terms_other', array('rows' => 6, 'cols' => 50)); ?>
    </div>
    <?php echo $form->error($model, 'shipping_terms_other'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'delivery'); ?>
        <?php //echo $form->textField($model, 'delivery'); ?>
        <?php echo $form->dropDownList(
            $model,
            'delivery',
            CHtml::listData(Delivery::model()->findAll(), 'id', 'name')
        ); ?>
        <?php echo $form->error($model, 'delivery'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'delivery_other'); ?>

    <div class="uni_form__textarea_large_container">
        <?php echo $form->textArea($model, 'delivery_other', array('rows' => 6, 'cols' => 50)); ?>
    </div>
    <?php echo $form->error($model, 'delivery_other'); ?>
    </p>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'returns_warranty'); ?>

    <div class="uni_form__textarea_large_container">
        <?php echo $form->textArea($model, 'returns_warranty', array('rows' => 6, 'cols' => 50)); ?>
    </div>
    <?php echo $form->error($model, 'returns_warranty'); ?>
    </p>


    <?php if (user()->isAdmin()) : ?>
        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'usr'); ?>

        <div class="uni_form__textarea_large_container">
            <?php if ((isset($model->user_id)) && (!empty($model->user_id))):?>
                <?php echo $form->dropDownList($model, 'usr', array(), array('options' => array($model->user_id=>array('selected'=>true)))); ?>
            <?php else:?>
                <?php echo $form->dropDownList($model, 'usr', OrangelotsUsers::usrForm()); ?>
            <?php endif;?>
        </div>
        <?php echo $form->error($model, 'usr'); ?>
        </p>

    <?php endif; ?>

    <p class="uni_form_row ">
        <?php echo $form->labelEx($model, 'vendors_information'); ?>

    <div class="uni_form__textarea_large_container">
        <?php echo $form->textArea($model, 'vendors_information', array('rows' => 6, 'cols' => 50)); ?>
    </div>
    <?php echo $form->error($model, 'vendors_information'); ?>
    </p>
    <?php if (user()->isAdmin()):?>
        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'set_on_main'); ?>
            <?php echo $form->checkBox($model, 'set_on_main', array('value' => 1, 'unCheckValue' => 0)); ?>
            <?php echo $form->error($model, 'set_on_main'); ?>
        </p>
    <?php endif; ?>


    <?php if (user()->isAdmin()):?>
        <p class="uni_form_row ">
            <?php echo $form->labelEx($model, 'sponsored'); ?>
            <?php echo $form->checkBox($model, 'sponsored', array('value' => 1, 'unCheckValue' => 0)); ?>
            <?php echo $form->error($model, 'sponsored'); ?>
        </p>
    <?php endif; ?>


</div>
<?php if (OrangelotsAuctions::hiddenId(Yii::app()->request->requestUri)): ?>
    <?php echo $form->hiddenField($model, 'id') ?>
<?php endif; ?>

<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
<?php if (OrangelotsAuctions::hiddenId(Yii::app()->request->requestUri)): ?>
    <button class="btn btn_orange">Create auction</button>
<?php else: ?>
    <button class="btn btn_orange">Update auction</button>
<?php endif; ?>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">

    $('#OrangelotsAuctions_usr').on('change', function () {
        var id = $(this).val();
        if (id != 'empty') {
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("user/user/usr-form") ?>',
                type: 'POST',
                data: {
                    id: id
                },
                success: function (data) {
                    $('#OrangelotsAuctions_vendors_information').html(data);
                }
            });
        }
    });

    $('#addItems').on('click', function (e) {
        $.ajax({
            url: '<?php echo Yii::app()->createUrl("auctions/orangelots-auctions/GetForm") ?>',
            type: 'POST',
            data: {
                uniq: $('div.dynamicForm').length
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status == "OK") {
                    $('#addItems').before(obj.html);
                }
            }
        });
        e.preventDefault();
    });
    $(document).on('click',"div.deleteItem", function(event){
        event.preventDefault();
    });

    var ajax = '';
    $(document).on('click', "a.deleteItem", function (event) {
        ajax = $(this).parents('div.dynamicForm').find('input.ajax').val();
        if(ajax == 'ajax'){
            var rid = $(this).parents('div.dynamicForm').find('input.itid').val();
            $.ajax({
                url: '<?php echo Yii::app()->createUrl("auctions/orangelots-auctions/deleteItems") ?>',
                type: 'POST',
                data: {
                    record: rid
                },
                success: function (data) {
                    if(data == "200"){
                        var idrow = '#record_'+rid;
                        $(idrow).parents('div.dynamicForm').remove();
                    } else {
                        alert('Some error with delete. Try again later');
                    }
                }
            });
            event.preventDefault();
        } else {
            $(this).parents('div.dynamicForm').remove();
            event.preventDefault();
        }
    });
</script>