<div class="dynamicForm">
    <p class="uni_form_row ">
        <?php echo CHtml::activeLabel($model,'name'); ?>
        <input type="text" name="<?php echo 'items[' . $uniq . '][name]' ?>"
               value="<?php echo (isset($value['name'])) ? $value['name'] : ''; ?>">
        <?php echo (isset($errors['name'][0])) ? '<div class="errorMessage">' . $errors['name'][0] . '</div>' : ''; ?>
    </p>
    <p class="uni_form_row ">
        <?php echo CHtml::activeLabel($model,'sku'); ?>
        <input type="text" name="<?php echo 'items[' . $uniq . '][sku]' ?>"
               value="<?php echo (isset($value['sku'])) ? $value['sku'] : ''; ?>">
        <?php echo (isset($errors['sku'][0])) ? '<div class="errorMessage">' . $errors['sku'][0] . '</div>' : ''; ?>
    </p>
    <p class="uni_form_row ">
        <?php echo CHtml::activeLabel($model,'length'); ?>
        <input type="text" name="<?php echo 'items[' . $uniq . '][length]' ?>"
               value="<?php echo (isset($value['length'])) ? $value['length'] : ''; ?>">
        <?php echo (isset($errors['length'][0])) ? '<div class="errorMessage">' . $errors['length'][0] . '</div>' : ''; ?>
    </p>
    <p class="uni_form_row ">
        <?php echo CHtml::activeLabel($model,'width'); ?>
        <input type="text" name="<?php echo 'items[' . $uniq . '][width]' ?>"
               value="<?php echo (isset($value['width'])) ? $value['width'] : ''; ?>">
        <?php echo (isset($errors['width'][0])) ? '<div class="errorMessage">' . $errors['width'][0] . '</div>' : ''; ?>
    </p>
    <p class="uni_form_row ">
        <?php echo CHtml::activeLabel($model,'height'); ?>
        <input type="text" name="<?php echo 'items[' . $uniq . '][height]' ?>"
               value="<?php echo (isset($value['height'])) ? $value['height'] : ''; ?>">
        <?php echo (isset($errors['height'][0])) ? '<div class="errorMessage">' . $errors['height'][0] . '</div>' : ''; ?>
    </p>
    <p class="uni_form_row ">
        <?php echo CHtml::activeLabel($model,'weight'); ?>
        <input type="text" name="<?php echo 'items[' . $uniq . '][weight]' ?>"
               value="<?php echo (isset($value['weight'])) ? $value['weight'] : ''; ?>">
        <?php echo (isset($errors['weight'][0])) ? '<div class="errorMessage">' . $errors['weight'][0] . '</div>' : ''; ?>
    </p>
    <?php if(isset($value['id'])):?>
        <input type="hidden" value="<?php echo $value['id'];?>" name="<?php echo 'items[' . $uniq . '][recordid]' ?>" id="<?php echo 'record_'.$value['id']; ?>" class="itid">
        <input type="hidden" value="<?php echo ($ajax)?(string)'ajax':(string)'std'; ?>" name="ajax" class="ajax">
    <?php endif; ?>
    <a href="" class="deleteItem btn btn_orange">Delete Item</a>
</div>
<script type="text/javascript">
    $(document).ready(function(){

    });

</script>