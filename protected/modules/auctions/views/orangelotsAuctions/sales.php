<div class="breadcrumb_row clearfix">
    <div class="main_container">

        <?php
        $this->widget(
            'zii.widgets.CBreadcrumbs',
            array(
                'links' => array(
                    'Sell',
                    'My deals',
                    'Sales'
                ),
                'separator' => '',
                'tagName' => 'ul',
                'htmlOptions' => array('class' => ''),
                'inactiveLinkTemplate' => '<li><a>{label}</a></li>',
                'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                        "site/index"
                    ) . '"></a></li>',
            )
        );
        ?>
    </div>
</div>


<div class="posted_by__container">
<div class="main_container">
<p class="posted_by__title">Search by</p>
<div class="posted_by__form_container clearfix">
    <form method="post" id="search_form">
        <div class="posted_by__form_left_side pull-left">
            <div class="posted_by__form_row">
                <div class="posted_by__form_element posted_by__form_element__name">
                    <label>Deal #, name</label>
                    <input type="text" name="Filter[name]" value="<?php if(isset($data['name'])):if(!empty($data['name'])): echo $data['name']; endif;endif; ?>">
                </div>



                <div class="posted_by__form_element posted_by__form_element__status">
                    <label>Status</label>
                    <select class="selectpicker" name="Filter[status]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['status'])):if($data['status'] == 1): echo 'selected="selected"';  endif;endif; ?>>Open</option>
                        <option value="2"<?php if(isset($data['status'])):if($data['status'] == 2): echo 'selected="selected"';  endif;endif; ?>>Close</option>
                    </select>
                </div>
            </div>

            <div class="posted_by__form_row ">
                <div class="posted_by__form_element posted_by__form_element__days col_filter">
                    <label>Start in</label>
                    <select class="selectpicker col_filter" name="Filter[start]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['start'])):if($data['start'] == 1): echo 'selected="selected"';  endif;endif; ?>>1 day</option>
                        <option value="3" <?php if(isset($data['start'])):if($data['start'] == 3): echo 'selected="selected"';  endif;endif; ?>>3 days</option>
                        <option value="7" <?php if(isset($data['start'])):if($data['start'] == 7): echo 'selected="selected"';  endif;endif; ?>>1 week</option>
                        <option value="14" <?php if(isset($data['start'])):if($data['start'] == 14): echo 'selected="selected"';  endif;endif; ?>>2 weeks</option>
                        <option value="30" <?php if(isset($data['start'])):if($data['start'] == 30): echo 'selected="selected"';  endif;endif; ?>>month</option>
                    </select>
                </div>

                <div class="posted_by__form_element posted_by__form_element__days col_filter ">
                    <label>Close in</label>
                    <select class="selectpicker" name="Filter[end]">
                        <option value="empty">Choose if need</option>
                        <option value="1" <?php if(isset($data['end'])):if($data['end'] == 1): echo 'selected="selected"';  endif;endif; ?>>1 day</option>
                        <option value="3" <?php if(isset($data['end'])):if($data['end'] == 3): echo 'selected="selected"';  endif;endif; ?>>3 days</option>
                        <option value="7" <?php if(isset($data['end'])):if($data['end'] == 7): echo 'selected="selected"';  endif;endif; ?>>1 week</option>
                        <option value="14" <?php if(isset($data['end'])):if($data['end'] == 14): echo 'selected="selected"';  endif;endif; ?>>2 weeks</option>
                        <option value="30" <?php if(isset($data['end'])):if($data['end'] == 30): echo 'selected="selected"';  endif;endif; ?>>month</option>
                    </select>
                </div>

                <div class="posted_by__form_element posted_by__form_element__days">

                    <button class="btn_image">
                        <img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt="">
                    </button>
                </div>
            </div>

            <div class="posted_by__form_row_checkers">
            </div>

            <div>
                <p><a href="" class="orange_link advanced_search">Advanced search</a></p>

                <div class="advanced_search_block" style="display:<?php echo $visible?>">
                <div class="posted_by__form_block">
                    <label>Start Date</label>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> 'from',
                                'name'=> "Filter[start_date_from]",
                                'value'=> (!empty($data['start_date_from']))?$data['start_date_from']:'',
                            )

                        )
                    );
                    ?>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> 'to',
                                'name'=> "Filter[start_date_to]",
                                'value'=> (!empty($data['start_date_to']))?$data['start_date_to']:'',
                            )

                        )
                    );
                    ?>
                </div>

                <div class="posted_by__form_block">
                    <label>Close Date</label>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> "from",
                                'name'=> "Filter[end_date_from]",
                                'value'=> (!empty($data['end_date_from']))?$data['end_date_from']:'',
                            )

                        )
                    );
                    ?>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => new OrangelotsAuctions,
                            'attribute' => 'au_date',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'placeholder'=> "to",
                                'name'=> "Filter[end_date_to]",
                                'value'=> (!empty($data['end_date_to']))?$data['end_date_to']:'',
                            )
                        )
                    );
                    ?>
                </div>
            </div>
            </div>
        </div>

        <?php if ((user()->isVendor()) || (user()->isAdmin())):?>
        <div class="posted_by__form__right_side text-center pull-left">
            <a href="<?php echo createUrl('flashdeal/default/create')?>" class="btn btn_image">
                <img src="<?php echo baseUrl().'/images/btn/new_deal_btn.png'?>" alt="">
            </a>
            <p><a href="<?php echo Yii::app()->createUrl('site/upload')?>" class="orange_link">or Import data file</a></p>
        </div>
        <?php endif; ?>
    </form>
</div>
    <div class="posted_by__table_auction_list__container">
        <?php $this->widget(
            'MyGridView',
            array(
                'id' => 'orangelots-category-grid',
                'dataProvider' => $dataProvider,
                'summaryText' => '',
                'columns' => array(
                    array(
                        'name' => 'col',
                        'header' => 'Deal#',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->id',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Name',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->name',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Start Time',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->start_date_unix)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Close Time',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->end_date_unix)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Status',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsAuctionsStatus::model()->findByPk(OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->status)->name',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'transaction_id',
                        'header' => 'TransactionID',
                        'type' => 'raw',
                        'sortable' => false,
                        'htmlOptions'=>array('width'=>'70'),
                        'value' => 'CHtml::link("$data->transaction_id",createUrl("user/sell/payments", array("transaction" => $data->transaction_id)), array("class" => "orange_link"))'
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Buyer Name',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsUsers::model()->findByPk($data->owner_id)->firstname." ".OrangelotsUsers::model()->findByPk($data->owner_id)->lastname',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Email',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsUsers::model()->findByPk($data->owner_id)->email',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Phone',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsUsers::model()->findByPk($data->owner_id)->phone',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Lots',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsWinners::model()->findByPk($data->winner_table_id)->qty',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Price Per Lot',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => '"$".OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->price_per_unit*OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->units_in_lot',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => 'Total',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => '"$".OrangelotsWinners::model()->findByPk($data->winner_table_id)->qty*(OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->price_per_unit*OrangelotsAuctions::model()->findByPk($data->auction_deal_id)->units_in_lot)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'purchase_date',
                        'header' => 'Purchase date',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", $data->purchase_date)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                    array(
                        'name' => 'col',
                        'header' => '',
                        'type' => 'raw',
                        'sortable' => false,
                        'value' => 'OrangelotsAuctions::invoice_href($data->transaction_id)',
                        'htmlOptions'=>array('width'=>'70'),
                    ),
                ),
                'pager' => array(
                    'htmlOptions' => array(
                        'class' => 'pager_lk'
                    )
                ),
                'itemsCssClass' => 'text-center',
            )
        ); ?>
    </div>
</div>
</div>


