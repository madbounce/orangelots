<?php
/* @var $this OrangelotsAuctionsController */
/* @var $data OrangelotsAuctions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manufacture')); ?>:</b>
	<?php echo CHtml::encode($data->manufacture); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mvrp')); ?>:</b>
	<?php echo CHtml::encode($data->mvrp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('units_in_lot')); ?>:</b>
	<?php echo CHtml::encode($data->units_in_lot); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('buy_now_price')); ?>:</b>
	<?php echo CHtml::encode($data->buy_now_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('min_bid')); ?>:</b>
	<?php echo CHtml::encode($data->min_bid); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('min_unit')); ?>:</b>
	<?php echo CHtml::encode($data->min_unit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_date')); ?>:</b>
	<?php echo CHtml::encode($data->start_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('condition')); ?>:</b>
	<?php echo CHtml::encode($data->condition); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('color')); ?>:</b>
	<?php echo CHtml::encode($data->color); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dimensions')); ?>:</b>
	<?php echo CHtml::encode($data->dimensions); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('weight')); ?>:</b>
	<?php echo CHtml::encode($data->weight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('terms')); ?>:</b>
	<?php echo CHtml::encode($data->terms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shipping_terms')); ?>:</b>
	<?php echo CHtml::encode($data->shipping_terms); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shipping_terms_other')); ?>:</b>
	<?php echo CHtml::encode($data->shipping_terms_other); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('delivery')); ?>:</b>
	<?php echo CHtml::encode($data->delivery); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('delivery_other')); ?>:</b>
	<?php echo CHtml::encode($data->delivery_other); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('returns_warranty')); ?>:</b>
	<?php echo CHtml::encode($data->returns_warranty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vendors_information')); ?>:</b>
	<?php echo CHtml::encode($data->vendors_information); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
	<?php echo CHtml::encode($data->category_id); ?>
	<br />

	*/ ?>

</div>