<?php
/* @var $this OrangelotsAuctionsController */
/* @var $model OrangelotsAuctions */

$this->breadcrumbs = array(
    'Orangelots Auctions' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List OrangelotsAuctions', 'url' => array('index')),
    array('label' => 'Manage OrangelotsAuctions', 'url' => array('admin')),
);
?>
<div class="breadcrumb_row clearfix">
    <div class="main_container">

        <?php

        $this->widget(
            'zii.widgets.CBreadcrumbs',
            array(
                'links' => array(
                    'Home' => array('site/index'),
                    'Create auction',
                ),
                'separator' => '',
                'tagName' => 'ul',
                'htmlOptions' => array('class' => ''),
                'inactiveLinkTemplate' => '<li><a href="">{label}</a></li>',
                'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href=""></a></li>',
            )
        );
        ?>
    </div>
</div>
<div class="uni_form_container">
    <div class="main_container">
        <?php $this->renderPartial('_form', array('model' => $model)); ?>
    </div>
</div>