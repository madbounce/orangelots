<div class="catalog_detail_container clearfix">
<div class="main_container">
<p class="catalog_detail__title">
    Auction ID
    <?php echo $model->id ?>
    - <?php echo $model->name ?>
    <span class="catalog_detail">(Original MSRP $<?php echo number_format($model->mvrp, 2) ?>)</span>
</p>
<div class="row">
<div class="main_collum">
    <div class="catalog_detail__image pull-left">
        <div class="catalog_detail__main_image">
            <?php if(OrangelotsAuctions::main_photo($model)):?>
                <? foreach ($model->galleryPhotos AS $key => $value): ?>
                    <? if ($value->set_main == 1): ?>
                        <img src="<?php echo createUrl($value->getThumb()) ?>" alt="">
                    <? endif; ?>
                <? endforeach; ?>
            <?php else:?>
                <?php if(isset($model->galleryPhotos[0])):?>
                    <img src="<?php echo createUrl($model->galleryPhotos[0]->getThumb()) ?>" alt="">
                <?php else:?>
                    <img src="<?php echo yii()->baseUrl.'/images/NIA.png' ?>" alt="">
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="catalog_detail__galery_image">
            <div class="catalog_detail__galery_image__carousel" id="card_detail_carousel">
                <ul>
                    <?php foreach ($model->galleryPhotos AS $key => $value): ?>
                        <li>
                            <a href="">
                                <img src="<?php echo createUrl($value->getThumb()) ?>" alt=""/>
                            </a>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
            <div class="catalog_detail__galery_image__control">
                <a href="" class="catalog_detail__galery_image__control__btn prev"></a>
                <a href="" class="catalog_detail__galery_image__control__btn next"></a>
            </div>
        </div>
    </div>
    <div class="catalog_detail__price_container pull-left">
        <p class="catalog_detail__price__numbers no_margin">Price per unit: <span>$<?php echo number_format($model->price_per_unit, 2) ?></span></p>
        <p class="catalog_detail__price__units">Units in Lot: <span><?php echo $model->units_in_lot ?></span></p>

        <div class="clearfix"></div>
        <?if(OrangelotsSubscribe::canSubscribe()):?>
            <p class="catalog_detail__price__units pull-left">Subscribe to our daily deals alerts
                <?$this->widget('ext.subscribe.SubscribeWidget')?>
            </p>
        <?else:?>
            <p class="catalog_detail__price__numbers no_margin"><span>You have already subscribed for daily deals</span></p>
        <?endif;?>

        <div class="catalog_detail__price__time_ended">
            <?$info = getTimeFromNowInfo($model->start_date_unix);?>
            <p>Starts in:<span>
                            <?php echo $info->days.' '.plural($info->days, 'Days', 'Day', 'Days').', ' ?>
                            <?php echo $info->h.' '.plural($info->h, 'hours', 'hour', 'hours').', '.$info->m.' '.plural($info->m,'minutes', 'minute', 'minutes')?>
                        </span>
            </p>
            <p>Closing time: <?php echo yii()->dateFormatter->format('MM/dd/yyyy hh:mm:ss a',$model->end_date_unix); ?> EST</p>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="card_detail__tabs_container clearfix">
        <div class="card_detail_tabs_menu">
            <?$questions = AuctionQuestion::getAuctionQuestions($model->id);?>
            <ul>
                <li>
                    <a href="javascript:void(0)" class="question <?php echo !empty($questions) ? 'active':''?>">Questions</a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="description <?php echo empty($questions) ? 'active':''?>">Descriptions</a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="terms">Terms</a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="returns_and_warranty">Returns and warranty</a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="comments">Feedback</a>
                </li>
            </ul>
        </div>

        <div class="card_detail__tabs_content card_detail__tab_question <?php echo empty($questions) ? 'hide':''?>">
            <?if(!OrangelotsAuctions::isOwner($model->id)):?>
                <div class="clearfix text-right">
                    <?php echo CHtml::ajaxLink(
                        "Ask a question",
                        yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                        array(
                            'data' => array(
                                'auction' => $model->id
                            ),
                            'type' => 'POST',
                            'success' => "function(data){ $('body').append(data);}",
                        ),
                        array(
                            'class' => 'btn question_btn'
                        )
                    );
                    ?>
                </div>
            <?endif;?>
            <div class="card_detail__tab_question__list ">
                <?$this->widget('ext.AuctionQuestionsWidget.AuctionQuestionsWidget', array('auctionId' => $model->id))?>
            </div>
        </div>
        <div class="card_detail__tabs_content card_detail__tab_description <?php echo !empty($questions) ? 'hide':''?>">
            <?if(!OrangelotsAuctions::isOwner($model->id)):?>
                <div class="clearfix text-right">
                    <?php echo CHtml::ajaxLink(
                        "Ask a question",
                        yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                        array(
                            'data' => array(
                                'auction' => $model->id
                            ),
                            'type' => 'POST',
                            'success' => "function(data){ $('body').append(data);}",
                        ),
                        array(
                            'class' => 'btn question_btn'
                        )
                    );
                    ?>
                </div>
            <?endif;?>
            <p><label>Description:</label><?php echo $model->description ?></p>
            <p><label>Condition:</label><?php echo Condition::model()->findByPk($model->condition)->name ?></p>
            <p><label>Manufacture:</label><?php echo OrangelotsManufacture::model()->findByPk($model->manufacture)->name ?></p>
            <p><label>Color:</label><?php echo $model->color ?></p>
            <p><label>Product Dimensions:</label><?php echo $model->dimensions ?></p>
            <?if(!empty($model->upholstery_material)):?>
                <p><label>Upholstery material:</label><?php echo $model->upholstery_material ?></p>
            <?endif;?>
            <?if(!empty($model->number_of_pieces)):?>
                <p><label>Number of pieces:</label><?php echo $model->number_of_pieces ?></p>
            <?endif;?>
            <?if(!empty($model->furniture_function)):?>
                <p><label>Furniture function:</label><?php echo $model->furniture_function ?></p>
            <?endif;?>
            <?if(!empty($model->style_id)):?>
                <p><label>Style:</label><?php echo OrangelotsStyle::getNameById($model->style_id); ?></p>
            <?endif;?>
        </div>
        </div>
        <div class="card_detail__tabs_content card_detail__tab_terms hide">
            <?if(!OrangelotsAuctions::isOwner($model->id)):?>
                <div class="clearfix text-right">
                    <?php echo CHtml::ajaxLink(
                        "Ask a question",
                        yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                        array(
                            'data' => array(
                                'auction' => $model->id
                            ),
                            'type' => 'POST',
                            'success' => "function(data){ $('body').append(data);}",
                        ),
                        array(
                            'class' => 'btn question_btn'
                        )
                    );
                    ?>
                </div>
            <?endif;?>
            <?php echo $model->terms ?>
        </div>
        <div class="card_detail__tabs_content card_detail__tab_returns_and_warranty hide">
            <?if(!OrangelotsAuctions::isOwner($model->id)):?>
                <div class="clearfix text-right">
                    <?php echo CHtml::ajaxLink(
                        "Ask a question",
                        yii()->createUrl('auctions/orangelots-auctions/ask-render'),
                        array(
                            'data' => array(
                                'auction' => $model->id
                            ),
                            'type' => 'POST',
                            'success' => "function(data){ $('body').append(data);}",
                        ),
                        array(
                            'class' => 'btn question_btn'
                        )
                    );
                    ?>
                </div>
            <?endif;?>
            <?php echo $model->returns_warranty ?>
        </div>
        <div class="card_detail__tabs_content card_detail__tab_comments hide">
            <?php echo OrangelotsAuctions::commentAuction($model->user_id);?>
        </div>
    </div>
<div class="second_collum">
    <div class="card_detail__delivery_info">

        <div class="card_detail__delivery_info__shipping">
            <p class="card_detail__delivery_info__title">Shipping information</p>

            <p class="card_detail__delivery_info__shiping_text">
                <span>Shipping Weight:</span> <?php echo $model->weight ?> pounds</p>

            <p class="card_detail__delivery_info__shiping_text">
                <span>Shipping Terms:</span> <?php echo ShippingTerms::model()->findByPK(
                    $model->shipping_terms
                )->name ?></p>

            <p class="card_detail__delivery_info__shiping_text">
                <span>Delivery:</span> <?php echo Delivery::model()->findByPK($model->delivery)->name ?></p>
        </div>

        <div class="card_detail__delivery_info__vendor">
            <p class="card_detail__delivery_info__vendor_title">Vendor information</p>

            <p class="card_detail__delivery_info__vendor_adress"><?php echo $model->vendors_information?><?php /*echo $vendor->company*/?><!-- <br/> --><?php /*echo $vendor->address_1 . ' '  . $vendor->address_2 . ' ' . OrangelotsStates::getStateLabel($vendor->state).  ' '  . $vendor->zipcode.', ' . 'USA' */?></p>
            <p class="card_detail__delivery_info__vendor_rate">Rating:
                <a href="<?php echo createUrl('/account/default/vendor-list')?>"><?php echo $rating; ?> out of 10</a>
            </p>
        </div>

    </div>
    <?php $this->widget('ext.RightColumnListWidget.RightColumnListWidget', array('seller'=> $model->user_id, 'category' => $model->category_id)); ?>
</div>
</div>

</div>
</div>
</div>