<?php
/**
 * Шаблон формы редактирования новости
 * @author maxshurko@gmail.com
 */
?>

	<h1>Редактирование программы</h1>

<?= $this->renderPartial('_form', array('model' => $model)); ?>