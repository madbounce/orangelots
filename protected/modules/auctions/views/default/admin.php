<?/***
 * @var $model Realty
 */
?>
	<h1>Управление недвижимостью</h1>
<? $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'realty-grid',
	'dataProvider' => $model->searchRealtor(),
	'filter' => $model,
	'columns' => array(
//		'id',
		array(
            'header' => 'Категория',
            'name' => 'category_id',
            'value' => 'Realty::model()->getCategoryById($data->category_id)',
            'filter' => Realty::model()->getAll(),
		),
		array(
			'name' => 'title',
		),
		'alias',
		'description',
		'timeCreate',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}{delete}'
		),
	),
));
