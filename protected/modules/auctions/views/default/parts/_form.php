<?/***
 * @var $form TbActiveForm
 * @var $model Programs
 */
?>


<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'realty-form',
	'type' => 'horizontal',
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->checkBoxRow($model, 'is_active'); ?>


<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'alias', array('class' => 'span5', 'maxlength' => 255)); ?>

<?php echo $form->textFieldRow($model, 'description', array('class' => 'span5', 'maxlength' => 10000)); ?>

<?= $form->textFieldRow($model, 'price') ?>
<?$this->widget('ext.editor.editMe.widgets.ExtEditMe', array(
    'model' => $model,
    'attribute' => 'content',
    'filebrowserImageUploadUrl'=>baseUrl().'/images/',
    'toolbar' => array(
        array('Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'),
        array('Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo',),
        array('Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'),
        array('Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'),
        '/',
        array('Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat',),
        array('NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl',),
        array('Link', 'Unlink', 'Anchor',),
        array('Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'),
        '/',
        array('Styles', 'Format', 'Font', 'FontSize',),
        array('TextColor', 'BGColor',),
        array('Maximize', 'ShowBlocks',),
    )))?>

<?php echo $form->textFieldRow($model, 'seo_keywords', array('class' => 'span5', 'maxlength' => 500)); ?>

<?php echo $form->textFieldRow($model, 'seo_description', array('class' => 'span5', 'maxlength' => 500)); ?>

<div class="form-actions">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' => $model->isNewRecord ? 'Create' : 'Save',
    )); ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
	$('body').on('click', '.js-group-title', function (e) {
		e.preventDefault();
		self = $(this).parent().parent().find('.control-group');
		if (self.css('display') == 'none') {
			self.show();
		} else {
			self.hide();
		}
	});
</script>
