<?php
/**
 * Шаблон списка в панеле управления
 * @author fen0man
 */
?>

<? // $this->widget("ext.VendorsPageMenu.VendorsPageMenuWidget") ?>
<div class="main_page__container">
    <div class="main_container clearfix">
        <div class="main_page__top_preview clearfix">

            <div class="main_page_dashboard__top_preview" style="background-color: #414141">
                <div class="main_page_dashboard__top_preview__contact">
                    <p class="main_page_dashboard__top_preview__title">Contact Information</p>

                    <p class="main_page__top_preview_image_text"><?php echo $user->firstname . ' ' . $user->lastname; ?></p>

                    <p class="main_page__top_preview_image_text"><?php echo $user->phone ?></p>

                    <p class="main_page__top_preview_image_text"><?php echo $user->email; ?></p>
                    <!--                    <button class="btn btn_orange">Edit</button>-->
                    <a href="<?php echo yii()->createUrl('user/orangelots-users/update'/*, array('id' => $user->id)*/) ?>"
                       class="btn btn_orange">Edit
                    </a>
                </div>

                <div class="main_page_dashboard__top_preview__contact">
                    <p class="main_page_dashboard__top_preview__title">Billing Information</p>

                    <p class="main_page__top_preview_image_text">**** **** **** 3427</p>

                    <p class="main_page__top_preview_image_text">VISA</p>

                    <p class="main_page__top_preview_image_text">01 / 14</p>
                    <button class="btn btn_orange">Edit</button>
                </div>
            </div>


            <div class="main_page__top_preview_argument">
                <p class="main_page__top_preview_argument__title">
                    <span>5 Reasons</span><br/>
                    to become a vendor
                </p>
                <ul class="main_page__top_preview_argument__list">
                    <li>No subscription for vendors</li>
                    <li>No upfront payment</li>
                    <li>No hidden fee</li>
                    <li>Personal manager for each vendor</li>
                </ul>
                <?if(!user()->isVendor(user()->id)):?>
                    <a class="btn_image main_page__top_preview_argument__btn" href="<?php echo createUrl('user/orangelots-users/create')?>"><img
                        src="<?php echo baseUrl() . '/images/btn/btn_become_a_vendor_black.png' ?>" alt=""/></a>
                <?endif;?>
                <p class="main_page__top_preview_argument__info">We don't charge you to list your product. We help you
                    to sale them fast and easy.</p>
            </div>
        </div>
    </div>
</div>
<div class="dashboard__messeges_notification">
    <div class="main_container">
        <p class="dashboard__messeges_notification__title">Messages and notifications</p>
        <table class="dashboard__messeges_notification__table" cellpading="0" cellspacing="0">
            <tbody>
            <tr>
                <td>
                    New Bid of $120.00 arrived for
                    <a href="">Dining Sets, Samsung 3D Glasses, Coach's Megaphone</a>
                    from Paul, FL

                </td>
                <td class="dashboard__messeges_notification__table_link_td">
                    <a href="">dismiss</a>
                </td>
            </tr>
            <tr>
                <td>
                    Paul, FL overbid you on
                    <a href="">Dining Sets, Samsung 3D Glasses, Coach's Megaphone</a>
                    - new bid is $200.00
                </td>
                <td class="dashboard__messeges_notification__table_link_td">
                    <a href="">dismiss</a>
                </td>
            </tr>

            <tr>
                <td>
                    Vendor selected you as winner for
                    <a href="">Dining Sets, Samsung 3D Glasses, Coach's Megaphone</a>
                </td>
                <td class="dashboard__messeges_notification__table_link_td">
                    <a href="">dismiss</a>
                </td>
            </tr>

            <tr>
                <td>
                    Payment of $1,300.00 arrived for your flash deal
                    <a href="">Dining Sets, Samsung 3D Glasses, Coach's Megaphone</a>
                </td>
                <td class="dashboard__messeges_notification__table_link_td">
                    <a href="">dismiss</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>



