<?php

class LogController extends Controller
{
	public $layout = '//layouts/lk';

	private $_model;

	public function actionIndex()
	{
		$model = $this->loadUser();
		$model = new CArrayDataProvider($model->feeds);
		$this->render("index", array(
			'model' => $model
		));
	}

	public function loadUser()
	{
		if ($this->_model === null) {
			if (Yii::app()->user->id) {
				$this->_model = Yii::app()->controller->module->user();
			}
			if ($this->_model === null) {
				$this->redirect(Yii::app()->controller->module->loginUrl);
			}
		}
		return $this->_model;
	}
}