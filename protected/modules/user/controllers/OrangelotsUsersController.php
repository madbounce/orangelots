<?php

class OrangelotsUsersController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';

    /**
     * @return array action filters
     */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//			'postOnly + delete', // we only allow deletion via POST request
//		);
//	}

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        /*return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('update'),
                'users' => array('@'),
            ),
            array('allow',
                'actions' => array('create'),
                'users' => array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );*/
        return array('rights');
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view', array(
                'model' => $this->loadModel($id),
            ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->layout = '//layouts/main';
        if(!yii()->user->isGuest){
            $model = OrangelotsUsers::model()->findByPk(yii()->user->id);
        } else {
            $model = new OrangelotsUsers("create");
        }

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['OrangelotsUsers'])) {
            if(!$model->isNewRecord){
                $model->attributes = $_POST['OrangelotsUsers'];
                if($model->save()){
                    yii()->authManager->revoke('Authenticated', $model->id);
                    yii()->authManager->assign('Vendor', $model->id);
                    yii()->user->setFlash('success', 'You registered as Vendor. Please login once more to use vendor functions.');
                    $this->redirect(yii()->createUrl('site/index'));
                }
            }
            $model->attributes = $_POST['OrangelotsUsers'];
            do {
                $usr = 'id' . rand(100000, 999999);
                $user = User::model()->findByAttributes(array('username' => $usr));
            } while ($user !== null);

            $model->username = $usr;
            if ($model->validate()) {
                $model->password = UserModule::encrypting($model->password);
                $model->activkey = UserModule::encrypting(microtime() . $model->password);
                $model->superuser = 0;
                $model->status = User::STATUS_NOACTIVE;
            }
            if ($model->save()) {
                $profile = new Profile;
                $profile->user_id = $model->id;
                $profile->save();
                yii()->user->setFlash('success', 'Registration is successful. Activation link is sent to your email. Please, check your email.');
                $activation_url = yii()->createAbsoluteUrl('/user/activation/activation', array("activkey" => $model->activkey, "email" => $model->email, "vendor" => 'y'));
                UserModule::sendMail(
                    $model->email,
                    UserModule::t("You registered from {site_name}", array('{site_name}' => yii()->name)),
                    UserModule::t(
                        "<p>Dear {username},</p></br>

                        <p>Thank you for registering for Orangelots.com. Before we can activate your account one last step must be taken to complete your registration.</p></br>

                        <p>Please note - you must complete this last step to become a registered member. You will only need to visit this url once to activate your account.</p></br>

                        <p>To complete your registration, please visit this url:</p></br>
                        <p>{activation_url} </p></br>

                        <p>Thanks very much,</p></br>
                        <p>Orangelots.com team</p></br>
                         ",
                        array(
                            '{activation_url}' => $activation_url,
                            '{userID}' => $model->id,
                            '{login}' => $model->email,
                            '{site_name}' => yii()->name,
                            '{username}' => $model->firstname
                        )
                    )
                );
                $this->redirect(yii()->createUrl('site/index'));
            }
        }

        $this->render('create', array(
                'model' => $model,
            ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id = null)
    {
        if (user()->isAdmin()) {
            $this->layout = 'application.views.layouts.admin';

        } else {
            $this->redirect(array('updateData'));
        }

        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['OrangelotsUsers'])) {
            if ((isset($_POST['OrangelotsUsers']['password'])) && ($_POST['OrangelotsUsers']['password'] != null) && (!empty($_POST['OrangelotsUsers']['password'])) && ($_POST['OrangelotsUsers']['password'] != '')){
                $model->attributes = $_POST['OrangelotsUsers'];
                $model->password = UserModule::encrypting($_POST['OrangelotsUsers']['password']);
            } else {
                unset($_POST['OrangelotsUsers']['password']);
                $model->attributes = $_POST['OrangelotsUsers'];
            }

            if ($model->save()) {
                if (user()->isAdmin()) {
                    $this->redirect(yii()->createUrl('user/orangelots-users/admin'));
                }else{
                    $this->redirect(yii()->createUrl('account'));
                }
            }
        }
        $this->render('update', array(
                'model' => $model,
                'buyer' => user()->isAuthenticated($id)
            ));
    }

    private function processFile($model)
    {
        if (yii()->request->isPostRequest) {
            $model->avatar = CUploadedFile::getInstance($model, 'avatar');
            if ($model->avatar instanceof CUploadedFile) {
                $model->avatar = yii()->storage->addFile($model->avatar->tempName, $model->avatar->name);
            }
            return $model;
        }
    }

//	public function actionUpdatePersonalSettings()
    public function actionUpdateData()
    {

        $this->layout = '//layouts/main';
        $model = $this->loadModel(user()->id);

        if (isset($_POST['OrangelotsUsers'])) {

            if ($_POST['OrangelotsUsers']['role'] == OrangelotsUsers::B) {
                $model->setScenario('buyerReg');
            }elseif($_POST['OrangelotsUsers']['role'] == OrangelotsUsers::VU){
                $model->setScenario('vendorUpdate');
            } else {
                $model->setScenario('create');
            }
            $pass = $_POST['OrangelotsUsers']['password'];
            unset($_POST['OrangelotsUsers']['password']);
            $model->attributes = $_POST['OrangelotsUsers'];
            if(!empty($pass)){
                $model->password = UserModule::encrypting($_POST['OrangelotsUsers']['password']);
            }
            if(isset($_FILES['OrangelotsUsers']) and !empty($_FILES['OrangelotsUsers']['size']['avatar'])){
                $model->setScenario('download-avatar');
            }
            if ($model->save()) {
                yii()->user->setFlash('success', 'Info updated');
            }
        }

        if (user()->isAuthenticated()) {
            $this->render('update', array(
                    'model' => $model,
                    'buyer' => true
                ));
        } else {
            $this->render('update', array('model' => $model));
        }
    }

    public function actionDownloadAvatar(){
        if(yii()->request->isAjaxRequest){
            if(isset($_FILES['OrangelotsUsers'])){
                $model = $this->loadModel(user()->id);
                $this->processFile($model);
                $model->setScenario('download-avatar');
                if($model->validate()){
                    $model->save();
                    yii()->user->setFlash('success', "Avatar was successfully downloaded.");
                    $this->redirect('updateData');

                }else{
                    yii()->user->setFlash('error', $model->getError('avatar'));
                    $this->redirect('updateData');
                }
            }
        }else{
            throw new CHttpException(400, 'Bad request');
        }
    }

    public function actionBillingInfo(){
        $this->layout = '//layouts/main';
        $current = CardInfo::getUserCardInfo(user()->id);
        if(!empty($current)){
            $model = $current;
        }else{
            $model = new CardInfo();
        }
        if(isset($_POST['CardInfo'])){
            $model->setScenario('create');
            $model->attributes = $_POST['CardInfo'];
            $model->type = $_POST['CardInfo']['type'];
            $model->user_id = user()->id;
            if($model->validate()){
                if($model->isNewRecord){
                    if($model->setPayPalCardInfo()){
                        user()->setFlash('success', 'Info updated');
                    }else{
                        user()->setFlash('error', $_SESSION['paypal_error']);
                    }
                }else{
                    $newInfo = new CardInfo();
                    $newInfo->owner_name = $_POST['CardInfo']['owner_name'];
                    $newInfo->number = $_POST['CardInfo']['number'];
                    $newInfo->type = $_POST['CardInfo']['type'];
                    $newInfo->month = $_POST['CardInfo']['month'];
                    $newInfo->year = $_POST['CardInfo']['year'];
                    $newInfo->cvv = $_POST['CardInfo']['cvv'];
                    $model->deleteFromPayPal();
                    $model->delete();
                    if($newInfo->setPayPalCardInfo()){
                        user()->setFlash('success', 'Info updated');
                    }else{
                        user()->setFlash('error', 'Something wrong with filled data. Please check it out.');
                    }
                }
            }
        }
        $this->render('updateCardInfo', array('model'=>$model,'current' => CardInfo::getUserCardInfo(user()->id)));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        /*
         *
         * $user = $this->loadModel($id);
        $user->deleted = 1;
        $user->update();
        */

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('OrangelotsUsers');
        $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new OrangelotsUsers('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['OrangelotsUsers'])) {
            $model->attributes = $_GET['OrangelotsUsers'];
            $model->role = $_GET['OrangelotsUsers']['role'];
        }

        $this->render('admin', array(
                'model' => $model,
            ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return OrangelotsUsers the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = OrangelotsUsers::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param OrangelotsUsers $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax'])) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionCreateBuyer()
    {
        $model = new OrangelotsUsers('admin_create_buyer');
        if (isset($_POST['OrangelotsUsers'])) {
            $model->attributes = $_POST['OrangelotsUsers'];
            $user = OrangelotsUsers::model()->createUser($model, true, true);
            $this->redirect(array('admin'));
        }
        $this->render('createBuyer', array(
                'model' => $model,
            ));
    }

    public function actionCreateVendor()
    {
        $model = new OrangelotsUsers('admin_create_vendor');
        if (isset($_POST['OrangelotsUsers'])) {
            $model->attributes = $_POST['OrangelotsUsers'];
            $user = OrangelotsUsers::model()->createUser($model, false, true);
            $this->redirect(array('admin'));
        }
        $this->render('createVendor', array(
                'model' => $model,
            ));
    }
}