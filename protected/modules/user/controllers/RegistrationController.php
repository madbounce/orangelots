<?php

class RegistrationController extends Controller
{
	public $layout = '//layouts/lk';
	public $defaultAction = 'registration';

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
		);
	}

	protected function generateRefCode()
	{
		do {
			$refCode = randString(8);
			$exists = User::model()->findByAttributes(array('ref_code' => $refCode));
		} while ($exists !== null);
		return $refCode;
	}

	/**
	 * Registration user
	 */
	public function actionRegistration()
	{
		Profile::$regMode = true;
		$model = new RegistrationForm;
		$profile = new Profile;

		if (Yii::app()->user->id) {
			$this->redirect(Yii::app()->controller->module->profileUrl);
		} else {
			if (isset($_POST['RegistrationForm']) /*&& isset($_POST['Profile'])*/) {
				$model->Register($_POST, $scenario = 'ajax_register');
			}
			$this->render('/user/registration', array('model' => $model, 'profile' => $profile));
		}
	}

	public function actionBuyerRegister()
	{
		$model = new OrangelotsUsers('buyerReg');
		$this->performAjaxValidation($model);
		$this->renderPartial('/user/buyer_register', array('model' => $model), false, true);
	}

	public function actionBuyerFirstStep()
	{
		if (yii()->request->isAjaxRequest) {
			if (isset($_POST['OrangelotsUsers'])) {
				$resp = array();
				$resp['err'] = 1;
				$model = new OrangelotsUsers('buyerReg');
				$model->attributes = $_POST['OrangelotsUsers'];
                if ($model->validate()) {
					OrangelotsUsers::model()->createUser($model, true);
					OrangelotsUsers::model()->buyerFirstLogin($model->email, $_POST['OrangelotsUsers']['password']);
					$card = new CardInfo();
					$resp['err'] = 0;
					$resp['form'] = $this->renderPartial('/user/paypal_form', array('model' => $card, 'name' => $model->firstname, 'reg' => 1), true, true);
				} else {
					$errors = $model->getErrors();
					foreach ($errors as $attr => $error){
						$resp[$attr] = $error[0];
					}
				}
				echo CJSON::encode($resp);
			}
		} else {
			throw new CHttpException('400', 'Bad request');
		}
	}

	public function actionBuyerSkipPayPal()
	{
		if (yii()->request->isAjaxRequest) {
			$resp = array();
			$user = OrangelotsUsers::model()->findByPk(user()->id);
			$resp['form'] = $this->renderPartial('/user/buyer_last_step', array('name' => $user->firstname), true, true);
			echo CJSON::encode($resp);
		} else {
			throw new CHttpException('400', 'Bad request');
		}
	}

	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'buyer-reg-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}