<?php
yii::import('application.modules.user.models.UserLogin');

class LoginController extends Controller
{
	public $layout = '//layouts/lk';

	public $defaultAction = 'login';

	public function actionLogin()
	{
		if (Yii::app()->user->isGuest) {
			$model = new UserLogin;
			// collect user input data
			if (isset($_POST['UserLogin'])) {
				$model->attributes = $_POST['UserLogin'];
				if ($model->validate()) {
					$this->lastViset();
					if (user()->isAdmin()) {
						$this->redirect(createUrl('//admin'));
					} elseif (user()->isVendor(user()->id)) {
						$this->redirect(createUrl('//account'));
					} elseif (user()->isAuthenticated()) {
						$this->redirect(createUrl('site/index'));
					}
				} else {
					user()->setFlash('error', UserModule::t('Incorrect login or password'));
				}
			}
			$this->render('/user/login', array('model' => $model));
		} else {
			$this->redirect(Yii::app()->controller->module->returnUrl);
		}
	}

	private function lastViset()
	{
		if (!Yii::app()->user->isGuest) {
			$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
			$lastVisit->lastvisit_at = date('m/d/Y H:i:s');
			$lastVisit->save();
		}
	}

	public function actionGetNewCode()
	{
		if (isset($_POST['UserLogin'])) {
			if (strpos($_POST['UserLogin']['username'], "@")) {
				$user = User::model()->notsafe()->findByAttributes(array('email' => $_POST['UserLogin']['username']));
			} else {
				$user = User::model()->notsafe()->findByAttributes(array('username' => $_POST['UserLogin']['username']));
			}
			if ($user !== null) {
				$user->onetime_pass = rand(1000, 9999);
				$user->save(false);
//				sms($user->profile->phone, param('managerLoginSmsCode'), array('#CODE#' => $user->onetime_pass));
				$arResponse['html'] = $this->render('getNewCode', null, true);
				echo CJSON::encode($arResponse);
				yii()->end();
			}
		}
	}

	public function actionRenderLogin()
	{
		$model = new UserLogin;
		$this->renderPartial('/user/login_popup', array('model' => $model, 'render' => true), false, true);
	}

	public function actionAjax()
	{
		if (Yii::app()->user->isGuest) {
			$model = new UserLogin;
			// collect user input data
			if ((isset($_POST['UserLogin'])) || (isset($_POST['OrangelotsUsers']))) {
				$model->attributes = (isset($_POST['UserLogin'])) ? $_POST['UserLogin'] : $_POST['OrangelotsUsers'];
				if ($model->validate()) {
					$this->lastViset();
					if (user()->isAdmin()) {
						$url = createUrl('//admin');
					} elseif (user()->isVendor(user()->id)) {
						$url = createUrl('//account');
					} elseif (user()->isAuthenticated()) {
						$url = createUrl('site/index');
					}
					echo json_encode(array('status' => 'authenticated', 'url' => $url));
				} else {
					echo json_encode(array('status' => 'error', 'html' => $this->renderPartial('/user/login_popup', array('model' => $model), true, false)));
				}
			}
		} else {
			$this->redirect(Yii::app()->controller->module->returnUrl);
		}
	}
}