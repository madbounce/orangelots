<?php

class RecoveryController extends Controller
{
	public $defaultAction = 'recovery';

	public $layout = '//layouts/main';

	/**
	 * Recovery password
	 */
	public function actionSetNewPassword()
	{
		if (Yii::app()->user->id) {
			$this->redirect(Yii::app()->controller->module->returnUrl);
		} else {
			$email = ((isset($_GET['email'])) ? $_GET['email'] : '');
			$activkey = ((isset($_GET['activkey'])) ? $_GET['activkey'] : '');
			if ($email && $activkey) {
                $form2 = new UserChangePassword;
				$find = User::model()->notsafe()->findByAttributes(array('email' => $email));
				if (isset($find) && $find->activkey == $activkey) {
					if (isset($_POST['UserChangePassword'])) {
						$form2->attributes = $_POST['UserChangePassword'];
						if ($form2->validate()) {
							$find->password = Yii::app()->controller->module->encrypting($form2->password);
							$find->activkey = Yii::app()->controller->module->encrypting(microtime() . $form2->password);
							if ($find->status == 0) {
								$find->status = 1;
							}
							$find->save();
							Yii::app()->user->setFlash('recoveryMessage', UserModule::t("New password is saved."));
							$this->redirect(Yii::app()->controller->module->recoveryUrl);
						}
					}
					$this->render('changepassword', array('form' => $form2));
				} else {
					Yii::app()->user->setFlash('recoveryMessage', UserModule::t("Incorrect recovery link."));
					$this->redirect(Yii::app()->controller->module->recoveryUrl);
				}
            }else{
                user()->setFlash('success', 'New password was set.');
                $this->redirect(createUrl('site/index'));
            }
        }
	}

    public function actionRenderRecovery(){
        if(yii()->request->isAjaxRequest){
            $form = new UserRecoveryForm;
            $form->setScenario('onlyEmail');
            $this->renderPartial('ajax_recovery', array('form' => $form), false, true);
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

    public function actionAjaxRecovery(){
        if(yii()->request->isAjaxRequest){
            $form = new UserRecoveryForm('onlyEmail');
            $resp = array();
            $resp['err'] = 1;
            if (isset($_POST['UserRecoveryForm'])) {
                $form->attributes = $_POST['UserRecoveryForm'];
                if ($form->validate()) {
                    $resp['err'] = 0;
                    $user = User::model()->notsafe()->findbyPk($form->user_id);
                    $activation_url = 'http://' . $_SERVER['HTTP_HOST'] . $this->createUrl(implode(Yii::app()->controller->module->recoveryUrl), array("activkey" => $user->activkey, "email" => $user->email));
                    $subject = UserModule::t("You have requested the password recovery site {site_name}",
                        array(
                            '{site_name}' => Yii::app()->name,
                        ));
                    $message = UserModule::t("You have requested the password recovery site {site_name}. To receive a new password, go to <a href='{activation_url}'>the link</a>",
                        array(
                            '{site_name}' => Yii::app()->name,
                            '{activation_url}' => $activation_url,
                        ));

                    UserModule::sendMail($user->email, $subject, $message);
                    $resp['message'] = 'Please check your email. An instructions was sent to your email address.';
                }else{
                    $resp['message'] = $form->getError('login_or_email');
                }
                echo CJSON::encode($resp);
            }
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

}