<?

class SellController extends Controller
{
    public $layout = '//layouts/main';
    private $_model;

    public function filters()
    {
        return array('rights');
    }

    public function actionPayments($transaction=null){
        if(isset($_SESSION['filter_sell']) and !empty($_SESSION['filter_sell'])){
            unset($_SESSION['filter_sell']);
        }
        if(isset($transaction) and !empty($transaction)){
            $userPayments = OrangelotsInvoices::model()->vendorPayments(user()->id, $transaction);
        }else{
            $userPayments = OrangelotsInvoices::model()->vendorPayments(user()->id);
        }
        $searchModel = new OrangelotsInvoices();
        if(isset($_POST['OrangelotsInvoices']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-vendor-payments-grid')){
            if(isset($_POST['OrangelotsInvoices'])){
                $_SESSION['OrangelotsInvoices'] = $_POST['OrangelotsInvoices'];
            }
            $arr = array();
            $arr['dateFrom'] = $_SESSION['OrangelotsInvoices']['dateFrom'];
            $arr['dateTo'] = $_SESSION['OrangelotsInvoices']['dateTo'];
            $arr['transaction_id'] = $_SESSION['OrangelotsInvoices']['transaction_id'];
            $arr['paypal_id'] = $_SESSION['OrangelotsInvoices']['paypal_id'];
            $arr['to_who'] = $_SESSION['OrangelotsInvoices']['to_who'];
            $arr['auction_deal_id'] = $_SESSION['OrangelotsInvoices']['auction_deal_id'];
            $arr['payed'] = $_SESSION['OrangelotsInvoices']['payed'];
            $_SESSION['filter_sell'] = $arr;
            $userPayments = OrangelotsInvoices::model()->userFilter($_SESSION['OrangelotsInvoices'], $_SESSION['OrangelotsInvoices']['dateFrom'], $_SESSION['OrangelotsInvoices']['dateTo'],true);
        }else{
            unset($_SESSION['OrangelotsInvoices']);
        }
        $this->render('payments', array('payments' => $userPayments, 'searchModel' => $searchModel));
    }

    public function actionConfirmation($transaction){
        $model = $this->loadModel($transaction);
        if($model->owner_id != user()->id and $model->winner_id != user()->id){
            throw new CHttpException('400', 'Bad request');
        }
        $buyerInfo = OrangelotsUsers::model()->findByPk($model->winner_id);
        if(!empty($model->pay_type)){
            $cardInfo = CardInfo::getUserCardInfo($buyerInfo->id);
            $bankInfo = param('payDetails');
        }
        $winnerInfo = OrangelotsWinners::model()->findByPk($model->winner_table_id);
        $this->render('/buy/confirmation',
            array(
                'vendor' => true,
                'canCancel' => OrangelotsInvoices::checkCanCancel($model->payed),
                'model' => $model,
                'buyerInfo' => $buyerInfo,
                'cardInfo' => $cardInfo,
                'bankInfo' => $bankInfo,
                'winnerInfo' => $winnerInfo
            )
        );
    }

    public function actionCancelInvoice(){
        if(yii()->request->isAjaxRequest){
            if(isset($_POST['invoiceId'])){
                $invoice = OrangelotsInvoices::model()->findByPk($_POST['invoiceId']);
                if(!empty($invoice)){
                    $invoice->payed = OrangelotsInvoices::STATUS_CANCELLED;
                    $invoice->transaction_date = time();
                    $invoice->vendor_comment = $_POST['comment'];
                    $invoice->cancel();
                    $invoice->save();
                }
            }
        }else{
            throw new CHttpException('400','Bad request');
        }
    }

    public function loadModel($id = null)
    {
        if ($this->_model === null) {
            if (isset($id)) {
                $this->_model = OrangelotsInvoices::model()->findByAttributes(array('transaction_id' => $id));
            }
            if ($this->_model === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
        }
        return $this->_model;
    }

    public function actionBuyerList()
    {
        $criteria = new CDbCriteria;
        $query = yii()->db->createCommand('SELECT DISTINCT user_id FROM orangelots_winners WHERE owner_auction_id ='.yii()->user->id)->queryAll();
        if((isset($query))&&(!empty($query))){
            foreach($query AS $key => $value){
                $ids[] = $value['user_id'];
            }
            $criteria->addInCondition('id ', $ids);
        }
        if (isset($_POST['Search']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-category-grid')){
            if(isset($_POST['Search'])){
                $_SESSION['Search'] = $_POST['Search'];
            }
            if(isset($_SESSION['Search']['name_phone_email'])){
                $criteria->addCondition('(firstname LIKE "%'.$_SESSION['Search']['name_phone_email'].'%") OR (phone LIKE "%'.$_SESSION['Search']['name_phone_email'].'%") OR (email LIKE "%'.$_SESSION['Search']['name_phone_email'].'%") ');
            }
            if(isset($_SESSION['Search']['company'])){
                $criteria->addCondition('company LIKE "%'.$_SESSION['Search']['company'].'%"');
            }
        }else{
            unset($_SESSION['Search']);
        }
        $dataProvider = new CActiveDataProvider('OrangelotsUsers', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),

        ));
        $this->render('contact_list', array(
                'dataProvider' => $dataProvider,
                'name_data' => 'Buyers',
            )
        );
    }

    public function actionRenderCancel(){
        if(yii()->request->isAjaxRequest){
            if(isset($_POST['id'])){
                $invoice = OrangelotsInvoices::model()->findByPk($_POST['id']);
                if(!empty($invoice)){
                    $this->renderPartial('cancelpopup', array('model' => $invoice), false, true);
                }
            }
        }else{
            throw new CHttpException('400', 'Bad request');
        }
    }

}