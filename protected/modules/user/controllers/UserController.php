<?

class UserController extends Controller
{
    private $_model;

    public function filters()
    {
        return array('rights');
        /*return CMap::mergeArray(parent::filters(), array(
            'accessControl',
        ));*/
    }

    /*public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }*/

    public function actionView()
    {
        $model = $this->loadModel();
        $this->render('view', array(
                'model' => $model,
            ));
    }

    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('User', array(
            'criteria' => array(
                'condition' => 'status>' . User::STATUS_BANNED,
            ),

            'pagination' => array(
                'pageSize' => Yii::app()->controller->module->user_page_size,
            ),
        ));

        $this->render('index', array(
                'dataProvider' => $dataProvider,
            ));
    }

    public function loadModel()
    {
        if ($this->_model === null) {
            if (isset($_GET['id'])) {
                $this->_model = User::model()->findbyPk($_GET['id']);
            }
            if ($this->_model === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
        }
        return $this->_model;
    }

    public function loadUser($id = null)
    {
        if ($this->_model === null) {
            if ($id !== null || isset($_GET['id'])) {
                $this->_model = User::model()->findbyPk($id !== null ? $id : $_GET['id']);
            }
            if ($this->_model === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
        }
        return $this->_model;
    }

    public function actionSendMailToUser()
    {
        if (yii()->request->isAjaxRequest) {
            if (isset($_POST['userEmail'])) {
                mail($_POST['userEmail'], 'Оплата заказа на сайте' . yii()->name . '', '
				Ваш платеж по счету к оплате №...
				на сумму (без комиссии) ....(валюта)
				был зачислен на счет получателя (ид получателя...то что  стоит в графе получатель)
				посредством (способ оплаты).
				С Вашего счета  Будет списано: (итого к оплате) валюта.
				');
            }
        }
    }

    public function actionSetCardInfo()
    {
        if (yii()->request->isAjaxRequest) {
            $model = new CardInfo();
            $this->performAjaxValidation($model);
            $this->renderPartial('paypal_form', array('model' => $model), false, true);
        } else {
            throw new CHttpException('400', 'Bad request');
        }
    }

    public function actionSaveCardInfo()
    {
        if (yii()->request->isAjaxRequest) {
            if (isset($_POST['CardInfo'])) {
                $resp = array();
				$resp['err'] = 1;
                $model = new CardInfo('create');
                $model->attributes = $_POST['CardInfo'];
                $model->user_id = user()->id;
                $model->setScenario('create');
                if ($model->validate()) {
                    $model->type = $_POST['CardInfo']['type'];
                    if($model->setPayPalCardInfo()){
                        $resp['err'] = 0;
                        $resp['form'] = $this->renderPartial('/user/buyer_last_step', array('name' => $model->user->firstname, 'forText' => $_POST['reg']), true, true);
                    }else{
                        $resp['errorMessage'] = $_SESSION['paypal_error'];
                    }
                }
                else {
                    $errors = $model->getErrors();
                    foreach ($errors as $attr => $error) {
                        $resp[$attr] = $error[0];
                    }
                }
                echo CJSON::encode($resp);
            }
        } else {
            throw new CHttpException('400', 'Bad request');
        }
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pay-card-info') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

	public function actionUsrForm()
	{
		$info = '';
		if (isset($_POST['id'])) {
			$usr = OrangelotsUsers::model()->findByPk((int)$_POST['id']);
//			$info = 'Name:' . $usr->firstname . ' ' . $usr->lastname . PHP_EOL . 'Phone: ' . $usr->phone . PHP_EOL . 'Email: ' . $usr->email . PHP_EOL . 'Company: ' . $usr->company . PHP_EOL . 'Address: ' . $usr->address_1 . ' ' . $usr->address_2 . PHP_EOL
//				. 'City: ' . $usr->city . PHP_EOL . 'State: ' . $usr->state . PHP_EOL . 'Zipcode: ' . $usr->zipcode . PHP_EOL;
            $info = $usr->company . PHP_EOL . $usr->address_1 . ' '  . $usr->address_2 . ' ' . OrangelotsStates::getStateLabel($usr->state).  ' '  . $usr->zipcode.', ' . 'USA';
		}
		echo $info;
	}
}