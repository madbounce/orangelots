<?

class QuestionsController extends Controller
{
    public $layout = '//layouts/main';
    private $_model;

    public function filters()
    {
        return array('rights');
    }

    public function actionIndex(){
        $newQuestions = AuctionQuestion::model()->UserQuestions(user()->id, AuctionQuestion::NOT_ARCHIVE);
        $archiveQuestions = AuctionQuestion::model()->UserQuestions(user()->id, AuctionQuestion::ARCHIVE);
        $myQuestions = AuctionQuestion::model()->getUserQuestions(user()->id);
        $this->render('index', array('new' => $newQuestions, 'archive' => $archiveQuestions, 'myQuestions' => $myQuestions));
    }

    public function loadModel($id = null)
    {
        if ($this->_model === null) {
            if (isset($id)) {
                $this->_model = AuctionQuestion::model()->findbyPk($id);
            }
            if ($this->_model === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
        }
        return $this->_model;
    }
}