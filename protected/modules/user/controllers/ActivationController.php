<?php

class ActivationController extends Controller
{
	public $defaultAction = 'activation';

	public $layout = '//layouts/lk';


	/**
	 * Activation user account
	 */
	public function actionActivation()
	{
		$email = $_GET['email'];
		$activkey = $_GET['activkey'];
		$vendor = $_GET['vendor'];
		if ($email && $activkey) {
			$find = User::model()->notsafe()->findByAttributes(array('email' => $email));
			if (isset($find) && $find->status) {
				user()->setFlash('info', UserModule::t("You account is active."));
			} elseif (isset($find->activkey) && ($find->activkey == $activkey)) {
				$find->activkey = UserModule::encrypting(microtime());
				$find->status = 1;
				$find->save();
				if (yii()->getModule('admin')->hasModule('rights')) {
					if (isset($vendor)) {
						yii()->authManager->assign('Vendor', $find->id);
					} else {
						$authenticatedName = yii()->getModule('admin')->getModule('rights')->authenticatedName;
						yii()->authManager->assign($authenticatedName, $find->id);
					}

				}
				user()->setFlash('success', UserModule::t("You account is activated."));
			} else {
				user()->setFlash('error', UserModule::t("Incorrect activation URL."));
			}
		} else {
			user()->setFlash('error', UserModule::t("Incorrect activation URL."));
		}
		$this->redirect(yii()->createUrl('site/index'));
	}

}