<?

class BuyController extends Controller
{
    public $layout = '//layouts/main';
    private $_model;

    public function filters()
    {
        return array('rights');
    }

    public function actionPayments(){
        if(!empty($_SESSION['filter_buy'])){
            unset($_SESSION['filter_buy']);
        }
        $userPayments = OrangelotsInvoices::model()->winnerPayments(user()->id);
        $searchModel = new OrangelotsInvoices();
        if(isset($_POST['OrangelotsInvoices']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-buyer-payments-grid')){
            if(isset($_POST['OrangelotsInvoices'])){
                $_SESSION['OrangelotsInvoices'] = $_POST['OrangelotsInvoices'];
            }
            $arr = array();
            $arr['dateFrom'] = $_SESSION['OrangelotsInvoices']['dateFrom'];
            $arr['dateTo'] = $_SESSION['OrangelotsInvoices']['dateTo'];
            $arr['transaction_id'] = $_SESSION['OrangelotsInvoices']['transaction_id'];
            $arr['paypal_id'] = $_SESSION['OrangelotsInvoices']['paypal_id'];
            $arr['from_who'] = $_SESSION['OrangelotsInvoices']['from_who'];
            $arr['auction_deal_id'] = $_SESSION['OrangelotsInvoices']['auction_deal_id'];
            $arr['payed'] = $_SESSION['OrangelotsInvoices']['payed'];
            $_SESSION['filter_buy'] = $arr;
            $userPayments = OrangelotsInvoices::model()->userFilter($_SESSION['OrangelotsInvoices'], $_SESSION['OrangelotsInvoices']['dateFrom'], $_SESSION['OrangelotsInvoices']['dateTo']);
        }else{
            unset($_SESSION['OrangelotsInvoices']);
        }
        $this->render('payments', array('payments' => $userPayments, 'searchModel' => $searchModel));
    }

    public function actionConfirmation($transaction){
        $model = $this->loadModel($transaction);
        if($model->owner_id != user()->id and $model->winner_id != user()->id){
            throw new CHttpException('400', 'Bad request');
        }
        $winnerInfo = OrangelotsWinners::model()->findByPk($model->winner_table_id);
        $auction = OrangelotsAuctions::model()->findByPk($winnerInfo->auction_id);
        $this->render('confirmation',
            array(
                'vendor' => false,
                'model' => $model,
                'buyerInfo' => OrangelotsUsers::model()->findByPk($model->winner_id),
                'cardInfo' => CardInfo::getUserCardInfo($model->winner_id),
                'winnerInfo' => $winnerInfo,
                'bankInfo' => param('payDetails'),
                'auction' => $auction,
            )
        );
    }

    public function actionBankWire(){
        $this->render('bankWire');
    }

    public function loadModel($id = null)
    {
        if ($this->_model === null) {
            if (isset($id)) {
                $this->_model = OrangelotsInvoices::model()->findbyAttributes(array('transaction_id' => $id));
            }
            if ($this->_model === null) {
                throw new CHttpException(404, 'The requested page does not exist.');
            }
        }
        return $this->_model;
    }

    public function actionVendorList()
    {
        $criteria = new CDbCriteria;
        $query = yii()->db->createCommand('SELECT DISTINCT owner_auction_id FROM orangelots_winners WHERE user_id ='.yii()->user->id)->queryAll();
        if((isset($query))&&(!empty($query))){
            foreach($query AS $key => $value){
                $ids[] = $value['owner_auction_id'];
            }
            $criteria->addInCondition('id ', $ids);
        }
        if (isset($_POST['Search']) || (isset($_GET['ajax']) and $_GET['ajax'] == 'orangelots-category-grid')){
            if($_POST['Search']){
                $_SESSION['Search'] = $_POST['Search'];
            }
            if(isset($_SESSION['Search']['name_phone_email'])){
                $criteria->addCondition('(firstname LIKE "%'.$_SESSION['Search']['name_phone_email'].'%") OR (phone LIKE "%'.$_SESSION['Search']['name_phone_email'].'%") OR (email LIKE "%'.$_SESSION['Search']['name_phone_email'].'%") ');
            }
            if(isset($_SESSION['Search']['company'])){
                $criteria->addCondition('company LIKE "%'.$_SESSION['Search']['company'].'%"');
            }
        }else{
            unset($_SESSION['Search']);
        }
        $dataProvider = new CActiveDataProvider('OrangelotsUsers', array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
        $this->render('/sell/contact_list', array(
                'dataProvider' => $dataProvider,
                'name_data' => 'Vendors',
                'buyer' => true
            )
        );
    }
}