<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */

$this->breadcrumbs=array(
	'Orangelots Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OrangelotsUsers', 'url'=>array('index')),
	array('label'=>'Create OrangelotsUsers', 'url'=>array('create')),
	array('label'=>'Update OrangelotsUsers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OrangelotsUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OrangelotsUsers', 'url'=>array('admin')),
);
?>

<h1>View OrangelotsUsers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
		'email',
		'phone',
		'activkey',
		'superuser',
		'status',
		'create_at',
		'lastvisit_at',
		'onetime_pass',
		'firstname',
		'lastname',
		'company',
		'address_1',
		'address_2',
		'city',
		'state',
		'zipcode',
	),
)); ?>
