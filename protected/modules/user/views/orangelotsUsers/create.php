<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */
?>
<div class="breadcrumb_row clearfix">
    <div class="main_container">

        <?php

        $this->widget(
            'zii.widgets.CBreadcrumbs',
            array(
                'links' => array(
                    'Home' => array('site/index'),
                    'Vendor Signup',
                ),
                'separator' => '',
                'tagName' => 'ul',
                'htmlOptions' => array('class' => ''),
                'inactiveLinkTemplate' => '<li><a href="">{label}</a></li>',
                'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                        "site/index"
                    ) . '"></a></li>',
            )
        );
        ?>
    </div>
</div>

<?php $this->renderPartial('_form', array('model' => $model)); ?>

<script type="text/javascript">

</script>