<?php
/* @var $this UserController */
/* @var $model OrangelotsUsers */
?>


<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'buyer-admin-reg-form',
		'enableAjaxValidation' => false,
	)); ?>

	<p class="singup__title">Create Buyer</p>
	<br>
	<p class="singup__title">Buyer information</p>

	<div class="popup__right_info__form__container pull-left">
		<div class="popup_form_row">
			<label>First name <span class="important_star">*</span></label>
			<?php echo $form->textField($model, 'firstname'); ?>
			<?php echo $form->error($model, 'firstname'); ?>
		</div>

		<div class="popup_form_row">
			<label>Email <span class="important_star">*</span></label>
			<?php echo $form->textField($model, 'email'); ?>
			<?php echo $form->error($model, 'email'); ?>
		</div>

		<div class="popup_form_row">
			<label>Phone</label>
			<?php echo $form->textField($model, 'phone'); ?>
			<?php echo $form->error($model, 'phone'); ?>
		</div>

		<div class="popup_form_row">
			<?php echo $form->labelEx($model, 'state'); ?>
			<div class="popup_form__select_container">
				<?php echo $form->dropDownList($model, 'state', CHtml::listData(OrangelotsStates::model()->findAll(), 'state_id', 'state_name')); ?>
				<?php echo $form->error($model, 'state'); ?>
			</div>
		</div>

		<div class="popup_form_row">
			<label>Last name <span class="important_star">*</span></label>
			<?php echo $form->textField($model, 'lastname'); ?>
			<?php echo $form->error($model, 'lastname'); ?>
		</div>
		<div class="popup_form_row">
			<label>Address </label>
			<?php echo $form->textField($model, 'address_1'); ?>
			<?php echo $form->error($model, 'address_1'); ?>
		</div>
		<div class="popup_form_row">
			<?php echo $form->labelEx($model, 'city'); ?>
			<?php echo $form->textField($model, 'city'); ?>
			<?php echo $form->error($model, 'city'); ?>
		</div>

		<?php echo CHtml::submitButton('Create', array('class' => 'btn btn_orange')); ?>
	</div>

	<?php $this->endWidget(); ?>
</div>