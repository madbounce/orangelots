<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */
/* @var $form CActiveForm */
CHtml::$afterRequiredLabel = '<span class="important_star"> *</span> ';

?>

<div class="singup__container">

	<?php $form = $this->beginWidget(
		'CActiveForm',
		array(
			'id' => 'vendor-admin-reg-form',
			'enableAjaxValidation' => false,
		)
	); ?>

	<p class="singup__title">Create Vendor</p>
	<br>
	<p class="singup__title">Vendor information</p>

	<?php //echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model, 'role', array('value' => 'vendor')); ?>

	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'firstname'); ?>
		<?php echo $form->textField($model, 'firstname'); ?>
		<?php echo $form->error($model, 'firstname'); ?>
	</p>

	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'lastname'); ?>
		<?php echo $form->textField($model, 'lastname'); ?>
		<?php echo $form->error($model, 'lastname'); ?>
	</p>

	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'phone'); ?>
		<?php echo $form->textField($model, 'phone'); ?>
		<?php echo $form->error($model, 'phone'); ?>
	</p>

	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'email'); ?>
		<?php echo $form->textField($model, 'email'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</p>

	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'company'); ?>
		<?php echo $form->textField($model, 'company'); ?>
		<?php echo $form->error($model, 'company'); ?>
	</p>

	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'status'); ?>
		<?php echo $form->dropDownList(
			$model,
			'status',
			User::getStatuses()
		); ?>
		<?php echo $form->error($model, 'state'); ?>
	</p>

	</br>

	<p class="singup__title">Vendor Address</p>


	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'address_1'); ?>
		<?php echo $form->textField($model, 'address_1'); ?>
		<?php echo $form->error($model, 'address_1'); ?>
	</p>

	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'address_2'); ?>
		<?php echo $form->textField($model, 'address_2'); ?>
		<?php echo $form->error($model, 'address_2'); ?>
	</p>

	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'city'); ?>
		<?php echo $form->textField($model, 'city'); ?>
		<?php echo $form->error($model, 'city'); ?>
	</p>


	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'state'); ?>
		<?php //echo $form->textField($model, 'state'); ?>
		<?php echo $form->dropDownList(
			$model,
			'state',
			CHtml::listData(OrangelotsStates::model()->findAll(), 'state_id', 'state_name')
		); ?>
		<?php echo $form->error($model, 'state'); ?>
	</p>


	<p class="singup_form_row">
		<?php echo $form->labelEx($model, 'zipcode'); ?>
		<?php echo $form->textField($model, 'zipcode', array('class' => 'zipcode')); ?>
		<?php echo $form->error($model, 'zipcode'); ?>
	</p>

	<p class="singup_form_row">
<!--		<button class="btn_image">-->
<!--			<img src="--><?php //echo yii()->baseUrl . '/images/btn/btn_update_information.png' ?><!--" alt="">-->
<!--		</button>-->
		<?php echo CHtml::submitButton('Create', array('class' => 'btn btn_orange')); ?>

	</p>

	<?php $this->endWidget(); ?>

</div>
