<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */
/* @var $form CActiveForm */
CHtml::$afterRequiredLabel = '<span class="important_star"> *</span> ';

?>
<div class="singup__container">
<div class="main_container">
<?php if (!user()->isAdmin()): ?>
    <div class="singup__right_info_block pull-right singup__right_info_new_class">
        <?if(user()->isVendor()):?>
            <div class="singup__right_info_block__argument_text singup__right_new_class">

                <?php $form = $this->beginWidget('UActiveForm', array(
                        'id' => 'image-form',
                        'enableAjaxValidation' => true,
                        'action' => createUrl('//user/orangelots-users/download-avatar'),
                        'htmlOptions' => array('enctype' => 'multipart/form-data'),
                    ));
                ?>
                <div class="singup_form_row">
                    <span class="uni_form_row_label avatar_label">Choose photo</span>
                    <div class="card_detail__tab_question__image avatar_another_class pull-left">
                        <?if(!empty($model->avatar)):?>
                            <img src="<?php echo yii()->baseUrl .yii()->storage->getFileUrl($model->avatar)  ?>" alt=""/>
                        <?endif;?>
                    </div>


                    <?= $form->error($model, 'avatar'); ?>
                    <?= $form->fileField($model, 'avatar', array('class' => 'hide', 'id' => 'add_file')); ?>
                    <button class="btn green_bnt green_ntns_class document_add_file gre_btn_text <?php echo !empty($model->avatar) ? "avatar_image_class":""?>" type="button">Choose file</button>
                    <?=
                    CHtml::htmlButton('Download',
                        array(
                            'class' => 'btn green_bnt green_ntns_class load_photo gre_btn_text',
                            'type' => 'button',
                            'onclick' => 'uploadPhotoButton(this, "'.createUrl('user/orangelots-users/download-avatar').'")'
                        ));
                    ?>
                </div>
                <?php $this->endWidget(); ?>
            </div>
        <?else:?>
            <div class="singup__right_info_block__argument_text">
                <p class="singup__right_info_block__argument_text__title">
                    <span>5 Reasons</span><br/>
                    to become a vendor
                </p>
                <ul class="singup__right_info_block__argument_list">
                    <li>No subscription for vendors</li>
                    <li>No upfront payment</li>
                    <li>No hidden fee</li>
                    <li>Personal manager for each vendor</li>
                </ul>

                <div class="singup__right_info_block__company_list">
                    <p class="singup__right_info_block__company_list__title">You are in a great company</p>
                    <ul class="singup__right_info_block__company_list__list clearfix">
                        <li>
                            <img src="<?php echo baseUrl() . '/images/brangs/brand_1.png' ?>" alt="">
                        </li>
                        <li>
                            <img src="<?php echo baseUrl() . '/images/brangs/brand_2.png' ?>" alt="">
                        </li>
                        <li>
                            <img src="<?php echo baseUrl() . '/images/brangs/brand_3.png' ?>" alt="">
                        </li>
                        <li>
                            <img src="<?php echo baseUrl() . '/images/brangs/brand_4.png' ?>" alt="">
                        </li>
                    </ul>
                </div>

                <div class="singup__right_info_block__note">
                    <p>OrangeLots is a powerful connector between my business and major and quality wholesalers.
                        Every
                        investment I did here returned with 400% in profit!</p>
                </div>

                <div class="singup__right_info_block__call_container">
                    <p>Still Wonder?</p>

                    <p class="singup__right_info_block__call_row">
                        Call us:
                        <span class="phone_number" >800-OrangeLots</span>
                    </p>
                </div>
            </div>
        <?endif;?>



    </div>
<?php endif; ?>
<div class="form">
    <div class="singup__form__block">
        <br>
        <p class="singup__title">Vendor Information</p>
        <? $form = $this->beginWidget('CActiveForm', array(
                'id' => 'orangelots-users-form',
                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                )
            )); ?>

        <?php echo $form->hiddenField($model, 'role', array('value' => user()->isVendor() ? OrangelotsUsers::VU : OrangelotsUsers::V)); ?>

        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'firstname'); ?>
            <?php echo $form->textField($model, 'firstname'); ?>
            <?php echo $form->error($model, 'firstname'); ?>
        </p>

        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'lastname'); ?>
            <?php echo $form->textField($model, 'lastname'); ?>
            <?php echo $form->error($model, 'lastname'); ?>
        </p>

        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'phone'); ?>
            <?php echo $form->textField($model, 'phone'); ?>
            <?php echo $form->error($model, 'phone'); ?>
        </p>

        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'email'); ?>
            <?php if(!$model->isNewRecord):?>
                <?php echo $form->textField($model, 'email',array('disabled'=>'disabled')); ?>
            <?php else:?>
                <?php echo $form->textField($model, 'email'); ?>
            <?php endif;?>
            <?php echo $form->error($model, 'email'); ?>
        </p>

        <?if(!user()->isAuthenticated()):?>
            <p class="singup_form_row">
                <?php echo $form->labelEx($model, 'password'); ?>
                <?php echo $form->passwordField($model, 'password', array('value' => '')); ?>
                <?php echo $form->error($model, 'password'); ?>
            </p>
        <?endif;?>

        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'company'); ?>
            <?php echo $form->textField($model, 'company'); ?>
            <?php echo $form->error($model, 'company'); ?>
        </p>

        </br>

        <p class="singup__title">Vendor Address</p>

        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'address_1'); ?>
            <?php echo $form->textField($model, 'address_1'); ?>
            <?php echo $form->error($model, 'address_1'); ?>
        </p>

        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'address_2'); ?>
            <?php echo $form->textField($model, 'address_2'); ?>
            <?php echo $form->error($model, 'address_2'); ?>
        </p>

        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'city'); ?>
            <?php echo $form->textField($model, 'city'); ?>
            <?php echo $form->error($model, 'city'); ?>
        </p>


        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'state'); ?>
            <?php echo $form->dropDownList(
                $model,
                'state',
                CHtml::listData(OrangelotsStates::model()->findAll(), 'state_id', 'state_name')
            ); ?>
            <?php echo $form->error($model, 'state'); ?>
        </p>


        <p class="singup_form_row">
            <?php echo $form->labelEx($model, 'zipcode'); ?>
            <?php echo $form->textField($model, 'zipcode', array('class' => 'zipcode')); ?>
            <?php echo $form->error($model, 'zipcode'); ?>
        </p>

        <p class="singup_form_row">
            <?php if ((yii()->controller->id == 'orangelotsUsers') && ((yii()->controller->action->id == 'update') || (yii()->controller->action->id == 'updateData'))): ?>
                <button class="btn_image">
                    <img src="<?php echo yii()->baseUrl . '/images/btn/btn_update_information.png' ?>" alt="">
                </button>
            <?php else: ?>
                <button class="btn_image"><img
                        src="<?php echo yii()->baseUrl . '/images/btn/btn_reg_vendor.png' ?>" alt=""></button>
            <?php endif; ?>
        </p>

        <p class="singup_form_row">
            <span class="singup__info_text">We don't sell or disclouse your information. We carefully protect our vendors and use this information just for our own business needs.</span>
        </p>

        <?php $this->endWidget(); ?>

    </div>
    <!-- form -->
</div>
</div>
</div>