<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */
/* @var $form CActiveForm */
CHtml::$afterRequiredLabel = '<span class="important_star"> *</span> ';

?>

<div class="singup__container">
	<div class="main_container">
		<?php if (!user()->isAdmin()): ?>
			<div class="singup__right_info_block pull-right">
				<div class="singup__right_info_block__argument_text">
					<p class="singup__right_info_block__argument_text__title">
						<span>5 Reasons</span><br/>
						to become a vendor
					</p>
					<ul class="singup__right_info_block__argument_list">
						<li>No subscription for vendors</li>
						<li>No upfront payment</li>
						<li>No hidden fee</li>
						<li>Personal manager for each vendor</li>
					</ul>

					<div class="singup__right_info_block__company_list">
						<p class="singup__right_info_block__company_list__title">You are in a great company</p>
						<ul class="singup__right_info_block__company_list__list clearfix">
							<li>
								<img src="<?php echo baseUrl() . '/images/brangs/brand_1.png' ?>" alt="">
							</li>
							<li>
								<img src="<?php echo baseUrl() . '/images/brangs/brand_2.png' ?>" alt="">
							</li>
							<li>
								<img src="<?php echo baseUrl() . '/images/brangs/brand_3.png' ?>" alt="">
							</li>
							<li>
								<img src="<?php echo baseUrl() . '/images/brangs/brand_4.png' ?>" alt="">
							</li>
						</ul>
					</div>

					<div class="singup__right_info_block__note">
						<p>OrangeLots is a powerful connector between my business and major and quality wholesalers.
							Every
							investment I did here returned with 400% in profit!</p>
					</div>

					<div class="singup__right_info_block__call_container">
						<p>Still Wonder?</p>

						<p class="singup__right_info_block__call_row">
							Call us:
							800-OrangeLots
						</p>
					</div>
				</div>


			</div>
		<?php endif; ?>
		<div class="form">
			<div class="singup__form__block">
				<p class="singup__title">Update Buyer Information</p>
				<br>
				<p class="singup__title">Buyer Information</p>
				<?php $form = $this->beginWidget(
					'CActiveForm',
					array(
						'id' => 'orangelots-users-buyer-form',
						// Please note: When you enable ajax validation, make sure the corresponding
						// controller action is handling ajax validation correctly.
						// There is a call to performAjaxValidation() commented in generated controller code.
						// See class documentation of CActiveForm for details on this.
						'enableAjaxValidation' => false,
					)
				); ?>

				<?php echo $form->hiddenField($model, 'role', array('value' => 'buyer')); ?>

				<p class="singup_form_row">
					<?php echo $form->labelEx($model, 'firstname'); ?>
					<?php echo $form->textField($model, 'firstname'); ?>
					<?php echo $form->error($model, 'firstname'); ?>
				</p>

				<p class="singup_form_row">
					<?php echo $form->labelEx($model, 'lastname'); ?>
					<?php echo $form->textField($model, 'lastname'); ?>
					<?php echo $form->error($model, 'lastname'); ?>
				</p>

				<p class="singup_form_row">
					<?php echo $form->labelEx($model, 'phone'); ?>
					<?php echo $form->textField($model, 'phone'); ?>
					<?php echo $form->error($model, 'phone'); ?>
				</p>

				<p class="singup_form_row">
					<?php echo $form->labelEx($model, 'email'); ?>
					<?php echo $form->textField($model, 'email'); ?>
					<?php echo $form->error($model, 'email'); ?>
				</p>

				<p class="singup_form_row">
					<?php echo $form->labelEx($model, 'password'); ?>
					<?php echo $form->passwordField($model, 'password', array('value' => '')); ?>
					<?php echo $form->error($model, 'password'); ?>
				</p>

				</br>

				<p class="singup__title">Buyer Address</p>


				<p class="singup_form_row">
					<?php echo $form->labelEx($model, 'address_1'); ?>
					<?php echo $form->textField($model, 'address_1'); ?>
					<?php echo $form->error($model, 'address_1'); ?>
				</p>

				<p class="singup_form_row">
					<?php echo $form->labelEx($model, 'city'); ?>
					<?php echo $form->textField($model, 'city'); ?>
					<?php echo $form->error($model, 'city'); ?>
				</p>


				<p class="singup_form_row">
					<?php echo $form->labelEx($model, 'state'); ?>
					<?php echo $form->dropDownList($model, 'state',
						CHtml::listData(OrangelotsStates::model()->findAll(), 'state_id', 'state_name'));
					?>
					<?php echo $form->error($model, 'state'); ?>
				</p>

                <p class="singup_form_row">
                    <?php echo $form->labelEx($model, 'zipcode'); ?>
                    <?php echo $form->textField($model, 'zipcode'); ?>
                    <?php echo $form->error($model, 'zipcode'); ?>
                </p>

				<p class="singup_form_row">
					<button class="btn_image">
						<img src="<?php echo yii()->baseUrl . '/images/btn/btn_update_information.png' ?>" alt="">
					</button>
				</p>

				<p class="singup_form_row">
					<span class="singup__info_text">We don't sell or disclouse your information. We carefully protect our vendors and use this information just for our own business needs.</span>
				</p>

				<?php $this->endWidget(); ?>

			</div>
			<!-- form -->
		</div>
	</div>
</div>