<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */
?>

	<div class="breadcrumb_row clearfix">
		<div class="main_container">

			<?php
			if (!user()->isAdmin()) {
				$this->widget(
					'zii.widgets.CBreadcrumbs',
					array(
						'links' => array(
							'Home' => array('site/index'),
							'Personal settings',
						),
						'separator' => '',
						'tagName' => 'ul',
						'htmlOptions' => array('class' => ''),
						'inactiveLinkTemplate' => '<li><a>{label}</a></li>',
                        'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                                "site/index"
                            ) . '"></a></li>',
					)
				);
			}
			?>
		</div>
	</div>

<?php $this->renderPartial((!empty($buyer) ? '_buyer_form' : '_form'), array('model' => $model)); ?>