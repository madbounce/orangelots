<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href="<?php echo yii()->createUrl("site/index")?>"></a></li>
            <li><a>Billing Settings</a></li>
        </ul>
    </div>
</div>

<div class="uni_form__container">
    <div class="main_container">
        <div class="clearfix">
            <?if(!empty($current)):?>
                <div class="update_card__info_block pull-right">
                    <p class="uni_form_title">Current Billing details</p>
                    <p class="update_card__row">
                        <?php echo ucfirst($current->firstname).' '.ucfirst($current->lastname)?>
                    </p>
                    <p class="update_card__row">
                        <?php echo $current->viewNumber()?>
                    </p>
                    <p class="update_card__row">
                        <?php echo ($current->expire_month<10 ? 0:"").$current->expire_month.'/'.$current->expire_year;?>
                    </p>

                </div>
            <?endif;?>
            <div class="uni_form__block pull-left">
                <p class="uni_form_title">New Billing details</p>

                <?php $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'update-set-pay-card-form',
                        'enableAjaxValidation' => false,
                    )); ?>

                <div class="uni_form_row ">
                    <label>Owner name</label>
                    <?php echo $form->textField($model, 'owner_name'); ?>
                    <?php echo $form->error($model, 'owner_name'); ?>
                </div>

                <div class="uni_form_row ">
                    <label>Number</label>
                    <?php echo $form->textField($model, 'number', array('maxlength' => 16)); ?>
                    <?php echo $form->error($model, 'number'); ?>
                </div>

                <div class="uni_form_row">
                    <label>Type</label>
                    <?php echo $form->dropDownList($model, 'type', CardInfo::$typesArray); ?>
                    <?php echo $form->error($model, 'type'); ?>
                </div>

                <div class="uni_form_row">
                    <label>Expiration date</label>
                    <div class="uni_form__card_date_month">
                        <?php echo $form->dropDownList($model, 'month', monthNumbers()); ?>
                    </div>

                    <div class="uni_form_row uni_form__card_date_year">
                        <?php echo $form->dropDownList($model, 'year', yearsFomCurrent()); ?>
                    </div>

                    <?php echo $form->error($model, 'month'); ?>

                </div>

                <div class="uni_form_row uni_form_cvv_row">
                    <label>CVV</label>
                    <?php echo $form->textField($model, 'cvv', array('maxlength' => 3, 'value' => '')); ?>
                    <?php echo $form->error($model, 'cvv'); ?>
                </div>

                <div class="uni_form_row ">
                    <?if(!empty($current)):?>
                        <button class="btn green_bnt">Update your card</button>
                    <?else:?>
                        <button class="btn green_bnt">Add your card</button>
                    <?endif;?>
                </div>

                <?php $this->endWidget(); ?>

            </div>
        </div>
    </div>
</div>