<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */
?>

<?php echo CHtml::link('Create buyer', createUrl('/user/orangelots-users/createBuyer'), array('class' => 'btn btn_orange', 'style' => 'margin-right: 10px;'))?>
<?php echo CHtml::link('Create vendor', createUrl('/user/orangelots-users/createVendor'), array('class' => 'btn btn_orange'))?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'orangelots-users-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'firstname',
		'lastname',
//		'password',
		'email',
		'phone',
		'address_1',
//		'activkey',
		/*
		'superuser',
		'status',
		'create_at',
		'lastvisit_at',
		'onetime_pass',
		'firstname',
		'lastname',
		'company',
		'address_1',
		'address_2',
		'city',
		'state',
		'zipcode',
		*/
		array(
			'name' => 'role',
			'header' => 'Permissions',
			'type' => 'raw',
            'value' => '$data->role($data->id)',
            'filter' => array('Vendor'=> 'Vendor', 'Authenticated'=> 'Buyer'),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => '{update}'
		),
	),
)); ?>
