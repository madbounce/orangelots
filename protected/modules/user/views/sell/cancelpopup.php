<div class="popup__container" id="cancel-invoice-popup-form">
    <div class="popup__bg"></div>
    <div class="popup_inner">
        <div class="popup__content">
            <a href="" class="popup_close">X</a>

            <div class="popup_body clearfix">
                <p class="payment_invoice_cancel_title pull-left">You can leave your comment</p>
                <?php $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                    )
                ); ?>
                <div class="popup__right_info__form__container">

                    <div class="popup_form_row">
                        <?php echo $form->textArea($model, 'vendor_comment', array('cols' => 60, 'rows' => 10, 'id' => 'cancel_comment')); ?>
                    </div>
                    <p class="popup_form_row">
                        <?php echo CHtml::ajaxButton(
                            'Cancel invoice',
                            yii()->createUrl('user/sell/cancelInvoice'),
                            array(
                                'type' => 'POST',
                                'data' => array(
                                    'invoiceId' => $model->id,
                                    'comment' => 'js:$("#cancel_comment").val()'
                                ),
                                'success' => 'function(data){
                                    location.reload();
                                }'
                            ),
                            array(
                                'class' => 'btn green_bnt'
                            )
                        );
                        ?>
                    </p>
                </div>
                <?php $this->endWidget(); ?>
            </div>

        </div>
    </div>
</div>