<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href="<?php echo yii()->createUrl("site/index")?>"></a></li>
            <?if(isset($buyer) and !empty($buyer)):?>
                <li><a>Buy</a></li>
                <li><a>Vendors list</a></li>
            <?else:?>
                <li><a>Sell</a></li>
                <li><a>Buyers list</a></li>
            <?endif;?>
        </ul>
    </div>
</div>



<div class="vendors__container">
    <div class="main_container">
        <p class="vendors__title"><?php echo $name_data?></p>

        <div class="vendors__form_container">
            <form method="POST">
                <div class="vendors__form_block vendors__form_block__name">
                    <label>Find Contact</label>
                    <input type="text" placeholder="name, phone, email" name="Search[name_phone_email]">
                </div>

                <div class="vendors__form_block vendors__form_block__compamy" >
                    <label>Company</label>
                    <input type="text" placeholder="name" name="Search[company]">
                </div>

                <div class="vendors__form_block">
                    <button class="btn green_bnt">search</button>
                </div>
            </form>
        </div>

        <div class="vendors__form_table">
            <?php $this->widget(
                'MyGridView',
                array(
                    'id' => 'orangelots-category-grid',
                    'dataProvider' => $dataProvider,
                    'summaryText' => '',
                    'columns' => array(
                        array(
                            'header' => '',
                            'value' => '$row+1',
                            'htmlOptions'=>array('width'=>'40'),
                        ),
                        array(
                            'name' => 'firstname',
                            'header' => 'Name',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'email',
                            'header' => 'Email',
                            'type' => 'raw',
                            'sortable' => false,
                            'htmlOptions'=>array('width'=>'210'),
                        ),
                        array(
                            'name' => 'phone',
                            'header' => 'Phone',
                            'type' => 'raw',
                            'sortable' => false,
                            'htmlOptions'=>array('width'=>'130'),
                        ),
                        array(
                            'name' => 'company',
                            'header' => 'Company',
                            'type' => 'raw',
                            'sortable' => false,
                            'htmlOptions'=>array('width'=>'180'),
                        ),
                        array(
                            'name' => 'id',
                            'header' => 'Reviews',
                            'type' => 'raw',
                            'sortable' => false,
                            'htmlOptions'=>array('width'=>'100'),
                            'value'=>"".'($data->getFeedbackLink($data->id,'.yii()->user->id.'))?($data->getFeedbackScore($data->id,'.yii()->user->id.')):'."CHtml::ajaxLink(
                                    'Rate now',
                                    yii()->createUrl('account/default/rating-popup'),
                                    array(
                                        'type' => 'POST',
                                        'data' => array(
                                            'Rating[user_id]' => ".'$data->id'.",
                                            'Rating[url]' => '".Yii::app()->controller->module->id."/".Yii::app()->controller->id."/".Yii::app()->controller->action->id."',
                                        ),
                                        'success' => 'function(data){
                                            var obj = JSON.parse(data);
                                            if (obj.status == \'ok\') {
                                             $(\'body\').append(obj.html);
                                            }

                                        }',
                                    ),
                                    array(
                                        'class' => 'orange_link',
                                        'id' => ".'user_.$data->id'.",
                                    )
                                )",
                        ),
                    ),
                    'itemsCssClass' => 'text-center',
                    'pager' => array(
                        'htmlOptions' => array(
                            'class' => 'pager_lk'
                        )
                    ),
                )
            ); ?>
        </div>
    </div>
</div>