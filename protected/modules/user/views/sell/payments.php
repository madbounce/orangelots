<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href="<?php echo yii()->createUrl("site/index")?>"></a></li>
            <li><a>Sell</a></li>
            <li><a>Payments Received</a></li>
        </ul>
    </div>
</div>
<div class="received__container">
    <div class="main_container">
        <p class="posted_by__title">Search by</p>
        <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'model-form',
                'enableAjaxValidation' => false,
            )); ?>
        <div class="received__row__form_container">
            <div class="received__row">
                <div class="received__row__block received__row__block__id">
                    <label>Transaction ID</label>
                    <?php echo $form->textField($searchModel, 'transaction_id', array('type' => 'text','maxlength' => 50, 'value' => $_SESSION['filter_sell']['transaction_id'])); ?>
                    <?php echo $form->error($searchModel, 'transaction_id'); ?>
                </div>
                <div class="received__row__block received__row__block__id">
                    <label>PayPal ID</label>
                    <?php echo $form->textField($searchModel, 'paypal_id', array('type' => 'text','maxlength' => 50, 'value' => $_SESSION['filter_sell']['paypal_id'])); ?>
                    <?php echo $form->error($searchModel, 'paypal_id'); ?>
                </div>
                <div class="received__row__block received__row__block__email">
                    <label>From, email, phone</label>
                    <?php echo $form->textField($searchModel, 'to_who', array('type' => 'text','maxlength' => 50, 'value' => $_SESSION['filter_sell']['to_who'])); ?>
                    <?php echo $form->error($searchModel, 'to_who'); ?>
                </div>
            </div>
            <div class="received__row">
                <div class="received__row__block received__row__block__deal_id">
                    <label>Auction ID /  Deal ID</label>
                    <?php echo $form->textField($searchModel, 'auction_deal_id', array('type' => 'text','maxlength' => 50, 'value' => $_SESSION['filter_sell']['auction_deal_id'])); ?>
                    <?php echo $form->error($searchModel, 'auction_deal_id'); ?>
                </div>

                <div class="received__row__block__calendar_block">
                    <p>Payment Date</p>
                    <label>from</label>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => $searchModel,
                            'attribute' => 'dateFrom',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'value'=> (!empty($_SESSION['filter_sell']['dateFrom']))?$_SESSION['filter_sell']['dateFrom']:'',
                            )

                        )
                    );
                    ?>
                    <label>to</label>
                    <?php
                    $this->widget(
                        'ext.CJuiDateTimePicker.CJuiDateTimePicker',
                        array(
                            'model' => $searchModel,
                            'attribute' => 'dateTo',
                            'language' => 'en-GB', //default Yii::app()->language
                            'mode' => 'date', //'datetime' or 'time' ('datetime' default)
                            'options' => array(
                                'dateFormat' => 'yy/mm/dd',
                            ),
                            'htmlOptions' => array(
                                'class' => 'uni_form__calendar',
                                'value'=> (!empty($_SESSION['filter_sell']['dateTo']))?$_SESSION['filter_sell']['dateTo']:'',
                            )

                        )
                    );
                    ?>
                </div>

                <div class="received__row__block">
                    <label>Status</label>
                    <div class="received__row__select_container">
                        <?php echo $form->dropDownList($searchModel, 'payed', OrangelotsInvoices::arrayForFilter(), array('prompt' => 'Choose status', 'options' => OrangelotsInvoices::getOptionSelected(OrangelotsInvoices::SELL))); ?>
                    </div>
                </div>

                <div class="received__row__block">
                    <button class="btn_image">
                        <img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt="">
                    </button>
                </div>

            </div>
        </div>
        <?php $this->endWidget(); ?>

        <div class="posted_by__table_auction_list__container text-center">
            <?php $this->widget(
                'MyGridView',
                array(
                    'id' => 'orangelots-vendor-payments-grid',
                    'dataProvider' => $payments,
                    'summaryText' => '',
                    'columns' => array(
                        array(
                            'header' => 'Trans.ID',
                            'type' => 'raw',
                            'value' => 'CHtml::link("$data->transaction_id", "" , array("class" => "orange_link"))'
                        ),
                        array(
                            'header' => 'PayPalID<br/>Transaction Date',
                            'type' => 'raw',
                            'value' => 'OrangelotsInvoices::getPayPalTransactionDate($data)',
                        ),
                        array(
                            'header' => 'From',
                            'value' => '$data->to_who',
                            'sortable' => true,
                        ),
                        array(
                            'header' => 'Email',
                            'value' => '$data->email_to'
                        ),
                        array(
                            'header' => 'Phone',
                            'value' => '$data->phone_to'
                        ),
                        array(
                            'header' => 'Amount',
                            'value' => '"$".number_format($data->amount, 2)'
                        ),
                        array(
                            'header' => 'Fee',
                            'value' => '"$".number_format($data->fee, 2)'
                        ),
                        array(
                            'header' => 'Net payment',
                            'value' => '"$".number_format($data->amount-$data->fee, 2)'
                        ),
                        array(
                            'header' => 'AuctionID/<br/>DealID',
                            'type' => 'raw',
                            'value' => 'CHtml::link("$data->auction_deal_id",createUrl("auctions/orangelots-auctions/view/", array("id" => $data->auction_deal_id)), array("class" => "orange_link"))'
                        ),
                        array(
                            'header' => 'Status',
                            'value' => 'OrangelotsInvoices::$statusesText[$data->payed]'
                        ),
                        array(
                            'header' => 'Invoice',
                            'type' => 'raw',
                            'value' => 'CHtml::link("Invoice",createUrl("user/sell/confirmation/", array("transaction" => $data->transaction_id)), array("class" => "orange_link"))'
                        ),
                    ),
                    'pager' => array(
                        'htmlOptions' => array(
                            'class' => 'pager_lk'
                        )
                    ),
                    'itemsCssClass' => 'text-center',
                )
            ); ?>
        </div>
    </div>
</div>