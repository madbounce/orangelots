<tr>
    <td><a href="" class="orange_link"><?php echo $data->transaction_id?></a></td>
    <?if(!empty($data->paypal_id) && empty($data->$data->transaction_date)):?>
        <td><?php echo $data->paypal_id?><br><?php echo date('m/d/Y h:ma',$data->transaction_date)?></td>
        <?else:?>
        <td></td>
    <?endif;?>
    <td><?php echo $data->to_who?></td>
    <td><?php echo $data->email_to?></td>
    <td><?php echo $data->phone_to?></td>
    <td>$<?php echo number_format($data->amount, 2)?></td>
    <td>$<?php echo number_format($data->fee, 2)?></td>
    <td>$<?php echo number_format($data->amount-$data->fee, 2)?></td>
    <td><a href="<?php echo createUrl('auctions/orangelots-auctions/view/', array('id' => $data->auction_deal_id))?>" class="orange_link"><?php echo $data->auction_deal_id?></a></td>
    <td><?php echo OrangelotsInvoices::$statusesText[$data->payed]?></td>
    <td><a href="<?php echo createUrl('user/sell/confirmation', array('transaction' => $data->transaction_id))?>" class="orange_link">Invoice</a></td>
</tr>