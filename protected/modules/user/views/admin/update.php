<?
$this->layout = '//layouts/column2';
$this->menu = array(
	array('label' => UserModule::t('Create User'), 'url' => array('create')),
	array('label' => UserModule::t('Группы пользователя'), 'url' => array('/rights/assignment/user', 'id' => $model->id)),
	array('label' => UserModule::t('Manage Users'), 'url' => array('admin')),
	array('label' => UserModule::t('Manage Profile Field'), 'url' => array('profileField/admin')),
);
?>
<div class="span5 offset2">
	<h1><?php echo UserModule::t('Update User') . " " . $model->id; ?></h1>

	<?= $this->renderPartial('_form', array('model' => $model, 'profile' => $profile)) ?>
</div>