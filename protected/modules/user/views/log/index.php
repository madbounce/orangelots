<table class="items table table-striped">
	<thead>
	<tr>
		<th class="ao_cell_1">Дата и время</th>
		<th class="ao_cell_2">IP</th>
		<th>Действие</th>
	</tr>
	</thead>
	<tbody>
	<? $this->widget('bootstrap.widgets.TbListView', array(
		'id' => 'feed-grid',
		'dataProvider' => $model,
		'itemView' => 'common/_feed',
		'template' => '{items}{pager}',
		'summaryText' => false,
	));?>
	</tbody>
</table>