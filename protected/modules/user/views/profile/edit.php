<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Profile");
$this->breadcrumbs = array(
	UserModule::t("Profile") => array('profile'),
	UserModule::t("Edit"),
);
$this->menu = array(
	((UserModule::isAdmin())
		? array('label' => UserModule::t('Manage Users'), 'url' => array('/user/admin'))
		: array()),
	array('label' => UserModule::t('List User'), 'url' => array('/user')),
	array('label' => UserModule::t('Profile'), 'url' => array('/user/profile')),
	array('label' => UserModule::t('Change password'), 'url' => array('changepassword')),
	array('label' => UserModule::t('Logout'), 'url' => array('/user/logout')),
);

$this->widget('bootstrap.widgets.TbMenu', array(
	'type' => 'tabs',
	'stacked' => false,
	'items' => array(
		array('label' => UserModule::t('Edit'), 'url' => array('edit'), 'active' => true),
		array('label' => UserModule::t('Change password'), 'url' => array('changepassword')),
//		array('label' => UserModule::t('Change MasterKey'), 'url' => array('changemasterkey')),
	),
));
?>
<h1><?= UserModule::t('Edit profile'); ?></h1>

<? if (Yii::app()->user->hasFlash('profileMessage')): ?>
	<div class="success">
		<?php echo Yii::app()->user->getFlash('profileMessage'); ?>
	</div>
<? endif; ?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'profile-form',
		'enableAjaxValidation' => true,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	)); ?>
	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>
	<?= $form->errorSummary(array($model, $profile)); ?>
	<?
	$profileFields = Profile::getFields();
	if ($profileFields) {
		foreach ($profileFields as $field) {
			?>
			<div class="row">
				<?=
				$form->labelEx($profile, $field->varname);
				if ($widgetEdit = $field->widgetEdit($profile)) {
					echo $widgetEdit;
				} elseif ($field->range) {
					echo $form->dropDownList($profile, $field->varname, Profile::range($field->range));
				} elseif ($field->field_type == "TEXT") {
					echo $form->textArea($profile, $field->varname, array('rows' => 6, 'cols' => 50));
				} else {
					echo $form->textField($profile, $field->varname, array('size' => 60, 'maxlength' => (($field->field_size) ? $field->field_size : 255)));
				}
				echo $form->error($profile, $field->varname); ?>
			</div>
		<?
		}
	}
	?>
	<div class="row">
		<?= $form->labelEx($model, 'username'); ?>
		<?= $form->textField($model, 'username', array('size' => 20, 'maxlength' => 20)); ?>
		<?= $form->error($model, 'username'); ?>
	</div>
	<div class="row">
		<?= $form->labelEx($model, 'email'); ?>
		<?= $form->textField($model, 'email', array('size' => 60, 'maxlength' => 128)); ?>
		<?= $form->error($model, 'email'); ?>
	</div>
	<div class="row buttons">
		<?= CHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save')); ?>
	</div>
	<? $this->endWidget(); ?>
</div>
