<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href="<?php echo yii()->createUrl("site/index")?>"></a></li>
            <li><a href="">Questions</a></li>
        </ul>
    </div>
</div>

<div class="messeges__container">
    <div class="main_container">
        <div class="messeges_sort__container">
            <label><input type="radio" name="type" checked onclick="showHide('new', 'my_questions','archive')">New Questions Only</label>
            <label><input type="radio" name="type" onclick="showHide('my_questions', 'new', 'archive')">My questions</label>
            <label><input type="radio" name="type" onclick="showHide('archive', 'my_questions', 'new')">Archive</label>
        </div>
        <div class="messeges_list" id="new">
            <?php $this->renderPartial('list', array('dataProvider' => $new));?>
        </div>

        <div class="messeges_list" id="my_questions" style="display:none">
            <?php $this->renderPartial('list', array('dataProvider' => $myQuestions, 'myQuestions' => true));?>
        </div>

        <div class="messeges_list" id="archive" style="display:none">
            <?php $this->renderPartial('list', array('dataProvider' => $archive));?>
        </div>
    </div>
</div>

<script type="text/javascript">
  function showHide(idShow, idHide_first, idHide_second){
      $("#"+idShow).show();
      $("#"+idHide_first).hide();
      $("#"+idHide_second).hide();
  }
</script>