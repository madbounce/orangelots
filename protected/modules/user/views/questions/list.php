    <ul>
        <?
        $this->widget('bootstrap.widgets.TbListView', array(
                'id' => 'new-questions-grid',
                'dataProvider' => $dataProvider,
                'ajaxUpdate' => true,
                'itemView' => 'common/_questions',
                'emptyText' => 'There are no questions.',
                'summaryText' => false,
                'template' => '{items}{pager}',
                'pager' => array(
                    'htmlOptions' => array(
                        'class' => 'pager_lk'
                    )
                ),
            ));
        ?>
    </ul>