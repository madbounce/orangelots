<div class="popup__container" id="user-ajax-recovery">
    <div class="popup__bg"></div>
    <div class="popup_inner">
        <div class="popup__content">
            <a href="" class="popup_close">X</a>

            <div class="popup_body clearfix">
                <p class="popup__title">Restore Password</p>
                <div class="form recovery_pass">
                    <?php $formHTML = $this->beginWidget(
                        'CActiveForm',
                        array(
                            'enableAjaxValidation' => true,
                        )
                    ); ?>

                    <div class="forgot_pass_container">
                        <div class="forgot_pass_line">
                            <label>Enter E-mail address</label>
                        </div>
                        <div class="forgot_pass_line">
                            <?php echo CHtml::activeTextField($form, 'login_or_email') ?>
                            <?php echo CHtml::error($form, 'login_or_email') ?>
                        </div>

                        <div class="forgot_pass_line">
                            <div class=" submit">
                                <?php echo CHtml::ajaxSubmitButton(
                                    'Restore',
                                    createUrl('user/recovery/ajax-recovery'),
                                    array(
                                        'type' => 'POST',
                                        'beforeSend'=>'function(){
                                            $(".alert-message").hide();
                                        }',
                                        'success' => 'function(data){
                                        var resp = $.parseJSON(data);
                                        if(resp.err == 0){
                                            alertAppendTo("response_message","success", resp.message);
                                        }else{
                                            alertAppendTo("response_message","error", resp.message);
                                        }
		                            }',
                                    ),
                                    array('class' => 'btn green_bnt', 'id' => 'ajax-recovery-button')
                                )
                                ?>
                            </div>
                        </div>
                    </div>

                    <div id="response_message" class="response_message_error"></div>
                    <?php $this->endWidget(); ?>
                </div><!-- form -->
            </div>

        </div>
    </div>
</div>