<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Restore");
$this->breadcrumbs = array(
    UserModule::t("Login") => array('/user/login'),
    UserModule::t("Restore"),
);
?>

<div class="main_container">
    <h1 class="title_kripto text_grey"><?php echo UserModule::t("Restore Password"); ?></h1>

    <p class="hint"><?php echo UserModule::t("Please enter your login or email address."); ?></p>

    <?php if (Yii::app()->user->hasFlash('recoveryMessage')): ?>
        <div class="success">
            <?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
        </div>
    <?php else: ?>

        <div class="form recovery_pass">
            <?php echo CHtml::beginForm(); ?>

            <!--					--><?php //echo CHtml::errorSummary($form); ?>
            <?php echo CHtml::activeLabel($form, 'login_or_email'); ?>

            <?php echo CHtml::activeTextField($form, 'login_or_email') ?>
            <?php echo CHtml::error($form, 'login_or_email') ?>

            <div class=" submit">
                <?php echo CHtml::submitButton(UserModule::t("Restore"), array('class' => 'btn btn_orange')); ?>
            </div>



            <?php echo CHtml::endForm(); ?>
        </div><!-- form -->
    <?php endif; ?>

</div>

