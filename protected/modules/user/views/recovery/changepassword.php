<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Change password");
$this->breadcrumbs = array(
	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Change password"),
);
?>



<div class="form new_form_recovery">
    <p class="popup__title popup_title_1"><?php echo UserModule::t("Change password"); ?></p>

    <?php echo CHtml::beginForm(); ?>

	<?php echo CHtml::errorSummary($form); ?>

	<div class="popup_form_row">
        <label>Password <span class="important_star">*</span></label>
        <?php echo CHtml::activePasswordField($form, 'password'); ?>
		<p class="hint">
			<?php echo UserModule::t("Minimal password length 4 symbols."); ?>
		</p>
	</div>

	<div class="popup_form_row">
        <label>Retype password <span class="important_star">*</span></label>
        <?php echo CHtml::activePasswordField($form, 'verifyPassword'); ?>
	</div>


	<div class="submit">
		<?php echo CHtml::submitButton(UserModule::t("Set new password"), array('class' => 'btn green_bnt')); ?>
	</div>

	<?php echo CHtml::endForm(); ?>
</div><!-- form -->