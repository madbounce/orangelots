<div class="popup__container" id="buyer_last_step">
	<div class="popup__bg"></div>
	<div class="popup_inner">
		<div class="popup__content">
			<a href="" class="popup_close">X</a>

			<div class="popup_body clearfix">
				<div class="popup__title_complite">
					<p class="popup__title_complite__name">Dear <?php echo ucfirst($name) ?>,</p>
					<p class="popup__title_complite__text">
                        <?if(empty($_SESSION['buy_now'])):?>
                            We listed wholesale furniture deals from top furniture<br/>
						    stores and suppliers.
                            <?else:?>
                            We authorized your credit card information.<br/>
                        <?endif;?>
                    </p>

					<p>Now you will be able to bid, buy from our best selections of products.</p>
                    <?if(empty($_SESSION['buy_now'])):?>
                        <?php echo CHtml::link(CHtml::image(Yii::app()->request->baseUrl . '/images/btn/continue_site.png'), ''/*createUrl('//account')*/, array('class' => 'btn btn_image last_step_close'));?>
                    <?endif;?>
                </div>
			</div>
		</div>
	</div>
</div>