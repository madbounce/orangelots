<div class="popup__container" id="buyer-reg-first-step">
	<div class="popup__bg"></div>
	<div class="popup_inner">
		<div class="popup__content">
			<a href="" class="popup_close">X</a>

			<div class="popup_body clearfix ">
				<?php $form = $this->beginWidget(
					'CActiveForm',
					array(
						'id' => 'buyer-reg-form',
						'enableAjaxValidation' => true,
						'clientOptions' => array(
							'validateOnSubmit' => true,
							'validateOnChange' => true,
							'validateOnType' => true,
						),
						'action' => createUrl('/user/registration/buyerRegister'),
					)); ?>
				<p class="popup__right_info__title ">Your personal information</p>

				<div class="popup__right_info__form__container pull-left">
					<div class="popup_form_row">
						<label>First name <span class="important_star">*</span></label>
						<?php echo $form->textField($model, 'firstname', array('tabindex' => 1)); ?>
						<?php echo $form->error($model, 'firstname'); ?>
					</div>
					<div class="popup_form_row">
						<label>Email <span class="important_star">*</span></label>
						<?php echo $form->textField($model, 'email', array('tabindex' => 3)); ?>
						<?php echo $form->error($model, 'email'); ?>
                    </div>
					<div class="popup_form_row">
						<label>Phone</label>
						<?php echo $form->textField($model, 'phone', array('tabindex' => 5)); ?>
						<?php echo $form->error($model, 'phone'); ?>
					</div>

					<div class="popup_form_row">
                        <div class="state_code">
                            <?php echo $form->labelEx($model, 'state'); ?>
                            <div class="popup_form__select_container">
                                <?php echo $form->dropDownList(
                                    $model,
                                    'state',
                                    CHtml::listData(OrangelotsStates::model()->findAll(), 'state_id', 'state_name'), array('tabindex' => 7)
                                ); ?>
                                <?php echo $form->error($model, 'state'); ?>
                            </div>
                        </div>

                        <div class="sieg_code">
                            <label>Zipcode</label>
                            <?php echo $form->textField($model, 'zipcode', array('tabindex' => 8, 'class' => 'zip_input_registration')); ?>
                            <?php echo $form->error($model, 'zipcode'); ?>
                        </div>
					</div>

				</div>
				<div class="popup__right_info__form__container pull-left">
					<div class="popup_form_row">
						<label>Last name <span class="important_star">*</span></label>
						<?php echo $form->textField($model, 'lastname', array('tabindex' => 2)); ?>
						<?php echo $form->error($model, 'lastname'); ?>
					</div>
					<div class="popup_form_row">
						<label>Password <span class="important_star">*</span></label>
						<?php echo $form->passwordField($model, 'password', array('tabindex' => 4)); ?>
						<?php echo $form->error($model, 'password'); ?>
					</div>
					<div class="popup_form_row">
						<label>Address </label>
						<?php echo $form->textField($model, 'address_1', array('tabindex' => 6)); ?>
						<?php echo $form->error($model, 'address_1'); ?>
					</div>
					<div class="popup_form_row">
						<?php echo $form->labelEx($model, 'city'); ?>
						<?php echo $form->textField($model, 'city', array('tabindex' => 9)); ?>
						<?php echo $form->error($model, 'city'); ?>
					</div>
				</div>

				<div class="clearfix"></div>
				<?php echo CHtml::button('',
					array(
						'id' => 'submitReg',
						'class' => 'btn btn_next_step green_bnt pull-right',
						'onClick' => 'buyerReg($("#buyer-reg-form").serializeArray())'
					)
				); ?>
				<p>
					<span class="popup__newsletters_checked">
						<?php echo $form->checkBox($model, 'subscribe', array('checked' => true)); ?>
						I want to receive daily deals alerts and weekly newsletters
					</span>
				</p>
				<?php $this->endWidget(); ?>
			</div>
		</div>
	</div>
</div>