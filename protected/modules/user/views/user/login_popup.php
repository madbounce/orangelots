<div class="popup__container" id="user-login-popup-form">
	<!--    --><?php //if ($render) { ?>
	<div class="popup__bg"></div>
	<!--    --><?php //} ?>
	<div class="popup_inner">
		<div class="popup__content">
			<a href="" class="popup_close" onclick="location.reload()">X</a>

			<div class="popup_body clearfix">
				<p class="popup__title">Welcome to <span>Orange</span>Lots</p>

				<div class="popup__right_info pull-right">
					<p class="popup__right_info__title">Signup for <span>free!</span></p>

					<p>
						New and used furniture available! Buy directly from wholesalers. Online, no comissions, no
						hidden fees. Cheap and great furniture models available right now for immediate delivery.
					</p>

						<?php echo CHtml::ajaxLink(
							CHtml::image(yii()->baseUrl . '/images/btn/sing_now.png'),
							yii()->createUrl('user/registration/buyer-register'),
							array(
								'type' => 'POST',
								'success' => "function(data){
									$('#user-login-popup-form').html(data);
									$('.popup_form__select_container select').selectpicker();
                                }",
							),
							array(
                                'id' => 'buyer-first-step',
                                'tabIndex' => '5',
                            )
						);
						?>

				</div>
				<?php $form = $this->beginWidget(
					'CActiveForm',
					array(
						'enableAjaxValidation' => false,
						'enableClientValidation' => false,
					)
				); ?>
				<div class="popup__right_info__form__container">
					<p class="popup__right_info__title ">Already a Member?</p>
					<?php if (isset($model->errors['status'][0])): ?>
						<div class="alert in alert-block fade alert-error">
							<a class="close" data-dismiss="alert">×</a>
							<?php echo $form->error($model, 'status'); ?>
						</div>
					<?php endif; ?>

					<div class="popup_form_row">
						<label>Email <span class="important_star">*</span></label>
						<?php echo $form->textField($model, 'username', array('tabIndex' => '1')); ?>
						<?php echo $form->error($model, 'username'); ?>
					</div>

					<div class="popup_form_row">
						<label>Password <span class="important_star">*</span></label>
						<?php echo $form->passwordField($model, 'password', array('tabIndex' => '2')); ?>
						<?php echo $form->error($model, 'password'); ?>
					</div>

					<p class="popup_form_row">
<!--						<a href="--><?php //echo createUrl('user/recovery') ?><!--" tabIndex="4">Forgot password</a>-->
                        <span class="link_fogot">
                            <?php echo CHtml::ajaxLink(
                            "Forgot password",
                            yii()->createUrl('user/recovery/render-recovery'),
                            array(
                                'type' => 'POST',
                                'success' => "function(data){
//                                    $('#user-login-popup-form').toggle();
                                    $('#user-login-popup-form').hide();
                                    $('body').append(data);
                                }",
                            ),
                            array(
                                'tabIndex' => '4',
                                'id' => 'forgot_link_id'
                            )
                        );
                        ?></span>
						<?php echo CHtml::ajaxLink(
							'<img src="' . yii()->baseUrl . '/images/btn/sing_in.png" alt="">',
							yii()->createUrl('user/login/ajax'),
							array(
								'type' => 'POST',
								'success' => 'function(data){
                                 var obj = JSON.parse(data);
                                 if (obj.status == "error") {
                                    $("#user-login-popup-form").html(obj.html);
                                 } else if (obj.status = "authenticated") {
                                    var redirect_url = obj.url;
                                    window.location.reload();
                                    //window.location.replace(redirect_url);
                                 }
                               }'
							),
							array(
								'id' => 'signin',
								'class' => 'btn_image disable',
								'tabIndex' => '3'
							)
						);
						?>
					</p>

				</div>
				<?php $this->endWidget(); ?>
			</div>

			<div class="popup_body clearfix hide">
				<form>
					<p class="popup__right_info__title ">Your personal information</p>

					<div class="popup__right_info__form__container pull-left">

						<div class="popup_form_row">
							<label>First name <span class="important_star">*</span></label>
							<input type="text"/>
						</div>
						<div class="popup_form_row">
							<label>Email <span class="important_star">*</span></label>
							<input type="text"/>
						</div>
						<div class="popup_form_row">
							<label>Phone <span class="important_star">*</span></label>
							<input type="text"/>
						</div>
						<div class="popup_form_row">
							<label>State <span class="important_star">*</span></label>

							<div class="popup_form__select_container">
								<select>
									<option value="1">male</option>
									<option value="2">female</option>
								</select>
							</div>
						</div>
					</div>

					<div class="popup__right_info__form__container pull-left">

						<div class="popup_form_row">
							<label>Last name <span class="important_star">*</span></label>
							<input type="text"/>
						</div>
						<div class="popup_form_row">
							<label>Password <span class="important_star">*</span></label>
							<input type="text"/>
						</div>
						<div class="popup_form_row">
							<label>Address <span class="important_star">*</span></label>
							<input type="text"/>
						</div>
						<div class="popup_form_row">
							<label>City <span class="important_star">*</span></label>
							<input type="text"/>
						</div>
					</div>

					<div class="clearfix"></div>
					<button class="btn green_bnt pull-right">Next step</button>
					<p><span class="popup__newsletters_checked"><input type="checkbox" checked="checked"> I want to receive daily deals alerts and weekly newsletters</span>
					</p>

				</form>
			</div>
		</div>
	</div>
</div>