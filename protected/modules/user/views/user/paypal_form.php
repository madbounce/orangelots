<div class="popup__container" id="buyer-pay-pal">
    <div class="popup__bg"></div>
    <div class="popup_inner">
        <div class="popup__content">
            <a href="" class="popup_close">X</a>

            <div class="popup_body clearfix">

                <p class="popup__right_info__title"><?php echo ucfirst($name); ?>, please
                    authorize your billing info
                    <?if(!$skipDis):?>
                        <?php echo CHtml::button('Skip for now', array('class' => 'popup_link_skip_step orange_link', 'onClick' => 'skipForNow($("#uName").text())')) ?>
                    <?endif;?>
                </p>

                <?php $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id' => 'pay-card-info',
                        'enableAjaxValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => true,
                        ),
                        'action' => createUrl('/user/user/setCardInfo'),
                    )); ?>
                <div class="popup__right_info__form__container ">
                    <div class="popup_form_row">
                        <label>Owner name <span class="important_star">*</span></label>
                        <?php echo $form->textField($model, 'owner_name'); ?>
                        <?php echo $form->error($model, 'owner_name'); ?>
                    </div>

                    <div class="popup_form_row">
                        <label>Number <span class="important_star">*</span></label>
                        <?php echo $form->textField($model, 'number', array('maxlength' => 16)); ?>
                        <?php echo $form->error($model, 'number'); ?>
                    </div>

                    <div class="popup_form_row">
                        <label>Type <span class="important_star">*</span></label>
                        <?php echo $form->dropDownList($model, 'type', CardInfo::$typesArray); ?>
                        <?php echo $form->error($model, 'type'); ?>
                    </div>

                    <div class="popup_form_row">
                        <div class="popup_fomr_row__card_row">
                            <label>Expiration date <span class="important_star">*</span></label>

                            <div>
                                <div class="popup_form__select_container">
                                    <?php echo $form->dropDownList($model, 'month', monthNumbers()); ?>
                                </div>

                                <div class="popup_form__select_container">
                                    <?php echo $form->dropDownList($model, 'year', yearsFomCurrent()); ?>
                                    <?php echo $form->error($model, 'year'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="popup_fomr_row__card_row">
                            <label>CVV <span class="important_star">*</span></label>
                            <?php echo $form->textField($model, 'cvv', array('maxlength' => 3)); ?>
                            <?php echo $form->error($model, 'cvv'); ?>
                        </div>
                    </div>
                    <?php echo $form->error($model, 'month'); ?>

                    <div><span class="popup__newsletters_checked">
							<?php echo $form->checkBox($model, 'agree', array('checked' => true)); ?>
                            I agree with terms and conditions
                            <?php echo $form->error($model, 'agree'); ?>
						</span>
                    </div>
                </div>
                <?php echo CHtml::button('',
                    array(
                        'class' => 'btn autorise_card',
                        'onClick' => 'registerCard($("#pay-card-info").serializeArray())'
                    )
                ) ?>
                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".popup_form__select_container select").selectpicker();
</script>