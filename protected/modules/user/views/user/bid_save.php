<div class="popup__container" id="user-login-form">
    <div class="popup__bg"></div>
    <div class="popup_inner">
        <div class="popup__content">
            <a href="" class="popup_close">X</a>


            <div class="popup_body clearfix">
                <div class="popup__title_complite">
                    <p class="popup__title_complite__name">Dear <?php echo $model->firstname ?>,</p>

                    <?if($buy):?>
                        <p class="popup__title_complite__text">Thank you, your request has been accepted.</p>
                    <?else:?>
                        <p class="popup__title_complite__text">Thank you, your bid has been accepted.</p>
                    <?endif;?>
                    <button class="btn green_bnt btn_sing_in">continue to the site</button>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        var input_login = $("#UserLogin_username").val();
        var input_password = $("#UserLogin_password").val();
        if ((input_login != '') && (input_password != '')) {
            $("#signin").removeClass('disable');
        } else {
            $("#signin").addClass('disable');
        }

        $("#UserLogin_username, #UserLogin_password").change(function(){
            var input_login = $("#UserLogin_username").val();
            var input_password = $("#UserLogin_password").val();
            if ((input_login != '') && (input_password != '')) {
                $("#signin").removeClass('disable');
            } else {
                $("#signin").addClass('disable');
            }
        });
        $("button.btn.green_bnt.btn_sing_in").bind("click",function(event){
            location.reload();
        });



    })
</script>

