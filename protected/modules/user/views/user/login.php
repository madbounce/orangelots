<div class="main_container login_margin">
    <? $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Login") ?>
    <br>
    <span class="popup__title">Welcome to <span>Orange</span>Lots</span>
    <? if (Yii::app()->user->hasFlash('loginMessage')): ?>
        <div class="success">
            <?= Yii::app()->user->getFlash('loginMessage'); ?>
        </div>
    <? endif; ?>

    <div class="form" id="user-login-form">
        <?= CHtml::beginForm(); ?>
        <?= CHtml::errorSummary($model); ?>
        <div class="popup_form_row">
            <label>Email <span class="important_star">*</span></label>
            <?= CHtml::activeTextField($model, 'username') ?>
        </div>
        <div class="popup_form_row">
            <label>Password <span class="important_star">*</span></label>
            <?= CHtml::activePasswordField($model, 'password') ?>
        </div>

        <div class="popup_form_row">
            <p class="hint">
                <?= CHtml::link(UserModule::t("Register"), Yii::app()->getModule('user')->registrationUrl); ?>
                | <?= CHtml::link(UserModule::t("Lost Password?"), Yii::app()->getModule('user')->recoveryUrl); ?>
            </p>
        </div>
        <div class="popup_form_row">
            <button class="btn_image">
                <img src="<?php echo yii()->baseUrl.'/images/btn/sing_in.png'?>" alt="">
            </button>
        </div>
        <?= CHtml::endForm(); ?>
    </div>
</div>
<?
$form = new CForm(array(
    'elements' => array(
        'username' => array(
            'type' => 'text',
            'maxlength' => 32,
        ),
        'password' => array(
            'type' => 'password',
            'maxlength' => 32,
        ),
        'rememberMe' => array(
            'type' => 'checkbox',
        )
    ),
    'buttons' => array(
        'login' => array(
            'type' => 'submit',
            'label' => 'Login',
        ),
    ),
), $model);
?>