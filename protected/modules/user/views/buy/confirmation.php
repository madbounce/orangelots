<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href=""></a></li>
            <li><a href=""><?php echo ucfirst(yii()->controller->id)?></a></li>
            <li><a href="<?php echo createUrl('/user/'.yii()->controller->id.'/payments')?>">Payments</a></li>
            <li><a href="">Payment Invoice</a></li>
        </ul>
    </div>
</div>

<div class="payment_invoice__container" id ="print_info">
    <div class="main_container">
        <p class="payment_invoice__title">
            Order confirmation: <?php echo $model->transaction_id?>
            <?php
            $this->widget('ext.mPrint.mPrint', array(
                    'title' => 'confirmation',
                    'element' => '#print_info',
                    'exceptions' => array(
                        '#print_ver_link',
                    ),
                    'publishCss' => true,
                    'cssFile'=>'style.css',
                    'visible' => true,
                    'alt' => 'confirmation',
                    'id' => 'print_ver_link'
                ));
            ?>
            <?if($canCancel):?>
                <?php echo CHtml::ajaxLink(
                    "Cancel invoice",
                    yii()->createUrl('user/sell/render-cancel'),
                    array(
                        'type' => 'POST',
                        'data' => array('id' => $model->id),
                        'success' => "function(data){
                            $('body').append(data);
                        }",
                    ),
                    array(
                        'class' => 'btn green_bnt'
                    )
                );
                ?>

            <?else:?>
                <?if($model->owner_id != user()->id):?>
                    <?if(empty($model->pay_type)):?>
                        <?if($model->amount<param('maxPaySum')):?>
                            <?php echo CHtml::ajaxLink(
                                "Pay with paypal",
                                yii()->createUrl('pay/index/popup'),
                                array(
                                    'type' => 'POST',
                                    'data' => array(
                                        'invoiceId' => $model->id
                                    ),
                                    'success' => "function(data){
                                    $('body').append(data);
                                }",
                                ),
                                array('class' => 'btn green_bnt')
                            )?>
                        <?endif;?>
                        <?php echo CHtml::link("Pay with bank", createUrl('pay/index/payWithBank', array('id' => $model->id)), array('class' => 'btn green_bnt'));
                        ?>
                    <?endif;?>
                <?endif;?>
            <?endif;?>
        </p>

        <div class="clearfix">
            <div class="payment_invoice__inform_block pull-left">
                <p class="payment_invoice__inform_block__title">Vendor information</p>
                <p><?php echo $model->auction_deal->vendors_information?></p>
            </div>

            <div class="payment_invoice__inform_block pull-left">
                <p class="payment_invoice__inform_block__title">Buyer information</p>
                <?if(!empty($buyerInfo->company)):?>
                    <p><?php echo $buyerInfo->company?></p>
                <?else:?>
                    <p><?php echo ucfirst($buyerInfo->firstname).' '. ucfirst($buyerInfo->lastname)?></p>
                <?endif;?>
                <p><?php echo $buyerInfo->address_1?></p>
                <p>
                    <?php echo OrangelotsInvoices::getBuyerAddress($buyerInfo)?>
                </p>
            </div>

            <div class="payment_invoice__inform_block pull-right">
                <p class="payment_invoice__inform_block__title">Invoice status</p>
                <?if($model->payed ==OrangelotsInvoices::NOT_PAYED):?>
                    <p><?php echo OrangelotsInvoices::$statusesText[OrangelotsInvoices::NOT_PAYED]?></p>
                <?elseif($model->payed == OrangelotsInvoices::STATUS_CANCELLED):?>
                    <p><?php echo OrangelotsInvoices::$statusesText[OrangelotsInvoices::STATUS_CANCELLED]?></p>
                    <p><?php echo date("m/d/Y H:m", $model->transaction_date);?></p>
                    <p><?php echo !empty($model->vendor_comment) ? "Vendor comment: ".$model->vendor_comment : ""?></p>
                <?elseif($model->payed >= OrangelotsInvoices::PAYED):?>
                    <?if($vendor):?>
                        <p><?php echo OrangelotsInvoices::$statusesText[OrangelotsInvoices::PAYED]?></p>
                        <p><?php echo date("m/d/Y H:m", $model->transaction_date);?></p>
                        <?if(!empty($model->withdraw_date)):?>
                            <p><?php echo OrangelotsInvoices::$statusesText[OrangelotsInvoices::WITHDRAW]?></p>
                            <p><?php echo date("m/d/Y H:m", $model->withdraw_date);?></p>
                        <?endif;?>
                    <?else:?>
                        <p><?php echo OrangelotsInvoices::$statusesText[OrangelotsInvoices::PAYED]?></p>
                        <p><?php echo date("m/d/Y H:m", $model->transaction_date);?></p>
                    <?endif;?>
                <?endif;?>
            </div>
        </div>

        <div class="payment_invoice__border clearfix">
            <div class="payment_invoice__inform_block pull-left">
                <p class="payment_invoice__inform_block__title">Shipping information</p>
                <p><span>Condition:</span> Returns</p>
                <p><span>Shipping Terms: </span><?php echo $model->auction_deal->shipping_terms_rel->name?></p>
                <p><span>Delivery: </span><?php echo $model->auction_deal->delivery_rel->name?></p>
                <p><span>Manufacturer:</span> <?php echo $model->auction_deal->manufacture_rel->name?></p>
            </div>
            <div class="payment_invoice__inform_block pull-left">
                <?if($model->pay_type == OrangelotsInvoices::PAY_WITH_BANK):?>
                    <p class="payment_invoice__inform_block__title">Bank information</p>
                    <?php echo $bankInfo?>
                    <br>
                    <p class="payment_invoice__inform_block__title">Wire instructions</p>
                    Please specify the invoice #<?php echo $model->transaction_id?> and the auction #<?php echo $model->auction_deal_id?>
                <?elseif($model->pay_type == OrangelotsInvoices::PAY_WITH_PAYPAL):?>
                    <p class="payment_invoice__inform_block__title">Payment information</p>
                    <p><span>Owner name: </span><?php echo ucfirst($cardInfo->owner_name)?></p>
                    <p><span>Number: </span> <?php echo $cardInfo->viewNumber()?></p>
                    <p><span>Expiration date: </span><?php echo $cardInfo->expire_month.'/'.$cardInfo->expire_year?></p>
                <?endif;?>
            </div>
        </div>

        <div class="payment_invoice__table" id = "table_to_print">
            <table>
                <thead>
                <tr>
                    <td class="text-left">Name</td>
                    <td width="150">Vendor</td>
                    <td width="110">Price per unit</td>
                    <td width="60">Units</td>
                    <td width="80">Sold</td>
                    <td width="125">Purchase date</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-left">
                        <a href="<?php echo createUrl('auctions/orangelots-auctions/view/', array('id' => $model->auction_deal->id))?>"><?php echo $model->auction_deal->name?> - Original MSRP $<?php echo number_format($model->auction_deal->mvrp, 2)?></a>
                        </br>
                        <?php OrangelotsAuctions::model()->itemsView($model->auction_deal->id);?>
                    </td>
                    <td>
                        <?php echo $model->auction_deal->vendors_information?>
                    </td>
                    <td>$<?php echo number_format($winnerInfo->price_per_unit, 2)?></td>
                    <?php if ($auction->is_flashdeal == 0):?>
                        <td><?php echo $winnerInfo->qty?></td>
                    <?php elseif ($auction->is_flashdeal == 1):?>
                        <td><?php echo $winnerInfo->qty*$auction->units_in_lot ?></td>
                    <?php endif;?>
                    <td>$<?php echo number_format($model->amount, 2)?></td>
                    <td><?php echo !empty($model->purchase_date) ? date('m/d/Y h:ma', $model->purchase_date) : "Not Paid"?></td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>
</div>