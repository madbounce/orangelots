<tr>
    <td><a href="" class="orange_link"><?php echo $data->transaction_id?></a></td>
    <td><?php echo $data->paypal_id?></td>
    <td><?php echo $data->from_who?></td>
    <td><?php echo $data->email?></td>
    <td><?php echo $data->phone?></td>
    <td>$<?php echo number_format($data->amount, 2)?></td>
    <td><a href="<?php echo createUrl('auctions/orangelots-auctions/view/', array('id' => $data->auction_deal_id))?>" class="orange_link"><?php echo $data->auction_deal_id?></a></td>
    <td><?php echo !empty($data->transaction_date) ? date('m/d/Y h:ma', $data->transaction_date) : ""?></td>
    <td><?php echo OrangelotsInvoices::payStatusBuyer($data->payed)?></td>
    <td><a href="<?php echo createUrl('user/buy/confirmation', array('transaction' => $data->transaction_id))?>" class="orange_link">Invoice</a></td>
</tr>