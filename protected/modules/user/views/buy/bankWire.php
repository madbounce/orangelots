<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href=""></a></li>
            <li><a href="">Bank Wire</a></li>
        </ul>
    </div>
</div>

<div class="payment_invoice__container" id ="print_info">
    <div class="main_container">
        <p class="payment_invoice__title">
            Bank Wire
            <?php
            $this->widget('ext.mPrint.mPrint', array(
                    'title' => 'confirmation',
                    'element' => '#print_info',
                    'exceptions' => array(
                        '#print_ver_link',
                    ),
                    'publishCss' => true,
                    'cssFile'=>'style.css',
                    'visible' => true,
                    'alt' => 'confirmation',
                    'id' => 'print_ver_link'
                ));
            ?>
        </p>

        <div class="payment_invoice__border clearfix">
            <div class="payment_invoice__inform_block pull-left">
                <?php echo param('payDetails');?>
            </div>
        </div>

    </div>
</div>