<?php
require_once(Yii::getPathOfAlias('application.components.paypal') . '/bootstrap.php');

use PayPal\Rest\ApiContext;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;
use PayPal\Exception\PPConnectionException;


/**
 * This is the model class for table "pay_card_info".
 *
 * The followings are the available columns in table 'pay_card_info':
 * @property integer $id
 * @property integer $user_id
 * @property string $hidden_number
 * @property string $type
 * @property string $paypal_id
 * @property string $firstname
 * @property string $lastname
 * @property string $state
 * @property string $card_token
 * @property integer $expire_month
 * @property integer $expire_year
 *
 */
class CardInfo extends CActiveRecord
{
	public $agree;
	public $owner_name;
	public $number;
	public $month;
	public $year;
    public $cvv;

    public static $payedStatus = 'approved';

    const VISA = 'visa';
    const MASTERCARD = 'mastercard';
    const AMEX = 'amex';
    const DISCOVER = 'discover';
    const MAESTRO = 'maestro';

    public static $typesArray = array(
        self::VISA => 'Visa',
        self::MASTERCARD => 'Master Card',
        self::AMEX => 'Amex',
        self::DISCOVER => 'Discover',
        self::MAESTRO => 'Maestro',
    );

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pay_card_info';
	}

	public function getCurrentMonth()
	{
		$date = getdate(time());
		return $date['mon'];
	}

	public function checkDate($attribute)
	{
		$date = getdate(time());
		$currMon = $date['mon'];
		$currYear = $date['year'];

		if ($this->year > $currYear) {
			return true;
		} else {
			if ($this->month < $currMon) {
				$this->addError($attribute, 'Wrong date');
			} else {
				return true;
			}
		}
	}

    public function checkName($attribute){
        $nameInfo = explode(' ', $this->owner_name);
        if(!empty($nameInfo[0]) && !empty($nameInfo[1])){
            return true;
        }else{
            $this->addError($attribute, 'You have to write first name and last name');
        }
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id, owner_name, number, cvv, month, year', 'required', 'on' => 'create'),
			array('user_id, number, cvv, month, year', 'numerical', 'integerOnly' => true, 'on' => 'create'),
			array('owner_name, number', 'length', 'max' => 255, 'on' => 'create'),
			array('number', 'length', 'max' => 19, 'on' => 'create'),
			array('number', 'length', 'min' => 11, 'on' => 'create'),
			array('agree', 'in', 'range' => array(1), 'message' => 'You have to accept agreement', 'on' => 'create'),
			array('month', 'checkDate', 'on' => 'create'),
            array('owner_name', 'checkName', 'on' => 'create'),
            array('user_id, hidden_number, type, paypal_id, expire_month, expire_year, firstname, lastname', 'required', 'on' => 'savePayPalInfo'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'user' => array(
				self::BELONGS_TO,
				'OrangelotsUsers',
				'user_id',
				'joinType' => 'INNER JOIN'
			)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t("Id"),
			'user_id' => 'User id',
		);
	}

	public function scopes()
	{
		return array();
	}

    public static function getUserCardInfo($userId){
        return self::model()->findByAttributes(array('user_id' => $userId));
    }

    public function viewNumber(){
        $numArr = str_split($this->hidden_number);
        $resArr = array();
        $count = count($numArr);
        $i = 1;
        foreach($numArr as $key=>$value){
            if($key < $count-4){
                $resArr[] = $value;
                if(is_int($i/4)){
                    $resArr[] = ' ';
                }
            }else{
                $resArr[] = $value;
            }
            $i++;
        }
        return implode('',$resArr);
    }

    public static function actionGetType($number){
        $numbers = str_replace (" ","",$number);
        $numArr = str_split($numbers);
        if($numArr[0] == 5){
            if($numArr[1] == 1 || $numArr[1] == 5){
                return self::$typesArray[self::MASTERCARD];
            }
        }elseif($numArr[0] == 4){
            return self::$typesArray[self::VISA];
        }elseif($numArr[0] == 3){
            if($numArr['1'] == 7 || $numArr['1'] == 4){
                return self::$typesArray[self::AMEX];
            }
        }elseif($numArr[0] == 6){
            if($numArr[1] == 5){
                return self::$typesArray[self::DISCOVER];
            }
            if($numArr[1] == 4 and $numArr[2] == 4){
                return self::$typesArray[self::DISCOVER];
            }
            if($numArr[1] == 0 and $numArr[2] == 1 and $numArr[3] == 1){
                return self::$typesArray[self::DISCOVER];
            }
        }elseif($numArr[0] == 6 || $numArr[0] == 3 || $numArr[0] == 5){
            return self::$typesArray[self::MAESTRO];
        }
    }

    public static function unsetErrors(){
        if(!empty($_SESSION['paypal_error'])){
            unset($_SESSION['paypal_error']);
        }
    }

    public static function getApiContext(){
        $sdkConfig = array(
            "mode" => "sandbox"
        );
        $apiContext = new ApiContext(new OAuthTokenCredential(yii()->params['payPal']['sandbox']['client_id'],yii()->params['payPal']['sandbox']['client_secret']));
        $apiContext->setConfig($sdkConfig);
        return $apiContext;
    }

    public function setPayPalCardInfo(){
        self::unsetErrors();
        $nameInfo = explode(' ', $this->owner_name);
        $card = new CreditCard();
        $card->setType($this->type);
        $card->setNumber(str_replace (" ","",$this->number));
        $card->setExpireMonth($this->month);
        $card->setExpireYear($this->year);
        $card->setCvv2($this->cvv);
        $card->setFirstName($nameInfo[0]);
        $card->setLastName($nameInfo[1]);
        $card->setPayerId(user()->id);
        try {
            $resp = $card->create(self::getApiContext());
        } catch (PPConnectionException $ex) {
            $json = $ex->getData();
            $errorData = CJSON::decode($json);
            if(!empty($errorData['details'][0]['issue'])){
                $_SESSION['paypal_error'] = $errorData['details'][0]['issue'];
            }
        }
        if($resp){
            $this->setScenario('savePayPalInfo');
            $this->hidden_number = $card->getNumber();
            $this->type = $card->getType();
            $this->paypal_id = $card->getId();
            $this->expire_month = $card->getExpireMonth();
            $this->expire_year = $card->getExpireYear();
            $this->firstname = $card->getFirstName();
            $this->lastname = $card->getLastName();
            $this->state = $card->getState();
            $this->user_id = user()->id;
            if($this->save()){
                return true;
            }
        }else{
            return false;
        }
    }

    public function deleteFromPayPal(){
        $apiContext = self::getApiContext();
        $card = CreditCard::get($this->paypal_id, $apiContext);
        if($card->delete($apiContext)){
            return true;
        }
    }

    public static function pay($data){
        self::unsetErrors();
        $resp = array();
        $resp['payed'] = false;

        $apiContext = self::getApiContext();
        $userCard = self::getUserCardInfo($data->winner_id);

        $creditCardToken = new CreditCardToken();
        $creditCardToken->setCreditCardId($userCard->paypal_id);
        $creditCardToken->setPayerId($userCard->user_id);

        $fundingInstrument = new FundingInstrument();
        $fundingInstrument->setCreditCardToken($creditCardToken);

        $payer = new Payer();
        $payer->setPaymentMethod("credit_card");
        $payer->setFundingInstruments(array($fundingInstrument));

        $amount = new Amount();
        $amount->setCurrency("USD");
        $amount->setTotal($data->amount);

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription("creating a payment with saved credit card");

        $payment = new Payment();
        $payment->setIntent("sale");
        $payment->setPayer($payer);
        $payment->setTransactions(array($transaction));

        try {
            $result = $payment->create($apiContext);
        } catch (PPConnectionException $ex) {
            $resp['error'] = $ex->getMessage();
        }
        if($result){
            $state = $payment->getState();
            if($state == self::$payedStatus){
                $resp['payed'] = true;
            }
            $resp['payPalId'] = $result->getId();
        }
        return $resp;
    }

}