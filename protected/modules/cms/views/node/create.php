<?php $this->breadcrumbs = array(
	Yii::t('CmsModule.core','Cms')=>array('admin/index'),
	Yii::t('CmsModule.core','Create node')
) ?>

<div class="node-create form main_container">

    <h1><?php echo Yii::t('CmsModule.core','Create node') ?></h1>

	<?php $form = $this->beginWidget('CActiveForm',array(
		'id'=>'cmsCreateNodeForm',
		'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	)) ?>

    <p class="uni_form_row">
	        <?php echo $form->label($model,'name') ?>
	        <?php echo $form->textField($model,'name') ?>
			<span class="hint"><?php echo Yii::t('CmsModule.core','Node name cannot be changed after creation.') ?></span>
	    </p>

    <p class="uni_form_row">
			<?php echo $form->label($model,'parentId') ?>
			<?php echo $form->dropDownList($model,'parentId',$model->getParentOptionTree()) ?>
			<?php echo $form->error($model,'parentId') ?>
		</p>

    <p class="uni_form_row">
            <?php echo $form->label($model,'level') ?>
			<?php echo $form->radioButtonList($model,'level',$model->getLevelOptions(), array('separator' => '')) ?>
			<?php echo $form->error($model,'level') ?>
		</p>

    <div class="row buttons">
			<?php echo CHtml::submitButton(Yii::t('CmsModule.core', 'Create'), array('class' => 'btn btn_orange')) ?>
		</div>

	<?php $this->endWidget() ?>

</div>