<?php $this->breadcrumbs = array(
	Yii::t('CmsModule.core', 'Cms') => array('admin/index'),
	Yii::t('CmsModule.core', 'Nodes'),
) ?>

<div class="node-index">

	<h1>Pages</h1>


	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider' => $model->search(),
		'columns' => array(
			'id',
			array(
				'name' => 'name',
				'type' => 'raw',
				'value' => 'CHtml::link("$data->name",createUrl("//cms/node/update",array("id"=>"$data->id")))'
			),
			array(
				'name' => 'parentId',
				'value' => '$data->parent !== null ? $data->parent->name : ""',
			),
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'viewButtonUrl'=>'Yii::app()->cms->createUrl($data->name)',
			),
		),
	)) ?>
    <p><?php echo CHtml::link(Yii::t('CmsModule.core', 'Create a new page'), array('node/create'), array('class' => 'btn btn_orange')) ?></p>

</div>