<div class="main_container">
    <div class="form main_container">
        <div class="uni_form_block">
            <fieldset class="form-content">

                <p class="uni_form_row">
                    <?php echo $form->labelEx($model, '[' . $model->locale . ']heading') ?>
                    <?php echo $form->textField($model, '[' . $model->locale . ']heading') ?>
                    <?php echo $form->error($model, '[' . $model->locale . ']heading') ?>
                </p>

                <p class="uni_form_row">
                    <?php echo $form->labelEx($model, '[' . $model->locale . ']body') ?>
                    <?php $this->widget(
                        'cms.widgets.markitup.CmsMarkItUp',
                        array(
                            'model' => $model,
                            'attribute' => '[' . $model->locale . ']body',
                            'set' => 'html',
                            'htmlOptions' => array('id' => 'body-' . $model->locale),
                        )
                    ) ?>
                    <?php echo $form->error($model, '[' . $model->locale . ']body') ?>

                <div class="tags">
                    <?php $this->renderPartial('_tags'); ?>
                </div>
                </p>

                <p class="uni_form_row">
                    <?php echo $form->labelEx($model, '[' . $model->locale . ']css') ?>
                    <?php echo $form->textArea($model, '[' . $model->locale . ']css', array('rows' => 6)) ?>
                    <?php echo $form->error($model, '[' . $model->locale . ']css') ?>
                </p>

            </fieldset>

            <fieldset class="form-attachments">

                <legend><?php echo Yii::t('CmsModule.core', 'Attachments') ?></legend>

                <p class="uni_form_row">
                    <?php $this->widget(
                        'zii.widgets.grid.CGridView',
                        array(
                            'id' => 'attachments_' . $model->locale,
                            'dataProvider' => $model->getAttachments(),
                            'template' => '{items} {pager}',
                            'emptyText' => Yii::t('CmsModule.core', 'No attachments found.'),
                            'showTableOnEmpty' => false,
                            'columns' => array(
                                array(
                                    'name' => 'id',
                                    'header' => '#',
                                    'value' => '$data->id',
                                ),
                                array(
                                    'header' => Yii::t('CmsModule.core', 'URL'),
                                    'value' => '$data->resolveName()',
                                ),
                                array(
                                    'header' => Yii::t('CmsModule.core', 'Tag'),
                                    'value' => '$data->renderTag()',
                                ),
                                array(
                                    'class' => 'bootstrap.widgets.TbButtonColumn',
                                    'template' => '{delete}',
                                    'buttons' => array(
                                        'delete' => array(
                                            'url' => 'Yii::app()->controller->createUrl("deleteAttachment", array("id"=>$data->id))',
                                        ),
                                    ),
                                ),
                            ),
                        )
                    ) ?>
                </p>

                <p class="uni_form_row">
                    <?php echo $form->labelEx($model, '[' . $model->locale . ']attachment') ?>
                    <?php echo $form->fileField($model, '[' . $model->locale . ']attachment') ?>
                    <?php echo $form->error($model, '[' . $model->locale . ']attachment') ?>
                </p>

            </fieldset>

            <?php if ($node->level === CmsNode::LEVEL_PAGE): ?>

                <fieldset class="form-page-settings">

                    <legend><?php echo Yii::t('CmsModule.core', 'Page settings') ?></legend>

                    <p class="hint"><?php echo Yii::t(
                            'CmsModule.core',
                            'Please note that the fields below are only used with pages.'
                        ) ?></p>

                    <p class="uni_form_row">
                        <?php echo $form->labelEx($model, '[' . $model->locale . ']url') ?>
                        <?php echo $form->textField($model, '[' . $model->locale . ']url') ?>
                        <?php echo $form->error($model, '[' . $model->locale . ']url') ?>
                    </p>

                    <p class="uni_form_row">
                        <?php echo $form->labelEx($model, '[' . $model->locale . ']pageTitle') ?>
                        <?php echo $form->textField($model, '[' . $model->locale . ']pageTitle') ?>
                        <?php echo $form->error($model, '[' . $model->locale . ']pageTitle') ?>
                    </p>

                    <p class="uni_form_row">
                        <?php echo $form->labelEx($model, '[' . $model->locale . ']breadcrumb') ?>
                        <?php echo $form->textField($model, '[' . $model->locale . ']breadcrumb') ?>
                        <?php echo $form->error($model, '[' . $model->locale . ']breadcrumb') ?>

                    <p class="hint"><?php echo Yii::t('CmsModule.core', 'The breadcrumb text for this node.') ?></p>
                    </p>

                    <p class="uni_form_row">
                        <?php echo $form->labelEx($model, '[' . $model->locale . ']metaTitle') ?>
                        <?php echo $form->textField($model, '[' . $model->locale . ']metaTitle') ?>
                        <?php echo $form->error($model, '[' . $model->locale . ']metaTitle') ?>
                    </p>

                    <p class="uni_form_row">
                        <?php echo $form->labelEx($model, '[' . $model->locale . ']metaDescription') ?>
                        <?php echo $form->textArea(
                            $model,
                            '[' . $model->locale . ']metaDescription',
                            array('rows' => 3)
                        ) ?>
                        <?php echo $form->error($model, '[' . $model->locale . ']metaDescription') ?>
                    </p>

                    <p class="uni_form_row">
                        <?php echo $form->labelEx($model, '[' . $model->locale . ']metaKeywords') ?>
                        <?php echo $form->textField($model, '[' . $model->locale . ']metaKeywords') ?>
                        <?php echo $form->error($model, '[' . $model->locale . ']metaKeywords') ?>
                    </p>

                    <!--        --><?php //echo CHtml::link(Yii::t('CmsModule.core','View page'), $node->getUrl(), array('class' => 'btn btn_orange')); ?>

                </fieldset>

            <?php endif; ?>
        </div>
    </div>
</div>