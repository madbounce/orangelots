<div class="container">
	<div class="row">
		<div class="span12">
			<div class="node-page">
				<div class="row">
					<div class="span9">
						<div class="node-content">
							<?= $content ?>
						</div>
					</div>
					<div class="span2">
						<?php if (Yii::app()->cms->checkAccess()): ?>
							<?php echo CHtml::link(Yii::t('CmsModule.core', 'Update'),
								array('node/update', 'id' => $model->id), array('class' => 'btn btn-small update-link')) ?>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>