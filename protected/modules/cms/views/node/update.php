<?php $this->breadcrumbs = CMap::mergeArray(
    $model->getBreadcrumbs(true),
    array(
        Yii::t('CmsModule.core', 'Update')
    )
) ?>
<div class="main_container">
    <div class="node-update form">

        <h1><?php //echo Yii::t('CmsModule.core','Update :name',array(':name'=>ucfirst($model->name))) ?></h1>

        <h1><?php echo 'Update Pages' ?></h1>

        <?php $form = $this->beginWidget(
            'CActiveForm',
            array(
                'id' => 'cmsUpdateNodeForm',
                'htmlOptions' => array('enctype' => 'multipart/form-data')
            )
        ); ?>

        <fieldset class="form-node">

            <p class="uni_form_row">
                <?php echo $form->label($model, 'name') ?>
                <span class="uneditable-input"><?php echo CHtml::encode($model->name) ?></span>
                <span class="hint"><?php echo Yii::t('CmsModule.core', 'Node name cannot be changed.') ?></span>
            </p>

            <p class="uni_form_row">
                <?php echo $form->label($model, 'parentId') ?>
                <?php echo $form->dropDownList($model, 'parentId', $model->getParentOptionTree()) ?>
                <?php echo $form->error($model, 'parentId') ?>
            </p>

            <p class="uni_form_row">
                <?php echo $form->label($model, 'level') ?>
                <?php echo $form->radioButtonList(
                    $model,
                    'level',
                    $model->getLevelOptions(),
                    array('separator' => '')
                ) ?>
                <?php echo $form->error($model, 'level') ?>
            </p>

            <p class="uni_form_row">
                <?php echo $form->label($model, 'published') ?>
                <?php echo $form->checkBox($model, 'published') ?>
                <?php echo $form->error($model, 'published') ?>
            </p>

        </fieldset>

        <?php $tabs = array();
        foreach ($translations as $locale => $content) {
            $language = Yii::app()->cms->languages[$locale];
            $tab = array(
                'content' => $this->renderPartial(
                        '_form',
                        array(
                            'model' => $content,
                            'form' => $form,
                            'node' => $model,
                            'language' => $language,
                        ),
                        true
                    ),
            );
            $tabs[$language] = $tab;
        } ?>

        <?php $this->widget(
            'zii.widgets.jui.CJuiTabs',
            array(
                'headerTemplate' => '<li><a href="{url}"></a></li>',
                'tabs' => $tabs,
            )
        ); ?>

        <div class="row buttons">
            <!--			<div class="pull-left">-->
            <button class="btn btn_orange"><?php echo $model->isNewRecord ? 'Create page' : 'Save page'; ?></button>
            <!--			</div>-->
            <!--			<div class="pull-right">-->
            <?php echo CHtml::link(
                Yii::t('CmsModule.core', 'Delete'),
                array('delete', 'id' => $model->id),
                array(
                    'class' => 'btn btn_orange',
                    'confirm' => Yii::t('CmsModule.core', 'Are you sure you want to delete this node?'),
                )
            ) ?>
            <?php echo CHtml::link(
                Yii::t('CmsModule.core', 'View page'),
                $model->getUrl(),
                array('class' => 'btn btn_orange')
            ); ?>
            <!--			</div>-->
        </div>

        <?php $this->endWidget() ?>

    </div>
</div>