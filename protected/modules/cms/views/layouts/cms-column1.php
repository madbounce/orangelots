<?php $this->beginContent(Yii::app()->cms->appLayout) ?>

	<div class="cms bp column1">

		<div class="clearfix">

			<div class="span12">

				<div class="cms-content">

					<?php echo $content ?>

				</div>

			</div>

		</div>

	</div>
<?php $this->endContent() ?>