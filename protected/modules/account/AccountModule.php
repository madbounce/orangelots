<?php

class AccountModule extends CWebModule
{
	public $defaultController = "Default";

	public function init()
	{
		$this->setImport(array(
			'account.models.*',
			'account.components.*',
//			'application.modules.user.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}
}