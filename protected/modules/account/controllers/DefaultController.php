<?php
yii::import('application.models.*');

class DefaultController extends Controller
{
    /**
     * Список фильтров
     * @return array
     */

    public $layout = '//layouts/main';

    public function filters()
    {
        return array('rights');
    }

    /**
     * Правила доступа к действиям
     * @return array
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'actions' => array('index', 'create', 'update', 'delete', 'Attributes'),
                'roles' => array('admin', 'moderator','Realter'),
            ),
            array(
                /*'deny',
                'users' => array('*',),*/
            ),
        );
    }

    /**
     * Добавить отзыв
     */
    public function actionCreate()
    {
        $model = new Orangelots();

        $data = Yii::app()->getRequest()->getParam('Orangelots', null);
        if (!empty($data)) {
            $model->attributes = $data;
            $model->user_id = user()->id;
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($model->save()) {
                $this->redirect('index');
            }
        }
        $this->registerScriptFiles();
        $this->setPageTitle(yii()->name . ' - Добавить программу');
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Редактирование отзыв
     * @param integer $id
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $model->scenario = 'updateAttributes';


        $data = Yii::app()->getRequest()->getParam('Realty', null);
        if (!empty($data)) {
            $model->attributes = $data;
            $model->file = CUploadedFile::getInstance($model, 'file');

            if ($model->save()) {
                $this->redirect('../index');
            }
        }

        $this->registerScriptFiles();
        $this->setPageTitle(yii()->name . ' - Редактировать программу');

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Удалить отзыв
     * @param integer $id
     */
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id);
            $model->delete();

            $ajax = Yii::app()->request->getParam('ajax', null);
            if (empty($ajax)) {
                $returnUrl = Yii::app()->request->getParam('returnUrl', null);
                $this->redirect(!empty($returnUrl) ? $returnUrl : array('index'));
            }
        } else {
            throw new CHttpException(400, INVALID_REQUEST);
        }
    }

    /**
     * Показать все отзывы
     */
    public function actionIndex()
    {
        $model = new Orangelots('search');
        $user = OrangelotsUsers::model()->findByPk(yii()->user->id);
        $model->unsetAttributes();

        $data = Yii::app()->getRequest()->getParam('Orangelots', null);
        if (!empty($data)) {
            $model->attributes = $data;
        }
        $model->user_id = user()->id;
        $this->setPageTitle(Yii::app()->name . ' - Dashboard');
        $cardInfo = CardInfo::getUserCardInfo(user()->id);
        $notifications = OrangelotsNotifications::model()->findAllByAttributes(array('user_id'=>yii()->user->id, 'reading' => 0));
        $this->render('index', array(
            'model' => $model,
            'user' => $user,
            'cardInfo' => $cardInfo,
            'notifications' => $notifications,
        ));
    }

    /**
     * Загружаем затребованную модель
     * @param integer
     */
    public function loadModel($id)
    {
        $model = Realty::model()->findByPk($id);
        if (empty($model)) {
            throw new CHttpException(404, REQUESTED_PAGE_DOES_NOT_EXIST);
        }
        return $model;
    }

    /**
     * Подключаем необходимые скрипты
     */
    private function registerScriptFiles()
    {
//		registerScriptFile(baseUrl() . '/admin/js/browser.video.js');
//		registerScriptFile(baseUrl() . '/admin/js/multipic.js');
    }

    public function getTabularTabs($model)
    {
        $tabs = array();

        $tabs[] = array(
            'active' => true,
            'label' => 'Программа',
            'content' => $this->renderPartial('parts/_form', array('model' => $model,), true),
        );

        $tabs[] = array(
            'active' => false,
            'label' => 'Фотогалерея',
            'content' => $this->renderPartial('parts/_photo', array('model' => $model,), true),
        );

        $tabs[] = array(
            'active' => false,
            'label' => 'Атрибуты',
            'content' => $this->renderPartial('parts/_attributes_form', array('model' => $model,), true),
        );

        return $tabs;
    }

    /**
     * Список атрибутов категории для формы обьявления
     */
    public function actionAttributes()
    {
        $category_id = Yii::app()->getRequest()->getParam('category_id');
        $this->renderPartial('parts/_attributes', array(
            'dataAttributes' => array(),
            'category_id' => $category_id,
        ));
    }

    public function actionRating(){
        if (isset($_POST['Rating']['user_id'])){
            $rating = OrangelotsRating::model()->findByAttributes(array('user_id'=>yii()->user->id,'rate_user_id'=>$_POST['Rating']['user_id']));
                echo json_encode(array('id'=>$_POST['Rating']['user_id'],'html' => $this->renderPartial('application.modules.account.views.default.rating', array('user_id'=>$_POST['Rating']['user_id']), true, true)));
        }

    }

    public function actionRatingPopup(){
        if (isset($_POST['Rating']['user_id'])){
            $rate = array();
            for($i=1;$i<=10;$i++){
                $rate[$i]=$i;
            }
            $model = new OrangelotsFeedback;
            $model->communication = 1;
            $model->billing = 1;
            $model->shipping = 1;
            Yii::app()->session['rating_url_redirect'] = $_POST['Rating']['url'];
            echo json_encode(
                            array(
                                'status' => 'ok',
                                'html' => $this->renderPartial(
                                        'application.modules.account.views.default.feedback',
                                        array(
                                            'model' => $model,
                                            'rate' => $rate,
                                            'rate_user_id'=>$_POST['Rating']['user_id']
                                        ),
                                        true,
                                        true
                                    )
                            )
                        );
        }

    }
    public function actionRateUser(){
        $model = new OrangelotsFeedback;
        $this->performAjaxValidation($model);
        if(isset($_POST['OrangelotsFeedback'])){
            $model->communication = ($_POST['OrangelotsFeedback']['communication']>10)?10:$_POST['OrangelotsFeedback']['communication'];
            $model->billing = ($_POST['OrangelotsFeedback']['billing']>10)?10:$_POST['OrangelotsFeedback']['billing'];
            $model->shipping = ($_POST['OrangelotsFeedback']['shipping']>10)?10:$_POST['OrangelotsFeedback']['shipping'];
            $model->message = $_POST['OrangelotsFeedback']['message'];
            $average = round((($model->communication+$model->billing+$model->shipping)/3),2);
            $model->user_id_rate = $_POST['OrangelotsFeedback']['rate_user_id'];
            $model->user_id = yii()->user->id;

            $prev_rate = OrangelotsFeedback::model()->findByAttributes(array('user_id_rate'=>(int)$_POST['OrangelotsFeedback']['rate_user_id']), array('order'=> 'id DESC'));
            if ((isset($prev_rate))&&(!empty($prev_rate))){
                $model->count = $prev_rate->count+1;
                $model->average = round(((($prev_rate->average*3)+$average)/($prev_rate->count+1)),2);
                if($model->save()){
                    if((isset(Yii::app()->session['rating_url_redirect']))&&(!empty(Yii::app()->session['rating_url_redirect'])))
                        $this->redirect(createUrl(Yii::app()->session['rating_url_redirect']));
                    else
                        $this->redirect(createUrl('account/default/buyer-list'));
                };
            } else {
                $model->count = 1;
                $model->average = $average;
                if($model->save()){
                    if((isset(Yii::app()->session['rating_url_redirect']))&&(!empty(Yii::app()->session['rating_url_redirect'])))
                        $this->redirect(createUrl(Yii::app()->session['rating_url_redirect']));
                    else
                        $this->redirect(createUrl('account/default/buyer-list'));
                };
            }
//            dump($model);
//            dump($model->getErrors());
         }
    }

    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'orangelots-feedback') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionSetRating($id){
        if ((isset($id)&&(isset($_POST['rateUserID']))&&(isset($_POST['rate'])))){
            $rate = new OrangelotsRating;
            $rate->user_id = $_POST['rateUserID'];
            $rate->rate_user_id = $id;
            $rate->score = $_POST['rate'];

            $prev_rate = OrangelotsRating::model()->findByAttributes(array('rate_user_id'=>$id), array('order'=> 'id DESC'));
            if ((isset($prev_rate))&&(!empty($prev_rate))){
                $rate->count = $prev_rate->count+1;
                $rate->average = (($prev_rate->average*$prev_rate->count)+(int)$_POST['rate'])/($prev_rate->count+1);
                if($rate->save()){
                    echo 'success';
                };
            } else {
                $rate->count = 1;
                $rate->average = $_POST['rate'];
                if($rate->save()){
                    echo 'success';
                };
            }

        }
    }
    public function actionMessages($id=0){
        $this->setPageTitle(Yii::app()->name . ' - Messages');
        $notifications = OrangelotsNotifications::model()->findAllByAttributes(array('user_id'=>yii()->user->id, 'reading' => $id));
        $this->render('messages', array(
                'notifications' => $notifications,
                'reading' => $id,
            ));
    }

      public function actionSelectMessages($id){
        $this->setPageTitle(Yii::app()->name . ' - Messages');
        $notifications = OrangelotsNotifications::model()->findAllByAttributes(array('user_id'=>yii()->user->id, 'reading' => $id));
//        $archive = OrangelotsNotifications::model()->findAllByAttributes(array('user_id'=>yii()->user->id, 'reading' => 1));
          echo json_encode(
              array(
                  'status' => 'error',
                  'url' => '',
                  'html' => $this->renderPartial(
                          'messages',
                          array(
                              'notifications' => $notifications,
                              'reading' => $id,
                          ),
                          true,
                          true
                      )
              )
          );
    }

}
