<?php
/**
 * Шаблон списка в панеле управления
 * @author fen0man
 */
?>

<? // $this->widget("ext.VendorsPageMenu.VendorsPageMenuWidget") ?>
<div class="main_page__container">
    <div class="main_container clearfix">
        <div class="main_page__top_preview top_preview_en clearfix">

            <div class="main_page_dashboard__top_preview" style="background-color: #414141;<?php echo (user()->isVendor(user()->id))? 'width:100%':'';?>">
                <div class="main_page_dashboard__top_preview__contact" style="<?php echo (user()->isVendor(user()->id))? 'width:50%':'';?>">
                    <p class="main_page_dashboard__top_preview__title">Contact Information</p>

                    <p class="main_page__top_preview_image_text"><?php echo $user->firstname . ' ' . $user->lastname; ?></p>

                    <p class="main_page__top_preview_image_text"><?php echo $user->phone ?></p>

                    <p class="main_page__top_preview_image_text"><?php echo $user->email; ?></p>
                    <!--                    <button class="btn btn_orange">Edit</button>-->
                    <a href="<?php echo yii()->createUrl('user/orangelots-users/update') ?>"
                       class="btn btn_orange">Edit
                    </a>
                </div>

                <div class="main_page_dashboard__top_preview__contact">
                    <p class="main_page_dashboard__top_preview__title">Billing Information</p>
                    <?if(empty($cardInfo)):?>
                        <p class="main_page__top_preview_image_text"><?php echo ucfirst($user->firstname).', please authorize your billing info'?></p>
                        <a class="btn btn_orange" href="<?php echo createUrl('user/orangelots-users/billingInfo')?>">Edit</a>
                    <?else:?>
                        <p class="main_page__top_preview_image_text"><?php echo $cardInfo->viewNumber()?></p>
                        <p class="main_page__top_preview_image_text"><?php echo ucfirst($cardInfo->type)?></p>
                        <p class="main_page__top_preview_image_text"><?php echo ($cardInfo->expire_month<10 ? 0:'').$cardInfo->expire_month.'/'.$cardInfo->expire_year;?></p>
                        <a class="btn btn_orange" href="<?php echo createUrl('user/orangelots-users/billingInfo')?>">Edit</a>
                    <?endif;?>


                </div>
            </div>

            <?if(!user()->isVendor(user()->id)):?>

            <div class="main_page__top_preview_argument">
                <p class="main_page__top_preview_argument__title">
                    <span>5 Reasons</span><br/>
                    to become a vendor
                </p>
                <ul class="main_page__top_preview_argument__list">
                    <li>No subscription for vendors</li>
                    <li>No upfront payment</li>
                    <li>No hidden fee</li>
                    <li>Personal manager for each vendor</li>
                </ul>
                    <a class="btn_image main_page__top_preview_argument__btn" href="<?php echo createUrl('user/orangelots-users/create')?>"><img
                            src="<?php echo baseUrl() . '/images/btn/btn_become_a_vendor_black.png' ?>" alt=""/></a>
                <p class="main_page__top_preview_argument__info">We don't charge you to list your product. We help you
                    to sale them fast and easy.</p>
            </div>

            <?endif;?>
        </div>
    </div>
</div>
<div class="dashboard__messeges_notification">
    <div class="main_container">
        <p class="dashboard__messeges_notification__title">Messages and notifications</p>
        <table class="dashboard__messeges_notification__table" cellpading="0" cellspacing="0">
            <tbody>
            <?php if (!empty($notifications)):?>
            <?php foreach($notifications AS $key => $value):?>
                <?php if($value->message_id == '4'): ?>
                    <tr class="<?php echo 'notification_'.$value->id; ?>">
                        <td>
                            <?php echo $value->content; ?>
                        </td>
                        <td class="dashboard__messeges_notification__table_link_td" >
                            <?php echo CHtml::ajaxLink(
                                "dismiss",
                                yii()->createUrl('auctions/orangelots-auctions/dismiss'),
                                array(
                                    'type' => 'POST',
                                    'data' => array(
                                        'Dismiss[id]' => $value->id,
                                    ),
                                    'success' => "function(data){
                                if(data == 'ok'){
                                    $('tr.notification_".$value->id."').remove();
                                    if ($('.dashboard__messeges_notification__table tr').size() == 0){
                                        $('.dashboard__messeges_notification__table tbody').append('No more notifications for you')
                                    }
                                }
                        }",
                                )
                            );
                            ?>
                        </td>

                    </tr>
                <?php else: ?>
            <tr class="<?php echo 'notification_'.$value->id; ?>">
                <td>
                    <?php echo $value->content; ?>
                </td>
                <td class="dashboard__messeges_notification__table_link_td" >
                    <?php echo CHtml::ajaxLink(
                        "dismiss",
                        yii()->createUrl('auctions/orangelots-auctions/dismiss'),
                        array(
                            'type' => 'POST',
                            'data' => array(
                                'Dismiss[id]' => $value->id,
                            ),
                            'success' => "function(data){
                                if(data == 'ok'){
                                    $('tr.notification_".$value->id."').remove();
                                    if ($('.dashboard__messeges_notification__table tr').size() == 0){
                                        $('.dashboard__messeges_notification__table tbody').append('No more notifications for you')
                                    }
                                }
                        }",
                        )
                    );
                    ?>
                </td>
            </tr>
                    <?php endif; ?>
            <?php endforeach; ?>
            <?php else:?>
                No more notifications for you
            <?php endif;?>
            </tbody>
        </table>
    </div>
</div>



