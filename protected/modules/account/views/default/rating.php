<?php

$this->widget('application.extensions.dzraty.DzRaty', array(
        'name' => 'rating_'.$user_id,
        'value' => 0,
        'options' => array(
            'number' => 10,
            'click' => "js:function(score, evt){
									$.ajax({
										url:'" . Yii::app()->createUrl('account/default/SetRating/', array('id' => $user_id)) . "',
										type:'POST',
										data:{
											rate:score,
											rateUserID:'" . Yii::app()->user->id . "',
										},
										success:function(data){

										    if(data == 'success'){
										        var id_element = evt.toElement.parentElement.id;
										        if (id_element != ''){
										            id_element = '#'+ id_element;
										            $(id_element).parent('td').html('You rate this user');
										        } else {
										                alert ('This is some error!');
										        }
										    }
										}
									});
								}",
        ),
    ));


?>