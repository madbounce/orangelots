<div class="popup__container" id="subscribe">
	<div class="popup__bg"></div>
	<div class="popup_inner">
		<div class="popup__content">
			<a href="" class="popup_close">X</a>

			<div class="popup_body clearfix">
                <?php $form = $this->beginWidget(
                    'CActiveForm',
                    array(
                        'id' => 'orangelots-feedback',
                        'enableAjaxValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                            'validateOnChange' => true,
                            'validateOnType' => true,
                        ),
                        'action' => Yii::app()->createUrl('account/default/rate-user'),
                    )); ?>

                <p class="singup_form_row">
                <?php echo $form->labelEx($model, 'communication'); ?>
                <?php echo $form->radioButtonList($model, 'communication', $rate, array('separator'=>' ', 'labelOptions'=>array('style'=>'display:inline')))?>
                <?php echo $form->error($model, 'communication'); ?>
                </p>
                <p class="singup_form_row">
                <?php echo $form->labelEx($model, 'billing'); ?>
                <?php echo $form->radioButtonList($model, 'billing', $rate, array('separator'=>' ', 'labelOptions'=>array('style'=>'display:inline')))?>
                <?php echo $form->error($model, 'billing'); ?>
                </p>
                <p class="singup_form_row">
                <?php echo $form->labelEx($model, 'shipping'); ?>
                <?php echo $form->radioButtonList($model, 'shipping', $rate, array('separator'=>' ', 'labelOptions'=>array('style'=>'display:inline')))?>
                <?php echo $form->error($model, 'shipping'); ?>
                </p>
                <p class="singup_form_row">
                <?php echo $form->labelEx($model, 'message'); ?>
                <?php echo $form->textArea($model, 'message',array('style'=>'width: 343px; height: 61px;'))?>
                <?php echo $form->error($model, 'message',array('style'=>'margin-left: 170px;')); ?>
                </p>
                <input type="hidden" name="OrangelotsFeedback[rate_user_id]" value="<?php echo $rate_user_id?>">
                <?php echo CHtml::submitButton('Leave Feedback', array('id' => 'sbm_btn', 'class' => 'btn btn_orange pull-right'))?>

                <?php $this->endWidget(); ?>
			</div>

		</div>
	</div>
</div>
