<?php
$this->breadcrumbs = array(
	'Realties' => array('index'),
	$model->name,
);

$this->menu = array(
	array('label' => 'List Realty', 'url' => array('index')),
	array('label' => 'Create Realty', 'url' => array('create')),
	array('label' => 'Update Realty', 'url' => array('update', 'id' => $model->id)),
	array('label' => 'Delete Realty', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
	array('label' => 'Manage Realty', 'url' => array('admin')),
);
?>

<h1>View Realty #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data' => $model,
	'attributes' => array(
		'id',
		'category_id',
		'name',
		'title',
		'alias',
		'description',
		'timeCreate',
	),
)); ?>
