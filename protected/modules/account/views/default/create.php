<?php
/**
 * Шаблон формы добавления новости
 * @author maxshurko@gmail.com
 */
?>

<h1>Добавить программу</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>