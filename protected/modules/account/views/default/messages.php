<?php
/**
 * Шаблон списка в панеле управления
 * @author fen0man
 */
?>

<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href="<?php echo yii()->createUrl("site/index")?>"></a></li>
            <li><a href="">Messages</a></li>
        </ul>
    </div>
</div>

<div class="dashboard__messeges_notification">
    <div class="main_container">
        <div class="messeges_sort__container">
        <?
        echo CHtml::radioButtonList('type',$reading,array(
                '0'=>'New Messages Only',
                '1'=>'Archive',
            ),
            array('separator'=>' ', 'labelOptions'=>array('style'=>'display:inline'),
                'onclick' => "
                var id = $(this).val();
                var url = '".Yii::app()->createUrl('account/default/select-messages/')."';
                var res = url+'/'+id;
                $.ajax({
										url:res,
										type:'POST',
										data:{

										},
										success:function(data){
                                            var obj = JSON.parse(data);
                                            $('div.breadcrumb_row.clearfix').parent().html(obj.html);
										}
									});",
            )
        );
        ?>
        </div>
        <table class="dashboard__messeges_notification__table" cellpading="0" cellspacing="0">
            <tbody>
            <?php if (!empty($notifications)):?>
            <?php foreach($notifications AS $key => $value):?>
                <?php if($value->message_id == '4'): ?>
                    <tr class="<?php echo 'notification_'.$value->id; ?>">
                        <td>
                            <?php echo $value->content; ?>
                        </td>
                        <?php if ($reading != 1): ?>
                        <td class="dashboard__messeges_notification__table_link_td" >
                            <?php echo CHtml::ajaxLink(
                                "dismiss",
                                yii()->createUrl('auctions/orangelots-auctions/dismiss'),
                                array(
                                    'type' => 'POST',
                                    'data' => array(
                                        'Dismiss[id]' => $value->id,
                                    ),
                                    'success' => "function(data){
                                if(data == 'ok'){
                                    $('tr.notification_".$value->id."').remove();
                                    if ($('.dashboard__messeges_notification__table tr').size() == 0){
                                        $('.dashboard__messeges_notification__table tbody').append('No more notifications for you')
                                    }
                                }
                        }",
                                )
                            );
                            ?>
                        </td>
                        <?php endif; ?>

                    </tr>
                <?php else: ?>
            <tr class="<?php echo 'notification_'.$value->id; ?>">
                <td>
                    <?php echo $value->content; ?>
                </td>
                        <?php if ($reading != 1): ?>
                <td class="dashboard__messeges_notification__table_link_td" >
                    <?php echo CHtml::ajaxLink(
                        "dismiss",
                        yii()->createUrl('auctions/orangelots-auctions/dismiss'),
                        array(
                            'type' => 'POST',
                            'data' => array(
                                'Dismiss[id]' => $value->id,
                            ),
                            'success' => "function(data){
                                if(data == 'ok'){
                                    $('tr.notification_".$value->id."').remove();
                                    if ($('.dashboard__messeges_notification__table tr').size() == 0){
                                        $('.dashboard__messeges_notification__table tbody').append('No more notifications for you')
                                    }
                                }
                        }",
                        )
                    );
                    ?>
                </td>
                            <?php endif; ?>
            </tr>
                    <?php endif; ?>
            <?php endforeach; ?>
            <?php else:?>
                No more notifications for you
            <?php endif;?>
            </tbody>
        </table>
    </div>
</div>



