<?php $category = ActivityCategory::model()->findByPk($category_id); ?>
<?php if (!empty($category)): ?>

	<?php $groups = $category->subCategoryGroups; ?>
	<?php foreach ($groups as $group): ?>

		<?php $attributes = ActivityAttribute::model()->findAll(array('condition' => "group_id={$group->group->id}", 'order' => 'rank ASC')); ?>

		<?php if (!empty($attributes)): ?>
			<div class="mt_20">
				<div class="tt_u f_11 bold"><a class="c_cc3300 js-group-title" href="#"><?= $group->group->title ?></a>
				</div>
				<div class="mt_15"></div>
			</div>

			<div>
				<?php foreach ($attributes as $attribute): ?>
					<div class="tb f_14 w_full mt_20">

						<?php if ($attribute->type != ActivityAttribute::TYPE_CHECKBOX): ?>
							<label class="label w_140 text_right" for="Activity_attributes_<?= $attribute->id ?>">
								<strong><?= ($attribute->is_required) ? '<span class="c_cc3300">*</span>' : '' ?> <?= $attribute->title ?>
									:</strong>
							</label>
						<?php endif; ?>

						<div class="tb_cell">
							<?php if (in_array($attribute->type, array(ActivityAttribute::TYPE_INPUT_STRING, ActivityAttribute::TYPE_INPUT_INTEGER, ActivityAttribute::TYPE_INPUT_FLOAT))): ?>
								<input type="text" id="Activity_attributes_<?= $attribute->id ?>"
								       name="Activity[post_attributes][<?= $attribute->id ?>]"
								       value="<?= (isset($dataAttributes[$attribute->id])) ? $dataAttributes[$attribute->id] : '' ?>"
								       class="field w_360" <?= ($attribute->is_required) ? 'data-rules="required:true"' : '' ?>>
							<?php endif; ?>

							<?php if ($attribute->type == ActivityAttribute::TYPE_CHECKBOX): ?>
								<label style="margin-left: 150px;">
									<input type="checkbox" id="Activity_attributes_<?= $attribute->id ?>"
									       name="Activity[post_attributes][<?= $attribute->id ?>]"
									       value="1" <?= (isset($dataAttributes[$attribute->id])) ? 'checked="checked"' : '' ?>>
									<?= $attribute->title ?>
								</label>
							<?php endif; ?>

							<?php if ($attribute->type == ActivityAttribute::TYPE_SELECT): ?>

								<?php $options = $attribute->getCacheOptions(); ?>
								<select id="Activity_attributes_<?= $attribute->id ?>"
								        name="Activity[post_attributes][<?= $attribute->id ?>]"
								        class="field w_360" <?= ($attribute->is_required) ? 'data-rules="required:true"' : '' ?>>
									<option value="">-</option>
									<?php foreach ($options as $option): ?>
										<option <?= (isset($dataAttributes[$attribute->id]) && $dataAttributes[$attribute->id] == $option->id) ? 'selected="selected"' : '' ?>
											value="<?= $option->id ?>"><?= $option->value ?></option>
									<?php endforeach; ?>
								</select>
							<?php endif; ?>
							<div class="field_error_label none w_360"></div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

	<?php endforeach; ?>

<?php endif; ?>