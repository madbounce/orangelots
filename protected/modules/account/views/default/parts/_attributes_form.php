<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 26.05.14
 * Time: 13:06
 */
?>
<?php $form = $this->beginWidget(
	'bootstrap.widgets.TbActiveForm',
	array(
		'id' => 'realty-form',
		'type' => 'horizontal',
		'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
		),
	)
); ?>

<?php echo $form->dropDownListRow(
	$model,
	'category_id',
	RealtyCategory::model()->getAll(),
	array('empty' => '-', 'class' => 'span3',)
); ?>

	<div id="attribute_container">
		<?php echo $this->renderPartial(
			'parts/_attributes',
			array(
				'dataAttributes' => $model->getDataAttributes(),
				'category_id' => $model->category_id,
				'errors' => $model->errors,
			)
		); ?>
	</div>
	<script type="text/javascript">
		$('#Realty_category_id').change(function () {
			$.post(baseUrl + '/?r=realty/realty/attributes', {'category_id': $(this).val()}, function (response) {
				$('#attribute_container').html(response);
			}, 'text')
		});
	</script>


	<div class="form-actions">
		<?php $this->widget(
			'bootstrap.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'htmlOptions' => ['name' => 'yt0'],
				'type' => 'success',
				'label' => 'Сохранить'
			)
		); ?>

		<?php $this->widget(
			'bootstrap.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'htmlOptions' => ['name' => 'apply', 'class' => 'btn-apply'],
				'label' => 'Применить'
			)
		); ?>

		<?php $this->widget(
			'bootstrap.widgets.TbButton',
			array(
				'buttonType' => 'submit',
				'htmlOptions' => ['name' => 'yt1', 'class' => 'btn-cansel'],
				'label' => 'Отмена'
			)
		); ?>
	</div>
<?php $this->endWidget(); ?>