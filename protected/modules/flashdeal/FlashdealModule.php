<?php


class FlashdealModule extends CWebModule
{
	public $defaultController = "Default";

	public function init()
	{
		$this->setImport(
			array(
				'flashdeal.models.*',
				'flashdeal.components.*',
			)
		);
	}

	/*public function beforeControllerAction($controller, $action)
	{
		if (parent::beforeControllerAction($controller, $action)) {
			return true;
		} else {
			return false;
		}
	}*/
}