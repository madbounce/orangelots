<?php

class DefaultController extends Controller
{

    public $layout = 'application.views.layouts.admin';
    public $auctions = null;

	public function filters()
	{
		return array(
			'rights',
		);
	}
    public function actionCreate()
    {
        if ((isset($_POST['OrangelotsAuctions'])) && (isset($_POST['OrangelotsAuctions']['id']))) {
            $id = (int)$_POST['OrangelotsAuctions']['id'];
            yii()->session['auction_id'] = $id;
        } else {
            $this->clearNotActiveAuctions(yii()->user->id);
            $my_model = new OrangelotsAuctions('deal_create');
            $my_model->user_id = yii()->user->id;
            $my_model->units_in_lot = '1';
            if (!user()->isAdmin()) {
                $info = '';
                $usr = OrangelotsUsers::model()->findByPk(yii()->user->id);
                if (!empty($usr)) {
                    /*$info = 'Name:' . $usr->firstname . ' ' . $usr->lastname . PHP_EOL . 'Phone: ' . $usr->phone . PHP_EOL . 'Email: ' . $usr->email . PHP_EOL . 'Company: ' . $usr->company . PHP_EOL . 'Address: ' . $usr->address_1 . ' ' . $usr->address_2 . PHP_EOL
                        . 'City: ' . $usr->city . PHP_EOL . 'State: ' . $usr->state . PHP_EOL . 'Zipcode: ' . $usr->zipcode . PHP_EOL;*/
                    $info = $usr->company . PHP_EOL . $usr->address_1 . ' '  . $usr->address_2 . ' ' . OrangelotsStates::getStateLabel($usr->state).  ' '  . $usr->zipcode.', ' . 'USA';
                    $my_model->vendors_information = $info;
                }
            }
            if ($my_model->save()) {
                $id = $my_model->id;
                yii()->session['auction_id'] = $id;
            };
        }

        $this->forward_yii(yii()->createUrl('auctions/orangelots-auctions/update/', array('id' => $id)));
    }

    public function actionUpdate()
    {
        $id = $_REQUEST['id'];
        if (isset($this->auctions)) {
            $id = $this->auctions;
            $this->auctions = null;
        }
        if (user()->isAdmin()) {
            $this->layout = 'application.views.layouts.admin';
        } else {
            $this->layout = 'application.views.layouts.main';
        }
        $model = $this->loadModel($id);
        $itm = Items::model()->findAllByAttributes(array('auction_deal_id'=>$model->id));
        $model->scenario = 'create_flash_deal';
        /*
         * Проверка на доступ к закрытому аукциону
         *
         * */
        if (!user()->isAdmin()) {
            if ($model->user_id != yii()->user->id) {
                $this->redirect(createUrl('site/index'));
            } else {
                if ($model->status == 2) {
                    $this->redirect(createUrl('site/index'));
                }
            }
        }


        /*if($model->is_flashdeal == 0){
            $this->redirect(createUrl('auctions/orangelots-auctions/update', array('id' => $model->id)));
        }*/
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if(isset(yii()->session['copy_auction'])){
            $model->attributes = yii()->session['copy_auction'];
            yii()->session['copy_auction'] = null;
        }
        if (isset($_POST['OrangelotsAuctions'])) {
            $model->attributes = $_POST['OrangelotsAuctions'];
            $model->au_time ='12:00';
            $model->au_am_pm = 'AM';
            $start_date = $model->au_date . ' ' . $model->au_time . ' ' . $model->au_am_pm;
            $model->start_date_unix = CDateTimeParser::parse($start_date, 'yyyy/MM/dd HH:mm a');
            $model->start_date = yii()->dateFormatter->format('yyyy/MM/dd HH:mm', $model->start_date_unix);

            $model->end_date_unix = $model->start_date_unix+(24*60*60);
            $model->end_date = yii()->dateFormatter->format('yyyy/MM/dd HH:mm', $model->end_date_unix);
            $model->au_date_end = yii()->dateFormatter->format('yyyy-MM-dd', $model->end_date_unix);
            $model->au_time_end = yii()->dateFormatter->format('HH:mm a', $model->end_date_unix);
            $model->au_am_pm_end = 'AM';

            $model->is_flashdeal = 1;
            if (($_POST['OrangelotsAuctions']['usr'] != 'empty') && (isset($_POST['OrangelotsAuctions']['usr'])) && (!empty($_POST['OrangelotsAuctions']['usr'])) && (is_numeric ($_POST['OrangelotsAuctions']['usr']))){
                $user_aviable = OrangelotsUsers::model()->findByPk($_POST['OrangelotsAuctions']['usr']);
                if ((isset($user_aviable))&&(!empty($user_aviable)))
                    $model->user_id = $_POST['OrangelotsAuctions']['usr'];
            }

            if ($model->shipping_terms == "3") {
                $model->scenario = 'shipping_terms_other';
            } else {
                if ($model->delivery == "5") {
                    $model->scenario = 'delivery_other';
                } else {
                    if ($model->delivery == "5" && $model->shipping_terms == "3") {
                        $model->scenario = 'delivery_shipping_terms_other';
                    }
                }
            }
            $model->status = 1;
            $items_errors = false;
            if(isset($_POST['items'])){
                if(!empty($_POST['items'])){
                    foreach ($_POST['items'] as $k=>$v) {
                        $frm = new ItemsForm();
                        $frm->attributes = $v;
                        if(!$frm->validate()){
                            $_POST['items'][$k]['errors'] = $frm->getErrors();
                            $items_errors = true;
                        }
                    }
                }
            }
            if($items_errors){
                $model->item_validate = false;
            }

            if ($model->validate()) {
                $model->active = '1';
            }

            if ($model->save()) {
                if (user()->isAdmin()){
                    if(isset($_POST['items']))
                        $this->saveItems($model->id, $_POST['items']);
                    $this->redirect(createUrl('flashdeal/default/admin'));
                } else {
                    $this->redirect(createUrl('auctions/orangelots-auctions/sell-deal'));
                }
            }
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'itm' => $itm,
            )
        );
    }
    public function clearNotActiveAuctions($id)
    {
        OrangelotsAuctions::model()->deleteAllByAttributes(array('user_id' => $id, 'active' => '0'));
    }
    public function forward_yii($route, $exit = false)
    {
        $routeArr = explode("/", $route);
        if (isset($routeArr[(count($routeArr) - 1)])) {
            $this->auctions = $routeArr[(count($routeArr) - 1)];
        }

        parent::forward($routeArr[(count($routeArr) - 2)], $exit);

    }

    public function loadModel($id)
    {
        $model = OrangelotsAuctions::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    public function actionAdmin()
    {
        $this->layout = 'application.views.layouts.admin';
        $model = new OrangelotsAuctions('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['OrangelotsAuctions'])) {
            $model->attributes = $_GET['OrangelotsAuctions'];
            $model->owner_email = $_GET['OrangelotsAuctions']['owner_email'];
            $model->on_main = $_GET['OrangelotsAuctions']['on_main'];
        }

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
    }

    public function saveItems($id, $r){
        if(isset($id) && (!empty($r))){
            foreach($r as $k=>$v){
                if(array_key_exists ('recordid',$v))
                    $frm = Items::model()->findByPk($v['recordid']);
                else
                    $frm = new Items();

                $frm->attributes = $v;
                $frm->auction_deal_id = $id;
                $frm->save();
            }
        }
    }





}
