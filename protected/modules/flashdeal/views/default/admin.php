<?php
/* @var $this OrangelotsAuctionsController */
/* @var $model OrangelotsAuctions */

$this->breadcrumbs=array(
	'Orangelots Auctions'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List OrangelotsAuctions', 'url'=>array('index')),
	array('label'=>'Create OrangelotsAuctions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#orangelots-auctions-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Orangelots Auctions</h1>

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orangelots-auctions-grid',
	'dataProvider'=>$model->search_flashdeal(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name' => 'id',
            'htmlOptions'=>array('width'=>'55px'),
        ),
		'name',
        array(
            'name' => 'manufacture',
            'type' => 'raw',
            'header' => 'Manufacture',
            'value' => 'OrangelotsManufacture::model()->findByPk($data->manufacture)->name',
        ),
        array(
            'name' => 'mvrp',
            'value' => '"$".$data->mvrp',
        ),
        array(
            'name' => 'units_in_lot',
            'htmlOptions'=>array('width'=>'55px'),
        ),
        array(
            'name' => 'buy_now_price',
            'value' => '"$".$data->buy_now_price',
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'header' => 'Status',
            'value' => '$data->status==1?Open:Close',
            'filter' => array('1'=>'Open', '2'=>'Close'),
        ),
        array(
            'name' => 'owner_email',
            'type' => 'raw',
            'header' => 'Owner Email',
            'value' => '$data->getOwnerEmail($data->user_id)',
        ),
        array(
            'name' => 'on_main',
            'type' => 'raw',
            'header' => 'Show On Main',
            'value' => '$data->set_on_main==1?Show:Hide',
            'filter' => array('1'=>'Show', '0'=>'Hide'),
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}',
		),
	),
)); ?>
