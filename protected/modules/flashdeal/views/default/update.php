<?php
/* @var $this OrangelotsAuctionsController */
/* @var $model OrangelotsAuctions */
?>
<?php if (OrangelotsAuctions::hiddenId(Yii::app()->request->requestUri)): ?>
    <div class="breadcrumb_row clearfix">
        <div class="main_container">

            <?php
            $this->widget(
                'zii.widgets.CBreadcrumbs',
                array(
                    'links' => array(
                        'Create Flash Deal',
                    ),
                    'separator' => '',
                    'tagName' => 'ul',
                    'htmlOptions' => array('class' => ''),
                    'inactiveLinkTemplate' => '<li><a href="">{label}</a></li>',
                    'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                            "site/index"
                        ) . '"></a></li>',
                )
            );
            ?>
        </div>
    </div>
<?php else: ?>
    <div class="breadcrumb_row clearfix">
        <div class="main_container">

            <?php
            $this->widget(
                'zii.widgets.CBreadcrumbs',
                array(
                    'links' => array(
                        'Update Deal',
                    ),
                    'separator' => '',
                    'tagName' => 'ul',
                    'htmlOptions' => array('class' => ''),
                    'inactiveLinkTemplate' => '<li><a href="">{label}</a></li>',
                    'activeLinkTemplate' => '<li><a class="breadcrumb_row__main_link" href="' . yii()->createUrl(
                            "site/index"
                        ) . '"></a></li>',
                )
            );
            ?>
        </div>
    </div>
    <?php endif; ?>
<?php $this->renderPartial('_form', array('model'=>$model, 'itm'=>$itm)); ?>