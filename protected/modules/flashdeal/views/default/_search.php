<?php
/* @var $this OrangelotsAuctionsController */
/* @var $model OrangelotsAuctions */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'manufacture'); ?>
		<?php echo $form->textField($model,'manufacture',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mvrp'); ?>
		<?php echo $form->textField($model,'mvrp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'units_in_lot'); ?>
		<?php echo $form->textField($model,'units_in_lot'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'buy_now_price'); ?>
		<?php echo $form->textField($model,'buy_now_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'min_bid'); ?>
		<?php echo $form->textField($model,'min_bid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'min_unit'); ?>
		<?php echo $form->textField($model,'min_unit'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_date'); ?>
		<?php echo $form->textField($model,'start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_date'); ?>
		<?php echo $form->textField($model,'end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'condition'); ?>
		<?php echo $form->textField($model,'condition',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'color'); ?>
		<?php echo $form->textField($model,'color',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dimensions'); ?>
		<?php echo $form->textField($model,'dimensions',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'weight'); ?>
		<?php echo $form->textField($model,'weight'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'terms'); ?>
		<?php echo $form->textArea($model,'terms',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shipping_terms'); ?>
		<?php echo $form->textField($model,'shipping_terms'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shipping_terms_other'); ?>
		<?php echo $form->textArea($model,'shipping_terms_other',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'delivery'); ?>
		<?php echo $form->textField($model,'delivery'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'delivery_other'); ?>
		<?php echo $form->textArea($model,'delivery_other',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'returns_warranty'); ?>
		<?php echo $form->textArea($model,'returns_warranty',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'vendors_information'); ?>
		<?php echo $form->textArea($model,'vendors_information',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'category_id'); ?>
		<?php echo $form->textField($model,'category_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->