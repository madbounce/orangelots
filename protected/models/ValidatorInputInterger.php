<?php
/**
 * Класс ValidatorInputInterger
 * @author dorosh_2009@meta.ua
 * @version 7.0.0
 * 
 * Модель валидации атрибута - тип integer
 */
class ValidatorInputInterger extends Validator {
		
	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules()
	{
		$rules = array();
		
		if ($this->is_required) {
			$rules[] = array(
				'value', 
				'required',
				'message' => $this->attribute . ' обязательное для заполнения',
				
			);
		}

		$rules[] = array(
			'value', 
			'numerical',
			'integerOnly' => true,
			'message' => $this->attribute . ' должно быть целым числом',
		);
		
		return $rules;
	}
	
}
