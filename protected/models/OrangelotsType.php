<?php

/**
 * This is the model class for table "module_orangelots_type".
 *
 * The followings are the available columns in table 'module_orangelots_type':
 * @property integer $id
 * @property integer $title
 * @property integer $alias
 * @property integer $category_id
 */
class OrangelotsType extends CActiveRecord
{
	public function tableName()
	{
		return 'module_orangelots_type';
	}

	public function rules()
	{
		return array(
			array('title', 'required'),
			array('alias', 'unique'),
			array('category_id', 'numerical', 'integerOnly' => true),
			array('id, title, alias, category_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'realties' => array(self::HAS_MANY, 'Orangelots', 'type_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'alias' => 'Alias',
			'category_id' => 'Category',
		);
	}


	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('title', $this->title);
		$criteria->compare('alias', $this->alias);
		$criteria->compare('category_id', $this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function getAll()
	{
		$model = new self;
		$model->unsetAttributes();
		return CHtml::listData($model->search()->getData(), 'id', 'title');
	}

	protected function beforeValidate()
	{
		$this->buildAlias();
		return parent::beforeValidate();
	}

	private function buildAlias()
	{
		if (empty($this->alias) && !empty($this->title)) {
			$this->alias = TextHelper::urlSafe($this->title);
		}
	}

	public function getTypeIdByAlias($alias)
	{
		$model = self::findByAttributes(array('alias' => $alias));
		return $model->id;
	}
}
