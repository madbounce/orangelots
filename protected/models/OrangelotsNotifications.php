<?php

/**
 * This is the model class for table "orangelots_notifications".
 *
 * The followings are the available columns in table 'orangelots_notifications':
 * @property integer $id
 * @property integer $user_id
 * @property integer $message_id
 * @property integer $auction_id
 * @property string $content
 * @property integer $reading
 */
class OrangelotsNotifications extends CActiveRecord
{
    const NEW_BID = 1;
    const OVERBID = 2;
    const YOU_SELECTED_AS_WINNER = 3;
    const YOU_AUCTION_IS_OVER = 4;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orangelots_notifications';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, message_id, auction_id, reading', 'numerical', 'integerOnly'=>true),
			array('content', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, message_id, auction_id, content, reading', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'message_id' => 'Message',
			'auction_id' => 'Auction',
			'content' => 'Content',
			'reading' => 'Reading',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('message_id',$this->message_id);
		$criteria->compare('auction_id',$this->auction_id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('reading',$this->reading);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrangelotsNotifications the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
