<?php

/**
 * This is the model class for table "orangelots_bidding_on".
 *
 * The followings are the available columns in table 'orangelots_bidding_on':
 * @property integer $id
 * @property integer $id_user
 * @property integer $id_auction
 * @property string $datetime
 * @property string $price_per_unit
 * @property integer $qty
 * @property integer $buy_now
 * @property string $summary_price
 */
class OrangelotsBiddingOn extends CActiveRecord
{
    public $auction_name;
    public $time;
    public $status;
    public $bidder_name;
    public $bidder_email;
    public $bidder_phone;
    public $actions;
    public $vendor;
    public $qty_in_lot;
    public $count_bids;
    public $current_bid;
    public $my_bid;
    public $my_bid_price;
    public $buy_now_price;
    public $bid_per_unit;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orangelots_bidding_on';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_user, id_auction, qty, id_owner', 'numerical', 'integerOnly'=>true),
			array('price_per_unit, summary_price', 'length', 'max'=>10),
			array('datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_user, id_auction, datetime, price_per_unit, qty, summary_price, id_owner', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'auction' => array(self::BELONGS_TO, 'OrangelotsAuctions', 'id_auction'),
            'winners' => array(self::HAS_ONE, 'OrangelotsWinners', 'bid_id'),
            'users' => array(self::BELONGS_TO, 'OrangelotsUsers', 'id_user'),
//            'saved' => array(self::BELONGS_TO, 'SaveForLater', 'id_user'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_user' => 'Id User',
			'id_auction' => 'Id Auction',
			'datetime' => 'Datetime',
			'price_per_unit' => 'Price Per Unit',
			'qty' => 'Qty',
			'summary_price' => 'Summary Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('id_auction',$this->id_auction);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('price_per_unit',$this->price_per_unit,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('summary_price',$this->summary_price,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function userAuctionsBids($userId){
        $criteria = new CDbCriteria();
        $criteria->addCondition('id_owner ='.$userId);
        $criteria->order = 'datetime ASC';

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrangelotsBiddingOn the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getWinnerLabel($data){
        $winner = OrangelotsWinners::model()->findByAttributes(array('user_id' => $data->id_user, 'auction_id' => $data->id_auction));
        if(!empty($winner)){
            if($winner->is_buy_now == 1){
                $text = 'Bought';
            }else{
                $text = 'Won';
            }
            $transaction = OrangelotsInvoices::model()->findByAttributes(array('auction_deal_id' => $data->id_auction, 'winner_id' => $data->id_user));
            if(!empty($transaction)){
                return CHtml::link($text, createUrl('user/sell/confirmation', array('transaction' => $transaction->transaction_id)), array('class' => 'orange_link'));
            }else{
                return $text;
            }
        } else {
            return 'Placed bid';
        }

    }
    public function BoldMyBid($my, $current){
        if ($my== null) {
            return '';
        }
        elseif ($my<$current)
            return '<b>'.$my.'$</b>';
        else
            return $my.'$';
    }
}
