<?php
/**
 * Класс ValidatorInputString
 * @author dorosh_2009@meta.ua
 * @version 7.0.0
 * 
 * Модель валидации атрибута - тип string
 */
class ValidatorInputString extends Validator {
	
	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules()
	{
		$rules = array();
		
		if ($this->is_required) {
			$rules[] = array(
				'value', 
				'required',
				'message' => $this->attribute . ' обязательное для заполнения',
			);
		}

		$rules[] = array(
			'value', 
			'type',
			'type' => 'string',
			'message' => $this->attribute . ' должно быть строкой',
		);
		
		$rules[] = array(
			'value', 
			'length',
			'max' => 255,
			'message' => $this->attribute . ' должно быть длиной до 255 символов',
		);
		
		return $rules;
	}
	
}
