<?php
/**
 * Класс Orangelots
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 *
 * Модель данных категорий статей
 * Описание атрибутов 'module_orangelots_category':
 * @property integer $id                        -- порядковый номер
 * @property integer $category_id                -- категория
 * @property integer $rank                        -- позиция
 * @property string $title                        -- название
 */
class OrangelotsGroupAttribute extends CActiveRecord
{
	/**
	 * Получить экземпляр класса
	 * @return ArticleCategory
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Ассоциированная таблица
	 * @return string
	 */
	public function tableName()
	{
		return 'module_orangelots_group_attribute';
	}

	/**
	 * Выполняем ряд действий перед валидацией модели
	 * @return boolean -- результат выполнения операции
	 */
	protected function beforeValidate()
	{
		return parent::beforeValidate();
	}


	/**
	 * Получить правила валидации
	 * @return array
	 */
	public function rules()
	{
		return array(
			array(
				'title,category_id',
				'required',
			),
			array(
				'rank, category_id',
				'numerical',
				'integerOnly' => true,
			),
			array(
				'title',
				'length',
				'max' => 255,
			),
			array(
				'id',
				'safe',
				'on' => 'search',
			),
		);
	}

	/**
	 * Взаимосвязи
	 * @return array
	 */
	public function relations()
	{
		return array(
			'relAttributes' => array(self::HAS_MANY, 'OrangelotsAttribute', 'group_id', 'order' => 'rank ASC'),
			'category' => array(self::BELONGS_TO, 'OrangelotsCategory', 'category_id', 'together' => true, 'order' => 'rank ASC'),
		);
	}

	/**
	 * Названия атрибутов
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'rank' => 'Rank',
			'title' => 'Name',
		);
	}

	public function beforeSave()
	{
		return parent::beforeSave();
	}

	/**
	 * Выполняем ряд обязательных действий после сохранения модели
	 * @return boolean -- результат выполнения действия
	 */
	protected function afterSave()
	{
		return parent::afterSave();
	}

	/**
	 * Поиск
	 * @return CActiveDataProvider
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('rank', $this->rank);
		$criteria->compare('title', $this->title, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 50,
			),
		));
	}
}