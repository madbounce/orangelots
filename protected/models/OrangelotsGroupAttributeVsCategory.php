<?php
/**
 * Класс Orangelots
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 *
 * @property integer $category_id                        -- id категории
 * @property integer $group_id                            -- id группы
 */
class OrangelotsGroupAttributeVsCategory extends CActiveRecord
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'module_orangelots_group_attribute_vs_category';
	}

	public function getPrimaryKey()
	{
		return array('category_id', 'group_id');
	}

	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules()
	{
		return array(
			array(
				'category_id, group_id',
				'required',
			),
		);
	}

	/**
	 * Взаимосвязи
	 * @return array
	 */
	public function relations()
	{
		return array(
			'category' => array(self::BELONGS_TO, 'OrangelotsCategory', 'category_id'),
			'group' => array(self::BELONGS_TO, 'OrangelotsGroupAttribute', 'group_id', 'together' => true),
		);
	}
}