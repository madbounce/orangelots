<?php
/**
 * Класс Orangelots
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 *
 * Модель данных категорий статей
 * Описание атрибутов 'module_orangelots_category':
 * @property integer $id                        -- порядковый номер
 * @property integer $is_active                    -- активность
 * @property integer $rank                        -- позиция
 * @property string $alias                        -- алиас
 * @property string $title                        -- название
 */
class OrangelotsCategory extends CActiveRecord
{
	/**
	 * URL категории
	 */
	const URL_PATTERN = '/realty/%s/';

	/**
	 * Главное фото
	 */
	public $main_photo;

	public $post_groups = array();

	/**
	 * Выбранная категория
	 * @var object
	 */
	static public $currentVolume;

	/**
	 * Получить экземпляр класса
	 * @return ArticleCategory
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Ассоциированная таблица
	 * @return string
	 */
	public function tableName()
	{
		return 'module_orangelots_category';
	}

	/**
	 * Выполняем ряд действий перед валидацией модели
	 * @return boolean -- результат выполнения операции
	 */
	protected function beforeValidate()
	{
		//создаем алиас к тайтлу
		$this->buildAlias();
		return parent::beforeValidate();
	}

	/**
	 * Создаем алиас к тайтлу
	 */
	private function buildAlias()
	{
		if (empty($this->alias) && !empty($this->title)) {
			$this->alias = TextHelper::urlSafe($this->title);
		}
	}

	/**
	 * Получить правила валидации
	 * @return array
	 */
	public function rules()
	{
		return array(
			array(
				'title',
				'required',
			),
			array(
				'is_active, rank, parent_id',
				'numerical',
				'integerOnly' => true,
			),
			array(
				'alias, title, description, seo_description, seo_keywords',
				'length',
				'max' => 255,
			),
			array(
				'post_groups',
				'safe',
			),
			array(
				'id, is_active, rank, alias, title',
				'safe',
				'on' => 'search',
			),
			array(
				'main_photo',
				'file',
				'types' => 'jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG',
				'allowEmpty' => true,
			),
		);
	}

	/**
	 * Взаимосвязи
	 * @return array
	 */
	public function relations()
	{
		return array(
			'realties' => array(self::HAS_MANY, 'Orangelots', 'category_id', 'together' => true),
			'parent' => array(self::BELONGS_TO, 'OrangelotsCategory', 'parent_id', 'together' => true),
			'childs' => array(self::HAS_MANY, 'OrangelotsCategory', 'parent_id'),
			'subCategoryGroups' => array(self::HAS_MANY, 'OrangelotsGroupAttributeVsCategory', 'category_id', 'order' => 'group.rank ASC', 'with' => array('group')),
			'categoryGroups' => array(self::HAS_MANY, 'OrangelotsGroupAttribute', 'category_id', 'order' => 'rank ASC'),
		);
	}

	/**
	 * Названия атрибутов
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'is_active' => 'Is Active',
			'parent_id' => 'Category',
			'rank' => 'Rank',
			'alias' => 'Alias',
			'title' => 'Title',
			'description' => 'Description',
			'main_photo' => 'Image',
			'realty_count' => 'realty count',
			'realty_count_active' => 'realty count active',
			'seo_keywords' => 'SEO Keys',
			'seo_description' => 'SEO Description',
		);
	}

	public function beforeSave()
	{
		if (empty($this->parent_id)) {
			$this->parent_id = null;
		}

		if (empty($this->main_photo) == false) {
			$this->image_ext = $this->main_photo->getExtensionName();
		}

		return parent::beforeSave();
	}

	/**
	 * Выполняем ряд обязательных действий после сохранения модели
	 * @return boolean -- результат выполнения действия
	 */
	protected function afterSave()
	{
		if (!empty($this->main_photo)) {
			Yii::app()->file->set($this->image_directory)->delete();
			$this->main_photo->saveAs($this->image_directory . '/origin.' . $this->image_ext);
		}

		if ($this->scenario == 'update') {
            OrangelotsGroupAttributeVsCategory::model()->deleteAllByAttributes(array('category_id' => $this->id));
		}

		foreach ($this->post_groups as $group_id) {
			$item = new OrangelotsGroupAttributeVsCategory;
			$item->category_id = $this->id;
			$item->group_id = $group_id;
			$item->save();
		}

		return parent::afterSave();
	}

	/**
	 * @param $attribute
	 * @param $value
	 */
	public function mySaveCounters($attribute, $value)
	{

		if ($value < 0 && $this->$attribute < abs($value)) {
			return false;
		}

		$this->saveCounters(array($attribute => $value));

		if ($this->parent) {
			$this->parent->mySaveCounters($attribute, $value);
		}
	}

	private function _deleteImage()
	{
		if ($this->image_ext) {
			Yii::app()->file->set($this->image_directory)->delete();
			$this->image_ext = '';
		}
	}

	public function getImage_directory($mkdir = false)
	{
//		$directory = Yii::app()->basePath . '/../photos/programs-category/' . $this->public_id;
		$directory = Yii::app()->basePath . '/../photos/orangelots-category/' . $this->public_id;
//        dump($directory);die;
		if ($mkdir == false && file_exists($directory) == false) {
			mkdir($directory);
			chmod($directory, 0777);
		}

		return $directory;
	}

	public function getThumb($width = null, $height = null, $mode = 'origin')
	{
		$dir = $this->getImage_directory(true) . '/';
		$originFile = $dir . 'origin.' . $this->image_ext;

		if (!is_file($originFile)) {
			return "http://www.placehold.it/{$width}x{$height}/EFEFEF/AAAAAA";
		}

		if ($mode == 'origin') {
			return '/photos/programs-category/' . $this->public_id . '/origin.' . $this->image_ext;
		}

		$fileName = $this->id . '_w' . $width . '_h' . $height . '.' . $this->image_ext;
		$filePath = $dir . $fileName;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
			}
		}

		return '/photos/programs-category/' . $this->public_id . '/' . $fileName;
	}

	public function getImage_preview_url()
	{
		return $this->getThumb(50, 50, 'crop');
	}

	public function getPublic_id()
	{
		return md5($this->id);
	}


	/**
	 * Поиск
	 * @return CActiveDataProvider
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('is_active', $this->is_active);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('rank', $this->rank);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('title', $this->title, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 50,
			),
		));
	}

	/**
	 * Получить список всех категорий новостей
	 * @return array
	 */
	public static function getAll()
	{
		return CHtml::listData(OrangelotsCategory::model()->findAll(), 'id', 'title');
	}


	/**
	 * Получить URL категории
	 * @return string
	 */
	public function getUrl($category = null)
	{
		if (empty($this->parent_id)) {
			return '/programs/' . $this->alias . '/';
		} else {
			$parent = empty($category) ? $this->parent : $category;
			return $this->parent->url . $this->alias . '/';
		}
	}

	public function getCheckedGroups()
	{
		if (Yii::app()->request->isPostRequest) {
			return $this->post_groups;
		} else {
			if ($this->isNewRecord) {
				return array();
			} else {
				$ids = array();
				foreach ($this->subCategoryGroups as $group) {
					$ids[] = $group->group_id;
				}

				return $ids;
			}
		}
	}

	public static function getAllCategory()
	{
		$model = new self;
		$model->unsetAttributes();
		return CHtml::listData($model->search()->getData(), 'id', 'title');
	}
}