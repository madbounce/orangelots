<?php

/**
 * This is the model class for table "orangelots_manufacture".
 *
 * The followings are the available columns in table 'orangelots_manufacture':
 * @property integer $id
 * @property string $name
 */
class OrangelotsManufacture extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orangelots_manufacture';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'numerical', 'integerOnly' => true),
			array('name,alias', 'length', 'max' => 255),
			array('name,alias', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, alias', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'alias' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('alias', $this->alias, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrangelotsManufacture the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public static function getList()
	{
		return self::model()->findAll();
	}

    public static function newRules()
    {
        $alias = OrangelotsManufacture::model()->findAll();
        $rules = '';
        $rule = '';
        if (!empty($alias)) {
            foreach ($alias AS $key => $value){
                if($key == 0)
                    $rules.=$value->alias;
                else
                    $rules.='|'.$value->alias;
            }
            if (!empty($rules))
                //$rule='<vendor:'.$rules.'>';
                return $rules;
        }

        /*return array(
            '<parent:new-furniture|used-furniture|orange-blossom-furniture|upcoming-deals-furniture|flash-deals-furniture>/<alias:\w+>/'.$rule=>'orangelots/orangelots-kind/view',
        );*/
    }
}
