<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $alias
 * @property string $img
 * @property integer $timeCreate
 * @property integer $group_attr_id
 *
 * The followings are the available model relations:
 * @property Realty[] $realties
 */
class Category extends CActiveRecord
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'category';
	}

	public function rules()
	{
		return array(
			array('title', 'required'),
			array('timeCreate, group_attr_id', 'numerical', 'integerOnly' => true),
			array('name, title, alias', 'length', 'max' => 255),
			array('alias', 'unique'),
			array('img', 'length', 'max' => 64),
			array('id, name, title, alias, img, timeCreate, group_attr_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'realties' => array(self::HAS_MANY, 'Realty', 'category_id'),
			'types' => array(self::HAS_MANY, 'RealtyType', 'category_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'title' => 'Title',
			'alias' => 'Alias',
			'img' => 'Img',
			'timeCreate' => 'Время создания',
			'group_attr_id' => 'Group Attr',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('img', $this->img, true);
		$criteria->compare('timeCreate', $this->timeCreate);
		$criteria->compare('group_attr_id', $this->group_attr_id);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function getAllCategory()
	{
		$data = self::findAll();
		return $data;
	}


	public static function getAll()
	{
		$model = new self;
		$model->unsetAttributes();
		return CHtml::listData($model->search()->getData(), 'id', 'title');
	}

	public function getCategoryById($id)
	{
		$model = self::findByPk($id);
		return $model->title;
	}

	public function getCategoryIdByAlias($alias)
	{
		$model = self::findByAttributes(array('alias' => $alias));
		return $model->id;
	}

	protected function beforeValidate()
	{
		if (empty($this->timeCreate)) {
			$this->timeCreate = time();
		} else {
			$this->timeCreate = TextHelper::unixTimeStamp($this->timeCreate);
		}

		$this->buildAlias();
		return parent::beforeValidate();
	}

	private function buildAlias()
	{
		if (empty($this->alias) && !empty($this->title)) {
			$this->alias = TextHelper::urlSafe($this->title);
		}
	}
}