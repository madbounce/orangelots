<?php
/**
 * Класс ValidatorInputFloat
 * @author dorosh_2009@meta.ua
 * @version 7.0.0
 * 
 * Модель валидации атрибута - тип float
 */
class ValidatorInputFloat extends Validator {
	
	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules()
	{
		$rules = array();
		
		if ($this->is_required) {
			$rules[] = array(
				'value', 
				'required',
				'message' => $this->attribute . ' обязательное для заполнения',
			);
		}

		$rules[] = array(
			'value', 
			'type',
			'type' => 'float',
			'message' => $this->attribute . ' должно быть числом',
		);
		
		return $rules;
	}
	
}
