<?php

/**
 * This is the model class for table "orangelots_category".
 *
 * The followings are the available columns in table 'orangelots_category':
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 *
 * The followings are the available model relations:
 * @property OrangelotsKind $parent
 * @property OrangelotsKind[] $orangelotsCategories
 */
class OrangelotsKind extends CActiveRecord
{
    public $on_main;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'module_orangelots_kind';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name,alias', 'required'),
			array('parent_id, active', 'numerical', 'integerOnly' => true),
			array('name, meta_description, meta_keywords, seo_text, page_title, alias', 'length', 'max' => 255),
            array('alias','advanced_unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('category_description', 'safe'),
			array('id, name, parent_id, active', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'OrangelotsKind', 'parent_id'),
			'orangelotsCategories' => array(self::HAS_MANY, 'OrangelotsKind', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'parent_id' => 'Parent',
			'active' => 'Show on main page',
			'alias' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('parent_id', $this->parent_id);
		$criteria->compare('active', $this->on_main);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrangelotsKind the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getParent($id)
	{
		return OrangelotsKind::model()->findByPk($id)->name;
	}

	public static function getParentCategories()
	{
		return self::model()->findAll('parent_id is NULL');
	}

	public static function getSubCategories($parentId)
	{
		return self::model()->findAllByAttributes(array('parent_id' => $parentId));
	}

    public static function getCategoryDescription($id){
        $category = self::model()->findByPk($id);
        if(!empty($category)){
            return $category->category_description;
        }
    }

    public function advanced_unique($attribute,$params)
    {
        $alias = array('new', 'used', 'orange-blossom','flash','upcoming-deals','search','all');
        /*$alias = array();
        $pages = OrangelotsStaticCategoryPages::model()->findAll();
        if (isset($pages) || !empty($pages))
            foreach($pages as $value){
                $alias[] = $value->url;
            }*/
        $unique = OrangelotsKind::model()->findAll();
        if (!empty($unique)){
            foreach($unique as $value){
                if($value->alias == $this->$attribute)
                    $this->addError($attribute,'Alias "'.$this->$attribute.'" already exists');
            }
        }
        if(in_array($this->$attribute,$alias)) {
            $this->addError($attribute,'You can`t use alias "'.$this->$attribute.'"');
        }
    }

    public static function defaultUrl($uri){
        if (isset($uri)){
            if(count($uri)<=2)
                return $uri[0];
            else{
                if (count($uri>=4))
                    return $uri[0];
                else
                    return $uri[(count($uri)-1)-2];
            }
        }


    }
}
