<?php

/**
 * This is the model class for table "orangelots_feedback".
 *
 * The followings are the available columns in table 'orangelots_feedback':
 * @property integer $id
 * @property integer $user_id
 * @property integer $user_id_rate
 * @property integer $communication
 * @property integer $billing
 * @property integer $shipping
 * @property string $average
 * @property integer $count
 * @property integer $date_add
 * @property string $message
 */
class OrangelotsFeedback extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orangelots_feedback';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('communication, billing, shipping, message', 'required'),
			array('user_id, user_id_rate, communication, billing, shipping, count', 'numerical', 'integerOnly'=>true),
			array('average', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, user_id_rate, communication, billing, shipping, average, count, message', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
    public function relations()
    {
        return array(
            'owner' => array(
                self::BELONGS_TO,
                'OrangelotsUsers',
                'user_id',
                'joinType' => 'INNER JOIN',
            ),
            'user' => array(
                self::BELONGS_TO,
                'OrangelotsUsers',
                'user_id_rate',
                'joinType' => 'INNER JOIN',
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'user_id_rate' => 'User Id Rate',
			'communication' => 'Communication',
			'billing' => 'Billing',
			'shipping' => 'Shipping',
			'average' => 'Average',
			'count' => 'Count',
			'message' => 'Message',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_id_rate',$this->user_id_rate);
		$criteria->compare('communication',$this->communication);
		$criteria->compare('billing',$this->billing);
		$criteria->compare('shipping',$this->shipping);
		$criteria->compare('average',$this->average,true);
		$criteria->compare('count',$this->count);
		$criteria->compare('message',$this->message,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrangelotsFeedback the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            $this->date_add = time();
            return true;
        } else {
            return false;
        }
    }
}
