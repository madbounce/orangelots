<?php

/**
 *
 * The followings are the available columns in table 'module_Offer_photo':
 * @property integer $id
 * @property integer $new_id
 * @property integer $rank
 * @property string $name
 * @property string $description
 * @property string $file
 *
 */
class ProgramsPhoto extends CActiveRecord
{
	const CACHE_KEY_PHOTOS = 'PROGRAMS_PHOTOS_';

	public $file;
	public $filePath;

	public $watemark = false;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GalleryPhoto the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'module_programs_photo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('programs_id', 'required'),
			array('name', 'length', 'max' => 512),
			array('file', 'length', 'max' => 128),
			array(
				'file',
				'file',
				'types' => 'jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG',
				'allowEmpty' => true
			),
			array('id, review_id, rank, name, description, file', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'programs' => array(self::BELONGS_TO, 'Programs', 'programs_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'programs_id' => 'Program',
			'rank' => 'Rank',
			'name' => 'Name',
			'description' => 'Description',
			'file' => 'File Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('programs_id', $this->programs_id);
		$criteria->compare('rank', $this->rank);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('description', $this->description, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public function beforeSave()
	{
		if ($this->isNewRecord) {
			$maxRankNumber = Yii::app()->db->createCommand()
				->select('max(rank) as rank')
				->from('module_programs_photo')
				->where("programs_id={$this->programs_id}")
				->queryScalar();
			$this->rank = $maxRankNumber + 1;
		}

		if (empty($this->file) == false) {
			$this->image_extension = $this->file->getExtensionName();
		}
		if (empty($this->filePath) == false) {
			$expl = explode('.', $this->filePath);
			$this->image_extension = end($expl);
		}

		return parent::beforeSave();
	}

	public function afterSave()
	{
		if (empty($this->file) == false) {
			$this->file->saveAs($this->getImage_directory(true) . '/origin.' . $this->image_extension);
		}

		if (empty($this->filePath) == false) {
			copy($this->filePath, $this->getImage_directory(true) . '/origin.' . $this->image_extension);
		}

		Yii::app()->cache->delete(self::CACHE_KEY_PHOTOS . $this->programs_id);

		return parent::afterSave();
	}

	public function afterDelete()
	{
		Yii::app()->cache->delete(self::CACHE_KEY_PHOTOS . $this->programs_id);

		$this->_deleteImage();

		return parent::afterDelete();
	}

	private function _deleteImage()
	{
		if ($this->image_extension) {
			Yii::app()->file->set($this->image_directory)->delete();
		}
	}

	public function getPublic_id()
	{
		return $this->id;
	}

	public function getImage_directory($mkdir = false)
	{
		if ($mkdir == false) {
			return Yii::app()->basePath . '/../photos/programs/' . $this->programs->id . '/' . $this->public_id;
		} else {
			$directory = Yii::app()->basePath . '/../photos/programs/' . $this->programs->id;
			if (file_exists($directory) == false) {
				mkdir($directory);
				chmod($directory, 0777);
			}

			$directory .= '/' . $this->public_id;
			if (file_exists($directory) == false) {
				mkdir($directory);
				chmod($directory, 0777);
			}

			return $directory;
		}
	}

	public function getPreview()
	{
		return $this->getThumb(300, 200, 'resize');
	}

	public function getThumb($width = null, $height = null, $mode = 'origin')
	{
		$dir = $this->getImage_directory(true) . '/';
		$originFile = $dir . 'origin.' . $this->image_extension;

		if (!is_file($originFile)) {
			return false;
		}

		if ($mode == 'origin') {
			return '/photos/programs/' . $this->programs->id . '/' . $this->public_id . '/origin.' . $this->image_extension;
		}

		$fileName = $mode . '_w' . $width . '_h' . $height . '.' . $this->image_extension;
		$filePath = $dir . $fileName;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
			}
		}

		return '/photos/programs/' . $this->programs->id . '/' . $this->public_id . '/' . $fileName;
	}

}