<?php

/**
 * This is the model class for table "orangelots_states".
 *
 * The followings are the available columns in table 'orangelots_states':
 * @property integer $state_id
 * @property string $state_name
 * @property string $state_abbreviation
 */
class OrangelotsStates extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orangelots_states';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('state_id, state_name, state_abbreviation', 'required'),
			array('state_id', 'numerical', 'integerOnly'=>true),
			array('state_name', 'length', 'max'=>22),
			array('state_abbreviation', 'length', 'max'=>2),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('state_id, state_name, state_abbreviation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'state_id' => 'State',
			'state_name' => 'State Name',
			'state_abbreviation' => 'State Abbreviation',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('state_id',$this->state_id);
		$criteria->compare('state_name',$this->state_name,true);
		$criteria->compare('state_abbreviation',$this->state_abbreviation,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrangelotsStates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getStateCodeById($id){
        if(!empty($id)){
            $state = self::model()->findByPk($id);
            return $state->state_abbreviation;
        }
    }

    public static function getStateLabel($idState){
        $stateInfo = OrangelotsStates::model()->findByPk($idState);
        return $stateInfo->state_name.', '.$stateInfo->state_abbreviation;
    }

    public static function getUserState($userId){
        return  OrangelotsUsers::model()->findByPk($userId)->state;
    }
}
