<?php
/**
 * RegistrationForm class.
 * RegistrationForm is the data structure for keeping
 * user registration form data. It is used by the 'registration' action of 'UserController'.
 */
class FilterForm extends OrangelotsAuctions
{
	public $verifyPassword;
	public $verifyCode;
	public $on_time_pass;

	public $upload;


	public function rules()
	{
		$rules = array(
			array('username, password, verifyPassword, email', 'required', 'on' => 'register'),
			array('username', 'length', 'max' => 20, 'min' => 3, 'message' => UserModule::t("Incorrect username (length between 3 and 20 characters).")),
			array('password', 'length', 'max' => 128, 'min' => 4, 'message' => UserModule::t("Incorrect password (minimal length 4 symbols).")),
			array('email', 'email'),
			array('email,password,verifyPassword, firstname, lastname, address_1, address_2, company, city, state, zipcode, phone', 'required', 'on' => 'ajax_register'),
			array('email', 'unique', 'on' => 'ajax_register'),
			array('email', 'email', 'on' => 'ajax_register'),
//			array('on_time_pass', 'numerical', 'integerOnly' => true, 'on' => 'ajax_register'),
			array('username', 'unique', 'message' => UserModule::t("This user's name already exists.")),
			array('email', 'unique', 'message' => UserModule::t("This user's email address already exists.")),
//			array('verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => UserModule::t("Retype Password is incorrect.")),
			array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => UserModule::t("Incorrect symbols (A-z0-9).")),
		);
		if (!(isset($_POST['ajax']) && $_POST['ajax'] === 'registration-form')) {
			array_push($rules, array('verifyCode', 'captcha', 'allowEmpty' => !UserModule::doCaptcha('registration'), 'on' => 'register'));
		}
		if ($this->scenario == 'ajax_register') {
			array_push($rules, array('verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => UserModule::t("Retype Password is incorrect.")));
		} else {
//			array_push($rules, array('verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => UserModule::t("Retype Password is incorrect.")));
		}
		return $rules;
	}
}