<?php

/**
 * Класс OrangelotsAttribute
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 *
 * Модель данных категорий статей
 * Описание атрибутов 'module_orangelots_category':
 * @property integer $id                        -- порядковый номер
 * @property integer $group_id                    -- группа
 * @property integer $rank                        -- позиция
 * @property string $title                        -- название
 * @property string $is_required                -- обязательный атрибут
 * @property string $is_filter                    -- выводить для фильтра
 * @property string $type                        -- тип
 * @property string $unit                        -- единица измерения
 */
class OrangelotsAttribute extends CActiveRecord
{
    const CACHE_KEY_OPTIONS = 'ATTRIBUTES_OPTIONS_';

    const TYPE_INPUT_STRING = 0;
    const TYPE_INPUT_INTEGER = 1;
    const TYPE_INPUT_FLOAT = 2;
    const TYPE_CHECKBOX = 3;
    const TYPE_SELECT = 4;

    public $post_options = array();
    public $optionsErrorsIndex;

    public $category_pointer;

    /**
     * Получить экземпляр класса
     * @return ArticleCategory
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Ассоциированная таблица
     * @return string
     */
    public function tableName()
    {
        return 'module_orangelots_attribute';
    }

    /**
     * Получить правила валидации
     * @return array
     */
    public function rules()
    {
        return array(
            array(
                'title,group_id,type',
                'required',
            ),
            array(
                'rank, group_id, type, is_required, is_filter',
                'numerical',
                'integerOnly' => true,
            ),
            array(
                'title, unit',
                'length',
                'max' => 255,
            ),
            array(
                'post_options',
                'safe',
            ),
            array(
                'post_options',
                'validateOptions',
            ),
            array(
                'id, category_pointer',
                'safe',
                'on' => 'search',
            ),
        );
    }

    public function validateOptions()
    {
        if ($this->type == self::TYPE_SELECT) {
            foreach ($this->post_options as $index => $item) {
                if (empty($item['value'])) {
                    $this->optionsErrorsIndex['value'][$index] = 1;
                }
            }
            if (!empty($this->optionsErrorsIndex)) {
                $this->addError('post_options', 'Значения списка пустое');
            }
        }
    }

    /**
     * Взаимосвязи
     * @return array
     */
    public function relations()
    {
        return array(
            'group' => array(self::BELONGS_TO, 'OrangelotsGroupAttribute', 'group_id', 'together' => true),
            'options' => array(self::HAS_MANY, 'OrangelotsAttributeOption', 'attribute_id'),
        );
    }

    /**
     * Названия атрибутов
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'group_id' => 'Group',
            'rank' => 'Rank',
            'title' => 'Title',
            'is_required' => 'Is Required Attribute',
            'is_filter' => 'Show On Filter',
            'type' => 'Type',
            'unit' => 'Unit',
        );
    }

    public function beforeSave()
    {
        return parent::beforeSave();
    }

    /**
     * Поиск
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('is_required', $this->is_required);
        $criteria->compare('is_filter', $this->is_filter);
        $criteria->compare('group_id', $this->group_id);
        $criteria->compare('rank', $this->rank);
        $criteria->compare('type', $this->type);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('unit', $this->unit, true);

//        dump($this->attributes,false);
//        dump($this->category_pointer,false);
//        dump($this->group->relations(),false);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 50,
            ),
        ));
    }

    public function getTypeTitle()
    {
        $types = self::getTypes();
        if (isset($types[$this->type])) {
            return $types[$this->type];
        } else {
            return false;
        }
    }

    public static function getTypes()
    {
        return array(
            self::TYPE_INPUT_STRING => 'Текстове поле (строка)',
            self::TYPE_INPUT_INTEGER => 'Текстове поле (целое число)',
            self::TYPE_INPUT_FLOAT => 'Текстове поле (дробное число)',
            self::TYPE_CHECKBOX => 'Галочка (checkbox)',
            self::TYPE_SELECT => 'Список (select)',
        );
    }

    public function getDataOptions()
    {
        if (Yii::app()->request->isPostRequest) {
            return $this->post_options;
        } else {
            if ($this->isNewRecord) {
                return array();
            } else {
                $data = array();
                foreach ($this->options as $option) {
                    $data[] = $option->attributes;
                }

                return $data;
            }
        }
    }

    public function getCacheOptions()
    {
        $key = self::CACHE_KEY_OPTIONS . $this->id;
//		$data = Yii::app()->cache->get($key);
        if (empty($data)) {
            $data = $this->options;
//			Yii::app()->cache->set($key, $data, 60 * 60 * 24);
        }
        return $data;
    }

    /**
     * Выполняем ряд действий перед валидацией модели
     * @return boolean -- результат выполнения операции
     */
    protected function beforeValidate()
    {
        $this->buildAlias();
        return parent::beforeValidate();
    }

    private function buildAlias()
    {
        if (empty($this->alias)) {
            $this->alias = $this->title;
        }

        $this->alias = TextHelper::urlSafe($this->alias);
    }

    /**
     * Выполняем ряд обязательных действий после сохранения модели
     * @return boolean -- результат выполнения действия
     */
    protected function afterSave()
    {
        if (!$this->isNewRecord && $this->scenario == 'update') {
            if ($this->type == self::TYPE_SELECT) {
                $optionIds = array();
                foreach ($this->post_options as $option) {
                    if (isset($option['id'])) {
                        $optionIds[] = $option['id'];
                    }
                }

                $criteria = new CDbCriteria();
                $criteria->compare('attribute_id', $this->id);
                $issetOptions = OrangelotsAttributeOption::model()->findAll($criteria);
                foreach ($issetOptions as $issetOption) {
                    if (!in_array($issetOption->id, $optionIds)) {
                        $issetOption->delete();
                    }
                }
            } else {
                OrangelotsAttributeOption::model()->deleteAllByAttributes(array('attribute_id' => $this->id));
            }
        }

        if ($this->type == self::TYPE_SELECT) {
            foreach ($this->post_options as $option) {
                $optionModel = null;
                if (isset($option['id'])) {
                    $optionModel = OrangelotsAttributeOption::model()->findByPk($option['id']);
                }

                if (empty($optionModel)) {
                    $optionModel = new OrangelotsAttributeOption();
                    $optionModel->attribute_id = $this->id;
                }

                $optionModel->value = $option['value'];
                $optionModel->save();
            }
        }

//		Yii::app()->cache->delete(self::CACHE_KEY_OPTIONS . $this->id);

        return parent::afterSave();
    }



}