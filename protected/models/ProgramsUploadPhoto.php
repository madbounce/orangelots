<?php

class ProgramsUploadPhoto extends CFormModel
{
	public $file;
	public $token;

	public function rules()
	{
		return array(
			array('file', 'file', 'types' => 'jpg, gif, png'),
		);
	}

	function upload()
	{
		$filename = time();
		$extension = $this->file->getExtensionName();

		$directory = $this->getDirectory(true);

		$Image = Yii::app()->image->load($this->file->getTempName());
		$Image->save($directory . '/' . $filename . '.' . $extension);

		$data = array(
			'image_url' => '/photos/temp/' . $this->token . '/' . $filename . '.' . $extension,
			'filename' => $filename . '.' . $extension,
		);

		return $data;
	}

	public function getDirectory($mkdir = false)
	{
		$directory = Yii::app()->basePath . '/../photos/temp/' . $this->token;

		if ($mkdir && file_exists($directory) == false) {
			mkdir($directory);
			chmod($directory, 0777);
		}

		return $directory;
	}
}