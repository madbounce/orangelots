<?php

/**
 * This is the model class for table "module_orangelots_auctions".
 *
 * The followings are the available columns in table 'module_orangelots_auctions':
 * @property integer $id
 * @property string $name
 * @property string $manufacture
 * @property integer $mvrp
 * @property integer $units_in_lot
 * @property double $buy_now_price
 * @property double $min_bid
 * @property integer $min_unit
 * @property string $start_date
 * @property string $end_date
 * @property string $condition
 * @property string $color
 * @property string $dimensions
 * @property double $weight
 * @property string $description
 * @property string $terms
 * @property integer $shipping_terms
 * @property string $shipping_terms_other
 * @property integer $delivery
 * @property string $delivery_other
 * @property string $returns_warranty
 * @property string $vendors_information
 * @property integer $category_id
 * @property string $sku
 */
class OrangelotsAuctions extends CActiveRecord
{
    public $usr;
    public $owner_email;
    public $on_main;
    public $colum_table;
    public $item_validate = true;

    static $savingCriterions = array(
        10 => array(
            'to' => 24
        ),
        25 => array(
            'to' => 49
        ),
        50 => array(
            'to' => 69
        ),
        70 => array(
            'to' => 100
        ),
    );

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'module_orangelots_auctions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('units_in_lot', 'Zero', 'on' => 'create'),
            array('price_per_unit, units_in_lot, lots_available', 'Zero', 'on' => 'create_flash_deal'),
            array('min_bid, buy_now_price', 'dual_not_zero', 'on' => 'create'),
            array('name, manufacture, mvrp, units_in_lot, buy_now_price, min_bid, min_unit, condition, color, dimensions, weight, description, terms, shipping_terms, delivery, returns_warranty, vendors_information, au_date, au_time, au_am_pm, au_date_end, au_time_end, au_am_pm_end, category_id, style_id, upholstery_material, number_of_pieces, furniture_function', 'required', 'on' => 'create'),
            array('name, manufacture, mvrp, units_in_lot, condition, color, dimensions, weight, description, terms, shipping_terms, delivery, returns_warranty, vendors_information, au_date, au_time, au_am_pm, category_id, style_id', 'required', 'on' => 'create_deal'),
            array('name, manufacture, mvrp, units_in_lot, condition, color, dimensions, weight, description, terms, shipping_terms, delivery, returns_warranty, vendors_information, au_date, au_time, au_am_pm, category_id, style_id, price_per_unit, saving_price_per_unit, lots_available, upholstery_material, number_of_pieces, furniture_function', 'required', 'on' => 'create_flash_deal'),
            array('name, manufacture, mvrp, units_in_lot, buy_now_price, min_bid, min_unit, condition, color, dimensions, weight, description, terms, shipping_terms, delivery, returns_warranty, vendors_information, au_date, au_time, au_am_pm, au_date_end, au_time_end, au_am_pm_end, category_id, delivery_other, style_id', 'required', 'on' => 'delivery_other'),
            array('name, manufacture, mvrp, units_in_lot, buy_now_price, min_bid, min_unit, condition, color, dimensions, weight, description, terms, shipping_terms, delivery, returns_warranty, vendors_information, au_date, au_time, au_am_pm, au_date_end, au_time_end, au_am_pm_end, category_id, shipping_terms_other, style_id', 'required', 'on' => 'shipping_terms_other'),
            array('name, manufacture, mvrp, units_in_lot, buy_now_price, min_bid, min_unit, condition, color, dimensions, weight, description, terms, shipping_terms, delivery, returns_warranty, vendors_information, au_date, au_time, au_am_pm, au_date_end, au_time_end, au_am_pm_end, category_id, shipping_terms_other, delivery_other, style_id', 'required', 'on' => 'delivery_shipping_terms_other'),
            array('id, name, manufacture, mvrp, units_in_lot, buy_now_price, min_bid, min_unit, start_date, end_date, condition, color, dimensions, weight, description, terms, shipping_terms, shipping_terms_other, delivery, delivery_other, returns_warranty, vendors_information, style_id', 'safe', 'on' => 'my_create'),
            array('units_in_lot, min_unit, shipping_terms, delivery, category_id, sponsored, is_flashdeal, lots_available', 'numerical', 'integerOnly' => true),
            array('buy_now_price, min_bid, weight', 'numerical'),
            array('mvrp, buy_now_price, min_bid, price_per_unit', 'length', 'max'=>10),
            array('name, manufacture, condition, color, dimensions,saving_buy_now, saving_min_bid', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('active, start_date_unix, end_date_unix, set_on_main, price_per_unit, saving_price_per_unit, import, sku, nmfc', 'safe'),
            array('id, name, manufacture, mvrp, units_in_lot, buy_now_price, min_bid, min_unit, start_date, end_date, condition, color, dimensions, weight, description, terms, shipping_terms, shipping_terms_other, delivery, delivery_other, returns_warranty, vendors_information, category_id, status, set_on_main', 'safe', 'on' => 'search'),
            array('id, name,description', 'safe', 'on' => 'import'),
            array('end_date_unix', 'compare', 'compareAttribute' => 'start_date_unix', 'operator' => '>', 'message' => 'Start time must be less than end time', 'on' => 'create'),
   			array('min_unit', 'compare', 'compareAttribute' => 'units_in_lot', 'operator' => '<', 'message' => 'Minimal unit amount must be less then units in lot'),
            array('au_date', 'checkDealDate', 'on' => 'create_flash_deal'),
            array('item_validate', 'checkItemValidate')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'galleryPhotos' => array(self::HAS_MANY, 'OrangelotsPhoto', 'realty_id', 'order' => '`rank` ASC',),
            'users' => array(self::BELONGS_TO, 'OrangelotsUsers', 'user_id'),
            'bidding' => array(self::HAS_MANY, 'OrangelotsBiddingOn', 'id_auction'),
            'winners_table' => array(self::HAS_MANY, 'OrangelotsWinners', 'auction_id'),
            'shipping_terms_rel' => array(self::BELONGS_TO,'ShippingTerms','shipping_terms','joinType' => 'INNER JOIN'),
            'delivery_rel' => array(self::BELONGS_TO,'Delivery','delivery','joinType' => 'INNER JOIN'),
            'manufacture_rel' => array(self::BELONGS_TO,'OrangelotsManufacture','manufacture','joinType' => 'INNER JOIN')
        );
    }
    public function checkItemValidate($attribute){
        $items = $this->$attribute;
        if(!$items) {
            $this->addError($attribute,'Some items not valid');
        }
    }

    public function checkDealDate($attribute){
        $time = getTime($this->$attribute);
        $timeToday = getTime(date('Y/m/d'));
        if($time <= $timeToday) {
            $this->addError($attribute,'You can not assign to this date because it is already started');
        }
    }

    public function Zero($attribute,$params)
    {
        if(($this->$attribute == 0) || ($this->$attribute == null) || (empty($this->$attribute))) {
            $this->addError($attribute,'Must be more than 0');
        }
    }

    public function dual_not_zero($attribute,$params)
    {
        if((($this->min_bid == 0) || ($this->min_bid == null) || (empty($this->min_bid))) &&(($this->buy_now_price == 0) || ($this->buy_now_price == null) || (empty($this->buy_now_price)))) {
            $this->addError($attribute,'One of this fileds must be more than 0');
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Auction name',
            'manufacture' => 'Manufacture',
            'mvrp' => 'MVRP (price per unit)',
            'units_in_lot' => 'Units in lot',
            'buy_now_price' => 'Buy Now Price (per unit)',
            'min_bid' => 'Min Bid (per unit)',
            'min_unit' => 'Min Unit (per bid)',
            'start_date' => 'Auction start date',
            'au_date' => 'Auction start date',
            'au_date_end' => 'Auction end date',
            'end_date' => 'Auction end date',
            'condition' => 'Condition',
            'color' => 'Color',
            'dimensions' => 'Product Dimensions',
            'weight' => 'Shipping Weight',
            'description' => 'Description',
            'terms' => 'Terms',
            'shipping_terms' => 'Shipping Terms',
            'shipping_terms_other' => 'Shipping Terms Other',
            'delivery' => 'Delivery',
            'delivery_other' => 'Delivery Other',
            'returns_warranty' => 'Returns Warranty',
            'vendors_information' => 'Vendors Information',
            'category_id' => 'Category',
            'usr' => 'Aviable Vendors',
            'style_id' => 'Style',
            'sponsored' => 'Sponsored',
            'upholstery_material' => 'Upholstery material',
            'number_of_pieces' => 'Number of pieces',
            'furniture_function' => 'Furniture function',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('manufacture', $this->manufacture, true);
        $criteria->compare('mvrp', $this->mvrp);
        $criteria->compare('units_in_lot', $this->units_in_lot);
        $criteria->compare('buy_now_price', $this->buy_now_price);
        $criteria->compare('min_bid', $this->min_bid);
        $criteria->compare('min_unit', $this->min_unit);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('end_date', $this->end_date, true);
        $criteria->compare('condition', $this->condition, true);
        $criteria->compare('color', $this->color, true);
        $criteria->compare('dimensions', $this->dimensions, true);
        $criteria->compare('weight', $this->weight);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('terms', $this->terms, true);
        $criteria->compare('shipping_terms', $this->shipping_terms);
        $criteria->compare('shipping_terms_other', $this->shipping_terms_other, true);
        $criteria->compare('delivery', $this->delivery);
        $criteria->compare('delivery_other', $this->delivery_other, true);
        $criteria->compare('returns_warranty', $this->returns_warranty, true);
        $criteria->compare('vendors_information', $this->vendors_information, true);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('set_on_main', $this->on_main);
        $criteria->compare('active', 1);
        $criteria->compare('is_flashdeal', 0);

        $criteria->with=array('users');
        $criteria->compare('users.email', $this->owner_email, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ModuleOrangelotsAuctions the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function hiddenId($url)
    {
        $uri = explode("/", $url);
        if (isset($uri[(count($uri) - 1)])) {
            if ($uri[(count($uri) - 1)] == 'create') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function getPrices($arr)
    {
        $resultArr = array('minPrices' => array(), 'maxPrices' => array());
        foreach ($arr as $key => $value) {
            $range = explode('_', $value);
            $resultArr['minPrices'][] = $range[0];
            $resultArr['maxPrices'][] = $range[1];
        }
        $min = min($resultArr['minPrices']);
        if (in_array('more', $resultArr['maxPrices'])) {
            $max = '';
        } else {
            $max = max($resultArr['maxPrices']);
        }
        return array('max' => $max, 'min' => $min);
    }

    public static function isChecked($elem)
    {
        if (empty(yii()->session['checked']) or !isset( yii()->session['checked'])) {
            return false;
        } else {
            if (array_key_exists($elem,  yii()->session['checked'])) {
                return true;
            } else {
                return false;
            }
        }

    }

    public function visibleEdit($id){
        if (isset($id)){
            $auction = OrangelotsAuctions::model()->findByPk($id)->status;
            if (isset($auction)){
                if($auction == '1') {
                    $bidds = OrangelotsBiddingOn::model()->findByAttributes(array('id_auction' => $id));
                    if(empty($bidds)){
                        return true;
                    }else{
                        return false;
                    }
                } else {
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public static function displayStyle($link){
        if($link){
            return 'display:block';
        } else {
            return 'display:none';
        }
    }

    public static function SaveUnSave($id){
        $saveforlater = SaveForLater::model()->findByAttributes(array('user_id'=>yii()->user->id, 'auction_id' =>$id));
        if (isset($saveforlater)){
            $link = true;
        } else {
            $link = false;
        }
        return $link;
    }

    public static function checkExist($id){
        $auction = self::model()->findByPk($id);
        if(!empty($auction)){
            return true;
        }else{
            return false;
        }
    }

    public static function isOwner($auctionId){
        if(user()->isGuest){
            return false;
        }else{
            $ownerId = OrangelotsAuctions::getOwner($auctionId);
            if($ownerId == user()->id){
                return true;
            }else{
                return false;
            }
        }
    }

    public static function getOwner($id){
        $auction = self::model()->findByPk($id);
        return $auction->user_id;
    }
    public static function getOwnerEmail($id){
        $usr = OrangelotsUsers::model()->findByPk($id);
        return $usr->email;
    }

    public function search_flashdeal()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('manufacture', $this->manufacture, true);
        $criteria->compare('mvrp', $this->mvrp);
        $criteria->compare('units_in_lot', $this->units_in_lot);
        $criteria->compare('buy_now_price', $this->buy_now_price);
        $criteria->compare('min_bid', $this->min_bid);
        $criteria->compare('min_unit', $this->min_unit);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('end_date', $this->end_date, true);
        $criteria->compare('condition', $this->condition, true);
        $criteria->compare('color', $this->color, true);
        $criteria->compare('dimensions', $this->dimensions, true);
        $criteria->compare('weight', $this->weight);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('terms', $this->terms, true);
        $criteria->compare('shipping_terms', $this->shipping_terms);
        $criteria->compare('shipping_terms_other', $this->shipping_terms_other, true);
        $criteria->compare('delivery', $this->delivery);
        $criteria->compare('delivery_other', $this->delivery_other, true);
        $criteria->compare('returns_warranty', $this->returns_warranty, true);
        $criteria->compare('vendors_information', $this->vendors_information, true);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('set_on_main', $this->on_main);
        $criteria->compare('active', 1);
        $criteria->compare('is_flashdeal', 1);

        $criteria->with=array('users');
        $criteria->compare('users.email', $this->owner_email, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    public function main_photo($model)
    {
        $main = 0;
        if ((isset($model)) && (!empty($model))){
            foreach ($model->galleryPhotos AS $key => $value){
                if ($value->set_main == 1){
                    $main++;
                }
            }
        }

        if ($main>0)
            return true;
        else
            return false;
    }

    public function select_step($data, $model, $id_select){
//        $rest = ($model->units_in_lot)%($model->min_unit);
        if($model->min_unit == 0)
            $model->min_unit = 1;

        $result = ' <select id="'.$id_select.' " class="selectpicker">';
        $rest_from = 0;

        for ($i = $model->min_unit; $i <= $model->units_in_lot; $i=$i+$model->min_unit){
            if (isset($data->price_per_unit)){
                if ($data->qty == $i){
                    $result .= '<option value=" '.$i.' " selected="true">'.$i.' </option>';

                }
                else {
                    $result .= '<option value=" '.$i.' "> '.$i.' </option>';
                }
            } else {
                $result .= '<option value=" '.$i.' "> '.$i.' </option>';
            }
            $rest_from = $i;
        }


        for ($i = $rest_from+1; $i <= (int)$model->units_in_lot; $i++){
            if (isset($data->price_per_unit)){
                if ($data->qty == $i){
                    $result .= '<option value=" '.$i.' " selected="true">'.$i.' </option>';
                }
                else {
                    $result .= '<option value=" '.$i.' "> '.$i.' </option>';
                }
            } else {
                $result .= '<option value=" '.$i.' "> '.$i.' </option>';
            }
        }
        $result.='</select>';
        return $result;

    }

    public function select_step_flashdeal($model, $toArray = false, $selected_field = false){
        $lots_available = $model->lots_available;
        $result = ' <select>';
        $array_flash = array();
        $array_selected = array();
        if ($lots_available != 0) {
            for ($i = 1; $i <= $lots_available; $i++){
                if($toArray)
                    $array_flash[$i] = $i;
                elseif($selected_field)
                    $array_selected[]=$i;
                else
                    $result .= '<option value=" '.$i.' "> '.$i.' </option>';
            }
        }
        $result.='</select>';
        if($toArray)
            return $array_flash;
        elseif($selected_field)
            return  $array_selected[0];
        else
            return $result;

        /*$array_flash = array();
        $array_selected = array();
        $data = OrangelotsWinners::model()->findByAttributes(
            array('user_id' => yii()->user->id, 'auction_id' => $model->id),
            array('order' => 'id DESC')

        );
        $lots_available = $model->lots_available;
        $units_in_lot = $model->units_in_lot;

        if($units_in_lot == 0) {
            $units_in_lot = 1;
        }
        $rest = ($lots_available)%($units_in_lot);
        $result = ' <select>';
        $rest_from = 0;

        for ($i = $units_in_lot; $i <= $lots_available; $i=$i+$units_in_lot){
                if ($data->qty == $i){
                    if($toArray)
                        $array_flash[$i] = $i;
                    elseif($selected_field)
                        $array_selected[]=$i;
                        else
                    $result .= '<option value=" '.$i.' " selected="true">'.$i.' </option>';

                }
                else {
                    if($toArray)
                        $array_flash[$i] = $i;
                    else
                    $result .= '<option value=" '.$i.' "> '.$i.' </option>';
                }
            $rest_from = $i;
        }


        for ($i = $rest_from+1; $i <= (int)$lots_available; $i++){
                if ($data->qty == $i){
                    if($toArray)
                        $array_flash[$i] = $i;
                    elseif($selected_field)
                        $array_selected[]=$i;
                    else
                    $result .= '<option value=" '.$i.' " selected="true">'.$i.' </option>';
                }
                else {
                    if($toArray)
                        $array_flash[$i] = $i;
                    else
                    $result .= '<option value=" '.$i.' "> '.$i.' </option>';
                }
        }
        $result.='</select>';
        if($toArray)
            return $array_flash;
        elseif($selected_field)
            return  $array_selected[0];
        else
        return $result;*/

    }

    public function checkQuantity($rest, $min_unit, $quantity){
        if($min_unit == 0)
            $min_unit = 1;
        $qty = (int)$quantity;
        $a = $rest%$min_unit;
        $response = true;
        $array = array();

        for ($i = $min_unit; $i <= $rest; $i=$i+$min_unit){
            if($i == $qty){
                $response = false;
            }
            $rest_from = $i;
        }
        for ($i = $rest_from+1; $i <= (int)$rest; $i++){
            if($i == $qty){
                $response = false;
            }
        }
        return $response;
    }
    public static function AmericanDate($date){
        $unix_date = CDateTimeParser::parse($date, 'yyyy-MM-dd HH:mm:ss');
        return yii()->dateFormatter->format("MM/dd/yyyy hh:mm a", $unix_date);
    }

    public function colorRowAuction($auction_id){
        if ((isset($auction_id)) && (!empty($auction_id))){
            $auction = OrangelotsInvoices::model()->findByAttributes(array('winner_id'=>yii()->user->id,'auction_deal_id'=>$auction_id));
            if ((isset($auction)) && (!empty($auction))){
                return true;
            } else {
                return false;
            }
        }
    }

    public function commentAuction($user){
        $text = '<ul>';
        if ((isset($user)) && (!empty($user))){
            $feedback = OrangelotsFeedback::model()->findAllByAttributes(array('user_id_rate' => $user),array('order' => 'id desc'));
        }

        if ((isset($feedback)) && (!empty($feedback))){
            foreach($feedback AS $feed){
                $owner = OrangelotsUsers::model()->findByPk($feed->user_id);
                $owner_state_name = OrangelotsStates::model()->findByPk($owner->state)->state_name;
                $owner_state_abbreviation = OrangelotsStates::model()->findByPk($owner->state)->state_abbreviation;
                $review = round(($feed->communication+$feed->billing+$feed->shipping)/3,2);
                $span = $owner->firstname.','.$owner_state_name.','.$owner_state_abbreviation.' - '.'Review: <span class="orng">'.$review.' out of 10</span>';
                $text .= '</br><li>
                <p class="card_detail__tab_question__question_author">

                    <span>'.$span.'</span>
                    <span><p class="rate_feed">Communication:'.$feed->communication.'/10 Billing:'.$feed->billing.'/10 Shipping:'.$feed->shipping.'/10 posted: '.yii()->dateFormatter->format('dd/MM/yyyy',$feed->date_add).'</p></span>
                </p>

                <p class="card_detail__tab_question__question_text"></p>
                <div class="clearfix">
                    <div class="row">
                        <div class="card_detail__tab_question__image pull-left margin_feed">';
                if(!empty($feed->owner->avatar)):
                    $text.='<img src="'.yii()->baseUrl .yii()->storage->getFileUrl($feed->owner->avatar)  .'" alt=""/>';
                else:
                    $text.='<img src='.yii()->baseUrl."/images/NIA.png" .' alt="">';
                endif;
                $text.='</div>
                        <div class="card_detail__tab_question__answer pull-left">
                            <p>'.$feed->message.'</p>
                        </div>
                    </div>
                                    </div>
                                    </br>
            </li>';
            }
        }
        $text .= '</ul>';
        return $text;
    }

    public function sold_units($id){
        $count = 0;
        if(isset($id)){
            $auction = OrangelotsAuctions::model()->findByPk($id);
            $winners = OrangelotsWinners::model()->findAllByAttributes(array('auction_id'=>$id));
            if(!empty($winners)){
                foreach($winners as $value){
                    $count = $count+$value['qty'];
                }
                $count = $count*$auction->units_in_lot;
            }
        }
        return $count;
    }
    public function sold_lots($id){
        $count = 0;
        if(isset($id)){
//            $auction = OrangelotsAuctions::model()->findByPk($id);
            $winners = OrangelotsWinners::model()->findAllByAttributes(array('auction_id'=>$id));
            if(!empty($winners)){
                foreach($winners as $value){
                    $count = $count+$value['qty'];
                }
            }
        }
        return $count;
    }

    public function sold($id){
        $count = 0;
        if(isset($id)){
            $winners = OrangelotsWinners::model()->findAllByAttributes(array('auction_id'=>$id));
            if(!empty($winners)){
                foreach($winners as $value){
                    $count = $count+$value['summary_price'];
                }
            }
        }
        return $count;
    }

    public function invoice_href($id){
        if (isset($id)){
            $link = '<a href="'.createUrl('user/buy/confirmation', array('transaction' => $id)).'" class="orange_link">invoice</a>';
            return $link;
        }

    }

    public static function real_date_diff($date1, $date2 = NULL){
        $diff = array();

        //Если вторая дата не задана принимаем ее как текущую
        if(!$date2) {
            $cd = getdate();
            $date2 = $cd['year'].'-'.$cd['mon'].'-'.$cd['mday'].' '.$cd['hours'].':'.$cd['minutes'].':'.$cd['seconds'];
        }

        //Преобразуем даты в массив
        $pattern = '/(\d+)-(\d+)-(\d+)(\s+(\d+):(\d+):(\d+))?/';
        preg_match($pattern, $date1, $matches);
        $d1 = array((int)$matches[1], (int)$matches[2], (int)$matches[3], (int)$matches[5], (int)$matches[6], (int)$matches[7]);
        preg_match($pattern, $date2, $matches);
        $d2 = array((int)$matches[1], (int)$matches[2], (int)$matches[3], (int)$matches[5], (int)$matches[6], (int)$matches[7]);

        //Если вторая дата меньше чем первая, меняем их местами
        for($i=0; $i<count($d2); $i++) {
            if($d2[$i]>$d1[$i]) break;
            if($d2[$i]<$d1[$i]) {
                $t = $d1;
                $d1 = $d2;
                $d2 = $t;
                break;
            }
        }

        //Вычисляем разность между датами (как в столбик)
        $md1 = array(31, $d1[0]%4||(!($d1[0]%100)&&$d1[0]%400)?28:29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        $md2 = array(31, $d2[0]%4||(!($d2[0]%100)&&$d2[0]%400)?28:29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
        $min_v = array(NULL, 1, 1, 0, 0, 0);
        $max_v = array(NULL, 12, $d2[1]==1?$md2[11]:$md2[$d2[1]-2], 23, 59, 59);
        for($i=5; $i>=0; $i--) {
            if($d2[$i]<$min_v[$i]) {
                $d2[$i-1]--;
                $d2[$i]=$max_v[$i];
            }
            $diff[$i] = $d2[$i]-$d1[$i];
            if($diff[$i]<0) {
                $d2[$i-1]--;
                $i==2 ? $diff[$i] += $md1[$d1[1]-1] : $diff[$i] += $max_v[$i]-$min_v[$i]+1;
            }
        }

        //Возвращаем результат
        return $diff;
    }

    public static function getAction($auctionId){
        $action = '';
        $isWinner = OrangelotsWinners::model()->findByAttributes(array('auction_id' => $auctionId, 'user_id' => user()->id));
        if(isset($isWinner) and !empty($isWinner)){
            if($isWinner->is_buy_now == 1){
                $action = 'Bought';
            }else{
                $action = 'Won';
            }
        }
        $isBid = OrangelotsBiddingOn::model()->findByAttributes(array('id_user' => user()->id, 'id_auction' => $auctionId));
        if(isset($isBid) and !empty($isBid)){
            if(!empty($action)){
                $action .= ',';
            }
            $action .= 'Placed bid';
        }else{
            $isSaved = SaveForLater::model()->findByAttributes(array('user_id' => user()->id, 'auction_id' => $auctionId));
            if(isset($isSaved) and !empty($isSaved)){
                if(!empty($action)){
                    $action .= ',';
                }
                $action .= 'Saved';
            }
        }
        if(!empty($action)){
            return $action.'<br>'.CHtml::link("view detail",array("orangelots-auctions/view/".$auctionId),array("class" => "orange_link"));
        }else{
            return CHtml::link("view detail",array("orangelots-auctions/view/".$auctionId),array("class" => "orange_link"));
        }
    }

    public static function canEdit($auctionID){
        $bidds = OrangelotsBiddingOn::model()->findByAttributes(array('id_auction' => $auctionID));

        if(self::isOwner($auctionID) and empty($bidds)){
            return true;
        }elseif(user()->isAdmin()){
            return true;
        }else{
            return false;
        }
    }

    public static function getMyBid($auId){
        $bid = '';

        $isBid = OrangelotsBiddingOn::model()->findByAttributes(array('id_user' => user()->id, 'id_auction' => $auId));
        if(!empty($isBid)){
            $text = 'Bid - $';
            $isWinner = OrangelotsWinners::model()->findByAttributes(array('auction_id' => $auId, 'user_id' => user()->id, 'bid_id' => $isBid->id));
            if(!empty($isWinner)){
                if($isWinner->is_buy_now == 1){
                    $text = 'Bought - $';
                }else{
                    $text = 'Won - $';
                }
            }

            $elseBid =  OrangelotsBiddingOn::model()->findByPk(yii()->db->createCommand("SELECT id FROM orangelots_bidding_on WHERE id_auction = ".$auId." AND summary_price = (SELECT max(summary_price) FROM orangelots_bidding_on WHERE id_auction = ".$auId.")")->queryRow());
            if(!empty($elseBid) and ($isBid->price_per_unit < $elseBid->price_per_unit)){
                $bid .= '<b>'.$text.$isBid->summary_price.'<b>';
            }else{
                $bid .= $text.$isBid->summary_price;
            }
            $transactions = OrangelotsInvoices::model()->findAllByAttributes(array('auction_deal_id' => $auId, 'winner_id' => user()->id));
            if(!empty($transactions) and count($transactions) > 1){
                $bid = CHtml::link($bid, createUrl('user/buy/payments'), array('class' => 'orange_link'));
            }else{
                $transaction = OrangelotsInvoices::model()->findByAttributes(array('auction_deal_id' => $auId, 'winner_id' => user()->id));
                if(!empty($transaction)){
                    $bid = CHtml::link($bid, createUrl('user/buy/confirmation', array('transaction' => $transaction->transaction_id)), array('class' => 'orange_link'));
                }
            }

        }
        return $bid;
    }


    public static function canBid($auId){
        if(user()->isGuest){
            return true;
        }else{
            $isUserWon = OrangelotsWinners::model()->findByAttributes(array('user_id' => user()->id, 'auction_id' => $auId));
            if(!empty($isUserWon)){
                return false;
            }else{
                return true;
            }
        }
    }

    public function itemsView($id){
        if(!empty($id) && (isset($id))){
            $items = Items::model()->findAllByAttributes(array('auction_deal_id'=>$id));
            if(!empty($items)){
                foreach($items as $k=>$v){
                    echo  $v->name.':'.$v->length.'" L '.$v->width.'" W x '.$v->height.'" H; Weight: '.$v->weight.' lbs.'.'</br>';
                }
            }
        }
    }

    public function weightItems($id){
        $w = 0;
        if(!empty($id) && (isset($id))){
            $items = Items::model()->findAllByAttributes(array('auction_deal_id'=>$id));
            if(!empty($items)){
                foreach($items as $k=>$v){
                    $w+=$v->weight;
                }
            }
        }
        return $w;
    }

    public function itemsV($id){
        $summary = 0;
        if(!empty($id) && (isset($id))){
            $items = Items::model()->findAllByAttributes(array('auction_deal_id'=>$id));
            if(!empty($items)){
                foreach($items as $k=>$v){
                    $summary+=($v->weight*$v->length*$v->height);
                }
            }
        }
        return $summary;
    }

}
