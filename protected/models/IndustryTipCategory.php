<?php

class IndustryTipCategory extends CActiveRecord
{
	/**
	 * The followings are the available columns in table 'tips_category':
	 * @var integer $id
	 * @var string $name
	 * @var string $alias
	 * @var string $types
	 */

    const BUYER = 1;
    const VENDOR = 2;
    const INDUSTRY = 3;

    const buyer_label = 'buyer';
    const vendor_label = 'vendor';
    const industry_label = 'industry';

    public static $types = array(
        self::BUYER => self::buyer_label,
        self::VENDOR => self::vendor_label,
        self::INDUSTRY => self::industry_label
    );

    public static $labelsFor = array(
        self::buyer_label => 'For buyers',
        self::vendor_label => 'For vendors',
        self::industry_label => 'Industry tips'
    );

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tips_category';
	}

    public function checkTypes($attribute){
        $val = unserialize($this->types);
        if(!empty($val)){
            return true;
        }else{
            $this->addError($attribute, 'You have to choose at least one type');
        }
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name, alias', 'required'),
			array('alias', 'unique'),
			array('name', 'length', 'max' => 255),
            array('types', 'length', 'max' => 500),
			array('alias', 'length', 'max' => 50),
            array('types', 'checkTypes'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => UserModule::t("Id"),
			'name' => 'Name',
			'alias' => 'Url',
			'types' => 'Types',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('types', $this->types, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public static function getList()
	{
		return self::model()->findAll();
	}

    public function viewCategories(){
        $cat = $this->types;
        $catArr = unserialize($cat);
        $res = array();
        foreach($catArr as $type){
            $res[] = self::$types[$type];
        }
        return implode(',', $res);
    }

    public static function getCategoriesByType($type){
        $tId = self::getConstId($type);
        $all = self::model()->findAll();
        $res = array();
        if(!empty($all)){
            foreach($all as $category){
                $types = unserialize($category->types);
                if(in_array($tId, $types)){
                    $res[] = $category;
                }
            }
        }
        return $res;
    }

    public static function getCategoryByAlias($alias){
        return self::model()->findByAttributes(array('alias' => $alias));
    }

    public static function getConstId($type){
        if($type == self::industry_label){
            $tId = self::INDUSTRY;
        }elseif($type == self::buyer_label){
            $tId = self::BUYER;
        }elseif($type == self::vendor_label){
            $tId = self::VENDOR;
        }
        return $tId;
    }

    public static function getTypesLabel($types){
        $tArray = unserialize($types);
        $res = array();
        if(!empty($tArray)){
            foreach($tArray as $elem){
                $res[] = self::$types[$elem];
            }
        }
        return implode(',',$res);
    }

    public static function getCategoryNameByAlias($alias){
        $category = self::getCategoryByAlias($alias);
        return $category->name;
    }

//    public static function checked($data, $type){
//        $val = unserialize($data);
//        if(in_array($type,$val)){
//            return true;
//        }else{
//            return false;
//        }
//    }

    public function checked(){
        return unserialize($this->types);
    }
}