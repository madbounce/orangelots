<?php

/**
 * This is the model class for table "orangelots_winners".
 *
 * The followings are the available columns in table 'orangelots_winners':
 * @property integer $id
 * @property integer $user_id
 * @property integer $auction_id
 * @property integer $bid_id
 * @property string $price_per_unit
 * @property integer $qty
 * @property string $summary_price
 * @property integer $owner_auction_id
 * @property integer $is_buy_now
 */
class OrangelotsWinners extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'orangelots_winners';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, auction_id, bid_id, qty, owner_auction_id', 'numerical', 'integerOnly'=>true),
            array('price_per_unit, summary_price', 'length', 'max'=>10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, auction_id, bid_id, price_per_unit, qty, summary_price, owner_auction_id', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'bid' => array(
                self::BELONGS_TO,
                'OrangelotsBiddingOn',
                'bid_id',
                'joinType' => 'INNER JOIN',
            ),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'auction_id' => 'Auction',
            'bid_id' => 'Bid',
            'price_per_unit' => 'Price Per Unit',
            'qty' => 'Qty',
            'summary_price' => 'Summary Price',
            'owner_auction_id' => 'Owner Auction',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('user_id',$this->user_id);
        $criteria->compare('auction_id',$this->auction_id);
        $criteria->compare('bid_id',$this->bid_id);
        $criteria->compare('price_per_unit',$this->price_per_unit,true);
        $criteria->compare('qty',$this->qty);
        $criteria->compare('summary_price',$this->summary_price,true);
        $criteria->compare('owner_auction_id',$this->owner_auction_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OrangelotsWinners the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function afterSave()
    {
        parent::afterSave();
        $viewLink = yii()->createAbsoluteUrl('/auctions/orangelots-auctions/view', array("id" => $this->auction_id));
        $winner = OrangelotsUsers::model()->findByPk($this->user_id);
        $owner = OrangelotsUsers::model()->findByPk($this->owner_auction_id);
        $auction = OrangelotsAuctions::model()->findByPk($this->auction_id);

        if($this->is_buy_now == 1){
            if(isset($auction) and $auction->is_flashdeal){
                self::buyerDeal($winner, $this->auction_id, $viewLink);
                self::vendorDeal($winner, $owner, $this->auction_id, $viewLink);
            }else{
                self::sendToVendor(true, $owner, $winner, $this->auction_id, $viewLink);
            }
        }else{
            self::sendToWinner($winner, $viewLink, $this->auction_id);
        }
    }

    public static function sendToWinner($winner, $viewLink, $auId){
        $detailsLink = yii()->createAbsoluteUrl('/auctions/orangelots-auctions/bidding');
        UserModule::sendMail(
            $winner->email,
            UserModule::t("Orangelots auctions"),
            UserModule::t(
                "Dear {username},

                Congrats!\n
                You were selected as a winner in auction <a href='{link}'>Number {auNumber}</a>
                \n
                Please see details in  <a href='{detailsLink}'>your account</a>
                \n
                Thank you, \n
                Orangelots.com team",
                array(
                    '{username}' => ucfirst($winner->firstname).' '.ucfirst($winner->lastname),
                    '{auNumber}' => $auId,
                    '{link}' => $viewLink,
                    '{detailsLink}' => $detailsLink
                )
            )
        );
    }

    public static function sendToVendor($buyNow = false, $owner, $winner, $auId, $link){
        $viewLink = yii()->createAbsoluteUrl('/user/sell/auction/posted-by-me');
        if($buyNow){
            UserModule::sendMail(
                $owner->email,
                UserModule::t("Orangelots auctions"),
                UserModule::t(
                    "Dear {username},

                    Congrats!\n
                    Buyer {buyerName} made buy now on your auction <a href='{link}'>Number {auNumber}</a>
                    \n
                    Please see details in <a href='{viewLink}'>your account</a>
                    \n
                    Thank you, \n
                    Orangelots.com team",
                    array(
                        '{username}' => ucfirst($owner->firstname).' '.ucfirst($owner->lastname),
                        '{buyerName}' => ucfirst($winner->firstname).' '.ucfirst($winner->lastname),
                        '{auNumber}' => $auId,
                        '{viewLink}' => $viewLink,
                        '{link}' => $link,
                    )
                )
            );
        }else{
            UserModule::sendMail(
                $owner->email,
                UserModule::t("Orangelots auctions"),
                UserModule::t(
                    "Dear {username},

                    Congrats!\n
                    Buyer {buyerName}'s bid has won on your auction <a href='{link}'>Number {auNumber}</a>
                    \n
                    Please see details in <a href='{viewLink}'>your account</a>
                    \n
                    Thank you, \n
                    Orangelots.com team",
                    array(
                        '{username}' => ucfirst($owner->firstname).' '.ucfirst($owner->lastname),
                        '{buyerName}' => ucfirst($winner->firstname).' '.ucfirst($winner->lastname),
                        '{auNumber}' => $auId,
                        '{viewLink}' => $viewLink,
                        '{link}' => $link,
                    )
                )
            );
        }
    }

    public static function buyerDeal($winner, $auId, $viewLink){
        UserModule::sendMail(
            $winner->email,
            UserModule::t("Orangelots auctions"),
            UserModule::t(
                "Dear {username},

                Congrats!\n
                You bought a deal <a href='{viewLink}'>Number {auNumber}</a>
                \n
                Please see details in <a href='{link}'>your account</a>
                \n
                Thank you, \n
                Orangelots.com team",
                array(
                    '{username}' => ucfirst($winner->firstname).' '.ucfirst($winner->lastname),
                    '{auNumber}' => $auId,
                    '{link}' => yii()->createAbsoluteUrl('/auctions/orangelots-auctions/deal'),
                    '{viewLink}' => $viewLink,
                )
            )
        );
    }

    public static function vendorDeal($winner, $owner, $auId, $link){
        UserModule::sendMail(
            $owner->email,
            UserModule::t("Orangelots auctions"),
            UserModule::t(
                "Dear {username},

                Congrats!\n
                Buyer {buyerName} bought a deal  <a href='{link}'>Number {auNumber}</a>
                \n
                Please see details in <a href='{viewLink}'>your account</a>
                \n
                Thank you, \n
                Orangelots.com team",
                array(
                    '{username}' => ucfirst($owner->firstname).' '.ucfirst($owner->lastname),
                    '{buyerName}' => ucfirst($winner->firstname).' '.ucfirst($winner->lastname),
                    '{auNumber}' => $auId,
                    '{link}' => $link,
                    '{viewLink}' => yii()->createAbsoluteUrl('/user/sell/deals/sales')
                )
            )
        );
    }
}
