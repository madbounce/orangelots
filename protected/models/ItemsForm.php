<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ItemsForm extends CFormModel
{
    public $name;
    public $sku;
    public $length;
    public $width;
    public $height;
    public $weight;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('name, sku, length, width, height, weight', 'required'),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'verifyCode'=>'Verification Code',
        );
    }
}