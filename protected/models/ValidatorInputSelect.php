<?php
/**
 * Класс ValidatorInputSelect
 * @author dorosh_2009@meta.ua
 * @version 7.0.0
 * 
 * Модель валидации атрибута - тип select
 */
class ValidatorInputSelect extends Validator {

	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules()
	{
		$rules = array();
		
		if ($this->is_required) {
			$rules[] = array(
				'value', 
				'required',
				'message' => $this->attribute . ' обязательное для заполнения',
			);
		}
		
		$rules[] = array(
			'value', 
			'exist', 
			'className' => 'RealtyAttributeOption',
			'attributeName' => 'id',
			'allowEmpty' => !$this->is_required,
			'message' => $this->attribute . ' - не найдено в БД',
		);
		
		return $rules;
	}
	
}
