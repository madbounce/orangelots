<?php
/**
 * Класс OrangelotsInstance::
 * @author dorosh_2009@meta.ua
 * @version 8.0.0
 * 
 * Нистанция обектов
 */
class OrangelotsInstance {
	
	public static function getValidator($type)
	{
		switch ($type) {
			case OrangelotsAttribute::TYPE_INPUT_STRING:
				return new ValidatorInputString;
			
			case OrangelotsAttribute::TYPE_INPUT_INTEGER:
				return new ValidatorInputInterger;
			
			case OrangelotsAttribute::TYPE_INPUT_FLOAT:
				return new ValidatorInputFloat;
			
			case OrangelotsAttribute::TYPE_SELECT:
				return new ValidatorInputSelect;
			
			default:
			  return new ValidatorInputString;
		}
	}
	
	public static function getValue($type)
	{
		switch ($type) {
			case OrangelotsAttribute::TYPE_INPUT_STRING:
				return new OrangelotsValueString;
			
			case OrangelotsAttribute::TYPE_INPUT_INTEGER:
				return new OrangelotsValueInteger;
			
			case OrangelotsAttribute::TYPE_INPUT_FLOAT:
				return new OrangelotsValueFloat;
			
			case OrangelotsAttribute::TYPE_SELECT:
				return new OrangelotsValueSelect;
			
			case OrangelotsAttribute::TYPE_CHECKBOX:
				return new OrangelotsValueCheckbox;
			
			default:
			  return new OrangelotsValueString;
		}
	}
	
}
