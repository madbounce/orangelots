<?php

/**
 * This is the model class for table "module_news".
 *
 * The followings are the available columns in table 'module_news':
 * @property integer $id
 * @property integer $tip_category_id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $seo_keywords
 * @property string $seo_description
 * @property integer $timeCreate
 * @property integer $is_active
 */
class News extends CActiveRecord
{

	const IS_ACTIVE = 1;

	public function tableName()
	{
		return 'module_news';
	}

	public function rules()
	{
		return array(
			array('title, description, tip_category_id', 'required'),
			array('timeCreate, is_active, tip_category_id', 'numerical', 'integerOnly' => true),
			array('title, seo_keywords,alias', 'length', 'max' => 255),
			array('description, seo_description', 'length', 'max' => 1000),
			array('content', 'length', 'max' => 10000),
			array(
				'id, title, description, content, seo_keywords, seo_description, timeCreate, is_active',
				'safe',
				'on' => 'search'
			),
		);
	}

	protected function beforeValidate()
	{

		if ($this->isNewRecord && empty($this->timeCreate)) {
			$this->timeCreate = time();
		}

		$this->buildAlias();
		return parent::beforeValidate();
	}

	private function buildAlias()
	{
		if (empty($this->alias)) {
			$this->alias = $this->title;
		}

		$this->alias = TextHelper::urlSafe($this->alias);
	}


	public function relations()
	{
		return array(
			'category' => array(
				self::BELONGS_TO,
				'IndustryTipCategory',
				'tip_category_id',
				'joinType' => 'INNER JOIN',
			),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tip_category_id' => 'Category',
			'title' => 'Title',
			'description' => 'Description',
			'content' => 'Content',
			'alias' => 'Url',
			'seo_keywords' => 'Seo Keywords',
			'seo_description' => 'Seo Description',
			'timeCreate' => 'Time Create',
			'is_active' => 'Published',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('tip_category_id', $this->tip_category_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('content', $this->content, true);
		$criteria->compare('seo_keywords', $this->seo_keywords, true);
		$criteria->compare('seo_description', $this->seo_description, true);
		$criteria->compare('timeCreate', $this->timeCreate);
		$criteria->compare('is_active', $this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function getUrl()
	{
		return baseUrl() . '/news/' . $this->alias;
	}

	public function scopes()
	{
		return array(
			'active' => array(
				'condition' => 't.is_active=' . self::IS_ACTIVE,
			),
		);
	}

	public static function getNewsByCategory($catId)
	{
		return self::model()->findAllByAttributes(array('tip_category_id' => $catId));
	}
}
