<?php
/**
 * Класс OrangelotsAttributeOption
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 * 
 * @property integer $id						-- id 
 * @property integer $attribute_id				-- id атрибута
 * @property integer $value						-- значение
 */
class OrangelotsAttributeOption extends CActiveRecord
{

	public static function model($className = __CLASS__) 
	{ 
		return parent::model($className);
	}
	
	public function tableName() 
	{ 
		return 'module_orangelots_attribute_option';
	}	
	
	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules()
	{
		return array(
			array(
				'attribute_id, value',
				'required',
			),
		);
	}

	/**
	 * Взаимосвязи
	 * @return array
	 */
	public function relations()
	{
		return array(
			'attribute' => array(self::BELONGS_TO, 'OrangelotsAttribute', 'attribute_id', 'together'=>true),
		);
	}
}