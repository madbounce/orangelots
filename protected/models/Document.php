<?php

class Document extends CFormModel {
    public $document;
    public function rules() {
        return array(
            array('document', 'required'),
//            array('document','file','types'=>'xls,xlsx'),
            array('document','file','types'=>'jpg'),
        );
    }

    public function attributeLabels() {
        return array(
            'document' => 'Document',
        );
    }

}