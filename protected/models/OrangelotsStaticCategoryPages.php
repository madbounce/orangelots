<?php

/**
 * This is the model class for table "orangelots_static_category_pages".
 *
 * The followings are the available columns in table 'orangelots_static_category_pages':
 * @property integer $id
 * @property string $category_description
 * @property string $page_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $seo_text
 * @property string $name
 * @property string $alias
 * @property string $url
 */
class OrangelotsStaticCategoryPages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orangelots_static_category_pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_title, meta_description, meta_keywords, name, alias', 'length', 'max'=>255),
			array('category_description', 'safe'),
			array('url', 'unique'),
			array('url', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_description, page_title, meta_description, meta_keywords, seo_text, name, alias', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_description' => 'Category Description',
			'page_title' => 'Page Title',
			'meta_description' => 'Meta Description',
			'meta_keywords' => 'Meta Keywords',
			'seo_text' => 'Seo Text',
			'name' => 'Name',
			'alias' => 'Url',
			'url' => 'URL',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_description',$this->category_description,true);
		$criteria->compare('page_title',$this->page_title,true);
		$criteria->compare('meta_description',$this->meta_description,true);
		$criteria->compare('meta_keywords',$this->meta_keywords,true);
		$criteria->compare('seo_text',$this->seo_text,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrangelotsStaticCategoryPages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function getCategoryByUrl($url){
        return self::model()->findByAttributes(array('url' => $url));
    }
}
