<?php

/**
 * Класс Orangelots
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 *
 * Модель данных категорий статей
 * Описание атрибутов 'module_orangelots':
 * @property integer $id                        -- порядковый номер
 * @property integer $category_id                -- категория
 * @property string $title                        -- название
 */
class Orangelots extends CActiveRecord
{
	private $_attr = array();
	public $post_attributes;
	public $current_category_id;
	public $current_is_active;

	public $delete_photo;

	public $file;

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Ассоциированная таблица
	 * @return string
	 */
	public function tableName()
	{
		return 'module_orangelots';
	}

	protected function beforeValidate()
	{
		if ($this->isNewRecord && empty($this->user_id)) {
			$this->user_id = Yii::app()->user->id;
		}
		if ($this->isNewRecord && empty($this->timeCreate)) {
			$this->timeCreate = time();
		}

		$this->buildAlias();
		return parent::beforeValidate();
	}


	/**
	 * Получить правила валидации
	 * @return array
	 */
	public function rules()
	{
		return array(
//			array('title,school_id', 'required',),
			array('title', 'required',),
			/*comments snitka*/
//			array('phone', 'add_realty'),
//			array('user_id, is_active,is_archive,school_id', 'numerical', 'integerOnly' => true,),
			array('user_id, is_active, orange_category', 'numerical', 'integerOnly' => true,),
//			array('title, alias, address', 'length', 'max' => 255,),
			array('title, alias', 'length', 'max' => 255,),
			array('price', 'length', 'max' => 100,),
			array('description', 'length', 'max' => 2000,),
			array('content', 'length', 'max' => 10000,),
			array('id', 'safe', 'on' => 'search',),
			array('post_attributes', 'validateAttributes',),
			array('post_attributes, seo_keywords, seo_description, name', 'safe',),
			array(
				'category_id',
				'exist',
				'className' => 'OrangelotsKind',
				'attributeName' => 'id',
			),
			array(
				'user_id',
				'exist',
				'className' => 'User',
				'attributeName' => 'id',
			),
			/*array(
				'city_id',
				'exist',
				'className' => 'City',
				'attributeName' => 'id',
			),*/
			/*array(
				'token',
				'length',
				'max' => 32,
			),*/
			array('file', 'file', 'types' => 'jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG', 'allowEmpty' => true),
		);
	}

	public function afterFind()
	{
		$this->current_category_id = $this->category_id;
		$this->current_is_active = $this->is_active;

		return parent::afterFind();
	}


	public function validateAttributes()
	{
		$errors = array();
		$attrIds = array();
		if (!empty($this->post_attributes)) {
			foreach ($this->post_attributes as $attr_id => $attr_val) {
				$attrIds[] = $attr_id;
			}

			$this->_attr = OrangelotsAttribute::model()->findAllByPk($attrIds, array('index' => 'id'));
			foreach ($this->_attr as $attribute) {
				$validator = OrangelotsInstance::getValidator($attribute->type);
				$validator->attribute = $attribute->title;
				$validator->value = $this->post_attributes[$attribute->id];
				$validator->is_required = $attribute->is_required;
				if (!$validator->validate()) {
					$this->addError($attribute->id, $validator->getErrMessage());
				}
			}
		}
	}

	/**
	 * Взаимосвязи
	 * @return array
	 */
	public function relations()
	{
		return array(
			'Schools' => array(
				self::BELONGS_TO,
				'Schools',
				'school_id',
			),
			'user' => array(self::BELONGS_TO, 'User', 'user_id', 'together' => true),
			'city' => array(self::BELONGS_TO, 'City', 'city_id', 'together' => true),
			'category' => array(self::BELONGS_TO, 'OrangelotsKind', 'category_id', 'together' => true),
			'galleryPhotos' => array(self::HAS_MANY, 'OrangelotsPhoto', 'realty_id', 'order' => '`rank` ASC',),
			'valuesSelect' => array(
				self::HAS_MANY,
				'OrangelotsValueSelect',
				'realty_id',
				'with' => array('attribute', 'option'),
				'index' => 'attribute_id'
			),
			'valuesString' => array(
				self::HAS_MANY,
				'OrangelotsValueString',
				'realty_id',
				'with' => array('attribute'),
				'index' => 'attribute_id'
			),
			'valuesInteger' => array(
				self::HAS_MANY,
				'OrangelotsValueInteger',
				'realty_id',
				'with' => array('attribute'),
				'index' => 'attribute_id'
			),
			'valuesFloat' => array(
				self::HAS_MANY,
				'OrangelotsValueFloat',
				'realty_id',
				'with' => array('attribute'),
				'index' => 'attribute_id'
			),
			'valuesCheckbox' => array(
				self::HAS_MANY,
				'OrangelotsValueCheckbox',
				'realty_id',
				'with' => array('attribute'),
				'index' => 'attribute_id'
			),
		);
	}

	/**
	 * Названия атрибутов
	 * @return array
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'title' => 'Title',
			'price' => 'Price',
			'content' => 'Content',
			'city_id' => 'City',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'district_id' => 'District',
			'is_active' => 'Active',
			'is_archive' => 'Archive',
			'is_webinar' => 'Webinar',
			'is_main' => 'Show On Main Page',
			'description' => 'Description',
			'address' => 'Address',
			'image_extension' => 'Image Extension',
		);

	}

	public function beforeSave()
	{
		if (empty($this->file) == false) {
			$this->image_extension = $this->file->getExtensionName();
		}

		return parent::beforeSave();
	}

	/**
	 * Выполняем ряд обязательных действий после сохранения модели
	 * @return boolean -- результат выполнения действия
	 */
	protected function afterSave()
	{
		$this->saveAddtAttributes();

		if ($this->delete_photo) {
			$this->_deleteImage();
		}

		if (empty($this->file) == false) {
			$this->file->saveAs($this->getImage_directory(true) . '/origin.' . $this->image_extension);
		}


//		Yii::app()->cache->delete(self::CACHE_KEY_WIDGET_OPTIONS . $this->id);

		return parent::afterSave();
	}

	public function getPreview()
	{
		return $this->getThumb(300, 200, 'resize');
	}


	/*function savePhotosByToken()
	{
		if (empty($this->token) == false) {
			$dir = Yii::app()->basePath . '/../photos/temp/' . $this->token;

			if (is_dir($dir)) {
				$files = scandir($dir);
				if (is_array($files)) {
					foreach ($files as $file) {
						if ($file != '.' && $file != '..') {
							$realtyPhoto = new RealtyPhoto;
							$realtyPhoto->category_id = $this->id;
							$realtyPhoto->filePath = Yii::app(
								)->basePath . '/../photos/temp/' . $this->token . '/' . $file;
							$realtyPhoto->save();
						}
					}
				}
			}
		}
	}*/

	/**
	 * Сохраняем дополнительные атрибуты
	 */
	private function saveAddtAttributes()
	{
		if ($this->scenario == 'updateAttributes') {
            OrangelotsValueSelect::model()->deleteAllByAttributes(array('realty_id' => $this->id));
            OrangelotsValueString::model()->deleteAllByAttributes(array('realty_id' => $this->id));
            OrangelotsValueInteger::model()->deleteAllByAttributes(array('realty_id' => $this->id));
            OrangelotsValueFloat::model()->deleteAllByAttributes(array('realty_id' => $this->id));
            OrangelotsValueCheckbox::model()->deleteAllByAttributes(array('realty_id' => $this->id));
		}

		if (!empty($this->post_attributes)) {
			foreach ($this->post_attributes as $attr_id => $attr_val) {
				if (trim($attr_val) == '') {
					continue;
				}
				$realtyValue = OrangelotsInstance::getValue($this->_attr[$attr_id]->type);
				$realtyValue->realty_id = $this->id;
				$realtyValue->attribute_id = $attr_id;
				if ($this->_attr[$attr_id]->type == OrangelotsAttribute::TYPE_SELECT) {
					$realtyValue->option_id = $attr_val;
				} else {
					if ($this->_attr[$attr_id]->type != OrangelotsAttribute::TYPE_CHECKBOX) {
						$realtyValue->value = $attr_val;
					}
				}
				$realtyValue->save();
			}
		}
	}

	/**
	 * Создаем алиас к тайтлу
	 */
	private function buildAlias()
	{
		if (empty($this->alias)) {
			$this->alias = $this->title;
		}

		$this->alias = TextHelper::urlSafe($this->alias);
	}

	/**
	 * Поиск
	 * @return CActiveDataProvider
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('title', $this->title, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 50,
			),
		));
	}

	public function searchRealtor()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('category_id', $this->category_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('user_id', $this->user_id, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination' => array(
				'pageSize' => 50,
			),
		));
	}

	public function getDataAttributes()
	{
		if (Yii::app()->request->isPostRequest) {
			return $this->post_attributes;
		} else {
			if ($this->isNewRecord) {
				return array();
			} else {
				$data = array();

				foreach ($this->valuesSelect as $value) {
					$data[$value->attribute->id] = $value->option_id;
				}

				foreach ($this->valuesString as $value) {
					$data[$value->attribute->id] = $value->value;
				}

				foreach ($this->valuesInteger as $value) {
					$data[$value->attribute->id] = $value->value;
				}

				foreach ($this->valuesFloat as $value) {
					$data[$value->attribute->id] = $value->value;
				}

				foreach ($this->valuesCheckbox as $value) {
					$data[$value->attribute->id] = 1;
				}

				return $data;
			}
		}
	}


	public function getImage_directory($mkdir = false)
	{
		if ($mkdir == false) {
			return Yii::app()->basePath . '/../photos/realty/' . $this->id;
		} else {
			$directory = Yii::app()->basePath . '/../photos/realty/' . $this->id;
			if (file_exists($directory) == false) {
				mkdir($directory);
				chmod($directory, 0777);
			}

			return $directory;
		}
	}

	public function getThumb($width = null, $height = null, $mode = 'origin')
	{
		$dir = $this->getImage_directory(true) . '/';
		$originFile = $dir . 'origin.' . $this->image_extension;

		if (!is_file($originFile)) {
			return false;
		}

		if ($mode == 'origin') {
			return '/photos/realty/' . $this->id . '/origin.' . $this->image_extension;
		}

		$fileName = $mode . '_w' . $width . '_h' . $height . '.' . $this->image_extension;
		$filePath = $dir . $fileName;
		if (!is_file($filePath)) {
			if ($mode == 'resize') {
				Yii::app()->image->load($originFile)->resize($width, $height)->save($filePath);
			} else {
				Yii::app()->image->cropSave($originFile, $width, $height, $filePath);
			}
		}

		return '/photos/realty/' . $this->id . '/' . $fileName;
	}

	public function getUrl()
	{
		return baseUrl() . '/real-estate/' . $this->category->alias . '/' . $this->alias;
	}

	public function add_realty()
	{
		if (!Yii::app()->user->isGuest && empty(Yii::app()->user->model->phone) && empty($this->phone)) {
			$this->addError('phone', "Введите номер телефона");
		}
	}

	public function getAll()
	{
		$criteria = new CDbCriteria();
		$criteria->condition = 't.is_active=1';
		return self::count($criteria);
	}

	public function getTag()
	{
		if (!$this->isNewRecord && $this->id) {
			$criteria = new CDbCriteria();
			$criteria->compare('program_id', $this->id);

			$newsTags = OrangelotsTag::model()->findAll($criteria);
			$tag = array();
			foreach ($newsTags as $newsTag) {
				$tag[] = $newsTag->tag->title;
			}
			return implode(', ', $tag);
		} else {
			return false;
		}
	}

	public function setTag($value)
	{
		$this->tag = $value;
	}

	public function getTags_title()
	{
		if (Yii::app()->request->isPostRequest) {
			return $_POST['Realty']['tag'];
		}

		if (empty($this->tags)) {
			return '';
		}
		$tags = array();

		foreach ($this->tags as $tag) {
			$tags[] = $tag->tag->title;
		}

		return implode(', ', $tags);
	}

	public function getRealtyTitle()
	{
		$result = UNDEFINED;
		if (!empty($this->category_id)) {
			$category = OrangelotsKind::model()->findByPk($this->category_id);
			if ($category) {
				$result = $category->title;
			}
		}
		return $result;
	}

	public function getCategoryById($id)
	{
		$model = self::findByPk($id);
		return $model->title;
	}
}