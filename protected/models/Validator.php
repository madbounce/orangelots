<?php
/**
 * Класс Validator
 * @author dorosh_2009@meta.ua
 * @version 7.0.0
 * 
 * Модель валидации атрибута
 */
class Validator extends CFormModel {
	
	/**
	 * Атрибут
	 * @var string
	 */
	public $is_required = false;
	public $attribute;
	public $value;
	
	public function getErrMessage()
	{
		$err = '';
		if ($this->hasErrors()) {
			foreach ($this->errors as $error) {
				$err .= ' ' . $error[0];
			}
		}
		return $err;
 	}
	
}
