<?php

/**
 * This is the model class for table "orangelots_invoices".
 *
 * The followings are the available columns in table 'orangelots_invoices':
 * @property integer $id
 * @property integer $paypal_id
 * @property string $email
 * @property string $transaction_id
 * @property string $email_to
 * @property string $phone
 * @property string $phone_to
 * @property string $amount
 * @property float $fee
 * @property integer $auction_deal_id
 * @property string $transaction_date
 * @property integer $purchase_date
 * @property integer $withdraw_date
 * @property integer $payed
 * @property string $to_who
 * @property string from_who
 * @property integer pay_type
 * @property integer winner_table_id
 * @property string admin_comment
 * @property string vendor_comment
 */
class OrangelotsInvoices extends CActiveRecord
{
     public $col;

    const PAYED = 1;
    const NOT_PAYED = 0;
    const STATUS_CANCELLED = -1;
    const TEXT_CANCELLED = 'Cancelled';
    const WITHDRAW = 2;

    const PAY_WITH_PAYPAL = 1;
    const PAY_WITH_BANK = 2;

    static $statusesText = array(
        self::STATUS_CANCELLED => self::TEXT_CANCELLED,
        self::NOT_PAYED => 'Not paid',
        self::PAYED => 'Paid',
        self::WITHDRAW => 'Withdraw'
    );

    static $adminStatuses = array(
        self::STATUS_CANCELLED => self::TEXT_CANCELLED,
        self::PAYED => 'Paid',
        self::WITHDRAW => 'Withdraw'
    );

    static $payTypeText = array(
        self::NOT_PAYED => 'Not chosen',
        self::PAY_WITH_BANK => 'Bank wire',
        self::PAY_WITH_PAYPAL => 'PayPal'
    );

    const SELL = 1;
    const BUY = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'orangelots_invoices';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('auction_deal_id, payed, pay_type, winner_table_id, withdraw_date', 'numerical', 'integerOnly'=>true),
            array('email, email_to, phone, phone_to, transaction_date, from_who, to_who, transaction_id', 'length', 'max'=>255),
            array('amount', 'length', 'max'=>10),
            array('fee', 'numerical'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, winner_table_id, transaction_id, paypal_id, email, email_to, phone, admin_comment, phone_to, amount, fee, vendor_comment, auction_deal_id, transaction_date, purchase_date, withdraw_date, to_who, payed, pay_type', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'auction_deal' => array(
                self::BELONGS_TO,
                'OrangelotsAuctions',
                'auction_deal_id',
                'joinType' => 'INNER JOIN',
            ),
            'winnerTable' => array(
                self::BELONGS_TO,
                'OrangelotsWinners',
                'winner_table_id',
                'joinType' => 'INNER JOIN',
            ),
            'owner' => array(
                self::BELONGS_TO,
                'OrangelotsUsers',
                'owner_id',
                'joinType' => 'INNER JOIN',
            ),
            'winner' => array(
                self::BELONGS_TO,
                'OrangelotsUsers',
                'winner_id',
                'joinType' => 'INNER JOIN',
            )
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'transaction_id' => 'Transaction ID',
            'paypal_id' => 'Paypal',
            'email' => 'Email',
            'email_to' => 'Email',
            'phone' => 'Phone',
            'phone_to' => 'Phone',
            'amount' => 'Amount',
            'auction_deal_id' => 'Auction Deal',
            'transaction_date' => 'Payment Date',
            'withdraw_date' => 'Withdraw Date',
            'purchase_date' => 'Purchase Date',
            'winner_table_id' => 'Winner Table Id',
            'payed' => 'Paid'
        );
    }

    public function userFilter($data, $dateFrom, $dateTo, $seller = false){
        $criteria=new CDbCriteria;
        if(!$seller){
            $criteria->compare('winner_id', user()->id);
        }else{
            $criteria->compare('owner_id', user()->id);
        }

        if(!empty($data['transaction_id'])){
            $criteria->addCondition('transaction_id = "' . $data['transaction_id'].'"');
        }
        if(!empty($data['paypal_id'])){
            $criteria->addCondition('paypal_id = "' . $data['paypal_id'].'"');
        }
        if(!empty($data['auction_deal_id'])){
            $criteria->addCondition('auction_deal_id = ' . $data['auction_deal_id']);
        }
        if(isset($data['payed']) && $data['payed'] != ''){
            $payed = (int)$data['payed'] - 2;
            $criteria->addCondition('payed = ' . $payed);
        }
        if(!empty($dateFrom)){
            $dateFrom = getTime($dateFrom);
            $criteria->addCondition('transaction_date >='.$dateFrom);
        }
        if(!empty($dateTo)){
            $dateTo = getTime($dateTo);
            $dateTo += 24*60*60;
            $criteria->addCondition('transaction_date <='.$dateTo);
        }

        if(!empty($data['from_who'])){
            $text = explode(',', $data['from_who']);
            foreach($text as $key=>$string){
                $string = trim($string);
                $criteria->addCondition('from_who LIKE "'.$string.'%" or email LIKE "'.$string.'%" or phone LIKE "'.$string.'%"');
            }
        }elseif(!empty($data['to_who'])){
            $text = explode(',', $data['to_who']);
            foreach($text as $key=>$string){
                $string = trim($string);
                $criteria->addCondition('to_who LIKE "'.$string.'%" or email_to LIKE "'.$string.'%" or phone_to LIKE "'.$string.'%"');
            }
        }

        $criteria->order = 'payed ASC, purchase_date DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id',$this->id);
        $criteria->compare('transaction_id',$this->transaction_id);
        $criteria->compare('paypal_id',$this->paypal_id);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('email_to',$this->email_to,true);
        $criteria->compare('phone',$this->phone,true);
        $criteria->compare('phone_to',$this->phone_to,true);
        $criteria->compare('amount',$this->amount,true);
        $criteria->compare('fee',$this->fee,true);
        $criteria->compare('auction_deal_id',$this->auction_deal_id);
        $criteria->compare('transaction_date',$this->transaction_date,true);
        $criteria->compare('purchase_date',$this->purchase_date,true);
        $criteria->compare('withdraw_date',$this->withdraw_date,true);
        $criteria->compare('to_who',$this->to_who);
        $criteria->compare('from_who',$this->from_who);
        $criteria->compare('owner_id',$this->owner_id);
        $criteria->compare('winner_id',$this->winner_id);
        $criteria->compare('payed',$this->payed);
        $criteria->compare('pay_type',$this->pay_type);
        $criteria->compare('winner_table_id',$this->winner_table_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,

        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return OrangelotsInvoices the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function winnerPayments($userId){

        $criteria=new CDbCriteria;
        $criteria->compare('winner_id',$userId);
        $criteria->order = 'payed ASC, purchase_date DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

    public function vendorPayments($userId, $transaction = null){
        $criteria=new CDbCriteria;
        $criteria->compare('owner_id',$userId);
        if(!empty($transaction)){
            $criteria->compare('transaction_id',$transaction);
        }
       // $criteria->order = 'payed ASC, purchase_date DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 10,
            ),
        ));
    }

    public function getWithdrawals($userId){
        $criteria=new CDbCriteria;
        $criteria->compare('owner_id',$userId);
        $criteria->compare('payed', OrangelotsInvoices::WITHDRAW);
        $criteria->order = 'transaction_date DESC, purchase_date DESC';

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function createInvoice($userInfo, $sellerInfo, $price, $auDealId, $winnerTableId){
        $invoice = new self;
        $invoice->email = $sellerInfo->email;
        $invoice->email_to = $userInfo->email;
        $invoice->phone = $sellerInfo->phone;
        $invoice->phone_to = $userInfo->phone;
        $invoice->from_who = $sellerInfo->firstname.' '. $sellerInfo->lastname;
        $invoice->to_who = $userInfo->firstname.' '. $userInfo->lastname;
        $invoice->auction_deal_id = $auDealId;
        $invoice->owner_id =  $sellerInfo->id;
        $invoice->winner_id = $userInfo->id;
        $invoice->amount = $price;
        $invoice->purchase_date = time();
        $invoice->winner_table_id = $winnerTableId;
        if($invoice->save()){
            do {
                $transaction_id = $invoice->id.randString(rand(4,10));
                $check = self::model()->findByAttributes(array('transaction_id' => $transaction_id));
            } while ($check !== null);
            $invoice->transaction_id = $transaction_id;
            $invoice->save();
            return $invoice->id;
        }else{
            return false;
        }
    }

    public static function getBuyerAddress($data){
        $string = '';
        $state = OrangelotsStates::getStateCodeById($data->state);
        if(!empty($data->city)){
            $string .=  $data->city.', ';
        }
        if(!empty($state)){
            $string .= $state;
        }
        if(!empty($data->zipcode)){
            $string .= ' '.$data->zipcode;
        }
        $string .= ', USA';
        return $string;
    }

    public function cancel(){
        UserModule::sendMail(
            $this->email_to,
            UserModule::t("Orangelots invoice"),
            UserModule::t(
                "Dear {username},

                <p>Your invoice № <b>{invoiceId}</b> has been cancelled.\n\n</p>

                Thank you, \n
                Orangelots.com team",
                array(
                    '{username}' => ucfirst($this->to_who),
                    '{invoiceId}' => $this->id,
                )
            )
        );
    }

    public function payed($admin= false){
        UserModule::sendMail(
            $this->email_to,
            UserModule::t("Orangelots invoice"),
            UserModule::t(
                "Dear {username},

                <p>Your invoice № <b>{invoiceId}</b> id payed.\n\n</p>

                Thank you, \n
                Orangelots.com team",
                array(
                    '{username}' => ucfirst($this->to_who),
                    '{invoiceId}' => $this->id,
                )
            )
        );
        if($admin){
            UserModule::sendMail(
                $this->email,
                UserModule::t("Orangelots invoice"),
                UserModule::t(
                    "Dear {username},

                    <p>Your invoice № <b>{invoiceId}</b> is payed by buyer.\n\n</p>
                    Thank you,\n
                    Orangelots.com team",
                    array(
                        '{username}' => ucfirst($this->from_who),
                        '{invoiceId}' => $this->id,
                    )
                )
            );
        }
    }

    public function adminActions(){
        if(!empty($this->transaction_date)){
            $this->transaction_date = getTime($this->transaction_date);
        }else{
            $this->transaction_date = time();
        }
        if($this->payed == self::STATUS_CANCELLED){
            $this->cancel();
        }elseif($this->payed == self::PAYED){
            $this->payed(true);
        }elseif($this->payed == self::WITHDRAW){
            if(!empty($this->withdraw_date)){
                $this->withdraw_date = getTime($this->withdraw_date);
            }
            $this->setWithdraw();
        }
        $this->save();

    }

    public function setWithdraw(){
        UserModule::sendMail(
            $this->email,
            UserModule::t("Orangelots invoice"),
            UserModule::t(
                "Dear {username},\n\n
                <p>Thank you for using our service.</p>\n\n

                <p>{amount} for {invoiceId} was send to you via bank wire.</p>
                \n\n
                Best regards,\n
                Orangelots.com team",
                array(
                    '{username}' => ucfirst($this->from_who),
                    '{amount}' => $this->amount,
                    '{invoiceId}' => $this->id,
                )
            )
        );
    }

    public static function getFee($amount,$type){
        if($type==self::PAY_WITH_PAYPAL){
            $percent = param('payPalProcessingFee') + param('payPalServiceFee');
        }elseif($type ==self::PAY_WITH_BANK){
            $percent = param('bankWireServiceFee');
            $fixSum =  param('bankWireSumFee');
        }
        $comission = $amount*$percent/100;
        if(!empty($fixSum)){
            $comission += $fixSum;
        }
        return $comission;
    }

    public static function checkCanCancel($payed){
        if($payed == self::NOT_PAYED){
            return true;
        }else{
            return false;
        }
    }

    public static function arrayForFilter(){
        return array(
            self::STATUS_CANCELLED +2 => self::TEXT_CANCELLED,
            self::NOT_PAYED +2 => 'Not paid',
            self::PAYED +2 => 'Paid',
            self::WITHDRAW +2 => 'Withdraw'
        );
    }

    public static function payStatusBuyer($status){
        if($status >= self::WITHDRAW){
            return self::$statusesText[self::PAYED];
        }else{
            return self::$statusesText[$status];
        }
    }

    public static function getOptionSelected($type){
        if($type == self::SELL){
            if(!empty($_SESSION['filter_sell']['payed'])){
                return array($_SESSION['filter_sell']['payed'] =>array('selected'=>true));
            }
        }elseif($type == self::BUY){
            if(!empty($_SESSION['filter_sell']['payed'])){
                return array($_SESSION['filter_buy']['payed'] =>array('selected'=>true));
            }
        }
    }

    public static function getPayPalTransactionDate($data){
        if(!empty($data->paypal_id) and !empty($data->transaction_date)){
            $str = $data->paypal_id."<br>".date('m/d/Y h:ma',$data->transaction_date);
        }else{
            $str = '';
        }
        return $str;
    }

}
