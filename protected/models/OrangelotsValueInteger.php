<?php
/**
 * Класс OrangelotsValueInteger
 * @author dorosh_2009@meta.ua
 * @copyright Aiken Interactive 2013
 * @version 8.0.0
 * 
 * @property integer $realty_id_id					-- id обьявления
 * @property integer $attribute_id				-- id атрибута
 * @property integer $value						-- значение
 */
class OrangelotsValueInteger extends CActiveRecord
{

	public static function model($className = __CLASS__) 
	{ 
		return parent::model($className);
	}
	
	public function tableName() 
	{ 
		return 'module_orangelots_value_integer';
	}	
	
	/**
	 * Правила валидации
	 * @return array
	 */
	public function rules()
	{
		return array(
			array(
				'attribute_id, attribute_id',
				'required',
			),
			array(
				'value',
				'numerical',
				'integerOnly' => true,
			),		
		);
	}

	/**
	 * Взаимосвязи
	 * @return array
	 */
	public function relations()
	{
		return array(
			'attribute' => array(self::BELONGS_TO, 'OrangelotsAttribute', 'attribute_id', 'together'=>true),
			'realty_id' => array(self::BELONGS_TO, 'Orangelots', 'realty_id_id', 'together'=>true),
		);
	}
}