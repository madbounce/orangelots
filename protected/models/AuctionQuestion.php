<?php

/**
 * This is the model class for table "orangelots_auction_question".
 *
 * The followings are the available columns in table 'orangelots_auction_question':
 * @property integer $id
 * @property integer $user_id
 * @property integer $owner_id
 * @property integer $auction_id
 * @property integer $date_create;
 * @property integer $date_answer;
 * @property integer $archive;
 * @property string $subject
 * @property string $question
 * @property string $answer
 *
 */
class AuctionQuestion extends CActiveRecord
{
    const ARCHIVE = 1;
    const NOT_ARCHIVE = 0;
    const MY_QUESTIONS = 2;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'orangelots_auction_question';
    }

    public function rules()
    {
        return array(
            array('auction_id, owner_id, subject, question', 'required'),
            array('user_id, auction_id, owner_id, date_create, date_answer, archive', 'numerical', 'integerOnly' => true),
            array('subject, user_name', 'length', 'max' => 255),
            array('user_name, ', 'required', 'on' => 'guestQuestion'),
            array('answer', 'required', 'on' => 'answerQuestion'),
            array('id, user_name, auction_id, owner_id, subject, question, user_id, answer, archive', 'safe'),
        );
    }

    public function relations()
    {
        return array(
            'auction' => array(
                self::BELONGS_TO,
                'OrangelotsAuctions',
                'auction_id',
                'joinType' => 'INNER JOIN',
            ),
            'owner' => array(
                self::BELONGS_TO,
                'OrangelotsUsers',
                'owner_id',
                'joinType' => 'INNER JOIN',
            ),
            'user' => array(
                self::BELONGS_TO,
                'OrangelotsUsers',
                'user_id',
                'joinType' => 'INNER JOIN',
            ),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'user_name' => 'User Name',
            'subject' => 'Subject',
            'user_id' => 'User Id',
            'owner_id' => 'Owner id',
            'auction_id' => 'Auction Id',
            'answer' => 'Answer',
            'question' => 'Question',
            'date_create' => 'Ask date',
            'date_answer' => 'Answer date',
            'archive' => 'Archive'
        );
    }

    public function UserQuestions($userId, $type = null){

        $criteria=new CDbCriteria;
        $criteria->compare('owner_id',$userId);
        if(isset($type)){
            $criteria->compare('archive',$type);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function getUserQuestions($userId){
        $criteria=new CDbCriteria;
        $criteria->compare('user_id',$userId);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function getAuctionQuestions($auctionId){
        return self::model()->findAllByAttributes(array('auction_id' => $auctionId));
    }

    public static function getModel($id){
        return self::model()->findByPk($id);
    }

    public static function getNewUserQuestions($userId = null){
        if(empty($userId)){
            $userId = user()->id;
        }
        return self::model()->findAllByAttributes(array('owner_id' => $userId, 'archive' => self::NOT_ARCHIVE));
    }

    public static function getShow($data){
        if($data->user_id == user()->id){
            return self::MY_QUESTIONS;
        }elseif($data->archive == self::ARCHIVE){
            return self::ARCHIVE;
        }else{
            return self::NOT_ARCHIVE;
        }
    }
}