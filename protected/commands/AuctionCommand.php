<?php
yii::import('application.modules.user.models.*');
class AuctionCommand extends CConsoleCommand
{
    public $quantity = 0;
    public $rest = 0;

    public function actionAuction()
    {
        $query = Yii::app()->db->createCommand('SELECT * FROM module_orangelots_auctions WHERE status = 1')->queryAll();
        $array_id = array();
        if (!empty($query)) {
            foreach ($query AS $key => $value) {
                if ($value['end_date_unix'] <= time()) {
                    $array_id[] = $value['id'];
                }
            }
            if (!empty($array_id)) {
                foreach ($array_id AS $id) {
                    $query = Yii::app()->db->createCommand(
                        'UPDATE module_orangelots_auctions SET status = 2 WHERE id =' . $id
                    )->execute();

                    /*Notification*/
                    $auctions_notify = OrangelotsAuctions::model()->findByPk($id);
                    $notification = new OrangelotsNotifications();
                    $notification->user_id =$auctions_notify->user_id;
                    $notification->message_id = OrangelotsNotifications::YOU_AUCTION_IS_OVER;
                    $notification->auction_id = $auctions_notify->id;
                    if($auctions_notify->is_flashdeal){
                        $notification->content = "You deal <a href = '".Yii::app()->request->hostInfo.Yii::app()->request->baseUrl.'/auctions/orangelots-auctions/posted-by-me/'.$auctions_notify->id."'>".$auctions_notify->name."</a>  is over - ".OrangelotsBiddingOn::model()->count(new CDbCriteria(array('condition' => 'id_auction = :id_auction','params' => array(':id_auction'=>$auctions_notify->id))))." bids arrived ";
                    }else{
                        $notification->content = "You auction <a href = '".Yii::app()->request->hostInfo.Yii::app()->request->baseUrl.'/auctions/orangelots-auctions/sell-deal/'.$auctions_notify->id."'>".$auctions_notify->name."</a>  is over - ".OrangelotsBiddingOn::model()->count(new CDbCriteria(array('condition' => 'id_auction = :id_auction','params' => array(':id_auction'=>$auctions_notify->id))))." bids arrived ";
                    }
                    $notification->save();
                    /*Notification*/

                    $auctions = Yii::app()->db->createCommand(
                        'SELECT * FROM module_orangelots_auctions WHERE id =' . $id
                    )->queryRow();
                    if (isset($auctions)) {
                        $this->quantity = $auctions['units_in_lot'];
                    }

                    $bidders = Yii::app()->db->createCommand(
                        'SELECT * FROM orangelots_bidding_on WHERE id_auction = ' . $id . ' AND buy_now <> 1 ORDER BY price_per_unit DESC'
                    )->queryAll();

                    if (isset($bidders)) {
                        $owner = Yii::app()->db->createCommand('SELECT * FROM users WHERE id ='.$auctions_notify->user_id)->queryRow();
                        $ownerObj = OrangelotsUsers::model()->findByPk($auctions_notify->user_id);
                        foreach ($bidders AS $key => $value) {
                            $winner = Yii::app()->db->createCommand('SELECT * FROM users WHERE id ='.$value['id_user'])->queryRow();
                            $winnerObj = OrangelotsUsers::model()->findByPk($value['id_user']);
                            if ($value['qty'] <= $this->quantity) {
                                $this->quantity = $this->quantity - $value['qty'];
                                $winnerTable = new OrangelotsWinners();
                                $winnerTable->user_id = $value['id_user'];
                                $winnerTable->auction_id = $value['id_auction'];
                                $winnerTable->price_per_unit = $value['price_per_unit'];
                                $winnerTable->qty = $value['qty'];
                                $winnerTable->summary_price = $value['qty'] * $value['price_per_unit'];
                                $winnerTable->bid_id = $value['id'];
                                $winnerTable->owner_auction_id = $auctions['user_id'];
                                $winnerTable->save();
                                if (isset($winner)) {
                                    $invoice = new OrangelotsInvoices();
                                    $invoice->createInvoice($winnerObj, $ownerObj, $value['qty'] * $value['price_per_unit'], $auctions['id'], $winnerTable->id);
                                }
                                $notification = new OrangelotsNotifications();
                                $notification->user_id = $winner["id"];
                                $notification->message_id = OrangelotsNotifications::YOU_SELECTED_AS_WINNER;
                                $notification->auction_id = $auctions['id'];
                                $notification->content = "Vendor selected you as winner for <a href = '".Yii::app()->request->hostInfo.Yii::app()->request->baseUrl.'/auctions/orangelots-auctions/view/'.$auctions['id']."'>".$auctions['name']."</a>  ";
                                $notification->save();

                            } else {
                                if (($value['qty'] > $this->quantity) && ($this->quantity != 0)) {
                                    $summary_price = $this->quantity * $value['price_per_unit'];
                                    $winnerTable = new OrangelotsWinners();
                                    $winnerTable->user_id = $value['id_user'];
                                    $winnerTable->auction_id = $value['id_auction'];
                                    $winnerTable->price_per_unit = $value['price_per_unit'];
                                    $winnerTable->qty = $this->quantity;
                                    $winnerTable->summary_price = $summary_price;
                                    $winnerTable->bid_id = $value['id'];
                                    $winnerTable->owner_auction_id = $auctions['user_id'];
                                    $winnerTable->save();
                                    if (isset($winner)) {
                                        $invoice = new OrangelotsInvoices();
                                        $invoice->createInvoice($winnerObj, $ownerObj, $value['qty'] * $value['price_per_unit'], $auctions['id'], $winnerTable->id);
                                    }
                                    $this->quantity = 0;
                                }
                            }
                        }
                        /*$file_name = '/home/vsnit/www/cron_log.txt';
                        $myfile = fopen($file_name, "a") or die("Unable to open file!");
                        $txt = yii()->dateFormatter->format('yyyy/MM/dd HH:mm:ss', time())." Крон сработал бл!\n";
                        fwrite($myfile, $txt);
                        fclose($myfile);*/
                    }
                }
            }

        }

    }
}

?>

<?php
/*$file_name = 'http://devserver.ti.dn.ua/~vsnit/cron_log.txt';
$myfile = fopen($file_name, "a") or die("Unable to open file!");
$txt = yii()->dateFormatter->format('yyyy/MM/dd hh:mm', time())." Крон сработал бл!";
fwrite($myfile, $txt);
fclose($myfile);*/
?>