<?php

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Cron',
    'preload' => array('log'),
    'import' => array(
        'application.components.*',
        'application.models.*',
        'application.modules.pay.models.*',
    ),
    'components' => array(
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'logFile' => 'cron.log',
                    'levels' => 'error, warning',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'logFile' => 'cron_trace.log',
                    'levels' => 'trace',
                ),
            ),
        ),
        'db' => array(
            'connectionString' => 'mysql:host=devserver.ti.dn.ua;dbname=orangelots_dev',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'hdyr74nc',
            'charset' => 'utf8',
            'enableProfiling' => YII_DEBUG,
            'schemaCachingDuration' => (YII_DEBUG ? 0 : 3600),
        ),
        'request' => array(
            'hostInfo' => 'http://devserver.ti.dn.ua',
            'baseUrl' => '/~vsnit/orangelots',
            'scriptUrl' => '',
        ),
    ),
);
