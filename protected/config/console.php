<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'My Console Application',

	// preloading 'log' component
	'preload' => array('log'),
	'import' => array(
		'application.models.*',
        'application.modules.pay.models.*',
    ),

	// application components
	'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=devserver.ti.dn.ua;dbname=orangelots_dev',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => 'hdyr74nc',
            'charset' => 'utf8',
            'tablePrefix' => '',
            'enableProfiling' => YII_DEBUG,
            'schemaCachingDuration' => (YII_DEBUG ? 0 : 3600),
        ),
		'resque' => array(
			'class' => 'application.components.yii-resque.RResque',
			'server' => 'localhost', // Redis server address
			'port' => '6379', // Redis server port
			'database' => 0 // Redis database number
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
	),
);