<div class="main_container">
    <div class="posted_by__table_auction_list__container">
        <p class="text_upload">Dear, <?php echo OrangelotsUsers::model()->findByPk(Yii::app()->user->id)->firstname ?></p>

        <p class="text_upload">You can import your products using import file. You can download a import <a href="<?php echo createUrl('site/get-file?deal=true')?>" class="orange_link">sample file</a></p>

        <p class="text_upload">Please make sure that some fields like manufacture, condition, category and etc. you fill from dictionary on
        separate read only sheets.</p>

        <?php
        echo CHtml::form('', 'POST', array('enctype' => 'multipart/form-data'));
        echo CHtml::openTag('div', array('class' => 'upload-file-container'));
        echo CHtml::activeFileField($docModel, 'document', array('class' => 'file_upload'));
        echo CHtml::closeTag('div');
        echo CHtml::openTag('div', array('class' => 'load_btn'));
        echo CHtml::button('Load', array('submit' => array('upload-deal'), 'class' => 'btn green_bnt load_btn_item'));
        echo CHtml::closeTag('div');
        echo CHtml::endForm();
        ?>
        <?php
        if ($display):
            echo '<table class="text-center">' . "\n";
            for ($row = 1; $row <= $highestRow; ++$row) {
                if ($row == 1) {
                    echo '<thead>' . "\n";
                    echo '<tr>' . "\n";
                } else {
                    echo '<tbody>' . "\n";
                    echo '<tr>' . "\n";
                }
                for ($col = 0; $col <= $highestColumnIndex; ++$col) {
                    if($col<27)
                        echo '<td>' . $objWorksheet->getCellByColumnAndRow($col, $row)->getValue() . '</td>' . "\n";
                }
                if ($row == 1) {
                    echo '</tr>' . "\n";
                    echo '</thead>' . "\n";
                } else {
                    echo '</tr>' . "\n";
                    echo '</tbody>' . "\n";
                }
            }
            echo '</table>' . "\n";

            if (isset($obj) && ($obj != null)) {
                echo CHtml::openTag('div', array('class' => 'finish_import'));
                echo CHtml::form('', 'POST');
                echo CHtml::hiddenField('FinishForm[obj]', CJSON::encode($obj));
                echo CHtml::submitButton('Finish Import', array('class' => 'btn green_bnt'));
                echo CHtml::endForm();
                echo CHtml::closeTag('div');
            }
        endif;
        ?>
    </div>
</div>
