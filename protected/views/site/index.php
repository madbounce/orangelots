<div class="main_page__container">
    <div class="main_container clearfix">
        <div class="main_page__top_preview clearfix">
            <div class="main_page__top_preview_image" style="background-image: url('/images/top_preview.png')">
                <p class="main_page__top_preview_image_title">We listed wholesale furniture deals from top furniture stores and suppliers</p>
                <p class="main_page__top_preview_image_text">New and used furniture available! Buy directly from wholesalers. Online, no comissions, no hidden fees. Cheap and great furniture models available right now for immediate delivery.</p>
                <a class="btn_image main_page__top_preview_btn" href="<?php echo createUrl('orangelots/orangelots-kind/all')?>"><img src="images/btn/btn_find_your_deal.png" alt=""></a>
            </div>
            <div class="main_page__top_preview_argument">
                <p class="main_page__top_preview_argument__title">
                    <span>5 Reasons</span><br/>
                    to become a vendor
                </p>
                <ul class="main_page__top_preview_argument__list">
                    <li>No subscription for vendors</li>
                    <li>No upfront payment</li>
                    <li>No hidden fee</li>
                    <li>Personal manager for each vendor</li>
                </ul>
                <?if(!user()->isVendor(user()->id)):?>
                    <a class="btn_image main_page__top_preview_argument__btn" href="<?php echo createUrl('user/orangelots-users/create')?>"><img
                            src="<?php echo baseUrl() . '/images/btn/btn_become_a_vendor_black.png' ?>" alt=""/></a>
                <?endif;?>
                <p class="main_page__top_preview_argument__info">We don't charge you to list your product. We help you to sale them fast and easy.</p>
            </div>
        </div>
    </div>
</div>
<div class="categories__container">
    <div class="main_container">
        <ul class="categories__menu clearfix">
            <li class="categories__menu__link"><span class="categories__menu__link__icon office"></span> We sell furniture and<br/> furniture only</a></li>
            <li class="categories__menu__link"><span class="categories__menu__link__icon upholstered"></span> Quality Inventory<br/> at value</a></li>
            <li class="categories__menu__link"><span class="categories__menu__link__icon special"></span> Every Item<br/> is a Deal</li>
            <li class="categories__menu__link"><span class="categories__menu__link__icon secure"></span> Fast. Easy.<br/> Secure.</li>
        </ul>
    </div>
</div>
<div class="catalog_list__container">
    <div class="main_container">
        <ul class="catalog_list__list clearfix">
            <?php if((isset($auctions) && (!empty($auctions)))):?>
                <?php foreach($auctions AS $key => $value):?>
                    <li>
                            <div class="catalog_list__image">
                                <a href="<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value->id))?>">
                                <?php if(OrangelotsAuctions::main_photo($value)):?>
                                    <? foreach ($value->galleryPhotos AS $key_img => $value_img): ?>
                                        <? if ($value_img->set_main == 1): ?>
                                            <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value->id))?>"><img src="<?php echo createUrl($value_img->getThumb()); ?>" alt=""></a>
                                        <? endif; ?>
                                    <? endforeach; ?>
                                <?php else:?>
                                    <?php if(isset($value->galleryPhotos[0])):?>
                                        <a href="<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value->id))?>"><img src="<?php echo createUrl($value->galleryPhotos[0]->getThumb()) ?>" alt=""></a>
                                    <?php else:?>
                                        <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value->id))?>"><img src="<?php echo yii()->baseUrl.'/images/NIA.png' ?>" alt=""></a>
                                    <?php endif; ?>
                                <?php endif; ?>
                                </a>
                            </div>
                            <div class="catalog_list__absolute_text pull-left text-left">
                                <a href = "<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => OrangelotsKind::model()->findByPk($value->category_id)->alias))?>"><p class="catalog_list__name"><?php echo OrangelotsKind::model()->findByPk($value->category_id)->name?></p></a>
                                <p class="catalog_list__categories_name">furniture</p>
                            </div>
                            <div class="catalog_list__side_text pull-right text-right">
                                <p class="catalog_list__price">Was $<?php echo number_format($value->mvrp, 2) ?></p>
                                <p class="catalog_list__price">Only <span>$<?php echo number_format($value->buy_now_price, 2) ?></span> (<?php echo round($value->saving_buy_now) ?>% off)</p>
                            </div>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>
<div class="main_container">
    <div class="brands_list__container">
        <p class="brands_list__title">from brands like:</p>
        <table class="brands_list__list">
            <tr>
                <td>
                    <img src="images/brangs/brand_1.png" alt="">
                </td>
                <td>
                    <img src="images/brangs/brand_2.png" alt="">
                </td>
                <td>
                    <img src="images/brangs/brand_3.png" alt="">
                </td>
                <td>
                    <img src="images/brangs/brand_4.png" alt="">
                </td>
                <td>
                    <img src="images/brangs/brand_5.png" alt="">
                </td>
                <td>
                    <img src="images/brangs/brand_6.png" alt="">
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="main_page_note__container">
    <div class="main_container">
        <p class="main_page_note__text">OrangeLots is a powerful connector between my business and major and quality   wholesalers. Every investment I did here returned with 400% in profit!</p>
    </div>
</div>
<div class="main_page_foot_preview_list__container">
    <div class="main_container">
        <ul class="main_page_foot_preview_list__list clearfix">
            <?php if((isset($footer) && (!empty($footer)))):?>
            <?php foreach($footer AS $key_footer => $value_footer):?>
            <li>
                <a href="<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value_footer->id))?>">
                    <div class="main_page_foot_preview_list__image">
                        <?php if(OrangelotsAuctions::main_photo($value_footer)):?>
                            <? foreach ($value_footer->galleryPhotos AS $key_img => $value_img): ?>
                                <? if ($value_img->set_main == 1): ?>
                                    <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value_footer->id))?>"><img src="<?php echo createUrl($value_img->getThumb()); ?>" alt=""></a>
                                <? endif; ?>
                            <? endforeach; ?>
                        <?php else:?>
                            <?php if(isset($value_footer->galleryPhotos[0])):?>
                                <img src="<?php echo createUrl($value_footer->galleryPhotos[0]->getThumb()) ?>" alt="">
                            <?php else:?>
                                <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value_footer->id))?>"><img src="<?php echo yii()->baseUrl.'/images/NIA.png' ?>" alt=""></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="main_page_foot_preview_list__name pull-left">
                        <p><?php echo $value_footer->name ?></p>
                    </div>
                    <div class="catalog_list__side_text pull-right text-right">
                        <p class="catalog_list__price">Was $<?php echo number_format($value_footer->mvrp, 2) ?></p>
                        <p class="catalog_list__price">Only <span>$<?php echo number_format($value_footer->buy_now_price, 2) ?></span> (<?php echo round($value_footer->saving_buy_now) ?>% off)</p>
                    </div>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>