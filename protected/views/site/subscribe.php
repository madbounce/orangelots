<div class="popup__container" id="subscribe">
	<div class="popup__bg"></div>
	<div class="popup_inner">
		<div class="popup__content">
			<a href="" class="popup_close">X</a>

			<div class="popup_body clearfix">
				<div class="popup__title_complite">
					<p class="popup__title_complite__text">
                            <?php echo $message; ?>
                    </p>
                </div>
			</div>
		</div>
	</div>
</div>