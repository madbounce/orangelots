<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */

$this->breadcrumbs=array(
	'Orangelots Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OrangelotsUsers', 'url'=>array('index')),
	array('label'=>'Manage OrangelotsUsers', 'url'=>array('admin')),
);
?>

<h1>Create OrangelotsUsers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>