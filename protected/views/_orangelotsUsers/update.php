<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */

$this->breadcrumbs=array(
	'Orangelots Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OrangelotsUsers', 'url'=>array('index')),
	array('label'=>'Create OrangelotsUsers', 'url'=>array('create')),
	array('label'=>'View OrangelotsUsers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OrangelotsUsers', 'url'=>array('admin')),
);
?>

<h1>Update OrangelotsUsers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>