<?php
/* @var $this OrangelotsUsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orangelots Users',
);

$this->menu=array(
	array('label'=>'Create OrangelotsUsers', 'url'=>array('create')),
	array('label'=>'Manage OrangelotsUsers', 'url'=>array('admin')),
);
?>

<h1>Orangelots Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
