<?php
/* @var $this OrangelotsUsersController */
/* @var $model OrangelotsUsers */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'orangelots-users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

    <h2>Vendor Information</h2>

    <div class="row">
        <?php echo $form->labelEx($model, 'firstname'); ?>
        <?php echo $form->textField($model, 'firstname'); ?>
        <?php echo $form->error($model, 'firstname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'lastname'); ?>
        <?php echo $form->textField($model, 'lastname'); ?>
        <?php echo $form->error($model, 'lastname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'phone'); ?>
        <?php echo $form->textField($model, 'phone'); ?>
        <?php echo $form->error($model, 'phone'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'email'); ?>
        <?php echo $form->textField($model, 'email'); ?>
        <?php echo $form->error($model, 'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password'); ?>
        <?php echo $form->error($model, 'password'); ?>
        <p class="hint">
            <?php echo UserModule::t("Minimal password length 4 symbols."); ?>
        </p>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'company'); ?>
        <?php echo $form->textField($model, 'company'); ?>
        <?php echo $form->error($model, 'company'); ?>
    </div>


    <h2>Vendor Address</h2>


    <div class="row">
        <?php echo $form->labelEx($model, 'address_1'); ?>
        <?php echo $form->textField($model, 'address_1'); ?>
        <?php echo $form->error($model, 'address_1'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'address_2'); ?>
        <?php echo $form->textField($model, 'address_2'); ?>
        <?php echo $form->error($model, 'address_2'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'city'); ?>
        <?php echo $form->textField($model, 'city'); ?>
        <?php echo $form->error($model, 'city'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'state'); ?>
        <?php echo $form->textField($model, 'state'); ?>
        <?php echo $form->error($model, 'state'); ?>
    </div>


    <div class="row">
        <?php echo $form->labelEx($model, 'zipcode'); ?>
        <?php echo $form->textField($model, 'zipcode'); ?>
        <?php echo $form->error($model, 'zipcode'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->