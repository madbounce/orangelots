<?php
/**
 * @var $form TbActiveForm
 * @var $model Realty
 */
?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action' => createUrl($this->route),
	'method' => 'get',
)); ?>

<?= $form->textFieldRow($model, 'id', array('class' => 'span5')); ?>
<!--comments snitka-->
<?= $form->dropDownListRow($model, 'category_id', /*Category*/RealtyCategory::getAll(),array('class' => 'span5')); ?>

<?= $form->textFieldRow($model, 'name', array('class' => 'span5', 'maxlength' => 255)); ?>

<?= $form->textFieldRow($model, 'title', array('class' => 'span5', 'maxlength' => 255)); ?>

<?= $form->textFieldRow($model, 'alias', array('class' => 'span5', 'maxlength' => 255)); ?>

<?= $form->textFieldRow($model, 'description', array('class' => 'span5', 'maxlength' => 10000)); ?>

<?= $form->textFieldRow($model, 'timeCreate', array('class' => 'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType' => 'submit',
		'type' => 'primary',
		'label' => 'Search',
	)); ?>
</div>

<?php $this->endWidget(); ?>
