<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 24.05.14
 * Time: 22:56
 *
 * @var $model Realty
 * @var $val OrangelotsPhoto
 */
$this->metaKeywords = $model->seo_keywords;
$this->metaDescription = $model->seo_description;
?>
<? if (!empty($model->galleryPhotos)): ?>
	<? foreach ($model->galleryPhotos as $val): ?>
		<img src="<?= createUrl($val->getPreview()) ?>">
	<? endforeach ?>
<? endif ?>



<?php
$this->widget(
	'ext.similar.SimilarWidget',
	array(
		'params' => array(
			'model' => $model,
		)
	)
);

?>
