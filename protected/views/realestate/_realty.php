<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 24.05.14
 * Time: 22:51
 *
 * @var $data Realty
 */
?>

<h3><?= $data->title ?></h3>
<div class="span5"><?= $data->description ?></div>
<a href="<?= $data->getUrl() ?>"><?= $data->alias ?></a>
<?= $data->content ?>

<h3><?= yii()->dateFormatter->formatDateTime($data->timeCreate, 'long') ?></h3>