<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 25.05.14
 * Time: 11:32
 *
 * @var $val Realty
 */
?>

<?php
$this->metaKeywords = $model->seo_keywords;
$this->metaDescription = $model->seo_description;
?>
<? if (!empty($realty)): ?>
	<? foreach ($realty as $val): ?>
		<a href="<?= $val->url ?>"><?= $val->alias ?></a>
		<br/>
		<hr>
	<? endforeach ?>
<? endif ?>