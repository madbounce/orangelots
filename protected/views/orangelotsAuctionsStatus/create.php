<?php
/* @var $this OrangelotsAuctionsStatusController */
/* @var $model OrangelotsAuctionsStatus */

$this->breadcrumbs=array(
	'Orangelots Auctions Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OrangelotsAuctionsStatus', 'url'=>array('index')),
	array('label'=>'Manage OrangelotsAuctionsStatus', 'url'=>array('admin')),
);
?>

<h1>Create OrangelotsAuctionsStatus</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>