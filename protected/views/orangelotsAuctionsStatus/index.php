<?php
/* @var $this OrangelotsAuctionsStatusController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orangelots Auctions Statuses',
);

$this->menu=array(
	array('label'=>'Create OrangelotsAuctionsStatus', 'url'=>array('create')),
	array('label'=>'Manage OrangelotsAuctionsStatus', 'url'=>array('admin')),
);
?>

<h1>Orangelots Auctions Statuses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
