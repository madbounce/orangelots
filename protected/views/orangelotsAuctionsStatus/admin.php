<?php
/* @var $this OrangelotsAuctionsStatusController */
/* @var $model OrangelotsAuctionsStatus */

$this->breadcrumbs=array(
	'Orangelots Auctions Statuses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List OrangelotsAuctionsStatus', 'url'=>array('index')),
	array('label'=>'Create OrangelotsAuctionsStatus', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#orangelots-auctions-status-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Orangelots Auctions Statuses</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orangelots-auctions-status-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}{delete}'
		),
	),
)); ?>
