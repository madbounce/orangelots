<?php
/* @var $this OrangelotsAuctionsStatusController */
/* @var $model OrangelotsAuctionsStatus */

$this->breadcrumbs = array(
    'Orangelots Auctions Statuses' => array('index'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'List OrangelotsAuctionsStatus', 'url' => array('index')),
    array('label' => 'Create OrangelotsAuctionsStatus', 'url' => array('create')),
    array('label' => 'View OrangelotsAuctionsStatus', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Manage OrangelotsAuctionsStatus', 'url' => array('admin')),
);
?>
<div class="main_container">
    <div class="row">
        <h1>Update Status</h1>
    </div>


    <?php $this->renderPartial('_form', array('model' => $model)); ?>
</div>