<?php
/* @var $this ShippingTermsController */
/* @var $model ShippingTerms */

$this->breadcrumbs=array(
	'Shipping Terms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ShippingTerms', 'url'=>array('index')),
	array('label'=>'Manage ShippingTerms', 'url'=>array('admin')),
);
?>

<h1>Create ShippingTerms</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>