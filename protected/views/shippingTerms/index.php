<?php
/* @var $this ShippingTermsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shipping Terms',
);

$this->menu=array(
	array('label'=>'Create ShippingTerms', 'url'=>array('create')),
	array('label'=>'Manage ShippingTerms', 'url'=>array('admin')),
);
?>

<h1>Shipping Terms</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
