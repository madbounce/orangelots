<?php
/* @var $this ShippingTermsController */
/* @var $model ShippingTerms */

$this->breadcrumbs=array(
	'Shipping Terms'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ShippingTerms', 'url'=>array('index')),
	array('label'=>'Create ShippingTerms', 'url'=>array('create')),
	array('label'=>'View ShippingTerms', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ShippingTerms', 'url'=>array('admin')),
);
?>

<h1>Update ShippingTerms <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>