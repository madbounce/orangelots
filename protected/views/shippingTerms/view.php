<?php
/* @var $this ShippingTermsController */
/* @var $model ShippingTerms */

$this->breadcrumbs=array(
	'Shipping Terms'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ShippingTerms', 'url'=>array('index')),
	array('label'=>'Create ShippingTerms', 'url'=>array('create')),
	array('label'=>'Update ShippingTerms', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ShippingTerms', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShippingTerms', 'url'=>array('admin')),
);
?>

<h1>View ShippingTerms #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
