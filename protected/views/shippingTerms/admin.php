<?php
/* @var $this ShippingTermsController */
/* @var $model ShippingTerms */

?>

<h1>Manage Shipping Terms</h1>
<!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'shipping-terms-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}',
        ),
	),
)); ?>
