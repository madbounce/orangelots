<?php
/* @var $this OrangelotsManufactureController */
/* @var $model OrangelotsManufacture */

$this->breadcrumbs = array(
    'Orangelots Manufactures' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List OrangelotsManufacture', 'url' => array('index')),
    array('label' => 'Manage OrangelotsManufacture', 'url' => array('admin')),
);
?>
<div class="main_container">
    <div class="row">
        <h1>Create Manufacture</h1>
    </div>
    <?php $this->renderPartial('_form', array('model' => $model)); ?>
</div>