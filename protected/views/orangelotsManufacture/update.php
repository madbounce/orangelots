<?php
/* @var $this OrangelotsManufactureController */
/* @var $model OrangelotsManufacture */

$this->breadcrumbs = array(
    'Orangelots Manufactures' => array('index'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'List OrangelotsManufacture', 'url' => array('index')),
    array('label' => 'Create OrangelotsManufacture', 'url' => array('create')),
    array('label' => 'View OrangelotsManufacture', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Manage OrangelotsManufacture', 'url' => array('admin')),
);
?>
<div class="main_container">
    <div class="row">
        <h1>Update Manufacture </h1>
    </div>
    <?php $this->renderPartial('_form', array('model' => $model)); ?>
</div>