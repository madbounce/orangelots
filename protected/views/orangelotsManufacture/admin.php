<?php
/* @var $this OrangelotsManufactureController */
/* @var $model OrangelotsManufacture */

?>

<h1>Manage Orangelots Manufactures</h1>

<?php echo CHtml::link('Create',createUrl('orangelots-manufacture/create'), array('class' => 'btn btn_orange'));?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orangelots-manufacture-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'alias',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}'
		),
	),
)); ?>
