<?php
/* @var $this OrangelotsManufactureController */
/* @var $model OrangelotsManufacture */

$this->breadcrumbs=array(
	'Orangelots Manufactures'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List OrangelotsManufacture', 'url'=>array('index')),
	array('label'=>'Create OrangelotsManufacture', 'url'=>array('create')),
	array('label'=>'Update OrangelotsManufacture', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OrangelotsManufacture', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OrangelotsManufacture', 'url'=>array('admin')),
);
?>

<h1>View OrangelotsManufacture #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'alias',
	),
)); ?>
