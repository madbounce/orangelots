<?php
/* @var $this OrangelotsStyleController */
/* @var $model OrangelotsStyle */

$this->breadcrumbs = array(
    'Orangelots Styles' => array('index'),
    $model->name => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'List OrangelotsStyle', 'url' => array('index')),
    array('label' => 'Create OrangelotsStyle', 'url' => array('create')),
    array('label' => 'View OrangelotsStyle', 'url' => array('view', 'id' => $model->id)),
    array('label' => 'Manage OrangelotsStyle', 'url' => array('admin')),
);
?>
<div class="main_container">
    <div class="row">
        <h1>Update Style </h1>
    </div>
    <?php $this->renderPartial('_form', array('model' => $model)); ?>
</div>