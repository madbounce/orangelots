<?php
/* @var $this OrangelotsStyleController */
/* @var $model OrangelotsStyle */
?>

<h1>Manage Styles</h1>

<?php echo CHtml::link('Create',createUrl('orangelots-style/create'), array('class' => 'btn btn_orange'));?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'orangelots-style-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		'alias',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}',
		),
	),
)); ?>
