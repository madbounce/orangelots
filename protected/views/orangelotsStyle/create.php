<?php
/* @var $this OrangelotsStyleController */
/* @var $model OrangelotsStyle */

$this->breadcrumbs = array(
    'Orangelots Styles' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List OrangelotsStyle', 'url' => array('index')),
    array('label' => 'Manage OrangelotsStyle', 'url' => array('admin')),
);
?>
<div clas="main_container">
    <div class="row">
        <h1>Create Style</h1>
    </div>
        <?php $this->renderPartial('_form', array('model' => $model)); ?>

</div>