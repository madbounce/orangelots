<?php
/* @var $this OrangelotsStyleController */
/* @var $model OrangelotsStyle */

$this->breadcrumbs=array(
	'Orangelots Styles'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List OrangelotsStyle', 'url'=>array('index')),
	array('label'=>'Create OrangelotsStyle', 'url'=>array('create')),
	array('label'=>'Update OrangelotsStyle', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OrangelotsStyle', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OrangelotsStyle', 'url'=>array('admin')),
);
?>

<h1>View OrangelotsStyle #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'alias',
	),
)); ?>
