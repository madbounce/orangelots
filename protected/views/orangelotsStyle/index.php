<?php
/* @var $this OrangelotsStyleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orangelots Styles',
);

$this->menu=array(
	array('label'=>'Create OrangelotsStyle', 'url'=>array('create')),
	array('label'=>'Manage OrangelotsStyle', 'url'=>array('admin')),
);
?>

<h1>Orangelots Styles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
