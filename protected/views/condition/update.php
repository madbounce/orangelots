<?php
/* @var $this ConditionController */
/* @var $model Condition */

$this->breadcrumbs=array(
	'Conditions'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Condition', 'url'=>array('index')),
	array('label'=>'Create Condition', 'url'=>array('create')),
	array('label'=>'View Condition', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Condition', 'url'=>array('admin')),
);
?>

<div class="main_container">
    <div class="row">
        <h1>Update Condition <?php echo $model->name; ?></h1>
    </div>
    <?php $this->renderPartial('_form', array('model' => $model)); ?>
</div>