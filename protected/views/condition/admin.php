<?php
/* @var $this ConditionController */
/* @var $model Condition */
?>

<h1>Manage Conditions</h1>
<!-- search-form -->
<!--<a href="--><?php //echo createUrl('condition/create')?><!--" class="btn btn_orange"> Create condition </a>-->
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'condition-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}'
		),
	),
)); ?>
