<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta name="language" content="en"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <? yii()->bootstrap->registerCoreScripts(); ?>
    <script type="text/javascript" src="<?= baseUrl() . '/js/jquery-2.1.1.js' ?>"></script>
    <script type="text/javascript" src="<?= baseUrl() . '/js/bootstrap-select.js' ?>"></script>
    <script type="text/javascript" src="<?= baseUrl() . '/js/jquery.jcarousel-core.js' ?>"></script>
    <script type="text/javascript" src="<?= baseUrl() . '/js/main_b.js' ?>"></script>
    <script type="text/javascript" src="<?= baseUrl() . '/js/orangelots.js' ?>"></script>
    <link type="text/css" href="<?= baseUrl() . '/css/cms.css' ?>" rel="stylesheet" media="all"/>
    <link href="<?php echo baseUrl() . '/css/fonts.css' ?>" rel="stylesheet" media="screen">
    <link href="<?php echo baseUrl() . '/css/new.css' ?>" rel="stylesheet" media="screen">
    <link href="<?php echo baseUrl() .'/css/style.css' ?>" rel="stylesheet" media="screen">
    <script type="text/javascript">var baseUrl = "<?=baseUrl();?>";</script>
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <? yii()->controller->widget(
        'ext.seo.widgets.SeoHead',
        array(
            'httpEquivs' => array(
                'Content-Type' => 'text/html; charset=utf-8',
                'Content-Language' => 'en-US'
            ),
            'defaultDescription' => yii()->params['main_description'],
            'defaultKeywords' => yii()->params['main_keywords'],
        )
    ); ?>
</head>
<body>
<div class="main_content">
    <div class="header">
        <? $this->widget('ext.adminMenu.AdminMenuWidget') ?>
    </div>


    <div>
        <? $this->widget('bootstrap.widgets.TbAlert', array('htmlOptions' => array('class' => 'yc'))) ?>
    </div>
    <div class="container">
        <?= $content; ?>
    </div>
    <div class="clear"></div>
    <div id="push"></div>
</div>
</body>
</html>