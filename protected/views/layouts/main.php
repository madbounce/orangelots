<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta name="language" content="en"/>
    <meta charset="utf-8">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <? yii()->bootstrap->registerCoreScripts(); ?>
    <script type="text/javascript" src="<?= baseUrl() . '/js/jquery-2.1.1.js' ?>"></script>
    <script type="text/javascript" src="<?= baseUrl() . '/js/bootstrap-select.js' ?>"></script>
    <script type="text/javascript" src="<?= baseUrl() . '/js/jquery.jcarousel-core.js' ?>"></script>
    <script type="text/javascript" src="<?= baseUrl() . '/js/main_b.js' ?>"></script>
    <script type="text/javascript" src="<?= baseUrl() . '/js/orangelots.js' ?>"></script>
    <link type="text/css" href="<?= baseUrl() . '/css/cms.css' ?>" rel="stylesheet" media="all"/>
    <link href="<?php echo baseUrl() . '/css/new.css'?>" rel="stylesheet" media="screen">
    <link href="<?php echo baseUrl() .'/css/style.css' ?>" rel="stylesheet" media="screen">
    <script type="text/javascript">
        var baseUrl = "<?=baseUrl();?>";
        var MaxSum = "<?php echo (int)param('maxPaySum');?>";
    </script>
    <title><?= CHtml::encode($this->pageTitle); ?>
    </title>
    <? yii()->controller->widget(
        'ext.seo.widgets.SeoHead',
        array(
            'httpEquivs' => array(
                'Content-Type' => 'text/html; charset=utf-8',
                'Content-Language' => 'en-US'
            ),
            'defaultDescription' => yii()->params['main_description'],
            'defaultKeywords' => yii()->params['main_keywords'],
        )
    ); ?>
</head>
<body>
<?php $this->widget('ext.mainMenuHeader.mainMenuHeaderWidget'); ?>
<div class="main_content">
    <div class="header">
        <?  if (user()->isAdmin()) {  ?>
            <?  $this->widget('ext.adminMenu.AdminMenuWidget')  ?>
        <?} ?>
    </div>
    <div class="header_search_row clearfix">
        <div class="main_container">
            <div class="header_search_row_search_form pull-right">
                <form class="form-inline" method="POST" action="<?php echo createUrl('/orangelots/orangelots-kind/search')?>">
                    <select class="selectpicker" name="Query[category]">
                        <option value="1" >New</option>
                        <option value="2" >Used</option>
                        <option value="3" >Refurbished</option>
                    </select>
                    <input type="text" class="form-control" name="Query[query]" placeholder="Search by id, model or keyword"/>
                    <button class="btn_image"><img src="<?php echo baseUrl() . '/images/btn/btn_search.png' ?>" alt="">
                    </button>
                    <?if(!user()->isVendor(user()->id)):?>
                        <a href = "<?php echo yii()->createUrl('user/orangelots-users/create');?>" class="btn_image "><img src="<?php echo baseUrl() . '/images/btn/btn_become_a_vendor.png' ?>"></a>
                    <?endif;?>
                </form>
            </div>
            <div class="header_search_row__logo pull-left">
                <a href="<?php echo createUrl('site/index')?>">
                    <img src="<?php echo baseUrl() . '/images/logo.png' ?>" alt=""/>
                </a>
            </div>
        </div>
    </div>
    <?php $this->widget('ext.menuNavbar.menuNavbarWidget'); ?>
    <div class="main_container">
        <? $this->widget('bootstrap.widgets.TbAlert', array('htmlOptions' => array('class' => 'yc'))) ?>
    </div>
    <div class="container">
        <?= $content; ?>
    </div>
    <div class="clear"></div>
    <div id="push"></div>
</div>
<div class="footer clearfix">
    <div class="main_container">
        <div class="footer__payment pull-right">
            <img src="<?php echo baseUrl() . '/images/paypal.png' ?>" alt="">
            <img src="<?php echo baseUrl() . '/images/bbb.png' ?>" alt="">
        </div>
        <ul class="footer__menu pull-left">
            <li>
                Tools and information
            </li>
            <li>
                <a href="<?php echo yii()->cms->createUrl('terms-and-conditions')?>">Terms and Conditions</a>
            </li>
            <li>
                <a href="<?php echo yii()->cms->createUrl('private-police')?>">Privace Police</a>
            </li>
            <li>
                <a href="<?php echo yii()->cms->createUrl('carrers')?>">Carrers</a>
            </li>
            <li>
                <a href="<?php echo yii()->cms->createUrl('about_us')?>">About Us</a>
            </li>
            <li>
                <a href="<?php echo yii()->cms->createUrl('contact_us')?>">Contact Us</a>
            </li>
        </ul>
        <ul class="footer__menu pull-left">
            <li>
                Buyers
            </li>
            <li>
                <?if(user()->isGuest):?>
                    <?php echo CHtml::ajaxLink(
                        "My Account",
                        yii()->createUrl('user/login/render-login'),
                        array(
                            'type' => 'POST',
                            'success' => "function(data){
                                    $('body').append(data);
                                }",
                        )
                    );
                    ?>
                <?else:?>
                    <?php echo CHtml::link('My account', createUrl('account'))?>
                <?endif;?>
            </li>
            <li>
            </li>
            <li>
                <a href="<?php echo createUrl('news/industry-tip/view', array('type'=> IndustryTipCategory::buyer_label))?>">Help for buyers</a>
            </li>
        </ul>
        <div class="footer__form pull-left">
            <? $this->widget('ext.subscribe.SubscribeWidget'); ?>
        </div>
    </div>
</div>
</body>
</html>