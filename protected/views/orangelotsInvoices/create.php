<?php
/* @var $this OrangelotsInvoicesController */
/* @var $model OrangelotsInvoices */

$this->breadcrumbs=array(
	'Orangelots Invoices'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OrangelotsInvoices', 'url'=>array('index')),
	array('label'=>'Manage OrangelotsInvoices', 'url'=>array('admin')),
);
?>

<h1>Create OrangelotsInvoices</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>