<?php
/* @var $this OrangelotsInvoicesController */
/* @var $model OrangelotsInvoices */

$this->breadcrumbs=array(
	'Orangelots Invoices'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OrangelotsInvoices', 'url'=>array('index')),
	array('label'=>'Create OrangelotsInvoices', 'url'=>array('create')),
	array('label'=>'Update OrangelotsInvoices', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OrangelotsInvoices', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OrangelotsInvoices', 'url'=>array('admin')),
);
?>

<h1>View OrangelotsInvoices #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'paypal_id',
		'email',
		'phone',
		'amount',
		'auction_deal_id',
		'transaction_date',
		'status',
	),
)); ?>
