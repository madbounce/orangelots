<?php
/* @var $this OrangelotsInvoicesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Orangelots Invoices',
);

$this->menu=array(
	array('label'=>'Create OrangelotsInvoices', 'url'=>array('create')),
	array('label'=>'Manage OrangelotsInvoices', 'url'=>array('admin')),
);
?>

<h1>Orangelots Invoices</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
