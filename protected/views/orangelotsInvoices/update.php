<?php
/* @var $this OrangelotsInvoicesController */
/* @var $model OrangelotsInvoices */

$this->breadcrumbs=array(
	'Orangelots Invoices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OrangelotsInvoices', 'url'=>array('index')),
	array('label'=>'Create OrangelotsInvoices', 'url'=>array('create')),
	array('label'=>'View OrangelotsInvoices', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OrangelotsInvoices', 'url'=>array('admin')),
);
?>

<h1>Update OrangelotsInvoices <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>