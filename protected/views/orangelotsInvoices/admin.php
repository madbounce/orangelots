<?php
/* @var $this OrangelotsInvoicesController */
/* @var $model OrangelotsInvoices */
?>

<div class="breadcrumb_row clearfix">
    <div class="main_container">
        <ul>
            <li><a class="breadcrumb_row__main_link" href=""></a></li>
            <li><a href="">Payments Received</a></li>
        </ul>
    </div>
</div>
<div class="received__container">
    <div class="main_container">
        <p class="posted_by__title">Search by</p>
        <form>
            <div class="received__row__form_container">
                <div class="received__row">
                    <div class="received__row__block received__row__block__id">
                        <label>Transaction ID</label>
                        <input type="text" />
                    </div>
                    <div class="received__row__block received__row__block__id">
                        <label>PayPal ID</label>
                        <input type="text" />
                    </div>
                    <div class="received__row__block received__row__block__email">
                        <label>From, email, phone</label>
                        <input type="text" />
                    </div>
                </div>
                <div class="received__row">
                    <div class="received__row__block received__row__block__deal_id">
                        <label>Auction ID /  Deal ID</label>
                        <input type="text" />
                    </div>

                    <div class="received__row__block__calendar_block">
                        <p>Payment Date</p>
                        <label>from</label>
                        <input class="received__row__block__calendar" type="text" />
                        <label>to</label>
                        <input class="received__row__block__calendar" type="text" />
                    </div>

                    <div class="received__row__block">
                        <label>Status</label>
                        <div class="received__row__select_container">
                            <select class="selectpicker">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>

                    <div class="received__row__block">
                        <button class="btn_image"><img src="<?php echo baseUrl().'/images/btn/form_search_btn.png'?>" alt=""></button>
                    </div>

                </div>
            </div>
        </form>

        <div class="posted_by__table_auction_list__container text-center">
            <?php $this->widget(
                'MyGridView',
                array(
                    'id' => 'orangelots-category-grid',
                    'dataProvider' => $model->search(),
                    'columns' => array(
                        array(
                            'name' => 'id',
                            'header' => 'Trans.ID',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'paypal_id',
                            'header' => 'PayPalID',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'From',
                            'header' => 'From',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'email',
                            'header' => 'Email',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'phone',
                            'header' => 'Phone',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'amount',
                            'header' => 'Amount',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'auction_deal_id',
                            'header' => 'AuctionID/' . PHP_EOL . 'DealID',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'Transaction Date',
                            'header' => 'transaction_date',
                            'type' => 'raw',
                            'sortable' => false,
                        ),
                        array(
                            'name' => 'status',
                            'header' => 'Status',
                            'type' => 'raw',
                            'sortable' => false,
                        ),

                    ),
                    'itemsCssClass' => 'text-center',
                )
            ); ?>
        </div>

    </div>
</div>