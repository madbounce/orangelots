<?php

class SimilarWidget extends CWidget
{
	public $params = array(
		'model' => '',
	);

	public function run()
	{
		$model = $this->params['model'];

		$criteria = new CDbCriteria();
		$criteria->condition = 't.alias<>"' . $model->alias . '"';
		$criteria->addCondition('price <=' . $model->price + 5000);
		$criteria->addCondition('price >=' . $model->price - 5000);
		$criteria->addCondition('area >=' . $model->area + 100);
		$criteria->addCondition('area >=' . $model->area - 100);
		$criteria->limit = 2;

		$result = Realty::model()->findAll($criteria);

		$this->render('default', array('result' => $result));
	}
}

