<?php
class SponsoredListWidget extends CWidget
{
    public $category;

	public function init()
	{
	}

	public function run()
	{
        $aucitons = OrangelotsAuctions::model()->findAllByAttributes(array('sponsored' => 1, 'active' => 1, 'status' => 1));
		$this->render('default', array(
                'auctions' => $aucitons,
            ));
	}
}

