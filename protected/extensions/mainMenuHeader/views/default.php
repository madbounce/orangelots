<div class="header_dark_row">
    <div class="main_container clearfix">
        <div class="header_dark_row__left_side pull-left">
            <p>Toll Free 800-xxxx
                <a href="mailto:support@orangelots.com">support@orangelots.com</a>
            </p>
        </div>
        <div class="header_dark_row__right_side pull-right">
            <ul class="header_dark_row_navigation_link">
                <?php if (yii()->user->isGuest): ?>
                    <li>
                        <?php echo CHtml::ajaxLink(
                            "Sign In",
                            yii()->createUrl('user/login/render-login'),
                            array(
                                'type' => 'POST',
                                'success'=> 'function(data) {
                                    $("body").append(data);
                                }',
                            )
                        );
                        ?>
                    </li>
                    <li>
                        <a href="<?php echo createUrl('news/industry-tip/view', array('type' => IndustryTipCategory::buyer_label))?>">For Buyers</a>
                    </li>
                    <li>
                        <a href="<?php echo createUrl('news/industry-tip/view', array('type' => IndustryTipCategory::vendor_label))?>">For Vendors</a>
                    </li>
                <?php else: ?>

					<li><a href="<?php echo createUrl('account') ?>">Account</a>
						<ul class="header_dark_row_navigation_link__submenu">
							<li>
								<a href="<?php echo createUrl('user/orangelots-users/update/') ?>">Personal
									Settings</a></li>
							<li><a href="<?php echo createUrl('user/orangelots-users/billingInfo/') ?>">Billing Settings</a></li>
						</ul>
					</li>
					<li><a>Messages</a>
						<ul class="header_dark_row_navigation_link__submenu">
							<li><a href="<?php echo createUrl('account/default/messages')?>">Messages</a></li>
							<li><a href="<?php echo createUrl('user/questions/') ?>">Questions</a></li>
						</ul>
					</li>
					<li>
						<a>Buy</a>
						<ul class="header_dark_row_navigation_link__submenu">
							<li><a href="<?php echo createUrl('auctions/orangelots-auctions/bidding') ?>">Auctions</a>
							</li>
							<li><a href="<?php echo createUrl('auctions/orangelots-auctions/deal')?>">Deals</a></li>
							<li><a href="<?php echo createUrl('user/buy/vendor-list')?>">Vendors</a></li>
                            <li><a href="<?php echo createUrl('user/buy/payments')?>">Payments</a></li>
                        </ul>
					</li>
					<?php if (user()->isVendor()) : ?>
                    <li>
						<a>Sell</a>
						<ul class="header_dark_row_navigation_link__submenu">
							<li>
								<a href="<?php echo createUrl('auctions/orangelots-auctions/posted-by-me') ?>">Auctions</a>
							</li>
							<li><a href="<?php echo createUrl('auctions/orangelots-auctions/bidders')?>">Bids</a></li>
							<li><a href="<?php echo createUrl('auctions/orangelots-auctions/sell-deal') ?>">Deals</a></li>
							<li><a href="<?php echo createUrl('auctions/orangelots-auctions/sales')?>">Sales</a></li>
							<li><a href="<?php echo createUrl('user/sell/buyer-list')?>">Buyers</a></li>
							<li><a href="<?php echo createUrl('user/sell/payments')?>">Payments</a></li>
						</ul>
					</li>
                        <?php endif; ?>
					<li>
						<a href="<?php echo yii()->createUrl('user/logout') ?>"><?php echo yii::t(
									'site',
									'Logout'
								) . '(' . yii()->user->email . ')' ?></a>
					</li>
				<?php endif; ?>

            </ul>
        </div>
    </div>
</div>