<?$buttonId = uniqid();?>
<form class="form_of_mass">
    <input type="text" placeholder="EMAIL" id="subscribe_email" value="<?php echo !empty($email) ? $email : ''?>">
    <div class="message" id="subscribe_message"></div>
    <?php echo CHtml::ajaxButton(
        'YES',
        createUrl('site/subscribe-email'),
        array(
            'type' => 'post',
            'data' => array(
                'Subscribe[mail]' => 'js:$(this).parents("form").find("#subscribe_email").val()',
            ),
            'success' => 'function(data) {
                                            var obj = JSON.parse(data);
                                            if (obj.status == "error") {
                                             $("body").append(obj.html);
                                            }
                                            else if (obj.status == "error_msg") {
                                              $("#subscribe_'.$buttonId.'").parents(".form_of_mass").find("#subscribe_message").html(obj.html)
                                            }
                                            else if (obj.status == "ok") {
                                                    window.location.reload();
                                            }
                                        }',
        ),
        array(
            'type'=>'image',
            'class' => 'btn_image subscribe_button',
            'src'=>yii()->baseUrl . '/images/btn/btn_get_daily_deals.png',
            'id'=>'subscribe_'.$buttonId,
        )
    );
    ?>
</form>