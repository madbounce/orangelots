<?php
class SubscribeWidget extends CWidget
{
    public function init()
    {
    }

    public function run()
    {
        $email = '';
        if(!user()->isGuest){
            $user = OrangelotsUsers::getUserInfo(user()->id);
            $email = $user->email;
        }
        $this->render('index', array('email' => $email));
    }
}

