<?php
/**
 * Widget that generates sharing buttons for facebook, twitter and google plus.
 * Usage:
 *    $this->widget('path.to.SocialShareWidget', array(
 *        'url' => 'http://example.org',                    //required
 *        'services' => array('google', 'twitter'),        //optional
 *        'htmlOptions' => array('class' => 'someClass'), //optional
 *        'popup' => false,                                //optional
 *    ));
 *
 * @author Pavle Predic <https://github.com/pavlepredic>
 * @version 0.1
 */
class SocialShareWidget extends CWidget
{
	const OK = 'ok';
	const FACEBOOK = 'facebook';
	const TWITTER = 'twitter';
	const GOOGLE = 'google';
	const VK = 'vk';

	/**
	 * URL to share (required)
	 * @var string
	 */
	public $url;

	/**
	 * List of social services to use.
	 * Buttons will be generated in the order specified here.
	 * Valid services are:
	 * self::FACEBOOK, self::TWITTER and self::GOOGLE
	 * @var array
	 */
	public $services = array(self::OK, self::FACEBOOK, self::TWITTER, self::GOOGLE, self::VK,);

	/**
	 * HTML options that will be used for rendering this widget
	 * @var array
	 */
	public $htmlOptions = array('class' => 'block_menu');

	public $itemCssClass = 'social_ref ';

	/**
	 * Whether or not to use a JS popup window
	 * @var bool
	 */
	public $popup = true;

	/**
	 * Display names for social services.
	 * These names will be rendered in 'title' attribute.
	 * @var array
	 */
	public $serviceNames = array(
		self::OK => 'ok',
		self::FACEBOOK => 'facebook',
		self::TWITTER => 'twitter',
		self::GOOGLE => 'google',
		self::VK => 'vk',
	);

	/**
	 * Sharing URLs used by each service.
	 * Normally there is no need to modify this.
	 * @var array
	 */
	public $serviceUrls = array(
		self::OK => 'http://www.odnoklassniki.ru/group/52093386621089',
		self::FACEBOOK => 'https://www.facebook.com/pages/XTRENK/284266481585612',
		self::TWITTER => 'https://twitter.com/XTRENK',
		self::GOOGLE => 'https://plus.google.com/u/0/communities/116338043762221935934',
		self::VK => 'http://vk.com/public61719666',
	);

	/**
	 * By default, this widget looks for assets in
	 * dirname(__FILE__) . '/assets/socialshare'.
	 * If you placed assets in a different directory,
	 * specify the full path here.
	 * @var string
	 */
	public $assetsPath;

	/**
	 * Publishes and registers required assets
	 * @see CWidget::init()
	 */
	public function init()
	{
		parent::init();

		$assetsDir = $this->assetsPath ? $this->assetsPath : dirname(__FILE__) . '/assets/socialshare';
		$dir = Yii::app()->assetManager->publish($assetsDir);
		$cs = Yii::app()->clientScript;
		$cs->registerCssFile($dir . '/style.css');

		if ($this->popup) {
			$cs->registerCoreScript('jquery');
			$cs->registerScriptFile($dir . '/script.js', CClientScript::POS_END);
		}
	}

	/**
	 * Outputs the widget HTML
	 * @see CWidget::run()
	 */
	public function run()
	{
		$opts = $this->htmlOptions;
		if (!isset($opts['id'])) {
			$opts['id'] = $this->getId();
		}
		echo CHtml::openTag('ul', array('class' => 'social_ref_list'));
		foreach ($this->services as $service) {
			echo CHtml::openTag('li', array());
			if (!array_key_exists($service, $this->serviceUrls)) {
				throw new CHttpException(500, "Non-existant service: '$service'");
			}
			$serviceName = isset($this->serviceNames[$service]) ? $this->serviceNames[$service] : $service;
			$url = $this->serviceUrls[$service] . urlencode($this->url);
			echo CHtml::openTag('a', array(
				'href' => $url,
				'title' => $serviceName,
				'target' => '_blank',
				'class' => 'social_ref ' . $service
			));
			echo CHtml::closeTag('a');
			echo CHtml::closeTag('li');
		}
		echo CHtml::closeTag('ul');
	}
}