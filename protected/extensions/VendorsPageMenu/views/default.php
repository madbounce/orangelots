<? $items = array(); ?>
<? $items[] = array('label' => 'New Items', 'url' => array('')) ?>
<? $items[] = array('label' => 'Used Items', 'url' => array('')) ?>
<? $items[] = array('label' => 'Orange Blossom', 'url' => array('')) ?>
<? $items[] = array('label' => 'Flash Deals', 'url' => array('')) ?>
<? $items[] = array('label' => 'Upcoming Deals', 'url' => array('')) ?>
<? $items[] = array('label' => 'Vendor Directory', 'url' => array('')) ?>
<? $items[] = array('label' => 'Industry Tips', 'url' => array()) ?>


<?$this->widget(
	'bootstrap.widgets.TbNavbar',
	array(
		'type' => '',
		'fixed' => false,
		'brand' => false,
		'brandUrl' => baseUrl(),
		'collapse' => true,
		'items' => array(
			array(
				'class' => 'bootstrap.widgets.TbMenu',
				'items' => $items,
			),
		),
	)
);
?>