<? $items = array(); ?>
<? $items[] = array('label' => yii::t('info', 'How to connect'), 'url' => yii()->cms->createUrl('how-to-connect')); ?>
<? $items[] = array('label' => yii::t('info', 'Work with system'), 'url' => yii()->cms->createUrl('work-with-system')); ?>
<? $items[] = array('label' => yii::t('info', 'Term of connect'), 'url' => yii()->cms->createUrl('Term of connect')); ?>
<? $items[] = array('label' => yii::t('info', 'Tariff'), 'url' => yii()->cms->createUrl('Tariff')); ?>
<? if (!user()->isGuest): ?>
	<? $items[] = array('label' => yii::t('info', 'Personal cabinet'), 'url' => createUrl('//user')); ?>
<? else: ?>
	<? $items[] = array('label' => yii::t('info', 'File and documents'), 'url' => yii()->cms->createUrl('File and documents')); ?>
<? endif ?>
<div class="head_menu">
	<div class="container">
		<?$this->widget('bootstrap.widgets.TbNavbar', array(
			'type' => 'inverse',
			'fixed' => false,
			'brand' => '',
			'brandUrl' => baseUrl(),
			'collapse' => true,
			'items' => array(
				array(
					'class' => 'bootstrap.widgets.TbMenu',
					'items' => $items,
				),
				array(
					'class' => 'bootstrap.widgets.TbMenu',
				),
			),
		));
		?>
	</div>
</div>