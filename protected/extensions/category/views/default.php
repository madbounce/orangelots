<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 24.05.14
 * Time: 23:13
 */
?>
<? if (!empty($categories)): ?>
	<? foreach ($categories as $val): ?>
		<?php $category = RealtyCategory::model()->findByPk($val->id); ?>
		<?php if (!empty($category)): ?>
			<h1>
				<a href="<?= createUrl("//real-estate/" . $val->alias) ?>"><?= $category->title ?></a>
			</h1>
			<?php if (!empty($category->categoryGroups)): ?>

				<?php foreach ($category->categoryGroups as $groupAttribute): ?>
					<?php if (!empty($groupAttribute)): ?>
						<div class="fields-group">
							<div class="title">
								<a href="#"
								   class="js-group-title"><?= $groupAttribute->title ?></a>
							</div>
							<br/>
							<?php foreach ($groupAttribute->relAttributes as $attribute): ?>
								<div class=" control-group">
									<?php if (in_array(
										$attribute->type,
										array(
											RealtyAttribute::TYPE_INPUT_STRING,
											RealtyAttribute::TYPE_INPUT_INTEGER,
											RealtyAttribute::TYPE_INPUT_FLOAT
										)
									)
									): ?>
                                <?php if ($attribute->is_filter == 1):?>
										<label
											class="control-label <?= isset($errors[$attribute->id]) ? 'error' : '' ?>"
											for="Realty_attributes_<?= $attribute->id ?>"><?= $attribute->title ?>
											<?= ($attribute->is_required) ? '<span class="required">*</span>' : '' ?>
										</label>
										<div class="controls">
											<div class="input-append">
												<input style="display:inline" type="text"
												       id="Realty_attributes_<?= $attribute->id ?>"
												       name="Realty[post_attributes][<?= $attribute->id ?>]"
												       value="<?= (isset($dataAttributes[$attribute->id])) ? $dataAttributes[$attribute->id] : '' ?>"
												       class="span3 <?= isset($errors[$attribute->id]) ? 'error' : '' ?>"
												       group_id="<?= $attribute->group_id ?>"
												       attr_id="<?= $attribute->id ?>"
												       is_filter="<?= $attribute->is_filter ?>"
												       type_id="<?= $attribute->type ?>"

													>
												<?php if (trim($attribute->unit) != ''): ?>
													<span class="add-on"><?= $attribute->unit ?></span>
												<?php endif; ?>
												<?php if (isset($errors[$attribute->id])): ?>
													<span
														class="help-inline error"><?= $errors[$attribute->id][0] ?></span>
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
									<?php endif; ?>
									<?php if ($attribute->type == RealtyAttribute::TYPE_CHECKBOX): ?>
                                        <?php if ($attribute->is_filter == 1):?>
                                        <div class="controls">
											<label class="checkbox">
												<input type="checkbox" id="Realty_attributes_<?= $attribute->id ?>"
												       name="Realty[post_attributes][<?= $attribute->id ?>]"
												       value="1" <?= (isset($dataAttributes[$attribute->id])) ? 'checked="checked"' : '' ?>
												       group_id="<?= $attribute->group_id ?>"
												       attr_id="<?= $attribute->id ?>"
												       is_filter="<?= $attribute->is_filter ?>"
												       type="<?= $attribute->type ?>"
													>
												<?= $attribute->title ?>
											</label>
											<?=
											CHtml::ajaxLink(
												$attribute->title,
												baseUrl() . '/site/search/' /*. $attribute->alias*/,
												array(
													'type' => 'POST',
													'data' => array(
														'group' => 'js:getGroup()',
														'attr' => 'js:getData()',
													),
													'success' => 'function(data){
														var data = $.parseJSON(data);
														$("#count_result").html(data.count);
														$(".main-filter__found a:first").attr("href",data.url);

													}'
												),
												array(
													'class' => 'filter-option active-option',
													'groupID' => $attribute->id,
													'attrID' => $attribute->id
												)
											)?>
										</div>
									<?php endif; ?>
									<?php endif; ?>
									<?php if ($attribute->type == RealtyAttribute::TYPE_SELECT): ?>
                                <?php if ($attribute->is_filter == 1):?>
                                        <label class="control-label"
										       for="Realty_attributes_<?= $attribute->id ?>"><?= $attribute->title ?>
											<?= ($attribute->is_required) ? '<span class="required">*</span>' : '' ?>
										</label>
										<div class="controls">
											<?php $options = $attribute->getCacheOptions(); ?>
											<select id="Realty_attributes_<?= $attribute->id ?>"
											        name="Realty[post_attributes][<?= $attribute->id ?>]"
											        class="span3 <?= isset($errors[$attribute->id]) ? 'error' : '' ?>"
											        group_id="<?= $attribute->group_id ?>"
											        attr_id="<?= $attribute->id ?>"
											        is_filter="<?= $attribute->is_filter ?>"
											        type="<?= $attribute->type ?>"
												>
												<option value="">-</option>
												<?php foreach ($options as $option): ?>
													<option <?= (isset($dataAttributes[$attribute->id]) && $dataAttributes[$attribute->id] == $option->id) ? 'selected="selected"' : '' ?>
														value="<?= $option->id ?>"><?= $option->value ?></option>
												<?php endforeach; ?>
											</select>
											<?php if (isset($errors[$attribute->id])): ?>
												<span class="help-inline error"><?= $errors[$attribute->id][0] ?></span>
											<?php endif; ?>
										</div>
									<?php endif; ?>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>

<?=
CHtml::ajaxSubmitButton(
	'Поиск',
	createUrl('RealEstateSearch/search'),
	array(
		'type' => 'POST',
		'data' => array(
			'data' => 'js:getAllData()',
		),
//		'dataType' => 'JSON',
		'success' => 'function(data){
            $("#push").html(data);
		}',
	)
)?>



<?registerScript(
	"markActiveCat",
	'
		$(".active-option").bind("click",function(){
			$(this).toggleClass("active");
		});
	',
	CClientScript::POS_END
)?>

<script type="text/javascript">
	function getGroup() {
		var arGroup = new Array();
		$("#filter_attributes a.active").each(function () {
			arGroup.push($(this).attr("group_id"));
		});
		return arGroup;
	}

	function getData() {
		var attr = new Array();
		$("#filter_attributes a.active").each(function () {
			var attrID = $(this).attr("attr_id");
			attr.push(attrID);
		});
		return attr;
	}

	function getAttrCheckbox() {
		var attrCheckbox = new Array();
		$('[type=checkbox]:checked').each(function () {
			attrCheckbox.push($(this).attr('attr_id'));
		});
		return attrCheckbox;
	}
	function getAttrInputString() {
		var attrInput = new Object();
		$('[type=text][type_id = 0]').each(function () {
			if ($(this).val() != '') {
				attrInput[$(this).attr('attr_id')] = $(this).val();
			}
		});
		return attrInput;
	}
	function getAttrInputInteger() {
		var attrInput = new Object();
		$('[type=text][type_id = 1]').each(function () {
			if ($(this).val() != '') {
				attrInput[$(this).attr('attr_id')] = $(this).val();
			}
		});
		return attrInput;
	}
	function getAttrInputFloat() {
		var attrInput = new Object();
		$('[type=text][type_id = 2]').each(function () {
			if ($(this).val() != '') {
				attrInput[$(this).attr('attr_id')] = $(this).val();
			}

		});
		return attrInput;
	}
	function getAttrSelect() {
		var attrSelectObj = new Object();
		$('select').each(function () {
//            attrSelectObj[$(this).attr('attr_id')] = $(this).val()
			if ($(this).find("option:selected").val() != '') {
				attrSelectObj[$(this).attr('attr_id')] = $(this).find("option:selected").val();
			}
		});
		return attrSelectObj;
	}

	function getAllData() {
		var allData = new Object();
		var a = getAttrCheckbox();
		var c = getAttrSelect();
		var b = getAttrInputString();
		var d = getAttrInputInteger();
		var e = getAttrInputFloat();
		allData['valuesCheckbox'] = a;
		allData['valuesString'] = b;
		allData['valuesInteger'] = d;
		allData['valuesFloat'] = e;
		allData['valuesSelect'] = c;
		//console.log(allData);
		return allData;
	}
</script>