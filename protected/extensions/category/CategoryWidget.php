<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 24.05.14
 * Time: 23:12
 */

class CategoryWidget extends CWidget
{
	public function init()
	{
		$categories = RealtyCategory::model()->findAll();
		$this->render('default', array(
			'categories' => $categories
		));
	}
}