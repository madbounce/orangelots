<? $items = array(); ?>
<?
$items[] = array('label' => yii::t('site', 'LK'), 'url' => array('//realtor/index'));

?>
<div class="head_menu">
	<?$this->widget('bootstrap.widgets.TbNavbar', array(
		'type' => 'inverse',
		'fixed' => false,
		'brand' => yii::t('site', 'LarkPay'),
		'brandUrl' => createUrl('site/index'),
		'collapse' => true,
		'items' => array(
			array(
				'class' => 'bootstrap.widgets.TbMenu',
				'items' => $items,
			),
			array(
				'class' => 'bootstrap.widgets.TbMenu',
				'htmlOptions' => array('class' => 'pull-right', 'style' => ''),
				'items' => array(array('label' => yii::t('site', 'Logout') . '(' . yii()->user->email . ')', 'url' => array('/user/logout'), 'visible' => !yii()->user->isGuest),)
			),
		),
	));
	?>
</div>