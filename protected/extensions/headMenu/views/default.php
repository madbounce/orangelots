<? $items = array(); ?>
<? $items[] = array('label' => 'Search With Attributes', 'url' => array('/real-estate/')) ?>
<? $items[] = array('label' => 'News', 'url' => array('/news')) ?>

<?$this->widget(
	'bootstrap.widgets.TbNavbar',
	array(
		'type' => '',
		'fixed' => false,
		'brand' => 'Main',
		'brandUrl' => baseUrl(),
		'collapse' => true,
		'items' => array(
			array(
				'class' => 'bootstrap.widgets.TbMenu',
				'items' => $items,
			),
		),
	)
);
?>