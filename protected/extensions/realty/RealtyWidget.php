<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 24.05.14
 * Time: 23:12
 */

class RealtyWidget extends CWidget
{
	public function init()
	{
		$model = Realty::model()->findAll();

		$this->render('default', array(
			'model' => $model
		));
	}
}