<?php
/**
 * Created by PhpStorm.
 * User: fen0man
 * Date: 24.05.14
 * Time: 23:13
 */

?>
<? if (!empty($model)): ?>
	<? foreach ($model as $val): ?>
		<h3><a href="<?= $val->getUrl() ?>"><?= $val->title ?></a></h3>
		<span><?= $val->description ?></span>
		<h4><?= yii()->dateFormatter->formatDateTime($val->timeCreate, 'long') ?></h4>
		<br/>
		<hr>
	<? endforeach ?>
<? endif ?>