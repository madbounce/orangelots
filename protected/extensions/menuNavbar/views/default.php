<div class="menu_row clearfix">
	<div class="main_container">
		<div class="menu_row__side_item pull-right">
			<ul>
				<li><a href="<?php echo createUrl('news/industryTip/view', array('type' => IndustryTipCategory::industry_label)) ?>">Industry tips</a></li>
			</ul>
		</div>
		<div class="menu_row__side_item pull-left">
			<ul>
				<li><a href="<?php echo createUrl('orangelots/orangelots-kind/new') ?>">New items</a></li>
				<li><a href="<?php echo createUrl('orangelots/orangelots-kind/used') ?>">Used items</a></li>
				<li><a href="<?php echo createUrl('orangelots/orangelots-kind/orange-blossom') ?>">Orange blossom</a></li>
				<li><a href="<?php echo createUrl('orangelots/orangelots-kind/flash') ?>">Flash deals</a></li>
				<li><a href="<?php echo createUrl('orangelots/orangelots-kind/upcoming-deals') ?>">Upcoming deals</a></li>
			</ul>
		</div>
	</div>
</div>