<?php
class AuctionQuestionsWidget extends CWidget
{
    public $auctionId;

    public function init()
    {
        $questions = AuctionQuestion::getAuctionQuestions($this->auctionId);
        $this->render('index', array('questions' => $questions, 'auctionId' => $this->auctionId));
    }
}