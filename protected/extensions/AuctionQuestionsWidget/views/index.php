<?if(empty($questions)):?>
    <span>There are no questions yet.</span>
<?else:?>
    <ul>
        <?foreach($questions as $question):?>
            <li id="question_<?php echo $question->id?>">
                <p class="card_detail__tab_question__question_author">
                    <?php echo ucfirst(!empty($question->user_name) ? $question->user_name : $question->user->firstname.', '. OrangelotsStates::getStateLabel($question->user->state))?> -
                    <span><?php echo ucfirst($question->subject)?></span>
                </p>

                <p class="card_detail__tab_question__question_text"><?php echo $question->question?></p>
                <div class="clearfix">
                    <div class="row" <?echo empty($question->answer) ? 'style="display:none"' :''?> id="answer_div_<?php echo $question->id?>">
                        <div class="card_detail__tab_question__image pull-left">
                            <?if(!empty($question->owner->avatar)):?>
                                <img src="<?php echo yii()->baseUrl .yii()->storage->getFileUrl($question->owner->avatar)  ?>" alt=""/>
                            <?endif;?>
                        </div>
                        <div class="card_detail__tab_question__answer pull-left">
                            <p id="answer_p_<?php echo $question->id?>"><?php echo $question->answer?></p>
                        </div>
                    </div>
                    <?if(OrangelotsAuctions::isOwner($question->auction_id)):?>
                        <div <?echo !empty($question->answer) ? 'style="display:none"' :''?>>
                            <div class="messeges_list__control">
                                <?php echo CHtml::button('reply', array('id' => 'reply_'.$question->id, 'class' => 'orange_link', 'onClick' => 'showArea('.$question->id.')'))?>
                            </div>
                            <div id="answer_<?php echo $question->id?>" style="display:none; width:300px;" >
                                <?php $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'ask-answer-form-'.$question->id,
                                        'enableAjaxValidation' => true,
                                        'clientOptions' => array(
                                            'validateOnSubmit' => true,
                                        ),
                                    )); ?>
                                <?php echo $form->textArea($question, 'answer', array('cols' => 60, 'rows' => 10, 'id' => 'answer_area_'.$question->id)); ?>
                                <?php echo $form->error($question, 'answer', array('id' => 'answer_area_error_'.$question->id)); ?>
                                <?php echo CHtml::hiddenField('auction_id', $auctionId)?>
                                <?=CHtml::ajaxSubmitButton(
                                    'Reply',
                                    createUrl('auctions/orangelots-auctions/save-answer', array('id' => $question->id)),
                                    array(
                                        'type' => 'POST',
                                        'success' => 'function(data){
                                                var resp = $.parseJSON(data);
                                                    console.log(resp);
                                                if(resp.err == 0){
                                                    $("#answer_'.$question->id.'").hide();
                                                    $("#answer_div_'.$question->id.'").show();
                                                    $("#answer_p_'.$question->id.'").text(resp.text);
                                                }else{
                                                    $("#answer_area_error_'.$question->id.'").show();
                                                    $("#answer_area_error_'.$question->id.'").text(resp.errText);
                                                }
                                            }',
                                    ),
                                    array(
                                        'id' => 'answer_submit_'.$question->id,
                                        'class' => 'btn green_bnt mar_for_green'
                                    )
                                )
                                ?>
                                <?php echo CHtml::button('Cancel', array('onClick' => 'hideArea('.$question->id.')', 'class' => 'btn green_bnt mar_for_green'))?>
                                <?php $this->endWidget(); ?>
                            </div>
                        </div>
                    <?endif;?>
                </div>
            </li>
        <?endforeach;?>
    </ul>
<?endif;?>