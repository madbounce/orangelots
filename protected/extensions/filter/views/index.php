<div class="auction_list_sidebar pull-left">
    <? if (!empty($categories)): ?>
        <p class="auction_list__sidebar__title tytle_first">By Product</p>
        <ul class="auction_list__sidebar__categories any_ul_class">
        <?$i=0;?>
        <?$seeMore=false;?>
        <?$amount = count($categories)?>
        <? foreach ($categories as $kind): ?>
            <li>
                <?php if(isset($url)):?>
                    <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $kind->alias)) ?>"><?php echo $kind->name ?></a>
                <?php else:?>
                    <?php if(isset($sub_url)):?>
                        <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $kind->alias)) ?>"><?php echo $kind->name ?></a>
                    <?php else:?>
                        <?php if (isset($empty_url)):?>
                            <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $kind->alias)) ?>"><?php echo $kind->name ?></a>
                        <?php else:?>
                            <?php if($parent_flag):?>
                                <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $kind->alias)) ?>"><?php echo $kind->name ?></a>
                            <?php else: ?>
                                <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $kind->alias, 'parent'=>OrangelotsKind::defaultUrl($uri))) ?>"><?php echo $kind->name ?></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif;?>
                <? $subCategories = OrangelotsKind::getSubCategories($kind->id);
                if (!empty($subCategories)):?>
                    <ul class="auction_list__sidebar__categories__sub_categories">
                        <? foreach ($subCategories as $sub): ?>
                            <li>
                                <?php if(isset($url)):?>
                                    <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $sub->alias, 'sub'=>$kind->alias)) ?>"><?php echo $sub->name ?></a>
                                <?php else:?>
                                    <?php if(isset($sub_url)):?>
                                        <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $sub->alias, 'sub'=>$sub_url)) ?>"><?php echo $sub->name ?></a>
                                    <?php else:?>
                                        <?php if($empty_url):?>
                                            <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $sub->alias,'sub'=>$kind->alias)) ?>"><?php echo $sub->name ?></a>
                                        <?php else: ?>
                                            <?php if($parent_flag):?>
                                                <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $sub->alias, 'parent'=>$kind->alias)) ?>"><?php echo $sub->name ?></a>
                                            <?php else: ?>
                                                <a href="<?php echo yii()->createUrl('orangelots/orangelots-kind/view/', array('alias' => $sub->alias, 'parent'=>OrangelotsKind::defaultUrl($uri), 'sub'=>$kind->alias)) ?>"><?php echo $sub->name ?></a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </li>
                        <? endforeach; ?>
                    </ul>
                <? endif; ?>
            </li>
            <?if($i==3):?>
                <?if($amount >$i+1):?>
                    <?$seeMore = true;?>
                <?endif;?>
                </ul>
                <ul class="auction_list__sidebar__categories" style="display:none" id="productFullList">
            <?endif;?>
            <?$i++;?>
        <? endforeach; ?>
        </ul>
        <?if($seeMore):?>
            <a class="auction_list__sidebar__categories__show_more" onclick="showFull($(this), 'productFullList')">see more</a>
        <?endif;?>
    <? endif; ?>

    <form method="POST" action="<?php echo $action ?>" id="filterFormWidget">
        <? if (!empty($vendors)): ?>
            <p class="auction_list__sidebar__title">By Vendor</p>
            <ul class="auction_list__sidebar__search_list any_ul_class" id="vendorShortList">
            <? $i = 0; ?>
            <?$seeMore=false;?>
            <?$amount = count($vendors)?>
            <? foreach ($vendors as $vendor): ?>
                <li><label><input type="checkbox"
                            <?php echo(OrangelotsAuctions::isChecked('vendor_' . $vendor->id) ? "checked='true'" : "") ?>
                                  value="<?php echo $vendor->id ?>"
                                  name="Filter[vendor_<?php echo $vendor->id ?>]"/>
                        <a href="<?php echo createUrl('orangelots/orangelots-kind/vendor/', array_merge($by_vendor_uri, array('vendor' => $vendor->alias)))?>"><?php echo $vendor->name ?></a>
                    </label>
                </li>
                <?if($i==3):?>
                    <?if($amount >$i+1):?>
                        <?$seeMore = true;?>
                    <?endif;?>
                    </ul>
                    <ul class="auction_list__sidebar__search_list" id="vendorFullList" style="display:none">
                <? endif; ?>
                <? $i++ ?>
            <? endforeach; ?>
            </ul>
            <?if($seeMore):?>
                <a class="auction_list__sidebar__categories__show_more" onclick="showFull($(this),'vendorFullList')">see more</a>
            <? endif; ?>
        <? endif; ?>

        <? if (!empty($styles)): ?>
            <p class="auction_list__sidebar__title">By Style</p>
            <ul class="auction_list__sidebar__search_list any_ul_class" id="styleShortList">
            <? $i = 0; ?>
            <?$seeMore=false;?>
            <?$amount = count($styles)?>
            <? foreach ($styles as $style): ?>
                <li><label><input
                            type="checkbox" <?php echo(OrangelotsAuctions::isChecked('style_' . $style->id) ? "checked" : "") ?>
                            value="<?php echo $style->id ?>"
                            name="Filter[style_<?php echo $style->id ?>]"/>
                        <a href="<?php echo createUrl('orangelots/orangelots-kind/style/', array_merge($by_style_uri, array('style' => $style->alias)))?>"><?php echo $style->name ?></a>
                    </label>
                </li>
                <?if($i==3):?>
                    <?if($amount >$i+1):?>
                        <?$seeMore = true;?>
                    <?endif;?>
                    </ul>
                    <ul class="auction_list__sidebar__search_list" id="styleFullList" style="display:none">
                <?endif;?>
                <? $i++ ?>
            <? endforeach; ?>
            </ul>
            <?if($seeMore):?>
                <a class="auction_list__sidebar__categories__show_more" onclick="showFull($(this),'styleFullList')">see more</a>
            <? endif; ?>
        <? endif; ?>

        <p class="auction_list__sidebar__title">By Saving</p>
        <ul class="auction_list__sidebar__search_list">
            <? foreach (OrangelotsAuctions::$savingCriterions as $key => $criterion): ?>
                <li><label><input type="checkbox" <input
                            type="checkbox" <?php echo(OrangelotsAuctions::isChecked('saving_' . $key) ? "checked" : "") ?>
                            name="Filter[saving_<?php echo $key ?>]"
                            value="<?php echo $key ?>"/> <?php echo $key ?>% OFF or more</label></li>
            <? endforeach; ?>
        </ul>
        <p class="auction_list__sidebar__title">By Price</p>
        <ul class="auction_list__sidebar__search_list">
            <li><label><input type="checkbox" <?php echo(OrangelotsAuctions::isChecked('price_0') ? "checked" : "") ?>
                              name="Filter[price_0]" value="0_100"/> Under $100</label></li>
            <li><label><input type="checkbox" <?php echo(OrangelotsAuctions::isChecked('price_1') ? "checked" : "") ?>
                              name="Filter[price_1]" value="100_300"/> $100 to $300</label></li>
            <li><label><input type="checkbox" <?php echo(OrangelotsAuctions::isChecked('price_2') ? "checked" : "") ?>
                              name="Filter[price_2]" value="300_500"/> $300 to $500</label></li>
            <li><label><input type="checkbox" <?php echo(OrangelotsAuctions::isChecked('price_3') ? "checked" : "") ?>
                              name="Filter[price_3]" value="500_700"/> $500 to $700</label></li>
            <li><label><input type="checkbox" <?php echo(OrangelotsAuctions::isChecked('price_4') ? "checked" : "") ?>
                              name="Filter[price_4]" value="700_more"/> $700 and above</label></li>
        </ul>
        <div class="auction_list__sidebar__price_range">

            <div class="auction_list__sidebar__price_range__row_range">
                <input type="text" placeholder="$" id="from" name="Filter[custom_from]"
                       onfocus="return onlyNumbers('from');" value="<?php echo $_SESSION['checked']['custom_from'] ?>">
                to
                <input type="text" placeholder="$" id="to" name="Filter[custom_to]" onfocus="return onlyNumbers('to');"
                       value="<?php echo $_SESSION['checked']['custom_to'] ?>">
                <p class="auction_list__sidebar__price_range__title">Custom Price Range</p>
                <button type="submit" class="btn btn_orange">SEARCH</button>
            </div>
        </div>
    </form>
</div>