<?php
class FilterWidget extends CWidget
{
    public $not_include_uri = array(
    'orangelots-kind'
);

	public function init()
	{
	}

	public function run()
	{
        $str = str_replace(yii()->baseUrl, "", Yii::app()->request->requestUri);
        $aray_uri = explode('/',$str);
        krsort($aray_uri);
        $uri = array_values($aray_uri);
        $uri = array_flip($uri);
        unset($uri['by-vendor']);
        unset($uri['by-style']);
        unset($uri['filter']);
        if (isset($uri['search'])){
            $empty_url = true;
            unset($uri['search']);
        }
        $uri = array_flip($uri);
        $uri = array_values($uri);

        if ((isset($_REQUEST['alias'])) && (!isset($_REQUEST['sub'])) && (!isset($_REQUEST['parent']))){
            $url = $_REQUEST['alias'];
        }
        if ((isset($_REQUEST['alias'])) && (isset($_REQUEST['sub'])) && (!isset($_REQUEST['parent']))){
            $sub_url = $_REQUEST['sub'];
        }
        $by_vendor_uri = array();
        $by_style_uri = array();

        if((isset($_REQUEST['parent'])) && (isset($_REQUEST['alias']))&& (isset($_REQUEST['sub']))){

            $by_vendor_uri = array('alias' => $_REQUEST['alias'], 'parent' => ($_REQUEST['parent']), 'sub' => ($_REQUEST['sub']));
            if (isset($_REQUEST['style']))
                $by_vendor_uri = array();

            $by_style_uri = array('alias' => $_REQUEST['alias'], 'parent' => ($_REQUEST['parent']), 'sub' => ($_REQUEST['sub']));
            if (isset($_REQUEST['vendor']))
                $by_style_uri = array();
        } elseif((isset($_REQUEST['parent'])) && (isset($_REQUEST['alias']))){
            $by_vendor_uri = array('alias' => $_REQUEST['alias'], 'parent' => ($_REQUEST['parent']));
            if (isset($_REQUEST['style']))
                $by_vendor_uri = array();

            $by_style_uri = array('alias' => $_REQUEST['alias'], 'parent' => ($_REQUEST['parent']));
            if (isset($_REQUEST['vendor']))
                $by_style_uri = array();
        }
        elseif((isset($_REQUEST['sub'])) && (isset($_REQUEST['alias']))){
            $by_vendor_uri = array('alias' => $_REQUEST['alias'], 'sub' => ($_REQUEST['sub']));
            if (isset($_REQUEST['style']))
                $by_vendor_uri = array();

            $by_style_uri = array('alias' => $_REQUEST['alias'], 'sub' => ($_REQUEST['sub']));
            if (isset($_REQUEST['vendor']))
                $by_style_uri = array();
        }
        elseif ((isset($_REQUEST['parent']))){
            $by_vendor_uri = array('parent' => $_REQUEST['parent']);
            if (isset($_REQUEST['style']))
                $by_vendor_uri = array();

            $by_style_uri = array('parent' => $_REQUEST['parent']);
            if (isset($_REQUEST['vendor']))
                $by_style_uri = array();
        } elseif ((isset($_REQUEST['alias']))){
            $by_vendor_uri = array('alias' => $_REQUEST['alias']);
            if (isset($_REQUEST['style']))
                $by_vendor_uri = array();

            $by_style_uri = array('alias' => $_REQUEST['alias']);
            if (isset($_REQUEST['vendor']))
                $by_style_uri = array();
        }
        else {

            $by_vendor_uri = array('parent' => $uri[0]);
            $by_style_uri =array('parent' => $uri[0]);
        }
        if((isset($_REQUEST['style'])) && (!isset($_REQUEST['parent']))&& (!isset($_REQUEST['alias']))&& (!isset($_REQUEST['sub']))&& (!isset($_REQUEST['vendor']))){
            $by_vendor_uri = array();
            $by_style_uri =array();
        }
        if((isset($_REQUEST['vendor'])) && (!isset($_REQUEST['parent']))&& (!isset($_REQUEST['alias']))&& (!isset($_REQUEST['sub']))&& (!isset($_REQUEST['style']))){
            $by_vendor_uri = array();
            $by_style_uri =array();
        }
        if((isset($_REQUEST['vendor'])) && (!isset($_REQUEST['alias']))&& (!isset($_REQUEST['sub']))&& (!isset($_REQUEST['parent']))){
            $empty_url = true;
        }
        if((isset($_REQUEST['style'])) && (!isset($_REQUEST['alias']))&& (!isset($_REQUEST['sub']))&& (!isset($_REQUEST['parent']))){
            $empty_url = true;
        }

        if ($by_style_uri['parent'] == 'grid' || $by_style_uri['parent'] == 'list')
            unset($by_style_uri['parent']);
        if ($by_vendor_uri['parent'] == 'grid' || $by_vendor_uri['parent'] == 'list')
            unset($by_vendor_uri['parent']);

        if(!empty($_SESSION['orangeBlossom'])){
            $categoryName = 'orange-blossom';
        }elseif(!empty($_SESSION['upcomingDeal'])){
            $categoryName = 'upcoming-deals';
        }elseif(!empty($_SESSION['flashDeal'])){
            $categoryName = 'flash-deals';
        }elseif(!empty($_SESSION['condition'])){
            $staticPage = OrangelotsStaticCategoryPages::model()->findByAttributes(array('alias' => strtolower(Condition::getNameById($_SESSION['condition']))));
            $categoryName = $staticPage->url;
        }

        $by_style_uri = $this->not_include($by_style_uri);
        $by_vendor_uri = $this->not_include($by_vendor_uri);

        $by_vendor_uri = $this->deleteEmptyUri($by_vendor_uri);
        $by_style_uri = $this->deleteEmptyUri($by_style_uri);
        $uri = $this->deleteEmptyUri($uri);
        if ((count($uri)<2) && isset($uri[0]))
            $parent_flag = $this->checkParentInDb($uri[0]);
        else
            $parent_flag = false;

        krsort($uri);
        $uri = array_values($uri);



        $action = createUrl('/orangelots/orangelots-kind/filter', array('alias' => $categoryName));
        $this->render('index', array(
                'categories' => OrangelotsKind::getParentCategories(),
                'vendors' => OrangelotsManufacture::getList(),
                'styles' => OrangelotsStyle::getList(),
                'uri' => $uri,
                'empty_url' => $empty_url,
                'by_vendor_uri' => $by_vendor_uri,
                'by_style_uri' => $by_style_uri,
                'action' => $action,
                'parent_flag' => $parent_flag
            ));
	}

    function deleteEmptyUri($uri){
        foreach($uri as $key=>$value){
            $value = str_replace(" ","",$value);
            if($value == ''){
                unset($uri[$key]);
            }
        }
        return $uri;
    }
    function checkParentInDb($alias){
        $db_alias = OrangelotsKind::model()->findAllByAttributes(array('alias' => $alias));
        if(isset($db_alias) && !empty($db_alias)){
            return true;
        } else{
            return false;
        }
    }

    function not_include($uri){
        foreach($uri as $key=>$value){
            $value = str_replace(" ","",$value);
            if( in_array($value, $this->not_include_uri)){
                unset($uri[$key]);
            }
        }
        return $uri;
    }
}

