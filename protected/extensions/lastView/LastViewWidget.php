<?php
class LastViewWidget extends CWidget
{

	public function init()
	{
	}

	public function run()
	{
        $lastView = Yii::app()->session['lastViewItem'];

        $current = $_REQUEST;
        if ((!empty($current['category'])) && (!empty($current['alias']))) {
            $category = RealtyCategory::model()->findByAttributes(array('alias' => $current['category']));
            $exclude = Realty::model()->findByAttributes(array('category_id' => $category->id, 'alias' => $current['alias']));
            $key = array_search($exclude->id, $lastView);
            if ($key !== false)
            {
                unset($lastView[$key]);
            }
            $exclude_array = array_values($lastView);
            $lastView = $exclude_array;
        }



        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $lastView);

        $result = Realty::model()->findAll($criteria);



		$this->render('default', array('result' => $result));
	}
}

