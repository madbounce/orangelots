<?// if (user()->isGuest): ?>
<?= CHtml::beginForm(null, 'POST', array('class' => 'form-inline login_form')); ?>
<div class="controls controls-row">
	<div class="row">
		<div class="span9">

			<?= CHtml::submitButton(UserModule::t("Login"), array('class' => 'btn btn-green login_btn')); ?>
			<?= CHtml::activeTextField($model, 'onetime_pass', array('placeholder' => 'Код', 'class' => 'input_with_button')) ?>
			<div style="display: none">
				<?= CHtml::activeTextField($model, 'username', array('placeholder' => yii::t('login', 'username'), 'class' => 'pull-right')) ?>
				<?= CHtml::activePasswordField($model, 'password', array('placeholder' => yii::t('login', 'password'))) ?>
			</div>

			<div class="registration_btn">
				<?= CHtml::link(UserModule::t("Registration"), Yii::app()->getModule('user')->registrationUrl, array('class' => 'btn')); ?>
			</div>

		</div>
		<div class="span4">
			<?= CHtml::link(UserModule::t("Lost Password?"), Yii::app()->getModule('user')->recoveryUrl, array('class' => 'pull-right')); ?>
		</div>
	</div>
</div>

<?= CHtml::endForm(); ?>
<?
$form = new CForm(array(
	'elements' => array(
		'username' => array(
			'type' => 'text',
			'maxlength' => 32,
		),
		'password' => array(
			'type' => 'password',
			'maxlength' => 32,
		),
		'rememberMe' => array(
			'type' => 'checkbox',
		)
	),
	'buttons' => array(
		'login' => array(
			'type' => 'submit',
			'label' => 'Login',
		),
	),
), $model);
?>
<?// else: ?>

<?// endif ?>
