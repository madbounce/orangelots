<?/***
 * @var $model User
 */
?>
<?= CHtml::beginForm(null, 'POST', array('class' => 'form-inline login_form')); ?>
<div class="controls controls-row">
	<div class="row">
		<div class="span9">
			<?= CHtml::activeTextField($model, 'username', array('placeholder' => yii::t('login', 'username'), 'class' => 'input_with_flag')) ?>
			<?= CHtml::submitButton(UserModule::t("Login"), array('class' => 'btn btn-green login_btn')); ?>
			<?= CHtml::activePasswordField($model, 'password', array('placeholder' => yii::t('login', 'password'), 'class' => 'input_with_button')) ?>
			<div class="registration_btn">
				<?= CHtml::link(UserModule::t("Registration"), Yii::app()->getModule('user')->registrationUrl, array('class' => 'btn')); ?>
			</div>
		</div>
	</div>
</div>
<div class="span3 offset3">
	<?= CHtml::link(UserModule::t("Lost Password?"), Yii::app()->getModule('user')->recoveryUrl, array('class' => 'pull-right')); ?>
</div>
<?= CHtml::endForm(); ?>
<? $form = new CForm(array(
	'elements' => array(
		'username' => array(
			'type' => 'text',
			'maxlength' => 32,
		),
		'password' => array(
			'type' => 'password',
			'maxlength' => 32,
		),
		'rememberMe' => array(
			'type' => 'checkbox',
		)
	),
	'buttons' => array(
		'login' => array(
			'type' => 'submit',
			'label' => 'Login',
		),
	),
), $model);?>
