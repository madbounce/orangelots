<?
class LoginWidget extends CWidget
{
	public function init()
	{
	}

	public function run()
	{
		if (Yii::app()->user->isGuest) {
			$model = new UserLogin();
			if (isset($_POST['UserLogin'])) {
				if (!isset($_POST['UserLogin']['onetime_pass'])) {
					$model->setScenario('managerPrepare');
				} else {
					$model->setScenario('step2');
				}

				$model->attributes = $_POST['UserLogin'];
				if ($model->validate() && isset($_POST['UserLogin']['onetime_pass'])) {
					$this->lastViset();
					Feed::add('Авторизация.', Feed::TYPE_LOGIN, user()->id, true);
					if (user()->isAdmin() || user()->isManager(user()->id)) {
						$this->owner->redirect(createUrl('//admin'));
					} else {
						$this->owner->redirect(createUrl('//paymenthistory/default/index'));
					}
				}

				if (!$model->hasErrors()) {
					$model->setScenario('step2');
					$this->render('loginWithCode', array('model' => $model));
				} else {
					$arErrors = array();
					foreach ($model->getErrors() as $error) {
						$arErrors[] = $error[0];
					}
					user()->setFlash('error', $arErrors[0]);
					$this->getOwner()->redirect("index");
				}
			}
			if ($model->scenario != 'step2') {
				$this->render('login', array('model' => $model));
			}
		}
	}

	private function lastViset()
	{
		if (!Yii::app()->user->isGuest) {
			$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
			$lastVisit->lastvisit_at = date('m/d/Y H:i:s');
			$lastVisit->save();
		}
	}
}

