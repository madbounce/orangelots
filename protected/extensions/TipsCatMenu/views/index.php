<div class="industry_tops__deliver_head">
	<p><?php echo $labelFor;?></p>
</div>
<div class="industry_tops__deliver_body">
	<ul>
		<? if (!empty($categories)): ?>
			<? foreach ($categories as $category): ?>
				<li>
                    <?if(empty($_GET['alias'])){
                        if($category->id == $tips){
                            $class = 'selected';
                        }else{
                            $class = '';
                        }
                    }else{
                        if($category->alias == $_GET['alias']){
                            $class = 'selected';
                        }else{
                            $class = '';
                        }
                    }?>
					    <a class="<?echo $class?>" href="<?php echo createUrl('news/industry-tip/view', array('type' => $type,'alias' => $category->alias)) ?>"><?php echo $category->name ?></a>
				</li>
			<? endforeach; ?>
		<? endif; ?>
	</ul>
</div>