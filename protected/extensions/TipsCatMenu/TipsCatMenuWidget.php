<?php
class TipsCatMenuWidget extends CWidget
{
    public $categories;
    public $type;
    public $tips;

	public function init()
	{
	}

	public function run()
	{
        $this->render('index', array(
			'categories' => $this->categories,
             'type' => $this->type,
             'tips' => $this->tips,
		     'labelFor' => IndustryTipCategory::$labelsFor[$this->type]
		));
	}
}

