<?php
class RightColumnListWidget extends CWidget
{
    public $seller;
    public $category;

    public function init()
    {
    }

    public function run()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'active = 1';
        $criteria->addCondition('status = 1');
        $criteria->addCondition('user_id = '.$this->seller);
        $criteria->addCondition('start_date_unix <'.time());
        $auctions = OrangelotsAuctions::model()->findAll($criteria);

        if ((empty($auctions))){
            $catCriteria = new CDbCriteria();
            $catCriteria->condition = 'active = 1';
            $catCriteria->addCondition('status = 1');
            $catCriteria->addCondition('category_id = '.$this->category);
            $catCriteria->addCondition('start_date_unix <'.time());
            $auctions = OrangelotsAuctions::model()->findAll($catCriteria);
        }

        $this->render('default', array(
                'auctions' => $auctions,
            ));
    }
}

