<div class="card_detail__collum_galery">
    <div class="card_detail__collum_carousel__control">
        <a href="" class="card_detail__collum_carousel__control_btn top"></a>
        <a href="" class="card_detail__collum_carousel__control_btn bottom"></a>
    </div>
    <div id="card_detail__collum_carousel">
        <ul>
            <?php if (!empty($auctions)): ?>
            <?php foreach ($auctions AS $key=>$value):?>
            <?php
                $link = OrangelotsAuctions::SaveUnSave($value->id);
                $auct_id = $value->id;
            ?>
            <li>
                <p class="card_detail__collum_carousel_date"><?php echo yii()->dateFormatter->format('MM/dd/yyyy hh:mm:ss a', $value->end_date_unix); ?> EST</p>

                <div class="card_detail__collum_carousel_image au_height">
                    <?php if(OrangelotsAuctions::main_photo($value)):?>
                        <? foreach ($value->galleryPhotos AS $key_1 => $value_1): ?>
                            <? if ($value_1->set_main == 1): ?>
                                <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value->id))?>"><img src="<?php echo createUrl($value_1->getThumb()) ?>" alt=""></a>
                            <? endif; ?>
                        <? endforeach; ?>
                    <?php else:?>
                        <?php if(isset($value->galleryPhotos[0])):?>
                            <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value->id))?>"><img src="<?php echo createUrl($value->galleryPhotos[0]->getThumb()) ?>" alt=""></a>
                        <?php else:?>
                            <a href = "<?php echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value->id))?>"><img src="<?php echo yii()->baseUrl.'/images/NIA.png' ?>" alt=""></a>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?/* foreach ($value->galleryPhotos AS $key_1 => $value_1): */?><!--
                        <?/* if ($value_1->set_main == 1): */?>
                            <a href = "<?php /*echo yii()->createUrl('auctions/orangelots-auctions/view', array('id' => $value->id))*/?>"><img src="<?php /*echo createUrl($value_1->getThumb()) */?>" alt=""></a>
                        <?/* endif; */?>
                    --><?/* endforeach; */?>
                </div>
                <a href="<?php echo yii()->createUrl('auctions/orangelots-auctions/view/', array('id' => $value->id))?>" class="auction_list__carousel__name"><?php echo $value->name?> - orig. price - $<?php echo $value->mvrp?></a>
                <p class="auction_list__carousel__price">Qty: <?php echo $value->units_in_lot ?> / Unit: <span>$<?php if($value->min_bid !=0):  echo $value->min_bid; else: echo $value->buy_now_price; endif;?></span> <?php if ($value->saving_buy_now != 0):?>(<?php echo $value->saving_buy_now ?>% off)</p><?php endif; ?>

                <p class="text-center card_detail__collum_carousel__btn_row">
                    <!--<button class="btn_image"><img src="<?php /*echo yii()->baseUrl . '/images/btn/btn_bid_now.png' */?>"
                                                   alt=""></button>-->
                    <a class="" href="<?php echo createUrl('auctions/orangelots-auctions/view',array('id' =>$value->id ))?>"><img src="<?php echo yii()->baseUrl . '/images/btn/btn_bid_now.png' ?>"
                                                                                                                                               alt=""></a>
                </p>
                <?php if (!user()->isGuest):?>
                <p>

                    <?php echo CHtml::ajaxLink(
                        "remove from wishlist",
                        yii()->createUrl('auctions/orangelots-auctions/un-save-for-later'),
                        array(
                            'type' => 'POST',
                            'data' => array(
                                'Save[auction]' => $auct_id,
                                'Save[user_id]' => yii()->user->id,
                            ),
                            'success' => "function(data){
                                if(data == 'ok'){
                                    $('#remove_from_whishlist_".$auct_id."').hide();
                                    $('#save_for_later_".$auct_id."').show();
                                    $('#remove_from_whishlist_sponsored_".$auct_id."').hide();
                                    $('#save_for_later_sponsored_".$auct_id."').show();
                                }
                        }",
                        ),
                        array(
                            'id' => 'remove_from_whishlist_sponsored_'.$auct_id,
                            'style' => ($link)?'display:block':'display:none',
                            'class' => 'save_later',
                        )
                    );
                    ?>
                    <!--                --><?php //else: ?>
                    <?php echo CHtml::ajaxLink(
                        "save for later",
                        yii()->createUrl('auctions/orangelots-auctions/save-for-later'),
                        array(
                            'type' => 'POST',
                            'data' => array(
                                'Save[auction]' => $auct_id,
                                'Save[user_id]' => yii()->user->id,
                            ),
                            'success' => "function(data){
                                if(data == 'ok'){
                                    $('#save_for_later_".$auct_id."').hide();
                                    $('#remove_from_whishlist_".$auct_id."').show();
                                    $('#save_for_later_sponsored_".$auct_id."').hide();
                                    $('#remove_from_whishlist_sponsored_".$auct_id."').show();
                                }
                        }",
                        ),
                        array(
                            'id' => 'save_for_later_sponsored_'.$auct_id,
                            'style' => ($link)?'display:none':'display:block',
                            'class' => 'save_later',

                        )
                    );
                    ?>

                </p>
                <?php endif; ?>

            </li>
            <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>

</div>