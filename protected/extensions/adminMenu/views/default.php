<? $items = array(); ?>
<? $items[] = array(
	'label' => yii::t('site', 'Users'),
	'url' => '#',
	'class' => 'pull-left',
	'items' => array(
		array('label' => yii::t('site', 'Management'), 'url' => array('/user/orangelots-users/admin')),
		array('label' => yii::t('site', 'Permissions'), 'url' => array('//admin/rights')),
	)
);
$items[] = array('label' => yii::t('site', 'CMS'), 'url' => array('//cms/node/index'));

$items[] = array(
    'label' => yii::t('site', 'Category'),
    'url' => '#',
    'class' => 'pull-left',
    'items' => array(
        array('label' => yii::t('site', 'Create Category'), 'url' => array('//orangelots/orangelots-kind/create')),
        array('label' => yii::t('site', 'Category Management'), 'url' => array('//orangelots/orangelots-kind/admin')),
        array('label' => yii::t('site', 'Update New Page'), 'url' => array('//orangelots/orangelots-kind/edit?alias=new')),
        array('label' => yii::t('site', 'Update Used Page'), 'url' => array('//orangelots/orangelots-kind/edit?alias=used')),
        array('label' => yii::t('site', 'Update Search Page'), 'url' => array('//orangelots/orangelots-kind/edit?alias=search')),
        array('label' => yii::t('site', 'Update Browse Inventory Page'), 'url' => array('//orangelots/orangelots-kind/edit?alias=all')),
        array('label' => yii::t('site', 'Update Flash Deals Page'), 'url' => array('//orangelots/orangelots-kind/edit?alias=flash')),
        array('label' => yii::t('site', 'Update Refurbished Page'), 'url' => array('//orangelots/orangelots-kind/edit?alias=refurbished')),
        array('label' => yii::t('site', 'Update Upcoming deals Page'), 'url' => array('//orangelots/orangelots-kind/edit?alias=upcoming_deals')),
    )
);

$items[] = array(
    'label' => yii::t('site', 'Auctions'),
    'url' => '#',
    'class' => 'pull-left',
    'items' => array(
        array('label' => yii::t('site', 'Create Auction'), 'url' => array('//auctions/orangelots-auctions/create')),
        array('label' => yii::t('site', 'Management'), 'url' => array('//auctions/orangelots-auctions/admin')),
    )
);

$items[] = array(
    'label' => yii::t('site', 'Invoices'),
    'url' => '',
    'class' => 'pull-left',
    'items' => array(
        array('label' => yii::t('site', 'List'), 'url' => array('/admin/invoices/')),
    )
);

$items[] = array(
    'label' => yii::t('site', 'Flash deal'),
    'url' => '#',
    'class' => 'pull-left',
    'items' => array(
        array('label' => yii::t('site', 'Create Flash Deal'), 'url' => array('//flashdeal/default/create')),
        array('label' => yii::t('site', 'Management'), 'url' => array('//flashdeal/default/admin')),
    )
);


$items[] = array(
	'label' => yii::t('site', 'Tips'),
	'url' => '#',
	'class' => 'pull-left',
	'items' => array(
		array('label' => yii::t('site', 'Create Tip'), 'url' => array('//news/default/create')),
		array('label' => yii::t('site', 'Management'), 'url' => array('//news/default/index')),
		array('label' => yii::t('site', 'Create category'), 'url' => array('//news/default/createCategory')),
		array('label' => yii::t('site', 'List of categories'), 'url' => array('//news/default/categoriesList')),
	)
);
$items[] = array(
    'label' => yii::t('site', 'Settings'),
    'url' => '#',
    'class' => 'pull-left',
    'items' => array(
        array('label' => yii::t('site', 'Settings'), 'url' => array('//admin/admin/settings')),
        array(
            'label' => yii::t('site', 'Delivery And Shipping Terms'),
            'url' => '#',
            'class' => 'pull-left',
            'items' =>array(
                array('label' => yii::t('site', 'Delivery'), 'url' => array('//delivery/admin')),
                array('label' => yii::t('site', 'Shipping Terms'), 'url' => array('//shipping-terms/admin')),
                array('label' => yii::t('site', 'Condition'), 'url' => array('//condition/admin')),
                array('label' => yii::t('site', 'Auctions Status'), 'url' => array('//orangelots-auctions-status/admin')),
                array('label' => yii::t('site', 'Manufacture'), 'url' => array('//orangelots-manufacture/admin')),
                array('label' => yii::t('site', 'Style'), 'url' => array('//orangelots-style/admin'))
            ),
        ),
    )
);

$items[] = array(
    'label' => yii::t('site', 'Subscribers'),
    'url' => '#',
    'class' => 'pull-left',
    'items' => array(
        array('label' => yii::t('site', 'Subscribers'), 'url' => array('//subscribe/default/index')),
    )
);

?>
<div class="">
	<?$this->widget(
		'bootstrap.widgets.TbNavbar',
		array(
			'type' => '',
			'fixed' => false,
			'brand' => '<img src="'. baseUrl().'/images/logo.png" '. 'alt="" style="height:33px">',
			'brandUrl' => createUrl('site/index'),
			'collapse' => true,
			'items' => array(
				array(
					'class' => 'bootstrap.widgets.TbMenu',
					'items' => $items,
				),
				array(
					'class' => 'bootstrap.widgets.TbMenu',
					'htmlOptions' => array('class' => 'pull-right', 'style' => ''),
					'items' => array(
						array(
							'label' => yii::t('site', 'Logout') . '(' . yii()->user->email . ')',
							'url' => array('/user/logout'),
							'visible' => !yii()->user->isGuest
						),
					)
				),
			),
		)
	);
	?>
</div>