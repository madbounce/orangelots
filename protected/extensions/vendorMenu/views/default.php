<? $items = array(); ?>
<?
$items[] = array('label' => yii::t('site', 'Account'), 'url' => '#', 'class' => 'pull-left', 'items' => array(
    array('label' => yii::t('site', 'Personal Settings'), 'url' => array('')),
    array('label' => yii::t('site', 'Billing Settings'), 'url' => array('')),
));


$items[] = array('label' => yii::t('site', 'Messages'), 'url' => '#', 'class' => 'pull-left', 'items' => array(
    array('label' => yii::t('site', 'Messages'), 'url' => array('')),
    array('label' => yii::t('site', 'Questions'), 'url' => array('')),
));

$items[] = array('label' => yii::t('site', 'Buy'), 'url' => '#', 'class' => 'pull-left', 'items' => array(
    array('label' => yii::t('site', 'Auctions'), 'url' => array('')),
    array('label' => yii::t('site', 'Deals'), 'url' => array('')),
    array('label' => yii::t('site', 'Vendors'), 'url' => array('')),
    array('label' => yii::t('site', 'Payments'), 'url' => array('')),
));

$items[] = array('label' => yii::t('site', 'Sell'), 'url' => '#', 'class' => 'pull-left', 'items' => array(
    array('label' => yii::t('site', 'Auctions'), 'url' => array('')),
    array('label' => yii::t('site', 'Bids'), 'url' => array('')),
    array('label' => yii::t('site', 'Deals'), 'url' => array('')),
    array('label' => yii::t('site', 'Sales'), 'url' => array('')),
    array('label' => yii::t('site', 'Buyers'), 'url' => array('')),
    array('label' => yii::t('site', 'Payments'), 'url' => array('')),
));

?>
<div class="">
    <?$this->widget('bootstrap.widgets.TbNavbar', array(
        'type' => '',
        'fixed' => false,
        'brand' => yii()->name,
        'brandUrl' => createUrl('site/index'),
        'collapse' => true,
        'items' => array(
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'items' => $items,
            ),
            array(
                'class' => 'bootstrap.widgets.TbMenu',
                'htmlOptions' => array('class' => 'pull-right', 'style' => ''),
                'items' => array(array('label' => yii::t('site', 'Logout') . '(' . yii()->user->email . ')', 'url' => array('/user/logout'), 'visible' => !yii()->user->isGuest),)
            ),
        ),
    ));
    ?>
</div>