<?

class RealEstateSearchController extends Controller
{
	public function actionSearch()
	{
		$arr = array(
			'valuesCheckbox' => '',
			'valuesSelect' => '',
			'valuesInteger' => '',
			'valuesFloat' => '',
			'valuesString' => '',
		);
		$arr2 = array();
		$i = 1;
		if (!empty($_REQUEST['data'])) {
			$req = $_REQUEST['data'];
			if (!empty($req)) {
				foreach ($req AS $key => $val) {
					foreach ($val AS $value_1 => $value_2) {
						if ($key == 'valuesCheckbox') {
							$arr['valuesCheckbox'] .= ' (' . $key . '.attribute_id=' . $value_2 . ') ';
							if ($i != count($val)) {
								$arr['valuesCheckbox'] .= 'OR';
								$i++;
							} else {
								$i = 1;
							}
						} else {
							if ($key == 'valuesSelect') {
								$arr['valuesSelect'] .= ' (' . $key . '.attribute_id=' . $value_1 . ' AND ' . $key . '.option_id = ' . $value_2 . ' ) ';
								if ($i != count($val)) {
									$arr['valuesSelect'] .= 'OR';
									$i++;
								} else {
									$i = 1;
								}
							} elseif ($key == 'valuesInteger') {
								$arr['valuesInteger'] .= ' (' . $key . '.attribute_id=' . $value_1 . ' AND ' . $key . '.value = ' . $value_2 . ' ) ';
								if ($i != count($val)) {
									$arr['valuesInteger'] .= 'OR';
									$i++;
								} else {
									$i = 1;
								}
							} elseif ($key == 'valuesFloat') {

								$arr['valuesFloat'] .= ' (' . $key . '.attribute_id=' . $value_1 . ' AND ' . $key . '.value = ' . $value_2 . ' ) ';
								if ($i != count($val)) {
									$arr['valuesFloat'] .= 'OR';
									$i++;
								} else {
									$i = 1;
								}
							} elseif ($key == 'valuesString') {
								$arr['valuesString'] .= ' (' . $key . '.attribute_id=' . $value_1 . ' AND ' . $key . '.value = ' . $value_2 . ' ) ';
								if ($i != count($val)) {
									$arr['valuesString'] .= 'OR';
									$i++;
								} else {
									$i = 1;
								}
							}
						}

					}
				}

				foreach ($arr as $key_1 => $condition_1) {
					if ($condition_1 != '') {
						$arr2[$key_1] = array(
							'select' => false,
							'condition' => $condition_1,
						);
					}

				}
				$new_obj = array();
				$realty_id = array();
				foreach ($arr2 AS $key_3 => $value_3) {
					$new_arr = array();
					$new_arr_2 = array();
					$new_arr_2[$key_3] = $value_3;
					$new_arr = $new_arr_2;
					$realty = Realty::model()->with($new_arr)->findAll();
					$new_obj[] = $realty;
				}

				foreach ($new_obj AS $value_4) {
					foreach ($value_4 AS $key5 => $value5) {
						$realty_id[] = $value5->id;
					}

				}
				$ids = array_unique($realty_id);
				$z = Realty::model()->findAllByPk($ids);
				$this->renderPartial('application.views.realestatesearch._view', array('result' => $z));

			} else {
				echo 'Ничего не найдено';
			}
		}
		echo 'Ничего не найдено';
	}

	public function actionSearch2()
	{
		if (yii()->request->isAjaxRequest) {
			$variables = $this->controller->getVariables();
			if (isset($variables['alias'])) {
				$this->alias = $variables['alias'];
				/*todo:Сделать как в Result * можно дергать в поиске для ajax чтобы были оиднаковые сборки $this->createRelCondition(....)*/

				$category = RealtyCategory::model()->findByAttributes(array('alias' => $this->alias));
				$filters = array();

				/*todo:Сделать как в Result * можно дергать в поиске для ajax чтобы были оиднаковые сборки $this->createRelCondition(....)*/
				if (isset($_POST['group'])) {
					$group = $_POST['group'];
					if (!empty($group)) {
						$group = array_unique($group);
					}
				}

				if (isset($_POST['attr'])) {
					$attr = $_POST['attr'];
				}
				$attrStr = '';
				if (!empty($attr)) {
					foreach ($attr as $key => $val) {
						$attribute = RealtyAttribute::model()->findByPk($val);
						$filters['attr'][$key] = $attribute->alias;
						if ($key == 0) {
							$attrStr .= 'valuesCheckbox.attribute_id=' . $val;
						} else {
							$attrStr .= ' OR valuesCheckbox.attribute_id=' . $val;
						}
					}
				}
				/*todo:Сделать как в Result * можно дергать в поиске для ajax чтобы были оиднаковые сборки $this->createRelCondition(....)*/

				$realty = Realty::model()->with(
					array(
						'category' => array(
							'select' => false,
							'condition' => 'category.id=' . $category->id
						),
						'valuesCheckbox' => array(
							'select' => false,
							'condition' => $attrStr,
						),
					)
				)->count('t.is_active=1');

				$data = CMap::mergeArray(array("cat_alias" => $this->alias), $filters);
				$url = baseUrl() . 'realty/list-result/?cat_alias=' . $this->alias;
				foreach ($data as $key => $val) {
					if (is_array($val)) {
						foreach ($val as $key1 => $val1) {
							if ($key1 == 0) {
								$url .= '&' . $key . '=' . $val1;
							} else {
								$url .= ',' . $val1;
							}
						}
					}
				}

				$arResp = array();
				$arResp['count'] = $realty;
				$arResp['url'] = $url;
				echo CJSON::encode($arResp);
			}
		}
		yii()->end();
	}

	public function actionList()
	{
		/*
		 * можно дергать в поиске для ajax чтобы были оиднаковые сборки $this->createRelCondition(....)
		 * */
		$categoryCondition = '';
		if (isset($_REQUEST['cat_alias'])) {
			$category = new RealtyCategory();
			$categoryCondition = $this->createRelCondition(
				$_REQUEST['cat_alias'],
				'cat_alias',
				$category,
				"category",
				'id'
			);
			$attrFilter = $categoryCondition['filterData'];
			$categoryCondition = $categoryCondition['condition'];

			$arAttr[] = $attrFilter;

		}

		$attrCondition = '';
		if (isset($_REQUEST['attr'])) {
			$attr = new RealtyAttribute();

			$attrCondition = $this->createRelCondition(
				$_REQUEST['attr'],
				'attr',
				$attr,
				'valuesCheckbox',
				'attribute_id'
			);

			$attrFilter = $attrCondition['filterData'];
			$attrCondition = $attrCondition['condition'];

			$arAttr[] = $attrFilter;
		}
		/*todo: перебрать relation  подставить нужный condition полузуясь конструктором ниже*/
		dump($categoryCondition, false);
		dump($attrCondition, false);
		$realty = Realty::model()->with(
			array(
				'category' => array(
					'condition' => $categoryCondition,
				),
				'valuesCheckbox' => array(
					'select' => false,
					'condition' => $attrCondition
				),
				/* 'valuesCheckbox' => array(
						'select' => false,
						'condition' => $attrCondition
					),
					'valuesCheckbox' => array(
						'select' => false,
						'condition' => $attrCondition
					),
					'valuesCheckbox' => array(
						'select' => false,
						'condition' => $attrCondition
					),
					'valuesCheckbox' => array(
						'select' => false,
						'condition' => $attrCondition
					),*/
			)
		)->findAll();

		$this->render(
			'list',
			array(
				'realty' => $realty,
				'aRattr' => $arAttr,
				'count' => count($realty),
			)
		);

	}


	private function createRelCondition($request, $requestName, $model, $relationName, $relAttr)
	{
		$condition = '';
		$items = explode(',', $request);
		$arAttr = array();
		if (!empty($items)) {
			$count = 0;
			foreach ($items as $key => $val) {
				if ($val != "") {
					$arAttr[$requestName][] = $val;
					$itemModel = $model->findByAttributes(array('alias' => $val));
					if ($itemModel != null) {
						if ($count == 0) {
							$count++;
							$condition .= $relationName . '.' . $relAttr . '=' . $itemModel->id;
						} elseif ($val != "") {
							$condition .= ' OR ' . $relationName . '.' . $relAttr . '=' . $itemModel->id;
						}
					}
				}
			}
		}

		return array('condition' => $condition, 'filterData' => $arAttr);
	}
}