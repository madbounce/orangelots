<?
Yii::import('application.controllers.*');

class SiteController extends Controller
{
    public $layout = '//layouts/main';

    public function actions()
    {
        return array(
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xe1e1e1,
            ),
        );
    }

    public function actionIndex()
    {
        $category = OrangelotsKind::model()->findAllByAttributes(array('active' => 1));
        if (isset($category)) {
            $array = array();
            foreach ($category AS $key => $value) {
                $category_auction = OrangelotsAuctions::model()->findByAttributes(
                    array('active' => 1, 'status' => 1, 'category_id' => $value->id),
                    array('order' => 'saving_buy_now DESC')
                );
                $category_auction_min_bid = OrangelotsAuctions::model()->findByAttributes(
                    array('active' => 1, 'status' => 1, 'category_id' => $value->id),
                    array('order' => 'saving_min_bid DESC')
                );
                if ($category_auction->saving_buy_now > $category_auction_min_bid->saving_min_bid) {
                    if ((isset($category_auction) && (!empty($category_auction)))) {
                        $array[] = $category_auction;
                    }
                } else {
                    if ((isset($category_auction_min_bid) && (!empty($category_auction_min_bid)))) {
                        $array[] = $category_auction_min_bid;
                    }
                }
            }
        }
        $footer = OrangelotsAuctions::model()->findAllByAttributes(
            array('set_on_main' => 1, 'active' => 1, 'status' => 1),
            array('limit' => 4)
        );
        $this->render(
            'index',
            array(
                'auctions' => $array,
                'footer' => $footer,
            )
        );
    }


    public function actionError()
    {
        if ($error = yii()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest) {
                echo $error['message'];
            } else {
                $this->render('error', $error);
            }
        }
    }

    public function actionSearch($category = null)
    {
        if (yii()->request->isAjaxRequest) {

            if (isset($category)) {
                $category = RealtyCategory::model()->findByAttributes(array('alias' => $category));
                $filters = array();

                if (isset($_POST['group'])) {
                    $group = $_POST['group'];
                    if (!empty($group)) {
                        $group = array_unique($group);
                    }
                }

                if (isset($_POST['attr'])) {
                    $attr = $_POST['attr'];
                }
                $attrStr = '';
                if (!empty($attr)) {
                    foreach ($attr as $key => $val) {
                        $attribute = RealtyAttribute::model()->findByPk(array('alias' => $val));
                        if ($key == 0) {
                            $attrStr .= 'attribute_id=' . $val;
                        } else {
                            $attrStr .= ' OR attribute_id=' . $val;
                        }
                    }
                }
                if ($attrStr != '') {
                    $realty = Realty::model()->with(
                        array(
                            'category' => array(
                                'select' => false,
                                'condition' => 'category.id=' . $category->id
                            ),
                            'valuesCheckbox' => array(
                                'select' => false,
                                'condition' => $attrStr,
                            ),
                            'valuesString' => array(
                                'select' => false,
                                'condition' => $attrStr,
                            ),
                            'valuesInteger' => array(
                                'select' => false,
                                'condition' => $attrStr,
                            ),
                            'valuesFloat' => array(
                                'select' => false,
                                'condition' => $attrStr,
                            ),
                            'valuesCheckbox' => array(
                                'select' => false,
                                'condition' => $attrStr,
                            ),
                        )
                    )->count();
                    $data = CMap::mergeArray(array("cat_alias" => $this->alias), $filters);
                    $url = baseUrl() . 'realty/list-result/?cat_alias=' . $this->alias;
                    foreach ($data as $key => $val) {
                        if (is_array($val)) {
                            foreach ($val as $key1 => $val1) {
                                if ($key1 == 0) {
                                    $url .= '&' . $key . '=' . $val1;
                                } else {
                                    $url .= ',' . $val1;
                                }
                            }
                        }
                    }

                    $arResp = array();
                    $arResp['count'] = $realty;
                    $arResp['url'] = $url;
                    echo CJSON::encode($arResp);
                }
            }
        }
        yii()->end();
    }

    public function actionSubscribeEmail()
    {
        if (isset($_POST['Subscribe']['mail'])) {
            $usr = OrangelotsUsers::model()->findAllByAttributes(array('email' => $_POST['Subscribe']['mail']));
            $model = new OrangelotsSubscribe;
            $model->email = $_POST['Subscribe']['mail'];
            if ($model->validate()) {
                if ($model->save()) {
                    echo json_encode(
                        array(
                            'status' => 'error_msg',
                            'url' => '',
                            'html' => '<div class="successMessage">You are subscribed</div>'
                        )
                    );
                }
            } else {
                echo json_encode(
                    array(
                        'status' => 'error_msg',
                        'url' => '',
                        'html' => '<div class="errorMessage">' . $model->getError('email') . '</div>'
                    )
                );
            }
        }
    }

    public function actionUpload()
    {
        if (yii()->user->isGuest) {
            $this->redirect(Yii::app()->createUrl('user/login'));
        }
        $display = false;
        $this->layout = '//layouts/main';
        $docModel = new Document;
        if (isset($_POST['FinishForm']['obj'])) {
            $object = $_POST['FinishForm']['obj'];
            $object_en = CJSON::decode($object);
            $unsaved = array();
            if (!empty($object_en)) {
                foreach ($object_en AS $auction) {
                    $model = OrangelotsAuctions::model()->findByPk($auction);
                    $model->active = 1;
                    $model->status = 1;
                    if ($model->save()) {

                    } else {
                        $unsaved[] = $model->id;
                    }
                }
                $this->redirect(Yii::app()->createUrl('user/sell/auction/posted-by-me'));
            }


        } elseif (isset($_POST['Document'])) {

            $uploaded = array();
            Yii::import('application.extensions.excelreaderphp.PHPExcel', true);
            $obj = CUploadedFile::getInstance($docModel, 'document');
            if (!isset($obj) || ($obj == NULL)) {
                Yii::app()->user->setFlash('error', "File not exists");
                $this->redirect(Yii::app()->createUrl('site/upload'));
            }
            $file = $obj->tempName;
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file); //$file --> your filepath and filename
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
            $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
            $this->clearNotActiveAuctions(yii()->user->id);
            for ($row = 0; $row <= $highestRow; ++$row) {
                if ($row > 1) {
                    $model = new OrangelotsAuctions;
                    $model->name = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $model->manufacture = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $model->mvrp = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $model->units_in_lot = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $model->buy_now_price = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $model->min_bid = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $model->min_unit = $objWorksheet->getCellByColumnAndRow(6, $row)->getValue();
                    $model->start_date_unix = CDateTimeParser::parse(
                        $objWorksheet->getCellByColumnAndRow(7, $row)->getValue(),
                        'MM/dd/yyyy HH:mm a'
                    );
                    $model->end_date_unix = CDateTimeParser::parse(
                        $objWorksheet->getCellByColumnAndRow(8, $row)->getValue(),
                        'MM/dd/yyyy HH:mm a'
                    );
                    $model->start_date = yii()->dateFormatter->format('yyyy/MM/dd HH:mm', $model->start_date_unix);
                    $model->end_date = yii()->dateFormatter->format('yyyy/MM/dd HH:mm', $model->end_date_unix);

                    $model->au_date = yii()->dateFormatter->format('yyyy-MM-dd', $model->start_date_unix);
                    $model->au_time = yii()->dateFormatter->format('HH:mm a', $model->start_date_unix);
                    $model->au_am_pm = yii()->dateFormatter->format('a', $model->start_date_unix);
                    $model->au_date_end = yii()->dateFormatter->format('yyyy-MM-dd', $model->end_date_unix);
                    $model->au_time_end = yii()->dateFormatter->format('HH:mm a', $model->end_date_unix);
                    $model->au_am_pm_end = yii()->dateFormatter->format('a', $model->end_date_unix);

                    $model->condition = Condition::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue()))
                    )->id; //id
                    $model->color = $objWorksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $model->upholstery_material = $objWorksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $model->number_of_pieces = $objWorksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $model->furniture_function = $objWorksheet->getCellByColumnAndRow(13, $row)->getValue();
                    $model->dimensions = $objWorksheet->getCellByColumnAndRow(14, $row)->getValue();
                    $model->weight = $objWorksheet->getCellByColumnAndRow(15, $row)->getValue();
                    $model->description = $objWorksheet->getCellByColumnAndRow(16, $row)->getValue();
                    $model->terms = $objWorksheet->getCellByColumnAndRow(17, $row)->getValue();
                    $model->shipping_terms = ShippingTerms::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()))
                    )->id; //id
                    $model->shipping_terms_other = $objWorksheet->getCellByColumnAndRow(19, $row)->getValue();
                    $model->delivery = Delivery::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(20, $row)->getValue()))
                    )->id; //id
                    $model->delivery_other = $objWorksheet->getCellByColumnAndRow(21, $row)->getValue();
                    $model->returns_warranty = $objWorksheet->getCellByColumnAndRow(22, $row)->getValue();
                    $model->vendors_information = $objWorksheet->getCellByColumnAndRow(23, $row)->getValue();
                    $model->category_id = OrangelotsKind::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(24, $row)->getValue()))
                    )->id; //id
                    $model->style_id = OrangelotsStyle::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(24, $row)->getValue()))
                    )->id; //id
                    $model->user_id = yii()->user->id;
                    $model->active = 0;
                    $model->import = 1;

                    if ($model->mvrp != 0 && isset($model->mvrp)) {
                        $model->saving_buy_now = round(((($model->mvrp - $model->buy_now_price) / $model->mvrp) * 100),0);
                    }
                    if ($model->mvrp != 0 && isset($model->mvrp)) {
                        $model->saving_min_bid = round(((($model->mvrp - $model->min_bid) / $model->mvrp) * 100),0);
                    }

                    $images_from_file = $objWorksheet->getCellByColumnAndRow(26, $row)->getValue();
                    if ($model->save()) {
                        $uploaded[] = $model->id;
                        $img = explode(' ', $images_from_file);
                        foreach ($img AS $key2 => $value2):
                            $value2 = trim($value2);
                            if (($value2 != '') && ($value2 != null)):
                                $url = $value2;
                                $parse = parse_url($url);
                                $img_ext = pathinfo($parse['path'], PATHINFO_EXTENSION);

                                $fileDir = dirname(dirname(__FILE__));
                                $baseDir = dirname($fileDir);
                                if (($img_ext == '') || ($img_ext == null) || ($img_ext == undefined)) {
                                    $path = $baseDir . '/images/wtf.png';
                                    file_put_contents($path, file_get_contents($url));
                                    $img_ext = pathinfo($path, PATHINFO_EXTENSION);
                                } else {
                                    $model_img = new OrangelotsPhoto;
                                    $model_img->watemark = !empty(Yii::app()->session['image_set_watemark']);
                                    $model_img->realty_id = $model->id;
                                    $model_img->file = '';
                                    $model_img->image_extension = $img_ext;
                                    if ($model_img->save()) {
                                        $my_path = dirname(Yii::app()->basePath);
                                        $directory = $my_path . '/photos/realty/' . $model_img->id;
                                        if (file_exists($directory) == false) {
                                            mkdir($directory);
                                            chmod($directory, 0777);
                                        }
                                        if (file_exists($directory)) {
                                            $path_2 = $baseDir . '/photos/realty/' . $model_img->id . '/origin.' . $model_img->image_extension;
                                            file_put_contents($path_2, file_get_contents($url));
                                        }

                                    }
                                }

                            endif;
                        endforeach;
                    } else {
                        throw new CHttpException(406, 'Some of data not acceptable');
                    }

                }
            }
            $display = true;
        }

        /*$this->render(
            '_upload',
            array(
                'docModel' => $docModel,
            )
        );*/
        $this->render(
            '_upload',
            array(
                'highestRow' => $highestRow,
                'highestColumnIndex' => $highestColumnIndex,
                'objWorksheet' => $objWorksheet,
                'docModel' => $docModel,
                'obj' => $uploaded,
                'display' => $display,
            )
        );
    }
    public function actionGetFile($deal=false) {
        if($deal)
            $file = Yii::app()->basePath.'/download/file_import_deal.xlsx';
        else
            $file = Yii::app()->basePath.'/download/file_import.xlsx';
        if (file_exists($file)) {
            // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
            // если этого не сделать файл будет читаться в память полностью!
            if (ob_get_level()) {
                ob_end_clean();
            }
            // заставляем браузер показать окно сохранения файла
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            // читаем файл и отправляем его пользователю
            readfile($file);
            exit;
        }
    }

    public function clearNotActiveAuctions($id)
    {
        OrangelotsAuctions::model()->deleteAllByAttributes(array('user_id' => $id, 'active' => '0', 'import'=> '1'));
    }

    public function actionUploadDeal()
    {
        if (yii()->user->isGuest) {
            $this->redirect(Yii::app()->createUrl('user/login'));
        }
        $display = false;
        $this->layout = '//layouts/main';
        $docModel = new Document;
        if (isset($_POST['FinishForm']['obj'])) {
            $object = $_POST['FinishForm']['obj'];
            $object_en = CJSON::decode($object);
            $unsaved = array();
            if (!empty($object_en)) {
                foreach ($object_en AS $auction) {
                    $model = OrangelotsAuctions::model()->findByPk($auction);
                    $model->active = 1;
                    if(($model->au_date == (yii()->dateFormatter->format('yyyy-MM-dd', time()))) || ($model->start_date_unix<=time()))
                        $model->status = 2;
                    else
                        $model->status = 1;

//                    dump($model->attributes);die;
                    if ($model->save()) {

                    } else {
                        $unsaved[] = $model->id;
                    }
                }
                $this->redirect(Yii::app()->createUrl('auctions/orangelots-auctions/sell-deal'));
            }


        } elseif (isset($_POST['Document'])) {

            $uploaded = array();
            Yii::import('application.extensions.excelreaderphp.PHPExcel', true);
            $obj = CUploadedFile::getInstance($docModel, 'document');
            if (!isset($obj) || ($obj == NULL)) {
                Yii::app()->user->setFlash('error', "File not exists");
                $this->redirect(Yii::app()->createUrl('site/upload'));
            }
            $file = $obj->tempName;
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file); //$file --> your filepath and filename
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
            $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
            $this->clearNotActiveAuctions(yii()->user->id);
            for ($row = 0; $row <= $highestRow; ++$row) {
                if ($row > 1) {
                    $model = new OrangelotsAuctions;
                    $model->name = $objWorksheet->getCellByColumnAndRow(0, $row)->getValue();
                    $model->manufacture = $objWorksheet->getCellByColumnAndRow(1, $row)->getValue();
                    $model->mvrp = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
                    $model->lots_available = $objWorksheet->getCellByColumnAndRow(3, $row)->getValue();
                    $model->units_in_lot = $objWorksheet->getCellByColumnAndRow(4, $row)->getValue();
                    $model->price_per_unit = $objWorksheet->getCellByColumnAndRow(5, $row)->getValue();
                    $model->start_date_unix = CDateTimeParser::parse(
                        $objWorksheet->getCellByColumnAndRow(6, $row)->getValue(),
                        'MM/dd/yyyy'
                    );
                    $model->end_date_unix = $model->start_date_unix + (24*60*60);
                    $model->start_date = yii()->dateFormatter->format('yyyy/MM/dd 00:00', $model->start_date_unix);
                    $model->end_date = yii()->dateFormatter->format('yyyy/MM/dd 00:00', $model->end_date_unix);

                    $model->au_date = yii()->dateFormatter->format('yyyy-MM-dd', $model->start_date_unix);
                    $model->au_time = '12:00 AM';//yii()->dateFormatter->format('HH:mm a', $model->start_date_unix);
                    $model->au_am_pm = 'AM';//yii()->dateFormatter->format('a', $model->start_date_unix);
                    $model->au_date_end = yii()->dateFormatter->format('yyyy-MM-dd', $model->end_date_unix);
                    $model->au_time_end = '12:00 AM';//yii()->dateFormatter->format('HH:mm a', $model->end_date_unix);
                    $model->au_am_pm_end = 'AM';//yii()->dateFormatter->format('a', $model->end_date_unix);

                    $model->condition = Condition::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue()))
                    )->id; //id
                    $model->color = $objWorksheet->getCellByColumnAndRow(8, $row)->getValue();
                    $model->upholstery_material = $objWorksheet->getCellByColumnAndRow(9, $row)->getValue();
                    $model->number_of_pieces = $objWorksheet->getCellByColumnAndRow(10, $row)->getValue();
                    $model->furniture_function = $objWorksheet->getCellByColumnAndRow(11, $row)->getValue();
                    $model->dimensions = $objWorksheet->getCellByColumnAndRow(12, $row)->getValue();
                    $model->weight = $objWorksheet->getCellByColumnAndRow(13, $row)->getValue();
                    $model->description = $objWorksheet->getCellByColumnAndRow(14, $row)->getValue();
                    $model->terms = $objWorksheet->getCellByColumnAndRow(15, $row)->getValue();
                    $model->shipping_terms = ShippingTerms::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(16, $row)->getValue()))
                    )->id; //id
                    $model->shipping_terms_other = $objWorksheet->getCellByColumnAndRow(17, $row)->getValue();
                    $model->delivery = Delivery::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(18, $row)->getValue()))
                    )->id; //id
                    $model->delivery_other = $objWorksheet->getCellByColumnAndRow(19, $row)->getValue();
                    $model->returns_warranty = $objWorksheet->getCellByColumnAndRow(20, $row)->getValue();
                    $model->vendors_information = $objWorksheet->getCellByColumnAndRow(21, $row)->getValue();
                    $model->category_id = OrangelotsKind::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(22, $row)->getValue()))
                    )->id; //id
                    $model->style_id = OrangelotsStyle::model()->findByAttributes(
                        array('name' => trim($objWorksheet->getCellByColumnAndRow(23, $row)->getValue()))
                    )->id; //id
                    $model->user_id = yii()->user->id;
                    $model->active = 0;
                    $model->import = 1;
                    $model->is_flashdeal = 1;

                    if ($model->mvrp != 0 && isset($model->mvrp)) {
                        $model->saving_price_per_unit = round(((($model->mvrp - $model->price_per_unit) / $model->mvrp) * 100),0);
                    }

                    $images_from_file = $objWorksheet->getCellByColumnAndRow(24, $row)->getValue();
//                    dump($model->attributes);die;
                    if ($model->save()) {
                        $uploaded[] = $model->id;
                        $img = explode(' ', $images_from_file);
                        foreach ($img AS $key2 => $value2):
                            $value2 = trim($value2);
                            if (($value2 != '') && ($value2 != null)):
                                $url = $value2;
                                $parse = parse_url($url);
                                $img_ext = pathinfo($parse['path'], PATHINFO_EXTENSION);

                                $fileDir = dirname(dirname(__FILE__));
                                $baseDir = dirname($fileDir);
                                if (($img_ext == '') || ($img_ext == null) || ($img_ext == undefined)) {
                                    $path = $baseDir . '/images/wtf.png';
                                    file_put_contents($path, file_get_contents($url));
                                    $img_ext = pathinfo($path, PATHINFO_EXTENSION);
                                } else {
                                    $model_img = new OrangelotsPhoto;
                                    $model_img->watemark = !empty(Yii::app()->session['image_set_watemark']);
                                    $model_img->realty_id = $model->id;
                                    $model_img->file = '';
                                    $model_img->image_extension = $img_ext;
                                    if ($model_img->save()) {
                                        $my_path = dirname(Yii::app()->basePath);
                                        $directory = $my_path . '/photos/realty/' . $model_img->id;
                                        if (file_exists($directory) == false) {
                                            mkdir($directory);
                                            chmod($directory, 0777);
                                        }
                                        if (file_exists($directory)) {
                                            $path_2 = $baseDir . '/photos/realty/' . $model_img->id . '/origin.' . $model_img->image_extension;
                                            file_put_contents($path_2, file_get_contents($url));
                                        }

                                    }
                                }

                            endif;
                        endforeach;
                    } else {
                        throw new CHttpException(406, 'Some of data not acceptable');
                    }

                }
            }
            $display = true;
        }
        $this->render(
            'upload_deal',
            array(
                'highestRow' => $highestRow,
                'highestColumnIndex' => $highestColumnIndex,
                'objWorksheet' => $objWorksheet,
                'docModel' => $docModel,
                'obj' => $uploaded,
                'display' => $display,
            )
        );
    }
}