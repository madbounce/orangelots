<?

class RealEstateController extends Controller
{
    public $layout = '//layouts/main';

    /*public function filters()
    {
        return array(
            array('ext.seo.components.SeoFilter + view'),
        );
    }*/

    public function behaviors()
    {
        return array(
            'seo' => array('class' => 'ext.seo.components.SeoControllerBehavior'),
        );
    }

    public function actionIndex()
    {
	    $model = new Realty('search');
        $model->unsetAttributes();
        if (isset($_GET['Realty'])) {
            $model->attributes = $_GET['Realty'];
        }
        $this->render('application.views.realestate.index', array(
            'model' => $model
        ));
    }

    public function actionCategory($category)
    {
        $model = RealtyCategory::model()->findByAttributes(array('alias' => $category));
        $realty = $model->realties;
        $this->render('application.views.realestate.category', array(
            'realty' => $realty,
            'model' => $model,
        ));
    }

    public function actionView($category, $alias)
    {
        $model = $this->loadModel($alias);
        if (!empty($model)){
            $session  = Yii::app()->session['lastViewItem'];
            $session[] = $model->id;
            $result = array_unique($session); 
            Yii::app()->session['lastViewItem'] = $result;
        }

        $this->render('application.views.realestate.view', array(
            'real-estate/view', 'model' => $model, 'foo' => 'bar'
        ));
    }

    public function loadModel($alias)
    {
        return Realty::model()->findByAttributes(array('alias' => $alias));
    }

    public function actionSearch($category)
    {

        if (isset($category)) {
            $category = RealtyCategory::model()->findByAttributes(array('alias' => $category));
            $filters = array();

            if (isset($_POST['group'])) {
                $group = $_POST['group'];
                if (!empty($group)) {
                    $group = array_unique($group);
                }
            }

            if (isset($_POST['attr'])) {
                $attr = $_POST['attr'];
            }
            $attrStr = '';
            if (!empty($attr)) {
                foreach ($attr as $key => $val) {
                    $attribute = RealtyAttribute::model()->findByPk(array('alias' => $val));
                    if ($key == 0) {
                        $attrStr .= 'attribute_id=' . $val;
                    } else {
                        $attrStr .= ' OR attribute_id=' . $val;
                    }
                }
            }
            if ($attrStr != '') {
                $realty = Realty::model()->with(array(
                    'category' => array(
                        'select' => false,
                        'condition' => 'category.id=' . $category->id
                    ),
                    'valuesCheckbox' => array(
                        'select' => false,
                        'condition' => $attrStr,
                    ),
                    'valuesString' => array(
                        'select' => false,
                        'condition' => $attrStr,
                    ),
                    'valuesInteger' => array(
                        'select' => false,
                        'condition' => $attrStr,
                    ),
                    'valuesFloat' => array(
                        'select' => false,
                        'condition' => $attrStr,
                    ), 'valuesCheckbox' => array(
                        'select' => false,
                        'condition' => $attrStr,
                    ),
                ))->count();
                $data = CMap::mergeArray(array("cat_alias" => $this->alias), $filters);
                $url = baseUrl() . 'realty/list-result/?cat_alias=' . $this->alias;
                foreach ($data as $key => $val) {
                    if (is_array($val)) {
                        foreach ($val as $key1 => $val1) {
                            if ($key1 == 0) {
                                $url .= '&' . $key . '=' . $val1;
                            } else {
                                $url .= ',' . $val1;
                            }
                        }
                    }
                }

                $arResp = array();
                $arResp['count'] = $realty;
                $arResp['url'] = $url;
                echo CJSON::encode($arResp);
            }
        }
//        }
        yii()->end();
    }
}