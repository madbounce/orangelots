<?php

class _OrangelotsUsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow all users to perform 'index' and 'view' actions
				'actions' => array('index', 'view'),
				'users' => array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('update'),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('create'),
				'users' => array('*'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions' => array('admin', 'delete'),
				'users' => array('admin'),
			),
			array('deny', // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new OrangelotsUsers;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if (isset($_POST['OrangelotsUsers'])) {
			$model->attributes = $_POST['OrangelotsUsers'];
			do {
				$usr = 'id' . rand(100000, 999999);
				$user = User::model()->findByAttributes(array('username' => $usr));
			} while ($user !== null);

			$model->username = $usr;
			if ($model->validate()) {
				$model->password = UserModule::encrypting($model->password);
				$model->activkey = UserModule::encrypting(microtime() . $model->password);
				$model->superuser = 0;
				$model->status = User::STATUS_NOACTIVE;
			}
			if ($model->save()) {
				$profile = new Profile;
				$profile->user_id = $model->id;
				$profile->save();
				yii()->user->setFlash('success', 'Registration has been successful. Activation link has been sent on your email. Please, check your email.');
				$activation_url = yii()->createAbsoluteUrl('/user/activation/activation', array("activkey" => $model->activkey, "email" => $model->email));
				UserModule::sendMail(
					$model->email,
					UserModule::t("You registered from {site_name}", array('{site_name}' => yii()->name)),
					UserModule::t(
						"Настоящим сообщаем, что системой {site_name} была получена заявка на участие в отборе участников для программы Битва Мастеров.\n
						 В заявке был указан ваш електронный адресс.\n
						 Если заявка была сформированна и отправленна вами, пройдите по следующей ссылке {activation_url} .\n
						 После активации заявки и ее удачного рассмотрения, мы дополнительно свяжемся с вами по вопросу последующего участия в программе отбора.\n
						 Вам присвоен регистрационный номер: {userID} .
						 Сохраните данное сообщение и регистрационный номер.
						 Данное сообщение не требует ответа.
						 ",
						array(
							'{activation_url}' => $activation_url,
							'{userID}' => $model->id,
							'{login}' => $model->email,
							'{password}' => $sourcePassword,
							'{site_name}' => yii()->name
						)
					)
				);
				$this->redirect(yii()->createUrl('site/index'));
			}
//				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if (isset($_POST['OrangelotsUsers'])) {
			$model->attributes = $_POST['OrangelotsUsers'];
			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('OrangelotsUsers');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = new OrangelotsUsers('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['OrangelotsUsers'])) {
			$model->attributes = $_GET['OrangelotsUsers'];
		}

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OrangelotsUsers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = OrangelotsUsers::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OrangelotsUsers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'orangelots-users-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
