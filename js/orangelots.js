$(document).ready(function () {

	$(".document_add_file").bind("click",function(){
		$("#add_file").click();
	});

	$("#signin").on('click', function(){
		if ($(this).hasClass('disable') ){
			return false;
		}
	});

	uploadPhotoButton = function (self, url) {
		var file = $(self).siblings("input[type=file]").val();
		{
			var data = new FormData($(self).parents("form")[0]);
			$.ajax({
				url: url,
				data: data,
				contentType: false,
				processData: false,
				type: "POST",
				beforeSend: function(){
					$("body").append(preload);
				},
				success: function (data) {
					$(".preload_mask").remove();
					$(".preloader").remove();
					location.reload();
//					$("body").html(data);
				}
			});
		}
	}

	$('#OrangelotsAuctions_price_per_unit, #OrangelotsAuctions_min_unit, input[name="Filter[current_bid_from]"], input[name="Filter[current_bid_to]"], input[name="Filter[count_bids_from]"], input[name="Filter[count_bids_to]"]').on('keypress', function (e) {
		var browser = get_name_browser();
		if (browser == 'Firefox'){
			e = e || event;

			if (e.ctrlKey || e.altKey || e.metaKey) return;
			/*if (e.keyCode < 48 || e.keyCode > 57) {
			 return false;
			 }*/
			if (e.key.match(/^\d+$/)){

			} else {
				return false;
			}
		} else {
			e = e || event;

			if (e.ctrlKey || e.altKey || e.metaKey) return;
			if (e.keyCode < 48 || e.keyCode > 57) {
			 return false;
			 }
			/*if (e.key.match(/^\d+$/)){

			} else {
				return false;
			}*/
		}

	});
	$('#price_per_unit, #OrangelotsAuctions_mvrp, #OrangelotsAuctions_buy_now_price, #OrangelotsAuctions_min_bid').on('keypress', function (e) {
		var browser = get_name_browser();
		if (browser == 'Firefox'){
		e = e || event;

		if (e.ctrlKey || e.altKey || e.metaKey) return;

		/*if ((e.keyCode < 44) || ( e.keyCode > 44 && e.keyCode < 46) || ( e.keyCode > 46 && e.keyCode < 48) ||( e.keyCode > 57)) {
			return false;
		}*/
		if ((e.key.match(/^\d+$/)) || (e.key == ".")){

		} else {
			return false;
		}
		} else {
			e = e || event;

			if (e.ctrlKey || e.altKey || e.metaKey) return;

			if ((e.keyCode < 44) || ( e.keyCode > 44 && e.keyCode < 46) || ( e.keyCode > 46 && e.keyCode < 48) || ( e.keyCode > 57) || (e.keyCode == 44)) {
				return false;
			}

		}
	});


	$('#OrangelotsAuctions_buy_now_price, #OrangelotsAuctions_min_bid').on('change', function () {
		var price = $(this).val();
		var mvrp = $('#OrangelotsAuctions_mvrp').val();
		if (mvrp == 0)
			mvrp = 1;
//		var total = (price * 100) / mvrp;
		var total = ((mvrp - price) / mvrp)*100;
		total = roundPlus(total, 0);
		$(this).parent().find('span.uni_form_row__afterinput_text_price').html(total + '%');
		$(this).parent().find('[type=hidden]').val(total);
	});
	$('#OrangelotsAuctions_mvrp').on('change', function () {
		var buy_now = $('#OrangelotsAuctions_buy_now_price').val();
		var min_bid = $('#OrangelotsAuctions_min_bid').val();
		var mvrp = $(this).val();
		if (mvrp == 0) {
			mvrp = 1;
		}
//		var total_buy_now = roundPlus((buy_now * 100) / mvrp, 2);
		var total_buy_now = roundPlus(((mvrp - buy_now) / mvrp)*100, 0);
//		var total_min_bid = roundPlus((min_bid * 100) / mvrp, 2);
		var total_min_bid = roundPlus(((mvrp - min_bid) / mvrp)*100, 0);
		$("#OrangelotsAuctions_saving_buy_now").val(total_buy_now);
		$("#OrangelotsAuctions_saving_min_bid").val(total_min_bid);
		$('#OrangelotsAuctions_buy_now_price').parent().find('span.uni_form_row__afterinput_text_price').html(total_buy_now + '%');
		$('#OrangelotsAuctions_min_bid').parent().find('span.uni_form_row__afterinput_text_price').html(total_min_bid + '%');


	});

	$('div.card_detail_tabs_menu ul li a').on('click', function(){
		if (!($(this).hasClass('active'))){
			var attr_class = $(this).attr('class');
			$('div.card_detail_tabs_menu ul li a').each(function(){
				$(this).removeClass('active');
			});
			$('div.card_detail__tabs_content').each(function(){
				$(this).removeClass('active');
				$(this).addClass('hide');
			});
			$(this).addClass('active');
			var div = 'div.card_detail__tab_'+ attr_class;
			$(div).removeClass('hide');
		}
	});


	$(".popup_close, button.btn.green_bnt.btn_sing_in").bind("click",function(event){
		event.preventDefault();
		$(this).closest(".popup__container").fadeOut()
	});

	$(document).on('click',"a.last_step_close img", function(){
		location.reload();
	});

	/*$("a.sign_in").bind("click",function(){
		$("div.popup__container").show();
	});*/

	$(document).on('change',"div.auction_list__sort_row__per_pages.pull-right select", function(){
		var index = ($(this)[0].selectedIndex) ;
		$("div.sorter.another_sorter ul li").eq(index).find('a').click();

	});
	 $(document).on('change',"#pagesizer", function(){
	 var data_url = ($(this).find('option:selected').attr("data-url")) ;
	 window.location.href = data_url;

	 });

	$(document).on('change', "#UserLogin_username, #UserLogin_password", function(){
		if($("#UserLogin_username").val != '' && $("#UserLogin_password").val()!=''){
			$('#signin').removeClass('disable');
		} else {
			$('#signin').addClass('disable');
		}
	})


	$(document).on('click', '.advanced_search', function(event){
		event.preventDefault();
		$('.advanced_search_block').stop().animate({height: 'toggle'});
	})
})

function roundPlus(x, n) { //x - число, n - количество знаков
	if (isNaN(x) || isNaN(n)) return false;
	var m = Math.pow(10, n);
	return Math.round(x * m) / m;
}
function get_name_browser(){
	// получаем данные userAgent
	var ua = navigator.userAgent;
	// с помощью регулярок проверяем наличие текста,
	// соответствующие тому или иному браузеру
	if (ua.search(/Chrome/) > 0) return 'Google Chrome';
	if (ua.search(/Firefox/) > 0) return 'Firefox';
	if (ua.search(/Opera/) > 0) return 'Opera';
	if (ua.search(/Safari/) > 0) return 'Safari';
	if (ua.search(/MSIE/) > 0) return 'Internet Explorer';
	// условий может быть и больше.
	// сейчас сделаны проверки только
	// для популярных браузеров
	return 'none';
}

function numeric_format(val, thSep, dcSep) {
	if (getDecimal(val) == '0.00'){
		val = val+'.00';
	}
	// Проверка указания разделителя разрядов
	if (!thSep) thSep = '';

	// Проверка указания десятичного разделителя
	if (!dcSep) dcSep = ',';

	var res = val.toString();
	var lZero = (val < 0); // Признак отрицательного числа

	// Определение длины форматируемой части
	var fLen = res.lastIndexOf('.'); // До десятичной точки
	fLen = (fLen > -1) ? fLen : res.length;

	// Выделение временного буфера
	var tmpRes = res.substring(fLen);
	var cnt = -1;
	for (var ind = fLen; ind > 0; ind--) {
		// Формируем временный буфер
		cnt++;
		if (((cnt % 3) === 0) && (ind !== fLen) && (!lZero || (ind > 1))) {
			tmpRes = thSep + tmpRes;
		}
		tmpRes = res.charAt(ind - 1) + tmpRes;
	}


	return tmpRes.replace('.', dcSep);

}
function getDecimal(num) {
	return (num - parseInt(num)).toFixed(2);
}






