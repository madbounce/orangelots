jQuery(document).ready(function ($) {
	preload = '<div class="preload_mask"><img class="preloader" src="' + baseUrl + '/images/loading.gif"></div>';
	$(".selectpicker").selectpicker();
    $(".singup__select_container select").selectpicker();
    $(".catalog_detail__price__numbers__select_row__select_container select").selectpicker();
    $(".uni_form__select_container select").selectpicker();
    $(".popup_form__select_container select").selectpicker();
    $(".posted_by__form_element select").selectpicker();
    $(".flash_deals_catalog__button_row__select_container select").selectpicker();
    $(".uni_form__card_date_month select, .uni_form__card_date_year select").selectpicker();
    $(".received__row__select_container select").selectpicker();


    $("#card_detail_carousel").jcarousel();


    $("#card_detail__collum_carousel").jcarousel({
        vertical: true,
        item: 2
    });

    $(document).on("click", ".catalog_detail__galery_image__control__btn", function (event) {
        event.preventDefault();
        if ($(this).hasClass("prev")) {
            $("#card_detail_carousel").jcarousel("scroll", "-=1");
        }
        if ($(this).hasClass("next")) {
            $("#card_detail_carousel").jcarousel("scroll", "+=1");
        }

    });

    $(document).on("click", ".card_detail__collum_carousel__control_btn", function (event) {
        //event.preventDefault();
        if ($(this).hasClass("top")) {
            $("#card_detail__collum_carousel").jcarousel("scroll", "-=1");
        }
        if ($(this).hasClass("bottom")) {
            $("#card_detail__collum_carousel").jcarousel("scroll", "+=1");
        }

        return false
    });

    $(document).on("click", ".catalog_detail__galery_image__carousel a", function (event) {
        event.preventDefault();
        var imageSrc = $(this).find("img").eq(0).attr("src");
        $(".catalog_detail__main_image").find("img").attr("src", imageSrc);


    });

	$("#auction_list__carousel").jcarousel({
		item: 3
	});

	$("#auction_list__carousel .jcarousel_controll").bind("click",function(event){
		event.preventDefault();
//        $("#auction_list__carousel").carousel("scroll", "1");
		if($(this).hasClass("jcarousel-prev")) {
			$("#auction_list__carousel").jcarousel("scroll", "-=1");
		}
		if($(this).hasClass("jcarousel-next")) {
			$("#auction_list__carousel").jcarousel("scroll", "+=1");
		}

	});

	$('#filterFormWidget input').on('click', function(){
		if($(this).attr('checked')){
			$(this).attr('checked', false);
		}
	});
});

function buyerReg(dataArr) {

    $.ajax({
        type: "POST",
        url: baseUrl + "/user/registration/buyerFirstStep",
        data: dataArr,
        success: function (data) {
            var resp = $.parseJSON(data);
            if (resp.err == 0) {
                $("#buyer-reg-first-step").replaceWith(resp.form);
            } else {
                if ('firstname' in resp) {
                    $("#OrangelotsUsers_firstname_em_").show();
                    $("#OrangelotsUsers_firstname_em_").text(resp.firstname);
                }
                if ('lastname' in resp) {
                    $("#OrangelotsUsers_lastname_em_").show();
                    $("#OrangelotsUsers_lastname_em_").text(resp.lastname);
                }
                if ('email' in resp) {
                    $("#OrangelotsUsers_email_em_").show();
                    $("#OrangelotsUsers_email_em_").text(resp.email);

                }
                if ('password' in resp) {
                    $("#OrangelotsUsers_password_em_").show();
                    $("#OrangelotsUsers_password_em_").text(resp.password);

                }
                if ('phone' in resp) {
                    $("#OrangelotsUsers_phone_em_").show();
                    $("#OrangelotsUsers_phone_em_").text(resp.phone);
                }
	            if ('zipcode' in resp) {
                    $("#OrangelotsUsers_zipcode_em_").show();
                    $("#OrangelotsUsers_zipcode_em_").text(resp.phone);
                }
            }
        }
    });
}

function askQuestion(dataArr){
	$.ajax({
		type: "POST",
		url: baseUrl + "/auctions/orangelots-auctions/saveQuestion",
		data: dataArr,
		success: function (data) {
			var resp = $.parseJSON(data);
			if (resp.err == 0) {
				location.reload();
			}else{
				if ('user_name' in resp) {
					$("#AuctionQuestion_user_name_em_").show();
					$("#AuctionQuestion_user_name_em_").text(resp.user_name);
				}
				if('subject' in resp){
					$("#AuctionQuestion_subject_em_").show();
					$("#AuctionQuestion_subject_em_").text(resp.subject);
				}
				if('question' in resp){
					$("#AuctionQuestion_question_em_").show();
					$("#AuctionQuestion_question_em_").text(resp.question);
				}
			}
		}
	});
}

function skipForNow(name) {
    $.ajax({
        type: "POST",
        url: baseUrl + "/user/registration/buyerSkipPayPal",
        data: ({name: name}),
        success: function (data) {
            var resp = $.parseJSON(data);
            $("#buyer-pay-pal").replaceWith(resp.form);
        }
    });
}

function registerCard(dataArr) {
    $.ajax({
        type: "POST",
        url: baseUrl + "/user/user/saveCardInfo",
        data: dataArr,
	    beforeSend : function(){
			$(".popup__content").append(preload);
		},
        success: function (data) {
	        $(".preload_mask").remove();
	        $(".preloader").remove();

            var resp = $.parseJSON(data);

            if (resp.err == 0) {
                $("#buyer-pay-pal").replaceWith(resp.form);
            } else {
                if ('owner_name' in resp) {
                    $("#CardInfo_owner_name_em_").show();
                    $("#CardInfo_owner_name_em_").text(resp.owner_name);
                }
                if ('number' in resp) {
                    $("#CardInfo_number_em_").show();
                    $("#CardInfo_number_em_").text(resp.number);
                }
                if ('cvv' in resp) {
                    $("#CardInfo_cvv_em_").show();
                    $("#CardInfo_cvv_em_").text(resp.cvv);
                }
                if ('month' in resp) {
                    $("#CardInfo_month_em_").show();
                    $("#CardInfo_month_em_").text(resp.month);
                }
	            if('errorMessage' in resp){
		            $(".popup__right_info__title").append($("<div class='alert-message alert alert-block alert-error fade in' data-alert><p> " + resp.errorMessage + " </p></div>"));
		            $(".alert-message").delay(5000).fadeOut("slow", function () {
			            $(this).remove();
		            });
	            }
            }
        }
    });
}

function onlyNumbers(id) {
    $("#" + id).keydown(function (event) {
        // Разрешаем: backspace, delete, tab и escape
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            // Разрешаем: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Разрешаем: home, end, влево, вправо
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // Ничего не делаем
            return;
        }
        else {
            // Обеждаемся, что это цифра, и останавливаем событие keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });
}

$(document).on("click",".auction_list__sidebar__categories__show_more",function(event){
    event.preventDefault();
    $(this).closest(".auction_list__sidebar__with_hide_list").addClass("open")
});

function showArea(id){
	$("#reply_"+id).hide();
	$("#answer_"+id).show();
}

function hideArea(id){
	$("#answer_"+id).hide();
	$("#reply_"+id).show();
}

function newAlert(type, message) {
	$(".yc").append($("<div class='alert-message alert alert-block alert-" + type + " fade in' data-alert><p> " + message + " </p></div>"));
	$(".alert-message").delay(5000).fadeOut("slow", function () {
		$(this).remove();
	});
}

function alertAppendTo(divId, type, message){
	$("#"+divId).append($("<div class='alert-message alert alert-block alert-" + type + " fade in' data-alert><p> " + message + " </p></div>"));
	$(".alert-message").delay(5000).fadeOut("slow", function () {
		$(this).remove();
	});
}

function showFull(idHide, idShow){
	$(idHide).hide();
	$("#"+idShow).show();
}

