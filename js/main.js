var rateFrom, rateTo;
function getRateFrom(currencyIdFrom) {
    rateFrom = currencyRate[currencyIdFrom];
}
function getRateTo(currencyIdTo) {
    rateTo = currencyRate[currencyIdTo];
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}