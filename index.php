<?php
error_reporting(0);
ob_start();
session_start();
$yii = dirname(__FILE__) . '/framework/yii.php';
$config = dirname(__FILE__) . '/protected/config/main.php';
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
require_once(dirname(__FILE__) . '/protected/functions/yii.php');
require_once($yii);
$app = Yii::createWebApplication($config);
loadSettings();
ini_set("memory_limit","-1");
$app->run();